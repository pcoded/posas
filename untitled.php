public function create(Request $request)
    {
      
         $data  = $request->all();
         $sale = $data['mysale'];

         //check if the product aleady exist in the product_cart
         $checkIfExist = Carts::where('productName',$sale)
                                 ->orWhere('productBarcode',$sale)
                                 ->get();
        // if the product is already exist increment the quantity and then return to use                         
        if($checkIfExist->count()){
            // Update the quantity cart for this products only
            foreach ($checkIfExist as $key => $quantityValue) {
                //Increment quantity
               $newquantity = $quantityValue['productQuantity']+1;

               // update total price
               $totalprice = $newquantity*$quantityValue['productSellingPrice'];
            }
            $incrementQuantity = Carts::where('productBarcode', $sale)
                                               ->orWhere('productName', $sale)
                                            ->update(['productQuantity'=>$newquantity,'total'=>$totalprice]);
           if($incrementQuantity){
            
                $resultIncrement = Carts::where('productBarcode', $sale)
                           ->orWhere('productName', $sale)
                           ->get();
                           //print_r($resultIncrement);
                          // die();
            //$resultIncrement = $resultIncrement->add($newquantity);
            //$resultIncrement = array_push($newquantity,$resultIncrement); 
                return response()->json(['data' => $resultIncrement]);

           }else{
            return response()->json(['data' => "no match"]);
           }
            
        }
       // The product does not exist in the cart so fetch it from the product table then return to user and also insert into the products_cart.
        else{

            $result = Product::where('productBarcode', $sale)
                           ->orWhere('productName', $sale)
                           ->get();
            // check if exist in the product table
                    if($result->count()){
                   // Insert product into cart then return
                       foreach ($result as $key => $value) {
                        $tprice = $value['productSellingPrice']*1; 
                         $saveIntoCart = Carts::create([
                            'productName'=>$value['productName'],
                            'productBarcode'=>$value['productBarcode'],
                            'productSellingPrice'=>$value['productSellingPrice'],
                            'productQuantity'=>1,
                            'total'=>$tprice,
                            'productCategory'=>$value['productCategory'],
                            'productBuyingPrice'=>$value['productBuyingPrice']
                            ]);
                       }
                        $res = Carts::where('productBarcode', $sale)
                           ->orWhere('productName', $sale)
                           ->get();
                    return response()->json(['data' => $res]);
                      }
                  if($result->isEmpty()){
                        return response()->json(['data' => "no match"]);
                      }
           
        }

          // $result = Product::where('productBarcode', $sale)
          //                  ->orWhere('productName', $sale)
          //                  ->get();
          // if($result->count()){
          //  // Insert product into cart
          //   return response()->json(['data' => $result]);
          // }
          // if($result->isEmpty()){
          //   return response()->json(['data' => "no match"]);
          // }

          
    }