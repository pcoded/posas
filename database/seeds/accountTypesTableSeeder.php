<?php

use Illuminate\Database\Seeder;

class accountTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Uncomment below to wipe the table before populating
        DB::table('accountTypes')->delete();

        $acctype = array(
                [
                'accName'=>'Asset accounts',
                'accdescriptions'=>'The different types of economic resources owned or controlled by an entity. Common examples of asset accounts are cash on hand, cash in bank, real estate, inventory, prepaid expenses, goodwill, and accounts receivable'
                ],

                [
                'accName'=>'Liability accounts',
                'accdescriptions'=>'Represent the different types of economic obligations of an entity, such as accounts payable, bank loans, bonds payable, and accrued expenses'
                ],

                [
                'accName'=>'Equity accounts',
                'accdescriptions'=>'represent the residual equity of an entity (the value of assets after deducting the value of all liabilities). Equity accounts include common stock, paid-in capital, and retained earnings. The type and captions used for equity accounts are dependent on the type of entity.'
                ],

                [
                'accName'=>'Revenue or income accounts',
                'accdescriptions'=>'represent the companys earnings and common examples include sales, service revenue and interest income'
                ],

                [
                'accName'=>'Expense accounts',
                'accdescriptions'=>'represent the companys expenditures. Common examples are utilities, rents, depreciation, interest, and insurance.'
                ],
                [
                'accName'=>'Contra-accounts',
                'accdescriptions'=>'are accounts with negative balances that offset other balance sheet accounts. Examples are accumulated depreciation (offset against fixed assets), and the allowance for bad debts (offset against accounts receivable).'
                ]
        	);

        // Uncomment below to run the seeder
        DB::table('accountTypes')->insert($acctype);
    }
}
