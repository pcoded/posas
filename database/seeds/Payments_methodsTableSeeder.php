<?php

use Illuminate\Database\Seeder;

class Payments_methodsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
          // Uncomment below to wipe the table before populating
        DB::table('payments_methods')->delete();

        $paymentmethods = array(
                [
                 'paymentmethodname'=>'Cash'
                ],

                [
                 'paymentmethodname'=>'Credit'
                ],
        	);

        // Uncomment below to run the seeder
        DB::table('payments_methods')->insert($paymentmethods);
    }
}
