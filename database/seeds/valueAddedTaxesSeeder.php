<?php

use Illuminate\Database\Seeder;

class valueAddedTaxesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       // Uncomment below to wipe the table before populating
       // DB::table('value_added_taxes')->delete();

        $vanames = array(
                [
                 'vname'=>'0',
                ],
                 [
                 'vname'=>'14',
                ],
        	);

        // Uncomment below to run the seeder
        DB::table('value_added_taxes')->insert($vanames);
    }
}
