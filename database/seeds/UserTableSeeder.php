<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Uncomment below to wipe the table before populating
        DB::table('users')->delete();

        $users = array(
                ['name'=>'Peter', 'email'=>'peter88tom@gmail.com','password'=>Hash::make('interface')],
        	);

        // Uncomment below to run the seeder
        DB::table('users')->insert($users);
    }
}
