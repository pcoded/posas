<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

         $this->call(UserTableSeeder::class);
         $this->call(Payments_methodsTableSeeder::class);
         $this->call(accountTypesTableSeeder::class);
         $this->call(valueAddedTaxesSeeder::class);
         
        
        Model::reguard();
    }
}
