<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stocks', function (Blueprint $table) {
           $table->increments('id');
            //$table->integer('user_id')->unsigned();
            $table->integer('product_id')->unsigned(); // Foreign key from products table
            $table->string('productBarcode')->nullable();
            //$table->integer('originalQuantity')->nullable();
            //$table->integer('currentQuantity')->nullable();
            $table->integer('availableQuantity')->default(0); // as originalQuantity =
            $table->integer('onhandQuantity')->default(0); // as currentQuantity it is what physically setting on shelves
            $table->integer('allocatedQuantity')->default(0);  // as sold quantity

            $table->integer('totalPurchaseprice')->default(0); // shipping and handling price increase as you purchase,
                                                              // calculated starting with initial price
            $table->integer('productBuyingPrice')->default(0); // supplier selling price
            $table->integer('productSellingPrice')->default(0); // price for selling to end customer
            $table->integer('reoderLevel')->default(0);
            $table->timestamps();

            // Associate foreign keys with respect to tables
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            //$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('stocks');
    }
}
