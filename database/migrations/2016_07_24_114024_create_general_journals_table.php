<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeneralJournalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //create schema generaljournal
        Schema::create('general_journals', function(Blueprint $table){
            $table->increments('gjournalId');
            $table->string('gjdate');
            $table->string('referenceNumber');
            $table->string('descriptions');
            $table->integer('gjchartaccountId')->unsigned(); // reference to debited acc
            $table->float('debit')->default(0.00);
            $table->float('credit')->default(0.00);
            $table->timestamps();

           // reference account to debit
            $table->foreign('gjchartaccountId')->references('chartaccId')->on('charts_of_accounts')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
