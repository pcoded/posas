<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeneralLedgersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //create schema for general ledger book
        Schema::create('general_ledgers', function(Blueprint $table){
         $table->increments('ledgerId');
         $table->string('gdate');
         $table->integer('transactionId')->unsigned(); // reference charts of account from ledger book
         $table->integer('journalId')->unsigned();//references the id from the journal book
         $table->float('gltotaldebit')->default(0.00);// total debit from referenced account from journal
         $table->float('gltotalcredit')->default(0.00);// total credit from the refenced account from jounal
         $table->float('glbalance')->default(0.00);
         $table->timestamps();
        
         // reference a charts of account
         $table->foreign('transactionId')->references('gjchartaccountId')->on('general_journals')->onDelete('cascade');

         // reference a charts of account
         $table->foreign('journalId')->references('gjournalId')->on('general_journals')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //drop talbe generalLedgers
         Schema::drop('generalLedgers');
    }
}
