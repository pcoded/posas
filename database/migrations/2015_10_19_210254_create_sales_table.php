<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $table->increments('id');
            $table->string('soNumber');
            $table->integer('user_id')->unsigned(); // Foreign key from user
            $table->integer('customer_id')->unsigned(); // Foreign key from customer table
            $table->integer('product_id')->unsigned(); // Foreign key from products table
            $table->string('unitPrice')->default(0);
            $table->string('discount')->default(0);
            $table->float('vat')->default(0);
            $table->string('quantity')->default(0);
            $table->string('total')->default(0);
            $table->date('solddate');
            $table->timestamps();

            // Associate foreign keys with respect to tables
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sales');
    }
}
