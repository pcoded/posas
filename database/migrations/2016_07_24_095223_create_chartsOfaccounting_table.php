<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChartsOfaccountingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //create charts of accounts schema
        Schema::create('charts_of_accounts', function(Blueprint $table){
            $table->increments('chartaccId');
            $table->integer('accIdenitifier')->nullable();
            $table->integer('accountTypeId')->unsigned();
            $table->string('accountName');
            $table->string('descriptions')->nullable();
            $table->timestamps();

           
           // referencing account category in accounttypes
            $table->foreign('accountTypeId')->references('acctypeId')->on('accountTypes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //drop table chartsOfAccounts
        Schema::drop('chartsOfAccounts');
    }
}
