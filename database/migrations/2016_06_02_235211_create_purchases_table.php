<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchasesTable extends Migration
{
     /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchases', function (Blueprint $table) {
           $table->increments('purchId');
           $table->string('poNumber')->nullable();
           $table->integer('createdBy')->unsigned();
           $table->integer('vendorId')->unsigned();
           $table->date('purchaseDueDate');
           $table->date('deliveryDate')->nullable();
           $table->integer('destinationLocation')->unsigned();
           //$table->integer('purchaseTotalCost')->default(0);
           $table->string('messageToVendor')->nullable();
           $table->integer('purchaseStatus')->default(0);
           $table->timestamps();
         


           $table->foreign('createdBy')->references('id')->on('users')->onDelete('cascade');
           $table->foreign('vendorId')->references('id')->on('suppliers')->onDelete('cascade');
           $table->foreign('destinationLocation')->references('id')->on('stores')->onDelete('cascade');
        });


      Schema::create('purchaseItemsDetails', function(Blueprint $table){
          $table->increments('purchaseItemsId');
          $table->integer('itemId')->unsigned();
          $table->integer('itemQuantity')->default(0);
          $table->integer('itemPrice')->default(0);
          $table->string('purchasevat')->defult(0); // value add tax
          $table->integer('purchasesId')->unsigned();
          $table->timestamps();


          $table->foreign('itemId')->references('id')->on('products')->onDelete('cascade');
          $table->foreign('purchasesId')->references('purchId')->on('purchases')->onDelete('cascade');

      });


       //THIS GRN WILL BE USED BY ACCOUNTIN TO GENERATE INVOICE
        Schema::create('goodsReceiveNote', function(Blueprint $table){
           $table->increments('grnid');
           $table->integer('purchaseorderid')->unsigned();
           $table->integer('processedBy')->unsigned();
           $table->timestamps();


           $table->foreign('purchaseorderid')->references('purchId')->on('purchases')->onDelete('cascade');
           $table->foreign('processedBy')->references('id')->on('users')->onDelete('No action');
        });



          Schema::create('deliveryNoteDetails',function(Blueprint $table){
          $table->increments('delNoteDetailsid');
          $table->string('deliveryNumber');
          $table->integer('grnNumber')->unsigned();
          $table->integer('deliveredQty')->defult(0);
          $table->integer('deliveredDamagedQty')->defult(0);
          $table->string('deliveredby')->nullable();
          $table->string('delivererContact')->nullable();
          $table->string('checkedBy')->nullable();
          $table->string('deliveryNoteDate');
          $table->timestamps();


    
          $table->foreign('grnNumber')->references('grnid')->on('goodsReceiveNote')->onDelete('cascade');
        });


    } 
    public function down()
    {
        Schema::drop('purchases');
        Schema::drop('purchaseItemsDetails');
        Schema::drop('goodsReceiveNote');
        Schema::drop('deliveryNoteDetails');
    } 
}
