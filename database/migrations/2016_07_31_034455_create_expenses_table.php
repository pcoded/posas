<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expenses', function (Blueprint $table) {
            $table->increments('expenseid');
            $table->string('expensedatecreated');
            $table->integer("paymentmethod")->unsigned();
            $table->string('expenseReceiptNumber')->nullable();
            $table->integer("expensechartaccountId")->unsigned();
            $table->integer("amount")->default(0);
            $table->integer("expensevat");
            $table->text("expensedescriptions")->nullable();
            $table->integer('expenseCreatedby')->unsigned();
            $table->integer('expenseStatus')->default(0);
            $table->timestamps();



        // reference a method used for paying the expense
         $table->foreign('paymentmethod')->references('methodid')->on('payments_methods')->onDelete('cascade');

         // reference a method used for paying the expense
         $table->foreign('expensechartaccountId')->references('chartaccId')->on('charts_of_accounts')->onDelete('cascade');

         // reference users table
         $table->foreign('expenseCreatedby')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('expenses');
    }
}
