<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrialBalancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //create trial balance table
        Schema::create('trial_balances', function(Blueprint $table){
            $table->increments('trialbIs');
            $table->integer('chartsaccountId')->unsigned();
            $table->integer('trialbdebit'); // total of all balances from general legder for particular account
            $table->integer('trialbcredit');
            $table->timestamps();


            // reference account to debit
            $table->foreign('chartsaccountId')->references('chartaccId')->on('charts_of_accounts')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //drop table  trialBalance
        Schema::drop('trial_balances');
    }
}
