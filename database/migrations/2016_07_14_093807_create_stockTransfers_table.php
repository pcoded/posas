<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockTransfersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stockTransfers', function (Blueprint $table) {
            $table->increments('transferId');
            $table->integer('itemtoTranfer')->unsigned();
            $table->integer('itemtransferQantity')->default(0);
            $table->integer('transferCreatedBy')->unsigned();
            $table->integer('transferSource')->unsigned();
            $table->integer('transferDestination')->unsigned();
            $table->string('transferdate');
            $table->integer('transferStatus')->default(0);
            $table->timestamps();


             $table->foreign('itemtoTranfer')->references('id')->on('products')->onDelete('cascade');
             $table->foreign('transferCreatedBy')->references('id')->on('users')->onDelete('cascade');
             $table->foreign('transferSource')->references('id')->on('stores')->onDelete('cascade');
             $table->foreign('transferDestination')->references('id')->on('stores')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('stockTransfers');
    }
}
