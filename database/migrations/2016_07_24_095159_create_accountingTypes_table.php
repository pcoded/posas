<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountingTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //create account type schemas
        Schema::create('accountTypes', function(Blueprint $table){
          $table->increments('acctypeId');
          $table->string('accName');
          $table->text('accdescriptions')->nullable();
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //drop table
        Schema::drop('accountTypes');
    }
}
