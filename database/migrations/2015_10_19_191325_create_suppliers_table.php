<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suppliers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('supplierName');
            $table->string('supplierEmail')->nullable();
            $table->string('supplierPhoneNumber')->nullable();
            $table->string('supplierPhoto')->nullable();
            $table->string('supplierAddress')->nullable();
            $table->string('supplierCountry')->nullable();
            $table->string('supplierAccountNumber')->nullable();
            $table->string('taxNumber')->nullable(); // TIN Number
            $table->string('website')->nullable();
            $table->string('descriptions')->nullable();
            $table->string('taxType')->nullable(); //eg purchases tax(18%) sales tax(18%)
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('suppliers');
    }
}
