<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('companyName');
            $table->string('companyAddress1');
            $table->string('companyAddress2')->nullable();
            $table->string('companyContact1');
            $table->string('companyContact2')->nullable();
            $table->string('companyLogo')->nullable();
            $table->string('companyWebsite')->nullable();
            $table->string('companyEmail')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('settings');
    }
}
