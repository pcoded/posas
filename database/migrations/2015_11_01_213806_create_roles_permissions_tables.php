<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolesPermissionsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         // Roles Table
        Schema::create('roles', function(Blueprint $table){
            $table->increments('id');
            $table->string('rolename');
            $table->string('description')->nullable(); // e.g admin, manager
            $table->timestamps();
        });

        // Permissions general names
         // Schema::create('perm_generals', function(Blueprint $table){
         //    $table->increments('permgid');
         //    $table->string('permgname');
         // });

       // Permissions Table
         Schema::create('permissions', function(Blueprint $table){
            $table->increments('id');
            $table->string('name');
            $table->string('description')->nullable(); // e.g admin, manager
           // $table->integer('permgid')->unsigned();
            $table->timestamps();

           // $table->foreign('permgid')->references('permgid')->on('perm_generals')->onDelete('cascade');
        });
      


       // Relation table between user and roles
         Schema::create('role_user', function(Blueprint $table){
            $table->integer('role_id')->unsigned();
            $table->integer('user_id')->unsigned();

            $table->foreign('role_id')
                      ->references('id')
                      ->on('roles')
                      ->onDelete('cascade');

            $table->foreign('user_id')
                      ->references('id')
                      ->on('users')
                      ->onDelete('cascade');
           

             $table->primary(['role_id', 'user_id']);
         });


         // Pivot table for roles and permissions table
         Schema::create('permission_role', function(Blueprint $table){
             $table->integer('role_id')->unsigned();
             $table->integer('permission_id')->unsigned();

             $table->foreign('role_id')
                      ->references('id')
                      ->on('roles')
                      ->onDelete('cascade');

             $table->foreign('permission_id')
                      ->references('id')
                      ->on('permissions')
                      ->onDelete('cascade');
              

              $table->primary(['role_id', 'permission_id']);
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {  
       // Schema::drop('perm_generals');
        Schema::drop('role_user');
        Schema::drop('permission_role');
        Schema::drop('roles');
        Schema::drop('permissions');
    }
}
