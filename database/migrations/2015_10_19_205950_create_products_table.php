<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   Schema::create('categories',function(Blueprint $table){
           $table->increments('id');
           $table->string('catName');
           $table->timestamps();
        });

       Schema::create('brands',function(Blueprint $table){
           $table->increments('id');
           $table->string('brandName');
           $table->timestamps();
       });



        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('productName');
           // $table->integer('productQuantity'); // on hand quantity
           // $table->integer('quantitytoSell')->nullable();
            $table->integer('availableQuantity')->default(0); // as productQuantity =
            $table->integer('onhandQuantity')->default(0); // as quantity to sale
            $table->integer('allocatedQuantity')->default(0); 
            $table->string('productSize')->nullable();
            $table->string('productcolor')->nullable();
            $table->string('productTag')->nullable();
            $table->string('productBrand')->nullable();
            $table->string('productBarcode');
            $table->string('productCategory')->nullable();
            $table->integer('productBuyingPrice')->nullable(); //supplier price
            $table->integer('totalPurchaseprice')->nullable(); // price include that include handling
            $table->integer('productSellingPrice')->nullable(); // price for selling to endcustomers
            $table->float('vat'); // true or false prince include vat
            $table->string('productLocationAtStore')->nullable();
            $table->integer('supplier_id')->unsigned(); // foreign key from suppliers table
            $table->integer('store_id')->unsigned()->nullable(); // foreign key from store table
            //$table->integer('user_id')->unsigned()->nullable();
            $table->string('productPhoto')->nullable();
            $table->text('productDescriptions')->nullable();
            $table->string('reOrderLevel')->nullable();
            $table->string('expirationDate')->nullable();
            $table->timestamps();

            // Associating foreign key with their tables
            $table->foreign('supplier_id')->references('id')->on('suppliers')->onDelete('cascade');
            $table->foreign('store_id')->references('id')->on('stores')->onDelete('cascade');
            //$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {   Schema::drop('categories');
        Schema::drop('brands');
        Schema::drop('products');
    }
}
