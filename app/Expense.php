<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
    protected $primaryKey = 'expenseid';

     protected $fillable = [
     'expensedatecreated',
    'paymentmethod',
    'expensechartaccountId',
    'amount',
    'expensedescriptions',
    ];

    // relate with users
    public function users()
    {
    	return $this->belongsTo('App\User');
    }
}
