<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    protected $fillable = 
    [
      'storeName',
      'storeEmail',
      'storeAddress',
      'storePhoneNumber'
    ];

    // store has many products
    public function productzs()
    {
      return $this->hasMany('App\Product');
    }
}
