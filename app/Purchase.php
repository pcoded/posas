<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
     protected $fillable = [
       'poNumber',
       'product_id',
       'orderquantity',
       'itemcost',
       'purchaseTax',
       'subtotal',
       'stockDue',
       'messageToSupplier',
       'itemStatus',
       'destinationLocation',
       'itemsubTotalCost',
       'supplier',
       'reference'
    ];

    public function supplierz()
    {
      return $this->belongsTo('App\Supplier');
    }

    public function productz()
    {
      return $this->belongsTo('App\Product');
    }

    public function storez()
    {
      return $this->belongsTo('App\Store');
    }
}
