<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ValueAddedTaxe extends Model
{
     protected $primaryKey = 'vid';

     protected $fillable = [
     'vname',
    ];
}
