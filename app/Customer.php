<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
   /**
    * Prevent MassAssignmentException in model
    * @var customer fillable inpust
    */
    protected $fillable = [
    'customerName',
    'customerCompanyName',
    'customerEmail',
    'customerPhoneNumber',
    'customerAddress',
    'customerCountry',
    ];


}
