<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Perm_general extends Model
{
    public function subpermissions()
   {
    return $this->hasToMany('App\Permission');
   }
}
