<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable=
    [
       'productName',
       'onhandQuantity',
       'availableQuantity',
       'allocatedQuantity',
       'productSize',
       'productcolor',
       'productTag',
       'productBrand',
       'productBarcode',
       'productCategory',
       'productBuyingPrice', //supplier price
       'productSellingPrice', // retailer price
       'totalPurchaseprice', // price include shipping and handling
       'supplier_id',
       'store_id',
       'vat',
       'product_photo',
       'expirationDate',
       'productDescriptions'
    ];

    /**
     * One product can be supplied by Many supplier
     * @return [type] [description]
     */
    public function supplier()
    {
    	return $this->belongsTo('App\Supplier');
    }

    // stores is owned by products
    public function storezz()
    {
      return $this->belongsTo('App\Store');
    }
}
