<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable=
    [
     'companyName',
     'companyAddress1',
     'companyAddress2',
     'companyContact1',
     'companyContact2',
     'companyLogo',
     'companyWebsite'
    ];
}
