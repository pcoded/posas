<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{

   protected $fillable = [ 'rolename', 'description'];
   

  /**
   * many roles belongs to many permission
   * @return [type] [description]
   */
   public function permissions()
   {
    return $this->belongsToMany('App\Permission');
   }


 /**
  * insert into role_permission table
  * @param  Permission $permission [description]
  * @return [type]                 [description]
  */
   // public static function assign(Permission $permission)
   // {
   //  return $this->permissions()->save($permission);
   // }

 public static function assign($request)
   {
   foreach ($request as $key => $value) {
    $roleid = $value['role_id'];
    $permid = $value['permission_id'];
   }
   return \DB::insert("INSERT into permission_role(role_id,permission_id) VALUES($roleid, $permid)");
    
   }



}