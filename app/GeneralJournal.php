<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GeneralJournal extends Model
{
   // protected $table = 'chartsOfAccounts';
    protected $primaryKey = 'chartaccId';

     protected $fillable = [
     'accIdenitifier',
    'accountTypeId',
    'accountName',
    'descriptions'
    ];
}
