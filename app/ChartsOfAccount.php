<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChartsOfAccount extends Model
{
    //
    //protected $table = 'chartsOfAccounts';
    protected $primaryKey = 'gjournalId';

     protected $fillable = [
     'gjdate',
    'referenceNumber',
    'descriptions',
    'gjchartaccountId',
    'debit',
    'credit'
    ];
}
