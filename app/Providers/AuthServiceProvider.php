<?php

namespace App\Providers;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\Permission;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        parent::registerPolicies($gate);
         
         //getting all the permission from the currnet user and check if the current permission has given a role
         foreach ($this->getPermissions() as $permission) {

            $gate->define($permission->name, function($user) use($permission){
                return $user->hasRole($permission->roles);
            });
         }



    }


    public function getPermissions()
    {
        return Permission::with('roles')->get();
    }
}
