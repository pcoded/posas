<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    public function products()
    {
      return $this->hasMany('App\Product');
    }

    public function customers()
    {
      return $this->hasMany('App\Customer');
    }
}
