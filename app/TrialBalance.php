<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrialBalance extends Model
{
     protected $primaryKey = 'trialbIs';
     protected $fillable = [
        'chartsaccountId', 'trialbdebit', 'trialbcredit',
    ];
}
