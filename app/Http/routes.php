<?php


// Route to return edit user form
Route::get('users/edit/{id}', ['middleware'=>'auth',  'uses'=>'EmployeeController@edit']);

// update user informations
Route::post('users/update', ['middleware'=>'auth',  'uses'=>'EmployeeController@update']);

// Route to delete user
Route::get('users/delete/{id}', ['middleware'=>'auth',  'uses'=>'EmployeeController@destroy']);

Route::post('users/role', ['middleware'=>'auth',  'uses'=>'EmployeeController@createRole']);

//Route cerate user
Route::post('users/registeruser', ['middleware'=>'auth',  'uses'=>'EmployeeController@createUsers']);

//Role registration
Route::post('users/roles', ['middleware'=>'auth',  'uses'=>'EmployeeController@createRoles']);

// Create role permissions
Route::post('users/createPermRole',['middleware'=>'auth',  'uses'=>'EmployeeController@assignRolePermissions']);

// Assign users to role
Route::post('users/userRoles',['middleware'=>'auth',  'uses'=>'EmployeeController@assignRoleUsers']);

// Recover Password
Route::controllers([ 'password'=>'Auth\PasswordController',]);

Route::post("users/getPermissionsBasedOnSelectedRole", ['middleware'=>'auth', 'uses'=>'EmployeeController@getPermissionsBasedOnSelectedRole']);


Route::post('users/deleteRole', ['middleware'=>'auth',  'uses'=>'EmployeeController@deleteRole']);
Route::post('users/editRole',['middleware'=>'auth',  'uses'=>'EmployeeController@editRole']);
Route::post('users/updateRole',['middleware'=>'auth',  'uses'=>'EmployeeController@UpdateEditedRole']);
Route::post('users/updatepermissions',['middleware'=>'auth',  'uses'=>'EmployeeController@UpdateEditedAssignedRoles']);

/**
 * End  User login, registration, recovery password and logout
 */









/**
 *  Start Customer routes
 */
Route::get('customer', ['middleware'=>'auth', 'uses'=>'CustomerController@index']);

// create new customer
Route::post('customer/store', ['middleware'=>'auth', 'uses'=>'CustomerController@store']);

// Edit customer
Route::post('customer/update',  ['middleware'=>'auth', 'uses'=>'CustomerController@update']);

// Drop customer
Route::get('customer/delete/{id}',  ['middleware'=>'auth', 'uses'=>'CustomerController@destroy']);

// Delete customer from checkbox
Route::post('customer/deletemultiple', ['middleware'=>'auth', 'uses'=>'CustomerController@destroymultiple']);

// Show more details on customer
Route::get('customer/show/{id}', ['middleware'=>'auth',  'uses'=>'CustomerController@show']);

// Upload excel file
Route::post('customer/importExcel', ['middleware'=>'auth',  'uses'=>'CustomerController@getFile']);

// Export to excel
Route::get('customer/export', ['middleware'=>'auth',  'uses'=>'CustomerController@exportExcel']);

// on sale create customer route
Route::post('customer/onsalescustomer',['middleware'=>'auth',  'uses'=>'CustomerController@onSaleNewCustomer']);



/**
 * End Customer routes
 */



/**
 * Start Products routes
 */
Route::get('product', ['middleware' => 'auth', 'uses' => 'ProductsController@index']);

Route::get('products/ajaxGetProducts', ['middleware' => 'auth', 'uses' => 'ProductsController@ajaxGetAllstock']);

// auto-complete product name 
Route::get('products/ajax-auto-complete', ['middleware' => 'auth', 'uses' => 'ProductsController@ajaxAutoComplete']);

// Create new procuct
Route::post('product/store', ['middleware' => 'auth', 'uses' => 'ProductsController@store']);

// getproduct to edit via ajax request
Route::post("products/getproductEdit",['middleware' => 'auth', 'uses' => 'ProductsController@ajaxGetproductupdate']);
// Update product
Route::post('product/productsupdate', ['middleware' => 'auth', 'uses' => 'ProductsController@productupdate']);

// Delete product
Route::post('product/deleteSingleProduct', ['middleware' => 'auth', 'uses' => 'ProductsController@destroySingleProduct']);

// Show more details on product
Route::get('product/show/{id}',  ['middleware'=>'auth',  'uses'=>'ProductsController@show']);

// Upload excel file
Route::post('product/importExcel', ['middleware'=>'auth',  'uses'=>'ProductsController@importExcelFile']);

Route::get('product/exportExcel',['middleware'=>'auth',  'uses'=>'ProductsController@getExcelFile']);


// create category route
Route::post('product/createcategory',['middleware'=>'auth',  'uses'=>'ProductsController@createCategory']);

// update/edit category
Route::post('product/updatecategory',['middleware'=>'auth',  'uses'=>'ProductsController@updateCategory']);

// Delete category
Route::get('product/deletecategory/{id}', ['middleware' => 'auth', 'uses' => 'ProductsController@destroycategory']);

// Delete multiple category
Route::post('product/deletemulitiplecategory', ['middleware' => 'auth', 'uses' => 'ProductsController@destroymultcategory']);

// Delete multiple category
Route::post('product/delmultproduct', ['middleware' => 'auth', 'uses' => 'ProductsController@destroymulproduct']);


// view inventory from ajax request
Route::post('products/getinventorydetails',['middleware' => 'auth', 'uses' => 'ProductsController@viewInventory']);

/**
 * End Products routes
 */



/**
 * Start Suppllier routes
 */
Route::get('supplier', ['middleware' => 'auth' , 'uses' =>'SuppliersController@index']);

// create new supplier
Route::post('supplier/store', ['middleware' => 'auth' , 'uses' =>'SuppliersController@store']);

// Edit supplier
Route::post('supplier/update', ['middleware' => 'auth' , 'uses' =>'SuppliersController@update']);

// Delete supplier
Route::get('supplier/delete/{id}', ['middleware' => 'auth' , 'uses' =>'SuppliersController@destroy']);

// Upload excel file
Route::post('supplier/importExcel', ['middleware'=>'auth',  'uses'=>'SuppliersController@getFile']);

// Export to excel
Route::get('supplier/export', ['middleware'=>'auth',  'uses'=>'SuppliersController@exportExcel']);

// Delete supplier from checkbox
Route::post('supplier/deletemultiple', ['middleware'=>'auth', 'uses'=>'SuppliersController@destroymultiple']);

// Download supplier excel sample
//Route::get('supplier/excel', ['middleware'=>'auth',  'uses'=>'SuppliersController@downloader'])->where('filename', '[A-Za-z0-9\-\_\.]+');


/**
 * End Suppllier routes
 */



/**
 * Start Sales routes
 */
Route::get('sales', ['middleware' => 'auth' , 'uses' =>'SalesController@index']);

//Ajax request scanned or entered items
Route::Post('sales/ajaxsale', ['middleware' => 'auth' , 'uses' =>'SalesController@create']);

//Ajax get item sold receipts
Route::get('sales/ajaxgetitemsreceipts', ['middleware' => 'auth' , 'uses' =>'SalesController@getSoldItemReceipts']);


// Customer autocompler route
Route::get('customer/autocomplete', ['middleware' => 'auth' , 'uses' =>'SalesController@customerAutocomplete']);

// post sale
Route::post('sale/completeSale',['middleware'=>'auth','uses'=>'SalesController@createNewSale']);

// print receipt route
Route::get('sales/completeSaleReceipt/{sordernumber}',['middleware'=>'auth','uses'=>'SalesController@printReceipt']);

/**
 * End Sales
 */




/**
 * Store routes
 */


Route::get('stores', ['middleware' => 'auth' , 'uses' =>'StoresController@index']);

// Create new Store
Route::post('stores/store', ['middleware' => 'auth' , 'uses' =>'StoresController@store']);

// Update store
Route::post('stores/update', ['middleware' => 'auth' , 'uses' =>'StoresController@update']);

// Delete store
Route::get('stores/delete/{id}', ['middleware' => 'auth' , 'uses' =>'StoresController@destroy']);

// Upload excel file
Route::post('stores/importExcel', ['middleware'=>'auth',  'uses'=>'StoresController@getFile']);

// Export to excel
Route::get('stores/export', ['middleware'=>'auth',  'uses'=>'StoresController@exportExcel']);

// Delete store from checkbox
Route::post('stores/deletemultiple', ['middleware'=>'auth', 'uses'=>'StoresController@destroymultiple']);

/**
 * End store routes
 */



/**
 * Stock routes
 */
Route::get('stock', ['middleware'=>'auth', 'uses'=>'StockController@index']);

// Show more details a specific product
Route::get('stock/show/{id}', ['middleware'=>'auth', 'uses'=>'StockController@show']);

// Query items to transer
Route::post('stock/autocompletetrItems',['middleware'=>'auth', 'uses'=>'StockController@getItemstoTransfer']);

// post created stock
Route::post('stock/createTransferStock',['middleware'=>'auth', 'uses'=>'StockController@createItemstoTransfer']);


/**
 * End Stock routes
 */



/**
 * Reports Routes
 */

Route::get('reports',  ['middleware'=>'auth', 'uses'=>'ReportsController@index']);
Route::post('reports/generalProfitReport',['middleware'=>'auth', 'uses'=>'ReportsController@getGeneralProfitReport']);

// sales summary reports
Route::post('reports/salessummary',['middleware'=>'auth', 'uses'=>'ReportsController@salesSummaryReport']);

// Finance reports
Route::get('reports/finance', ['middleware'=>'auth', 'uses'=>'ReportsController@financeReports']);

// Income statement
Route::post('reports/finance/incomestatement',['middleware'=>'auth', 'uses'=>'ReportsController@incomeStatmentReport']);

//balance sheet
Route::post('reports/finance/balancesheet',['middleware'=>'auth', 'uses'=>'ReportsController@balanceSheetReport']);



/**
 * End Reports routes
 */



/**
 * System settings routes
 */
Route::get('settings', ['middleware'=>'auth', 'uses'=>'SettingsController@index']);

// Create company settings
Route::post('settings/create', ['middleware'=>'auth', 'uses'=>'SettingsController@store']);

//get settings via ajax
Route::get('settings/ajaxGetSettings',['middleware'=>'auth', 'uses'=>'SettingsController@getSettingsAjax']);

// delete single company details
Route::post('settings/deleteSinglecompany',['middleware'=>'auth', 'uses'=>'SettingsController@deleteSingleCompany']);

//get settings to edit
Route::post('settings/editsettings',['middleware'=>'auth', 'uses'=>'SettingsController@getSettingsEdit']);

//update settings
Route::post('settings/updatesettings',['middleware'=>'auth', 'uses'=>'SettingsController@updateSettings']);





// Route to get list of users
Route::get('employee', ['middleware'=>'auth',  'uses'=>'EmployeeController@getUsers']);

// Create role
Route::post('employee/createrole', ['middleware'=>'auth',  'uses'=>'EmployeeController@createRole']);



//Create role
Route::post('employee/createpermission', ['middleware'=>'auth',  'uses'=>'EmployeeController@createPermission']);


// Ordes routes
Route::get('orders',  ['middleware'=>'auth', 'uses'=>'OrdersController@index']);

// Purchases
Route::get('purchases', ['middleware'=>'auth', 'uses'=>'PurchasesController@index']);
Route::post('purchase/getSupplierDetails',['middleware'=>'auth', 'uses'=>'PurchasesController@getSupplierInfo']);

Route::post('purchase/autocompleteItems',['middleware'=>'auth', 'uses'=>'PurchasesController@getItemsAutocomplete']);

// post new purchase
Route::post('purchase/postpurchase',['middleware'=>'auth', 'uses'=>'PurchasesController@postPurchaseDetails']);
Route::post('purchase/postpurchaseItem',['middleware'=>'auth', 'uses'=>'PurchasesController@postPurchaseitemsDetails']);

// edit posted purchase
Route::post('purchase/editpostpurchase',['middleware'=>'auth', 'uses'=>'PurchasesController@editpostedPurchaseDetails']);
Route::post('purchase/editpostpurchaseItem',['middleware'=>'auth', 'uses'=>'PurchasesController@editpostedPurchaseitemsDetails']);

//get purchase withour refresh
Route::get('purchase/ajaxgetpurchaselist', ['middleware'=>'auth', 'uses'=>'PurchasesController@ajaxgetPurchaseOrders']);

// view purchases order
Route::get('purchase/purchase/{poNumber}',['middleware'=>'auth', 'uses'=>'PurchasesController@receiveViewPrintPurchaseOrders']);

// receive purchases order
Route::post('purchase/purchase/receivepurchaseorder',['middleware'=>'auth', 'uses'=>'PurchasesController@receivePurchaseOrders']);

// view purchases voucher order
Route::get('purchase/purchase/voucher/{poNumber}',['middleware'=>'auth', 'uses'=>'PurchasesController@purchasePaymentVoucher']);

// Delete purchase stock
Route::post('purchase/deleteSinglePurchase',['middleware'=>'auth', 'uses'=>'PurchasesController@purchaseDelete']);

// Delete multiple purchase
Route::post('purchase/deletePurchMultiple',['middleware'=>'auth', 'uses'=>'PurchasesController@purchaseDeleteMultiple']);

Route::post('purchase/getPurchaseToEdit',['middleware'=>'auth', 'uses'=>'PurchasesController@getPurchaseEditModal']);


//Record journ when purchase is carried out
Route::post('purchase/purchase/newpurchasejournal',['middleware'=>'auth', 'uses'=>'AccountingController@newPurchasejournal']);

// end purchase roots


// Expenses routes
Route::get('expenses',['middleware'=>'auth', 'uses'=>'ExpensesController@index']);

// create new expense route
Route::post("expenses/postnewexpense",['middleware'=>'auth', 'uses'=>'ExpensesController@createnewExpense']);

// Get a list of expenses
Route::get("accouting/ajaxGetexpenses",['middleware'=>'auth', 'uses'=>'ExpensesController@getListOfExpenses']);
/**
 * End System settings routes
 */


// Normal user default controller
Route::get('/home', ['middleware'=>'auth', 'uses'=>'DashboardController@index']);

//get top selling items
Route::post('/home/topselling',['middleware'=>'auth', 'uses'=>'DashboardController@getTopSellingItems']);

Route::get('/home/defaulttopselling',['middleware'=>'auth', 'uses'=>'DashboardController@getDefaultTopSellingItems']);

Route::get('/', function() {
  
    return view('auth.login');
});

/**
 * Start User login, registration, recover password and logout
 */
// Return Login page
Route::get('auth/login', 'Auth\AuthController@getLogin');

// Process login page
Route::post('auth/login', 'Auth\AuthController@postLogin');

// Logout
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Return registration view
Route::get('auth/register', 'Auth\AuthController@getRegister');

// Process registration
Route::post('auth/register', 'Auth\AuthController@postRegister');

// Reset password
//Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
//Route::post('password/reset', 'Auth\PasswordController@postReset');
// Route to change password
Route::post('auth/passchange','CustomerController@passmanage');


// Accounting routes
Route::get('/accounting',['middleware'=>'auth', 'uses'=>'AccountingController@index']);

// return charts of accounting
Route::get('accouting/ajaxGetchartsaccounting',['middleware'=>'auth', 'uses'=>'AccountingController@getchartsOfaccounting']);

// new accounting route
Route::post('accounting/newaccountingroute',['middleware'=>'auth', 'uses'=>'AccountingController@createnewChartsOfaccounting']);

// delete single account
Route::post('accounting/deleteSingleaccount',['middleware'=>'auth', 'uses'=>'AccountingController@deleteSingleAccount']);

// delete multi account
Route::post('accounting/deleteMultaccount',['middleware'=>'auth', 'uses'=>'AccountingController@deleteMultipleAccount']);

// get accounting to edit
Route::post('accounting/getAccountingToEdit',['middleware'=>'auth', 'uses'=>'AccountingController@getAccoutToEdit']);

// update edited account
Route::post('accounting/updateaccounting',['middleware'=>'auth', 'uses'=>'AccountingController@updateEditedAccount']);

// route debit in journal
Route::post('accounting/newjournalroute',['middleware'=>'auth', 'uses'=>'AccountingController@createNewJournal']);

// route get expense claims
Route::get('accouting/getclaims',['middleware'=>'auth', 'uses'=>'AccountingController@getExpenseClaims']);

//view more expenses
Route::post("accounting/expenseviewmore",['middleware'=>'auth', 'uses'=>'AccountingController@viewmoreExpense']);

// Accept new expense
Route::post('accounting/acceptExpenses',['middleware'=>'auth', 'uses'=>'AccountingController@acceptExpenses']);

// Record cash sales when item is sold 
Route::post('accounting/salesjournalroute',['middleware'=>'auth', 'uses'=>'AccountingController@newCashSalesjournal']);

// Delete expense claim
Route::post('accounting/expenseclaimdelete',['middleware'=>'auth', 'uses'=>'AccountingController@deleteSingleExpenseClaim']);



