<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use App\Supplier;
use App\User;
use App\Customer;
use App\Product;
use Excel;

class SuppliersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      
          // get authenticated user
          $userdata = \Auth::user();
          
          $supplier = Supplier::all();

          $supplier2 = Supplier::all();


          //count total customer
         $customerdata  = Customer::all();

         $user =  User::all();

          // Return total products
          $products = Product::all();

        return view('suppliers.index', compact('userdata', 'supplier','supplier2', 'customerdata', 'products', 'user'));
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
         //validate inpusts
         $this->validate($request, [
               'name' => 'required',
               'phonenumber'=> 'required',
            ]);
        $input = $request;

       $supplier = new Supplier;

       $supplier->supplierName = $input['name'];
       $supplier->supplierEmail = $input['email'];
       $supplier->supplierPhoto = $input['photo'];
       $supplier->supplierAddress = $input['supplieraddress'];
       $supplier->supplierPhoneNumber = $input['phonenumber'];
       $supplier->supplierCountry = $input['country'];
       $supplier->supplierAccountNumber = $input['accountnumber'];
       $supplier->taxNumber = $input['taxNumber'];
       $supplier->website  = $input['website'];
        $supplier->status  = $input['status'];

        $supplier->save();

       session()->flash('flash_message', 'Suppllier been added Successfully!');

        return redirect('supplier');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return $id;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
             $this->validate($request, [
               'name' => 'required',
               'phonenumber'=> 'required',
            ]);

      $supplierinput = $request;
      $supplierId = $supplierinput['asdgasdga'];
      

      Supplier::where('id', $supplierId)->update([

               'supplierName'=>$supplierinput['name'],
               'supplierEmail' => $supplierinput['email'],
               'supplierPhoto' => $supplierinput['photo'],
               'supplierAddress' => $supplierinput['supplieraddress'],
               'supplierPhoneNumber' => $supplierinput['phonenumber'],
               'supplierCountry' => $supplierinput['country'],
               'supplierAccountNumber' => $supplierinput['accountnumber'],

        ]);


       session()->flash('flash_message', 'Supplier  updated!');
       return redirect('supplier');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
          $checkid = Supplier::findOrFail($id);

           if(! is_null($checkid)){
            Supplier::where('id', $id)->delete();
           }
           
           session()->flash('flash_message', 'Supplier Deleted Successfully!');

           return redirect('supplier');
    }




/**
 * Delete multiple supplier from chechbox
 */
public function destroymultiple(Request $request)
{

   $this->validate($request, [
                 'checkedsupplier' => 'required',
            ]);
 $data = $request->all();
//return $data['checkedValues'] ;
 // Customer::where('id', $data)->delete([]);
 // return response()->json(['responseText' => 'Success!'], 200);
 


  foreach($data['checkedsupplier'] as $id){
  Supplier::where('id', $id)->delete();
  }

  //return Response::json(['data' => array('message' => 'Customer deleted', 'redirecturl' => '/customer')]);
  session()->flash('deleteajax_message', 'Suppllier Deleted Successfully!');
   return response()->json(['responseText' => 'Success!'], 200);
//return Response::json(array('responseText' => 'Success!'))->session()->flash('deleteajax_message', 'Customer Deleted Successfully!');
 

}




/**
 * Upload supplier lists
 * @param  Request $request [description]
 * @return [type]           [description]
 */
    public function getFile(Request $request)
{  
    // Validate file
     $this->validate($request, [
                 'uploadfile' => 'required | mimes:xls,xlsx,ods,csv,xml,xlt',

            ]);

     // Read the input file
    $fileToUpload = $request->file('uploadfile');

    /**
     * Start import the file
     */
    Excel::load($fileToUpload, function($reader){

    $reader->each(function($row){
   foreach ($row as $value) {
       Supplier::create([
      'supplierName'               => $value['name'],
      'supplierEmail'                => $value['email'],
      'supplierPhoneNumber'         => $value['phone'],
      'supplierAddress'            => $value['address'],
      'supplierCountry'            => $value['country'],
      'supplierAccountNumber' =>  $value['accountnumber']
        ]);
   }
});

    });

    session()->flash('flash_message', 'Supplier has been added Successfully!');
    return redirect('supplier');
}


/**
 * Export supplier to excel
 * @return [type] [description]
 */
public function exportExcel()
{
   // $customerdata = Customer::all();
    Excel::create('Supplier_sheet', function($excel){
        
        $excel->sheet('supplier', function($sheet){
            $sheet->fromArray(Supplier::all(['supplierName as NAME',  'supplierEmail as EMAIL', 'supplierPhoneNumber as PHONE', 'supplierAddress as ADDRESS', 'supplierCountry as COUNTRY', 'supplierAccountNumber as ACCOUNTNUMBER']));
        });
    })->export('xlsx');

    return redirect('supplier');
}

// public function downloader()
// {
//     $filename = 'suppliers_sheet.xlsx';
//     $file_path = storage_path() . '/file/'.$filename;
//     if(file_exists($file_path))
//     {
//       //send download
//       return Response::download($file_path, $filename, ['Content-Length: '.filesize($file_path)]);
//     }else{
//       //Error
//       exit('Requested file does not exist on our server!');
//     }
// }




}
