<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Supplier;
use App\User;
use App\Customer;
use App\Product;
use App\Carts;
use App\Setting;

class SettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         // get authenticated user
          $userdata = \Auth::user();
          
          $supplier = Supplier::all();
     
          //count total customer
          $customerdata  = Customer::all();

          $user =  User::all();

          // Return total products
          $products = Product::all();

        return view('settings.index', compact('userdata', 'supplier', 'customerdata', 'products', 'user','settings'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request,[
            'companyName'=>'required',
            'companyAddress1'=>'required',
            'companyContact1'=>'required'
            ]);
        $settings = $request->all();

        $newsettings = Setting::create($settings);
    }

    /**
     * get cuurrent settings
     */
    public function getSettingsAjax()
    {
       $settings = Setting::all();

       return response()->json(["data"=>$settings]);
    }

    /**
     * Delete company settings
     */
    public function deleteSingleCompany(Request $request)
    {
        $this->validate($request,[
            'deletecompan'=>'required'
            ]);

        $del = Setting::where('id',$request['deletecompan'])->delete();

        if($del){
               return response(['msg'=>'settings deleted', 'status'=>'success']);
        }else{
               return response(['msg'=>'Failed to delete', 'status'=>'fail']);
        }
    }

/*
*Get settings details to edit
*/

   public function getSettingsEdit(Request $request)
   {
      $getsettings = Setting::where('id',$request['editid'])->get();
    
      return response()->json(["data"=>$getsettings]);
   }


    /**
     * Update company settings
     */
    public function updateSettings(Request $request)
    {
       $this->validate($request,[
          "name"=>"required",
          "address1"=>"required",
          "contact1"=>"required"
        ]);

       $updatesettings = Setting::where('id',$request['id'])->update([
                                                                  "companyName"=>$request['name'],
                                                                   "companyAddress1"=>$request['address1'],
                                                                    "companyAddress2"=>$request['address2'],
                                                                     "companyContact1"=>$request['contact1'],
                                                                      "companyContact2"=>$request['contact2'],
                                                                       "companyWebsite"=>$request['website'],
                                                                       "companyEmail"=>$request['email'],
                                                                  ]);

       if($updatesettings){
           return response(['msg'=>'settings updated', 'status'=>'success']);
       }else{
        return response(['msg'=>'Failed to update', 'status'=>'fail']);
       }
    }

   
}
