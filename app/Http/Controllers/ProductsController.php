<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\collection;
use App\Product;
use App\Customer;
use App\Supplier;
use App\User;
use App\Store;
use Excel;
use App\Stock;
use App\Brand;
use App\Category;
//use Datatables;
use App\ChartsOfAccount;
use App\GeneralJournal;
use App\GeneralLedger;
use App\TrialBalance;
use Yajra\Datatables\Facades\Datatables;


class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {

        // get authenticated user
         $userdata = \Auth::user();

         $products = Product::all();
      


          //count total customer
          $customerdata  = Customer::all();

          $user =  User::all();

          // Return total supplier
          $suppliers = Supplier::all()->count();

          $supplier = Supplier::all(); //Using this on edit modal giving error that "You are trying to access a non-object property"

          $supplier2= Supplier::all(); // Used during editing the product


          // get store locations
          $stores = Store::all();

          // get products categories
          $categories = Category::all();

          // get products brands
          $brands = Brand::all();
          $paymentmethods = \DB::table('payments_methods')->get();
          
        // return Datatables::of($listofbooking)->make(true);

          // get products in stock for showing more details
          // $stock = \DB::table('products')->join('stocks', 'products.id', '=', 'stocks.product_id')->join('suppliers', 'products.supplier_id', '=', 'suppliers.id')->join('stores', 'products.store_id', '=', 'stores.id')->select('products.*', 'stocks.*','suppliers.supplierName')->get();
          
         return view('products.index', compact('userdata','paymentmethods','products', 'customerdata', 'suppliers', 'supplier', 'supplier2','user', 'stores','brands','categories','brandz'));

    }

    // View inventory details
    public function viewInventory(Request $request)
    {     
           $inveId = $request['inveid'];
         // get products in stock for showing more details
         // $detailsstock = \DB::table('products')->where('id',$inveId)->join('suppliers', 'products.supplier_id', '=', 'suppliers.id')->join('stores', 'products.store_id', '=', 'stores.id')->select('products.*','suppliers.supplierName')->get();
           
           $detailsstock = Product::with('supplier','storezz')->where('id',$inveId)->get();


          return response()->json(["data"=>$detailsstock]);
    }


    /*************************
    *   Get all products via ajax
    *****************************
    */
    public function ajaxGetAllstock()
    {
        // get products in stock for showing more details
      $stock =  \DB::table('products')->join('stocks', 'products.id', '=', 'stocks.product_id')->join('suppliers', 'products.supplier_id', '=', 'suppliers.id')->join('stores', 'products.store_id', '=', 'stores.id')->select('products.*', 'stocks.*','suppliers.supplierName','stores.storeName')->get();

     // return Datatables::of(Product::query())->join('stocks', 'products.id', '=', 'stocks.product_id')->join('suppliers', 'products.supplier_id', '=', 'suppliers.id')->select('products.*', 'stocks.*','suppliers.supplierName')->get();
       //return Datatables::of(Product::query())->make(true);

      //$stock =  \DB::table('products')->get();
     // return Datatables::of($stock)->make(true);

      // ERROR OCCURING WHEN USING YAJRA
      //Call to a member function getQuery() on array

        return response()->json(["data"=>$stock]);
    }


    public function ajaxAutoComplete()
    {
      $productnames = \DB::table('products')->select('products.productName')->get();
       
       $productarray = array();
      foreach ($productnames as $key => $value) {
        // $prarray = array();
        // $prarray['productname'] = $value->productName; 
        array_push($productarray, $value->productName);
      }
      return response()->json([$productarray]);
    }


    // get products to edit via ajax request
    public function ajaxGetproductupdate(Request $request)
    {
       $productid = $request['productid'];

        $productsds = \DB::table('products')->where("id",$productid)->get();

        return response()->json(["data"=>$productsds]);
    }



    /***************************************
     * create new product category.
     ***************************************
    */
    public function createCategory(Request $request)
    {
        $cat = $request->all();
       

        $this->validate($request,[
                  'catName'=>'required'
            ]);

        $catsuccess = new Category;
        $catsuccess->catName = $cat['catName'];

        $bb = $catsuccess->save();
      //if($bb){
        session()->flash('flash_message', 'Category successfully saved!');
         return redirect('product');
      //}else{
       // echo "error";
      //}
      
         
    }


     /*
     *********************
     *  Update category
     ********************
     */
     public function updateCategory(Request $request)
    {
       $catedit = $request;
       $this->Validate($request,['catName'=>'required']);
       $catid  = $request['catId'];
       $updatecat  = Category::where('id',$catid)->update(['catName'=>$catedit['catName']]);

       session()->flash('flash_message', 'Category successfully Updated!');
       return redirect('product');

    }


   /*
   **************************************
   * Destroy individual category
   **************************************
   */
    public function destroycategory($id)
    {
         $checkIfExist = Category::findOrFail($id);

         if($checkIfExist){

            Category::where('id', $id)->delete();

            session()->flash('flash_message', 'Category successfully Deleted');
            return redirect('product');
            
         }else{
            die;
         }
    }



/*
 **********************************************************
 * Destroy multiple category id's using ajax form subumition from a checkbox
 ***********************************************************
 */
public function destroymultcategory(Request $request)
{

   $this->validate($request, [
                 'checkedcategeories' => 'required',
            ]);
 $data = $request->all();


  foreach($data['checkedcategeories'] as $id){
    $delcat = Category::where('id', $id)->delete();
  }

  if($delcat)
  {
   return response(['msg'=>'Category(s) deleted', 'status'=>'success']);
  }else{
    return response(['msg'=>'Failed deleting the category(s)', 'status'=>'failed']);
  }
}


  /*
 **********************************************************
 * Destroy multiple products id's using ajax form subumition from a checkbox
 ***********************************************************
 */
public function destroymulproduct(Request $request)
{
    
   $this->validate($request, [
                 'checkedproduct' => 'required',
            ]);
   $data = $request->all();

  //var_dump($data['checkedproduct']);

    foreach($data['checkedproduct'] as $id){
    $delp = Product::where('id', $id)->delete();
    }
    if($delp){
      return response(['msg'=>'Product(s) deleted', 'status'=>'success']);
    }else{
    return response(['msg'=>'Failed deleting the product', 'status'=>'failed']);
    }




}

    /*
     *********************************************
     * Store a newly created products.
     *********************************************
     */
    public function store(Request $request)
    {
          //validate inputs
          $this->validate($request,
            [
             'name' => 'required',
             'onhandquantity' => 'required|integer',
             'allocatedquantity' => 'required|integer',
             'supplierPricelist' =>'required|integer', //supplier price
             'totalPurchasingCost'=>'required|integer', // price include handling and shipping
             'sellingprice' => 'required|integer', // end customer price
             'supplier'=>'required',
            ]);
          
         //$userdata = \Auth::user();
         $productinput = $request;

         //convert vat
         $vatcal = $productinput['vat']/100;

         // calculated available quantity
         /*
           Onhand quantity is the quantity of active inventory
           Allocated Quantity is the quantity sold or ordered or shipped to customer
           Available quantity is the quantity available for sale
         */
         $availableqty = $productinput['onhandquantity'] - $productinput['allocatedquantity'];
         
        $newproduct = new Product;

        $newproduct->productName           = $productinput['name'];
        $newproduct->onhandQuantity       = $productinput['onhandquantity']; 
        $newproduct->allocatedQuantity    = $productinput['allocatedquantity'];
        $newproduct->availableQuantity     = $availableqty; 
        $newproduct->productSize           = $productinput['size'];
        $newproduct->productColor          = $productinput['color'];
        $newproduct->productTag            = $productinput['tag'];
        $newproduct->productBrand          = $productinput['brand'];
        $newproduct->productBarcode        = $productinput['barcode'];
        $newproduct->productCategory       = $productinput['category'];
        $newproduct->productBuyingPrice    = $productinput['supplierPricelist']; //supplier price
        $newproduct->vat                   = $vatcal;
        $newproduct->productSellingPrice   = $productinput['sellingprice']; // consumer price
        $newproduct->totalPurchaseprice    = $productinput['totalPurchasingCost']; // shipping and handling
        $newproduct->supplier_id           = $productinput['supplier'];
        $newproduct->store_id              = $productinput['store'];
        $newproduct->productDescriptions   = $productinput['descriptions'];
        $newproduct->reOrderLevel          = $productinput['reorderlevel'];
        //$newproduct->user_id               = $userdata->id;
        $newproduct->expirationDate        = $productinput['expiredata'];
        $newproduct->productLocationAtStore= $productinput['locationatStore'];   

        // save into products able
        $newproduct->save();
       
       // get the last inserted id;
        $lastinsertId = $newproduct->id;

        // insert into stock
        $freshstock = New Stock;

        $freshstock->product_id         = $lastinsertId;
        $freshstock->productBarcode     = $productinput['barcode'];
        $freshstock->onhandQuantity       = $productinput['onhandquantity']; 
        $freshstock->allocatedQuantity    = $productinput['allocatedquantity'];
        $freshstock->availableQuantity     = $availableqty; 
        $freshstock->totalPurchaseprice = $productinput['totalPurchasingCost'];
        $freshstock->productBuyingPrice = $productinput['supplierPricelist']; //supplier price
        $freshstock->productSellingPrice= $productinput['sellingprice']; // consumer price
        $freshstock->save();

 
            
             // CREATE JOURNAL ENTRY FOR THIS ITEM

          //  echo $productinput['productpaymentmethod'];
         
            $purchasedate = date('Y-m-d');
            $totalprice = $productinput['onhandquantity'] * $productinput['supplierPricelist'];

            if($productinput['productpaymentmethod'] == 1) // cash terms
             {
             // credit cash account
             $purchasecreditacc = ChartsOfAccount::where("accountName","cash account")->select("chartaccId")->limit(1)->get();
             
              foreach($purchasecreditacc as $pcreditacc=>$pcredacc){
              $purchasecchart = $pcredacc['chartaccId'];
             }
               
               // debit inventory account
             $purchasedebitacc = ChartsOfAccount::where("accountName","inventory account")->select("chartaccId")->limit(1)->get();

              foreach($purchasedebitacc as $pdebitacc=>$pdebacc){
              $purcdchart = $pdebacc['chartaccId'];
             }


             // create journal for inventory account
             $newproductjournalDebit = new GeneralJournal;

             $newproductjournalDebit->referenceNumber = $lastinsertId;
             $newproductjournalDebit->gjdate = $purchasedate;
             $newproductjournalDebit->descriptions = "Purchase of new item(s)";
             $newproductjournalDebit->gjchartaccountId = $purcdchart;
             $newproductjournalDebit->debit = $totalprice;
             $newproductjournalDebit->credit = 0;


             $journalid = $newproductjournalDebit->save();

             // Get the last inserted journal id which will be referenced in ledger
              $lstjId = \DB::table('general_journals')
                           ->where('gjchartaccountId',$purcdchart)
                           ->orderBy('gjournalId','desc')
                           ->limit(1)
                           ->get();
                     // last inserted journal where id is specified
                        foreach($lstjId as $ket=>$v)
                        {
                          $jlastid = $v->gjournalId;
                        }


             //GENERAL LEDGER FOR INVENTORY ACCOUNT ON PURCHASING OF NEW ITEMS
             // Fetch the last insert id where account is inventory account
             $generalLeger  = \DB::table('general_ledgers')
                                  ->where('transactionId',$purcdchart) // Inventory account
                                  ->orderBy('ledgerId','desc')
                                  ->limit(1)
                                  ->get();

             if($generalLeger)
             {
              // The account is alerdy in,  so take the balance then plus with the new totalcost;
              foreach($generalLeger as $gledger=>$ledger)
              {



                $currentbalance = $ledger->glbalance;
  

                $newbalance = $currentbalance + $totalprice;


                 $updatenewledger = new GeneralLedger;

                  
                  $updatenewledger->gdate = $purchasedate;
                 // $newledger->referenceNumber = $lastinsertId;
                  $updatenewledger->transactionId = $purcdchart;
                  $updatenewledger->journalId   = $jlastid;
                  $updatenewledger->gltotaldebit = $totalprice;
                  $updatenewledger->gltotalcredit = 0;
                  $updatenewledger->glbalance = $newbalance;


                  $updatenewledger->save();

                   // update the trial balance for this account
                   TrialBalance::where('chartsaccountId',$purcdchart)->update(['trialbdebit'=>$newbalance]);
              }
             }else{
              // Account not available int general ledger, so create new one
                  $newledger = new GeneralLedger;

                  
                  $newledger->gdate = $purchasedate;
                 // $newledger->referenceNumber = $lastinsertId;
                  $newledger->transactionId = $purcdchart;
                  $newledger->journalId   = $jlastid;
                  $newledger->gltotaldebit = $totalprice;
                  $newledger->gltotalcredit = 0;
                  $newledger->glbalance = $totalprice;


                  $newledger->save();

                  // create new trial balance for inventory account it has debit nature
                  $productnewtrialbalance = new TrialBalance;

                  $productnewtrialbalance->chartsaccountId = $purcdchart;
                  $productnewtrialbalance->trialbdebit = $totalprice;
                  $productnewtrialbalance->trialbcredit = 0;

                  $productnewtrialbalance->save();


             }
            

             // create journal for cash account
             $newproductjournalCredit = new GeneralJournal;

             $newproductjournalCredit->referenceNumber = $lastinsertId;
             $newproductjournalCredit->gjdate = $purchasedate;
             $newproductjournalCredit->descriptions = "Purchase of new item(s)";
             $newproductjournalCredit->gjchartaccountId = $purchasecchart;
             $newproductjournalCredit->debit = 0;
             $newproductjournalCredit->credit = $totalprice;


             $newproductjournalCredit->save();

            
             // fetch the above inserted id from general journal
              $olstjId = \DB::table('general_journals')
                           ->where('gjchartaccountId',$purchasecchart)
                           ->orderBy('gjournalId','desc')
                           ->limit(1)
                           ->get();
                     // last inserted journal where id is specified
                        foreach($olstjId as $ket=>$v)
                        {
                          $ojlastid = $v->gjournalId;
                        }

             // CREAT LEDGER FOR CASH ACCOUNT
             //First check if the account already exist
              $ogeneralLeger  = \DB::table('general_ledgers')
                                  ->where('transactionId',$purchasecchart) // Inventory account
                                  ->orderBy('ledgerId','desc')
                                  ->limit(1)
                                  ->get();

            if(count($ogeneralLeger))
            {
               // cash account already in so update the balance
               foreach($ogeneralLeger as $key => $kvalue)
               {
                        $ocurrentbalance = $kvalue->glbalance;
                      //$ledgerdebit = $ledger->gltotaldebit;
                      //$ledgercredit = $ledger->gltotalcredit;

                      $onewbalance = $ocurrentbalance - $totalprice;


                       $oupdatenewledger = new GeneralLedger;

                        
                        $oupdatenewledger->gdate = $purchasedate;
                       // $newledger->referenceNumber = $lastinsertId;
                        $oupdatenewledger->transactionId = $purchasecchart;
                        $oupdatenewledger->journalId   = $ojlastid;
                        $oupdatenewledger->gltotaldebit = 0;
                        $oupdatenewledger->gltotalcredit = $totalprice;
                        $oupdatenewledger->glbalance = $onewbalance;


                        $oupdatenewledger->save();

                        //update trial balance for cash account
                        TrialBalance::where('chartsaccountId',$purchasecchart)->update(['trialbcredit'=>$onewbalance]);
               }
            }else{
              // cash account does not exist already, so creat new one
                
                $newledger = new GeneralLedger;

                  
                  $newledger->gdate = $purchasedate;
                  $newledger->transactionId = $purchasecchart;
                  $newledger->journalId   = $ojlastid;
                  $newledger->gltotaldebit = 0;
                  $newledger->gltotalcredit = $totalprice;
                  $newledger->glbalance = $totalprice;


                  $newledger->save();

                  //create new trial balance for cash account,since it does not exist
                  //Cash account has a debit nature, in trial balance will have balance of debit
                  $newcashtrialbalance = new TrialBalance;

                  $newcashtrialbalance->chartsaccountId = $purchasecchart;
                  $newcashtrialbalance->trialbdebit = 0;
                  $newcashtrialbalance->trialbcredit = $totalprice;

                  $newcashtrialbalance->save();
            }
         
           // END GENERAL LEDGER WHEN NEW ITEM WITH CASH PURCHASE ENTERED

           

       }
       elseif($productinput['productpaymentmethod'] == 2) // credit terms payments
       {

             // ON CREDIT TERMS PAYMENTS, Inventory account will be DEBITED while account payable will be CREDITED

             // credit account
             $purchasecreditacc = ChartsOfAccount::where("accountName","accounts payable")->select("chartaccId")->limit(1)->get();
              foreach($purchasecreditacc as $pcreditacc=>$pcredacc){
              $cchart = $pcredacc['chartaccId'];
             }
               
               // debit account
             $purchasedebitacc = ChartsOfAccount::where("accountName","inventory account")->select("chartaccId")->limit(1)->get();
              foreach($purchasedebitacc as $pdebitacc=>$pdebacc){
              $dchart = $pdebacc['chartaccId'];
             }



              // create journal for inventory account
             $newproductjournalDebitCreditpayment = new GeneralJournal;

             $newproductjournalDebitCreditpayment->referenceNumber = $lastinsertId;
             $newproductjournalDebitCreditpayment->gjdate = $purchasedate;
             $newproductjournalDebitCreditpayment->descriptions = "Purchase of new item(s) on credit terms";
             $newproductjournalDebitCreditpayment->gjchartaccountId = $dchart;
             $newproductjournalDebitCreditpayment->debit = $totalprice;
             $newproductjournalDebitCreditpayment->credit = 0;


             $newproductjournalDebitCreditpayment->save();
 


            // CREATE NEW LEDGER FOR inventory account
              $payablelstjId = \DB::table('general_journals')
                                   ->where('gjchartaccountId',$dchart)
                                   ->orderBy('gjournalId','desc')
                                   ->limit(1)
                                   ->get();

                     // last inserted journal where id is specified
                        foreach($payablelstjId as $ke=>$value)
                        {
                          $payablelastid = $value->gjournalId;

                        }


              //Then check if the account already exist in ledger
              $opayablegeneralLeger  = \DB::table('general_ledgers')
                                  ->where('transactionId',$dchart) // inventory account
                                  ->orderBy('ledgerId','desc')
                                  ->limit(1)
                                  ->get();

            if(count($opayablegeneralLeger))
            {
              // insert and update the balance in ledger
              foreach($opayablegeneralLeger as $v=>$values)
              {
                 $paybnewblance = $values->glbalance;
                
                // when inventory account is debited, means we increase it
                $newpaybalance = $paybnewblance + $totalprice;


                 // Create ledger
                $payableupdatenewledger = new GeneralLedger;

                  
                $payableupdatenewledger->gdate = $purchasedate;
               // $newledger->referenceNumber = $lastinsertId;
                $payableupdatenewledger->transactionId = $dchart;
                $payableupdatenewledger->journalId   = $payablelastid;
                $payableupdatenewledger->gltotaldebit = $totalprice;
                $payableupdatenewledger->gltotalcredit = 0;
                $payableupdatenewledger->glbalance = $newpaybalance;

                $payableupdatenewledger->save();

                //update trial balance for inventory account
                TrialBalance::where('chartsaccountId',$dchart)->update(['trialbdebit'=>$newpaybalance]);

              }
              
            }else{

              // create new ledger for this account
                  $newledger = new GeneralLedger;

                  
                  $newledger->gdate = $purchasedate;
                  $newledger->transactionId = $dchart;
                  $newledger->journalId   = $payablelastid;
                  $newledger->gltotaldebit = $totalprice;
                  $newledger->gltotalcredit = 0;
                  $newledger->glbalance = $totalprice;


                  $newledger->save();

                  // create new trial balance for inventory account
                  $trialbalanceinventoryacc = new TrialBalance;
                  
                  $trialbalanceinventoryacc->chartsaccountId = $dchart;
                  $trialbalanceinventoryacc->trialbdebit = $totalprice;
                  $trialbalanceinventoryacc->trialbcredit = 0;

                  $trialbalanceinventoryacc->save();

            }

            // END CREATE LEDGDER inventory account when account exist
             


            // create journal for account payable
             $newitemcreditjournal = new GeneralJournal;

             $newitemcreditjournal->referenceNumber = $lastinsertId;
             $newitemcreditjournal->gjdate = $purchasedate;
             $newitemcreditjournal->descriptions = "Purchase of new item(s) on credit terms";
             $newitemcreditjournal->gjchartaccountId = $cchart;
             $newitemcreditjournal->debit = 0;
             $newitemcreditjournal->credit = $totalprice;


            $savepurchasecredit = $newitemcreditjournal->save();
            
             // START GENERAL LEDGER FOR ACCOUNT PAYABLE
            // Get the id of the above journal entry
              $accpayablelstjId = \DB::table('general_journals')
                           ->where('gjchartaccountId',$cchart)
                           ->orderBy('gjournalId','desc')
                           ->limit(1)
                           ->get();

                     // last inserted journal where id is specified
                        foreach($accpayablelstjId as $ke=>$value)
                        {
                          $accpayablelastid = $value->gjournalId;

                        }


              //Then check if the account already exist in ledger
              $accopayablegeneralLeger  = \DB::table('general_ledgers')
                                              ->where('transactionId',$cchart) // account payable
                                              ->orderBy('ledgerId','desc')
                                              ->limit(1)
                                              ->get();

               
              if(count($accopayablegeneralLeger))
              {
                // account already exist in the ledger, so insert it and update the balance
                // account payable is increased when credited, and decreased when debited
                
                foreach($accopayablegeneralLeger as $keve=>$value)
                {
                     $balanc = $value->glbalance;

                     // Here the account has been credited, so we increase the balance in the ledger
                     $newbalanc = $balanc + $totalprice;

                     // now insert and update the ledger
                     $ccnewpledger = new GeneralLedger;

                  
                    $ccnewpledger->gdate = $purchasedate;
                    $ccnewpledger->transactionId = $cchart;
                    $ccnewpledger->journalId   = $accpayablelastid;
                    $ccnewpledger->gltotaldebit = 0;
                    $ccnewpledger->gltotalcredit = $totalprice;
                    $ccnewpledger->glbalance = $newbalanc;

                    $ccnewpledger->save();

                    //update inventory for account payable, it has credit nature hence will have credit balance
                    TrialBalance::where('chartsaccountId',$cchart)->update(['trialbcredit'=>$newbalanc]);

                }

              }else{
                // account not exist in the ledger, so create new ledger
                  $newpledger = new GeneralLedger;

                  
                  $newpledger->gdate = $purchasedate;
                  $newpledger->transactionId = $cchart;
                  $newpledger->journalId   = $accpayablelastid;
                  $newpledger->gltotaldebit = 0;
                  $newpledger->gltotalcredit = $totalprice;
                  $newpledger->glbalance = $totalprice;


                  $newpledger->save();

                  // create new account payable in trial balance
                  $trialaccountpayable = new TrialBalance;

                  $trialaccountpayable->chartsaccountId = $cchart;
                  $trialaccountpayable->trialbdebit = 0;
                  $trialaccountpayable->trialbcredit = $totalprice;

                  $trialaccountpayable->save();
              }


       }

        session()->flash('flash_message', 'New product successfully saved!');

        return redirect('product');

    }




    /**
     ********************************************
     * Update products.
     *********************************************
     */
    public function productupdate(Request $request)
    {
      
     //$userdata = \Auth::user();

       $productinput = $request;

       $productId = $productinput['asdhgasha'];

       Product::where('id', $productId)
                    ->update( [ 
                        'productName'             => $productinput['name'], 
                        'onhandQuantity'         => $productinput['onhandquantity'],
                        'allocatedQuantity'      => $productinput['allocatedquantity'],
                        'availableQuantity'     => $productinput['availablequantity'],
                        'productSize'                => $productinput['size'],
                        'productColor'            =>$productinput['color'],
                        'productTag'               =>$productinput['tag'],
                        'productBrand'         => $productinput['brand'],
                         'productBarcode'         => $productinput['barcode'],
                         'productCategory'        => $productinput['category'],
                         'productBuyingPrice'    => $productinput['supplierPricelist'],
                         'vat'                     =>$productinput['vat'],
                         'productSellingPrice'    => $productinput['sellingprice'],
                         'supplier_id'                 => $productinput['supplier'],
                         'store_id'                     => $productinput['store'],
                         'expirationDate'             => $productinput['expiredata'],
                          'productDescriptions'  => $productinput['descriptions'],
                          'reOrderLevel'            => $productinput['reorderlevel'],
                         //'user_id'                      => $userdata->id
                          ] );


                     Stock::where('product_id', $productId)
                    ->update( [ 
                        //'productName'             => $productinput['name'], 
                        'onhandQuantity'         => $productinput['onhandquantity'],
                        'allocatedQuantity'      => $productinput['allocatedquantity'],
                        'availableQuantity'     => $productinput['availablequantity'],
                       /* 'productSize'                => $productinput['size'],
                        'productColor'            =>$productinput['color'],
                        'productTag'               =>$productinput['tag'],
                        'productBrand'         => $productinput['brand'],
                         'productBarcode'         => $productinput['barcode'],
                         'productCategory'        => $productinput['category'],*/
                         'totalPurchaseprice'    => $productinput['totalPurchasingCost'],
                         'productBuyingPrice'    => $productinput['supplierPricelist'],
                       //  'vat'                     =>$productinput['vat'],
                         'productSellingPrice'    => $productinput['sellingprice'],
                        /* 'supplier_id'                 => $productinput['supplier'],
                         'store_id'                     => $productinput['store'],
                         'expirationDate'             => $productinput['expiredata'],
                          'productDescriptions'  => $productinput['descriptions'],*/
                          'reoderLevel'            => $productinput['reorderlevel'],
                         //'user_id'                      => $userdata->id
                          ] );




    
            return response(['msg'=>'Item updated successfully', 'status'=>'success']);
         
    }


    /**
     ******************************************
     * Remove the specified resource from storage.
     ******************************************
     */
    public function destroySingleProduct(Request $request)
    {
        $id = $request['delproduct'];
         $checkIfExist = Product::findOrFail($id);

       

           $deleteprod = Product::where('id', $id)->delete();

            if($deleteprod){
            return response(['msg'=>'Product deleted', 'status'=>'success']);
          }else{
          return response(['msg'=>'Failed deleting the Product', 'status'=>'failed']);
          }
         
    }




/*
******************************************************
* Upload product excel sheet to product table and stock table
* First import the Excel class above
*******************************************************
*/
public function importExcelFile(Request $request)
{  
   
    // Validate file
     $this->validate($request, [
                 'uploadfile' => 'required|mimes:xls,xlsx,ods,csv,xml',

            ]);

     // Read the input file
    $fileToUpload = $request->file('uploadfile');

    /**
     * Start import the file
     */
    Excel::load($fileToUpload, function($reader){

    $reader->each(function($row){
   foreach ($row as $value) {
     // $userdata = \Auth::user();
     $availableqty = $value['onhandquantity'] - $value['allocatedquantity'];

    //var_dump($value);
       $crp = Product::create([
      'productName'         =>$value['productname'],
      'supplier_id'         =>$value['supplier_id'],
      'expirationDate'      =>$value['expirationdate'],
      'productCategory'     =>$value['productcategory'],
      'productBrand'        =>$value['productbrand'],
      'productDescriptions' =>$value['productdescriptions'],
      'productTag'          =>$value['producttag'],
      'productSize'         =>$value['productsize'],
      'productcolor'        =>$value['productcolor'],
      'onhandQuantity'      =>$value['onhandquantity'],
      'allocatedQuantity'   =>$value['allocatedquantity'],
      'availableQuantity'   =>$availableqty,
      'totalPurchaseprice'  =>$value['totalpurchaseprice'],
      'productBuyingPrice'  =>$value['productbuyingprice'],
      'productSellingPrice' =>$value['productsellingprice'],
      'vat'                 =>($value['vat']/100),
      'store_id'            =>$value['store_id'],
      'productBarcode'      =>$value['productbarcode'],
       //'user_id'            => "2"
        ]);

       // get the last inserted id;
        $lastinsertId = $crp->id;

       // insert into stock
        $freshstock = New Stock;

        $freshstock->product_id         = $lastinsertId;
        $freshstock->productBarcode     = $value['productbarcode'];
        $freshstock->onhandQuantity     = $value['onhandquantity'];
        $freshstock->allocatedQuantity  = $value['allocatedquantity'];
        $freshstock->availableQuantity  = $availableqty;
        $freshstock->totalPurchaseprice = $value['totalpurchaseprice'];
        $freshstock->productBuyingPrice = $value['productbuyingprice']; //supplier price
        $freshstock->productSellingPrice= $value['productsellingprice']; // consumer price
        $freshstock->save();



   }
});

    });


     $lastinsertId = \DB::table('products')->max('id');
     $purchasedate = date('Y-m-d');

     //sum buying price
     //$totcost = \DB::table('products')->sum('productbuyingprice');//WRONG QUERY FORMULA
      $calcInventory= \DB::table('products')->select(\DB::raw('sum(onhandQuantity *  productBuyingPrice) as inveValue' ))->get();

     foreach ($calcInventory as $key => $value) {
      $totcost=$value->inveValue;
     }
    



    //TRANSACTION FOR CASH AND OWNERS CAPTAL
     //1.DEBITING CASH
     $cashfromcapital = ChartsOfAccount::where("accountName","Cash Account")->select("chartaccId")->limit(1)->get();
        foreach ($cashfromcapital as $key => $value) {
          $cashcapital = $value['chartaccId'];
        }

        $newcashjournal = new GeneralJournal;

        $newcashjournal->referenceNumber = $lastinsertId;
        $newcashjournal->gjdate = $purchasedate;
        $newcashjournal->descriptions = "Cash from owners capital";
        $newcashjournal->gjchartaccountId = $cashcapital;
        $newcashjournal->debit = $totcost;
        $newcashjournal->credit = 0;
        
        $newcashjournal->save();

        // Fetch the last id of cash account
        $maxmuncashid = GeneralJournal::where('gjchartaccountId',$cashcapital)->max('gjournalId');

        // Then create general ledger for owners cash account, but first check if already exist
       $ledgercashaccount = \DB::table('general_ledgers')->where('transactionId',$cashcapital)->orderBy('ledgerId','desc')->limit(1)->get();

       if($ledgercashaccount){
            foreach ($ledgercashaccount as $key => $value) {
                
                $cashtrialbalance = $value->glbalance;

                $newcashbalance = $cashtrialbalance + $totcost;
                    
                  // update ledger for cash account
                $ledgernewcash  = new GeneralLedger;

                $ledgernewcash->gdate = $purchasedate;
                $ledgernewcash->transactionId = $cashcapital;
                $ledgernewcash->journalId   = $maxmuncashid;
                $ledgernewcash->gltotaldebit =$totcost;
                $ledgernewcash->gltotalcredit =  0;
                $ledgernewcash->glbalance = $newcashbalance;


                $ledgernewcash->save();


                // update trial balance for cash account
                TrialBalance::where('chartsaccountId',$cashcapital)->update(['trialbdebit'=>$newcashbalance]);
            }
         
          
       }else{
                // create new ledger for cash account

                $newcashledger  = new GeneralLedger;

                $newcashledger->gdate = $purchasedate;
                $newcashledger->transactionId = $cashcapital;
                $newcashledger->journalId   = $maxmuncashid;
                $newcashledger->gltotaldebit =$totcost;
                $newcashledger->gltotalcredit =  0;
                $newcashledger->glbalance = $totcost;

                $newcashledger->save();

           //create new trial balance for cash account
                $newcashtrialbalance = new TrialBalance;

                $newcashtrialbalance->chartsaccountId = $cashcapital;
                $newcashtrialbalance->trialbdebit = $totcost;
                $newcashtrialbalance->trialbcredit = 0;

                $newcashtrialbalance->save();
       }

    //1.END DEBITING CASH


    //2.CREDITING OWNERS CAPITAL
       //From here, we assume that this is the owner's capital, so we debit cash account and credit owener's capital account
       $owner = ChartsOfAccount::where("accountName","Owners capital")->select("chartaccId")->limit(1)->get();
        foreach ($owner as $key => $value) {
          $ownerscapital = $value['chartaccId'];
        }

    
        $capitaljournal = new GeneralJournal;

        $capitaljournal->referenceNumber = $lastinsertId;
        $capitaljournal->gjdate = $purchasedate;
        $capitaljournal->descriptions = "Owners capital";
        $capitaljournal->gjchartaccountId = $ownerscapital;
        $capitaljournal->debit = 0;
        $capitaljournal->credit = $totcost;
        
        $capitaljournal->save();
        
       // Fetch the last id of owners capital
        $maxmuncapitalid = GeneralJournal::where('gjchartaccountId',$ownerscapital)->max('gjournalId');

      // Then create general ledger for owners capital account, but first check if already exist
       $generalLegercapital  = \DB::table('general_ledgers')->where('transactionId',$ownerscapital)->orderBy('ledgerId','desc')->limit(1)->get();

       if($generalLegercapital)
       {

          foreach ($generalLegercapital as $key => $value) {

                $capitalbalance = $value->glbalance;

                $newcapitalbalance = $capitalbalance + $totcost;
                  
                // create update general ledger for owners capital account
                $updatecapital = new GeneralLedger;


                $updatecapital->gdate = $purchasedate;
                // $newledger->referenceNumber = $lastinsertId;
                $updatecapital->transactionId = $ownerscapital;
                $updatecapital->journalId   = $maxmuncapitalid;
                $updatecapital->gltotaldebit =0;
                $updatecapital->gltotalcredit =  $totcost;
                $updatecapital->glbalance = $newcapitalbalance;


                $updatecapital->save();

                // Update the trial balance too for owners capital
                TrialBalance::where('chartsaccountId',$ownerscapital)->update(['trialbcredit'=>$newcapitalbalance]);
          }



       }else{
              // Capital does not exist, so create new
               $newgenerallegercapital = new GeneralLedger;


                $newgenerallegercapital->gdate = $purchasedate;
                $newgenerallegercapital->transactionId = $ownerscapital;
                $newgenerallegercapital->journalId   = $maxmuncapitalid;
                $newgenerallegercapital->gltotaldebit =0;
                $newgenerallegercapital->gltotalcredit =  $totcost;
                $newgenerallegercapital->glbalance = $totcost;;


                $newgenerallegercapital->save();

                // create new trial balance owners capital, since it does not exists
                $newcapitaltrialbalance = new TrialBalance;

                $newcapitaltrialbalance->chartsaccountId = $ownerscapital;
                $newcapitaltrialbalance->trialbdebit = 0;
                $newcapitaltrialbalance->trialbcredit = $totcost;

                $newcapitaltrialbalance->save();
       }
      //2. END CREDITING OWNERS CAPITAL
   // END TRANSACTION FOR CASH AND OWNERS CAPITAL
// die();



    //1. CREATE JOURNAL ENTRY FOR CASH AND INVENTORY ACCOUNT
        
               // debit inventory account
             $purchasedebitacc = ChartsOfAccount::where("accountName","inventory account")->select("chartaccId")->limit(1)->get();

              foreach($purchasedebitacc as $pdebitacc=>$pdebacc){
              $purcdchart = $pdebacc['chartaccId'];
             }


             // create journal for inventory account
             $newproductjournalDebit = new GeneralJournal;

             $newproductjournalDebit->referenceNumber = $lastinsertId;
             $newproductjournalDebit->gjdate = $purchasedate;
             $newproductjournalDebit->descriptions = "Purchase of new item(s) by cash";
             $newproductjournalDebit->gjchartaccountId = $purcdchart;
             $newproductjournalDebit->debit = $totcost;
             $newproductjournalDebit->credit = 0;


             $journalid = $newproductjournalDebit->save();

             // Get the last inserted journal id which will be referenced in ledger
              $lstjId = \DB::table('general_journals')
                           ->where('gjchartaccountId',$purcdchart)
                           ->orderBy('gjournalId','desc')
                           ->limit(1)
                           ->get();
                     // last inserted journal where id is specified
                        foreach($lstjId as $ket=>$v)
                        {
                          $jlastid = $v->gjournalId;
                        }


             //GENERAL LEDGER FOR INVENTORY ACCOUNT ON PURCHASING OF NEW ITEMS
             // Fetch the last insert id where account is inventory account
             $generalLeger  = \DB::table('general_ledgers')
                                  ->where('transactionId',$purcdchart) // Inventory account
                                  ->orderBy('ledgerId','desc')
                                  ->limit(1)
                                  ->get();

             if($generalLeger)
             {
              // The account is alerdy in,  so take the balance then plus with the new totalcost;
              foreach($generalLeger as $gledger=>$ledger)
              {



                $currentbalance = $ledger->glbalance;
  

                $newbalance = $currentbalance + $totcost;


                 $updatenewledger = new GeneralLedger;

                  
                  $updatenewledger->gdate = $purchasedate;
                 // $newledger->referenceNumber = $lastinsertId;
                  $updatenewledger->transactionId = $purcdchart;
                  $updatenewledger->journalId   = $jlastid;
                  $updatenewledger->gltotaldebit = $totcost;
                  $updatenewledger->gltotalcredit = 0;
                  $updatenewledger->glbalance = $newbalance;


                  $updatenewledger->save();

                   // update the trial balance for this account
                   TrialBalance::where('chartsaccountId',$purcdchart)->update(['trialbdebit'=>$newbalance]);
              }
             }else{
              // Account not available int general ledger, so create new one
                  $newledger = new GeneralLedger;

                  
                  $newledger->gdate = $purchasedate;
                 // $newledger->referenceNumber = $lastinsertId;
                  $newledger->transactionId = $purcdchart;
                  $newledger->journalId   = $jlastid;
                  $newledger->gltotaldebit = $totcost;
                  $newledger->gltotalcredit = 0;
                  $newledger->glbalance = $totcost;


                  $newledger->save();

                  // create new trial balance for inventory account it has debit nature
                  $productnewtrialbalance = new TrialBalance;

                  $productnewtrialbalance->chartsaccountId = $purcdchart;
                  $productnewtrialbalance->trialbdebit = $totcost;
                  $productnewtrialbalance->trialbcredit = 0;

                  $productnewtrialbalance->save();


             }
            

              
              // credit cash account
             $purchasecreditacc = ChartsOfAccount::where("accountName","cash account")->select("chartaccId")->limit(1)->get();
             
              foreach($purchasecreditacc as $pcreditacc=>$pcredacc){
              $purchasecchart = $pcredacc['chartaccId'];
             }

             // create journal for cash account
             $newproductjournalCredit = new GeneralJournal;

             $newproductjournalCredit->referenceNumber = $lastinsertId;
             $newproductjournalCredit->gjdate = $purchasedate;
             $newproductjournalCredit->descriptions = "Purchase of new item(s)";
             $newproductjournalCredit->gjchartaccountId = $purchasecchart;
             $newproductjournalCredit->debit = 0;
             $newproductjournalCredit->credit = $totcost;


             $newproductjournalCredit->save();

            
             // fetch the above inserted id from general journal
              $olstjId = \DB::table('general_journals')
                           ->where('gjchartaccountId',$purchasecchart)
                           ->orderBy('gjournalId','desc')
                           ->limit(1)
                           ->get();

                     // last inserted journal where id is specified
                        foreach($olstjId as $ket=>$v)
                        {
                          $ojlastid = $v->gjournalId;
                        }

             // CREAT LEDGER FOR CASH ACCOUNT
             //First check if the account already exist
              $ogeneralLeger  = \DB::table('general_ledgers')
                                  ->where('transactionId',$purchasecchart) // Inventory account
                                  ->orderBy('ledgerId','desc')
                                  ->limit(1)
                                  ->get();

            if(count($ogeneralLeger))
            {
               // cash account already in so update the balance
               foreach($ogeneralLeger as $key => $kvalue)
               {
                        $ocurrentbalance = $kvalue->glbalance;
                      //$ledgerdebit = $ledger->gltotaldebit;
                      //$ledgercredit = $ledger->gltotalcredit;

                      $onewbalance = $ocurrentbalance - $totcost;


                       $oupdatenewledger = new GeneralLedger;

                        
                        $oupdatenewledger->gdate = $purchasedate;
                       // $newledger->referenceNumber = $lastinsertId;
                        $oupdatenewledger->transactionId = $purchasecchart;
                        $oupdatenewledger->journalId   = $ojlastid;
                        $oupdatenewledger->gltotaldebit = 0;
                        $oupdatenewledger->gltotalcredit = $totcost;
                        $oupdatenewledger->glbalance = $onewbalance;


                        $oupdatenewledger->save();

                        //update trial balance for cash account
                        TrialBalance::where('chartsaccountId',$purchasecchart)->update(['trialbdebit'=>$onewbalance]);
               }
            }else{
              // cash account does not exist already, so creat new one
                
                $newledger = new GeneralLedger;

                  
                  $newledger->gdate = $purchasedate;
                  $newledger->transactionId = $purchasecchart;
                  $newledger->journalId   = $ojlastid;
                  $newledger->gltotaldebit = 0;
                  $newledger->gltotalcredit = $totcost;
                  $newledger->glbalance = $totcost;


                  $newledger->save();

                  //create new trial balance for cash account,since it does not exist
                  //Cash account has a debit nature, in trial balance will have balance of debit
                  $newcashtrialbalance = new TrialBalance;

                  $newcashtrialbalance->chartsaccountId = $purchasecchart;
                  $newcashtrialbalance->trialbdebit = $totcost;
                  $newcashtrialbalance->trialbcredit = 0;

                  $newcashtrialbalance->save();
            }
         
           // END GENERAL LEDGER WHEN NEW ITEM WITH CASH PURCHASE ENTERED

           
    session()->flash('flash_message', 'Products uploaded Successfully!');
    return redirect('product');
}


/**
**********************************************
*   Export products to excel
***********************************************
*/
public function  getExcelFile()
{
       // $products = Customer::all();
    Excel::create('Products_sheet', function($excel){
        
        $excel->sheet('products', function($sheet){
            $sheet->fromArray(Product::all([
                                            'productName',
                                            'productBarcode',
                                            'onhandQuantity',
                                            'allocatedQuantity',
                                            'totalPurchaseprice',
                                            'productBuyingPrice', 
                                            'productSellingPrice',
                                            'supplier_id',
                                            'expirationDate',
                                            'productCategory',
                                            'productBrand',
                                            'productDescriptions',
                                            'productTag',
                                            'productSize',
                                            'productcolor',
                                            'vat',
                                            'store_id'
                                        ]));
        });
    })->export('xlsx');

    return redirect('product');
}


}
