<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Customer;
use App\Product;
use App\User;
use App\Supplier;
use App\ChartsOfAccount;
use App\GeneralJournal;
use App\Expense;
use App\GeneralLedger;
use App\TrialBalance;

class AccountingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // get authenticated user
         $userdata = \Auth::user();
          
         $supplier = Supplier::all();

         $supplier2 = Supplier::all();

          //count total customer
         $customerdata  = Customer::all();

         $user =  User::all();

         // Return total products
         $products = Product::all();
         
         // return list of account category
         $cataccouting = \DB::table('accountTypes')->get();
         $editcataccouting = \DB::table('accountTypes')->get();
         $charts = ChartsOfAccount::all();
         $chartofaccountexpense = ChartsOfAccount::whereIn('accountTypeId',['1','2','3'])->get();
         $chartsexpens = ChartsOfAccount::whereIn('accountTypeId',['5'])->get();

         //accounts balances
         
          
         return view('accounting.index', compact('userdata', 'supplier','supplier2', 'customerdata', 'products', 'user','cataccouting','editcataccouting','charts','chartofaccountexpense','chartsexpens'));
    }

    /**
     * return list of charts of accounting
     */
    public function getchartsOfaccounting()
    {
        // return charts of accounts
         $chartsaccount = \DB::table('charts_of_accounts')
                               ->join('accountTypes', 'charts_of_accounts.accountTypeId', '=', 'accountTypes.acctypeId')
                               ->select('charts_of_accounts.*','accountTypes.accName')
                               ->get();

         return response()->json(["data"=>$chartsaccount]);
    }

    /**
     * Store a newly created charts of accounting
     */
    public function createnewChartsOfaccounting(Request $request)
    {
        //validate the inputs
        $this->validate($request,[
                'accountCategory'=>'required',
                'accountName'=>'required'
            ]);
      $add = $request->all();

     
        // save
        $newaccount = new ChartsOfAccount;

        $newaccount->accountTypeId = $add['accountCategory'];
        $newaccount->accountName = $add['accountName'];
        $newaccount->accIdenitifier = $add['accidentifier'];
        $newaccount->descriptions = $add['accdescriptions'];

        $saved = $newaccount->save();
        if($saved){
           return response()->json(['data' => "success"],200);
          }else{
            return response()->json(['data' => "error"],400);
          }

    }

    /**
     * Get expense claims via ajax
     */
    public function getExpenseClaims()
    {
         //$claims = Expense::with('users')->get();
         $claims = \DB::table('expenses')->join('users','expenses.expenseCreatedby','=','users.id')->get();

         return response()->json(["data"=>$claims]);
    }

    /**
     * return expenses for viewing more details
     */
    public function viewmoreExpense(Request $request)
    {
       
         $claimsdetails = \DB::table('expenses')
                               ->where("expenseid",$request['expenseid'])
                               ->join('users','expenses.expenseCreatedby','=','users.id')
                               ->join('charts_of_accounts','expenses.expensechartaccountId','=','charts_of_accounts.chartaccId')
                               ->select('expenses.*','users.name','charts_of_accounts.*')
                               ->get();

         return response()->json(["data"=>$claimsdetails]);
    }



    /**
     * Record journal when item is sold by cash
     */
    public function newCashSalesjournal(Request $request)
    {
         $this->validate($request,[
                  'amount'=>'required',
                  //'itemsoldto'=>'required',
                  'salereference'=>'required',
                  'costofsolditems'=>'required'
         ]);

         $saledate = date('Y-m-d');

         //account debited
         $debitacc = ChartsOfAccount::where("accountName","cash account")->select("chartaccId")->limit(1)->get();

         foreach($debitacc as $dacc=>$debacc){
          $dchart = $debacc['chartaccId'];
         }
      


         $newsalejournal = new GeneralJournal;

         $newsalejournal->referenceNumber = $request['salereference'];
         $newsalejournal->gjdate = $saledate;
         $newsalejournal->descriptions = "Sold item(s) by Cash";
         $newsalejournal->gjchartaccountId = $dchart;
         $newsalejournal->debit = $request['amount'];
         $newsalejournal->credit = 0;


        $savesaledebit = $newsalejournal->save();
        
            // START GENERAL LEDGER FOR CASH ACCOUNT
            // Get the id of the above journal entry
              $saleledgerId = \DB::table('general_journals')
                           ->where('gjchartaccountId',$dchart)
                           ->orderBy('gjournalId','desc')
                           ->limit(1)
                           ->get();

                     // last inserted journal where id is specified
                        foreach($saleledgerId as $ke=>$value)
                        {
                          $salelastid = $value->gjournalId;

                        }


              //Then check if the sale account already exist in ledger
              $salegeneralLeger  = \DB::table('general_ledgers')
                                              ->where('transactionId',$dchart) // cash account
                                              ->orderBy('ledgerId','desc')
                                              ->limit(1)
                                              ->get();

           if(count($salegeneralLeger))
           {
             // Sales account already exist in the ledger, so create new and update the balance
                 foreach($salegeneralLeger as $key=>$va)
                 {
                    $salebal = $va->glbalance;

                    // Cash account is increased by debiting, and here we are debiting it, so we increase its balance.
                    $newsalesbalance  = $salebal + $request['amount'];
                    

                    $newsaleledgerbook = new GeneralLedger;

                    $newsaleledgerbook->gdate = $saledate;
                    $newsaleledgerbook->transactionId = $dchart;
                    $newsaleledgerbook->journalId   = $salelastid;
                    $newsaleledgerbook->gltotaldebit = $request['amount'];
                    $newsaleledgerbook->gltotalcredit = 0;
                    $newsaleledgerbook->glbalance = $newsalesbalance;


                    $newsaleledgerbook->save();

                    // update the trial balance
                    TrialBalance::where('chartsaccountId',$dchart)->update(['trialbdebit'=>$newsalesbalance]);


                 }

           }else{

             // cash account does not exist, so start it is new ledger
                  $salenewledger = new GeneralLedger;

                  
                  $salenewledger->gdate = $saledate;
                  $salenewledger->transactionId = $dchart;
                  $salenewledger->journalId   = $salelastid;
                  $salenewledger->gltotaldebit = $request['amount'];
                  $salenewledger->gltotalcredit = 0;
                  $salenewledger->glbalance = $request['amount'];


                  $salenewledger->save();

                  // create trialbalance
                  $cashtrialbalance = new TrialBalance;

                  $cashtrialbalance->chartsaccountId = $dchart;
                  $cashtrialbalance->trialbdebit =$request['amount'];
                  $cashtrialbalance->trialbcredit =0;

                  $cashtrialbalance->save();


           }








        //if($savesaledebit){

         // account to credited(sales account)
         $creditacc = ChartsOfAccount::where("accountName","sales account")->select("chartaccId")->limit(1)->get();
         
          foreach($creditacc as $cacc=>$credacc){
             $creditonsalechart = $credacc['chartaccId'];
         }

           //return response()->json(['data' => "success"],200);
         $newsalecreditjournal = new GeneralJournal;

         $newsalecreditjournal->referenceNumber = $request['salereference'];
         $newsalecreditjournal->gjdate = $saledate;
         $newsalecreditjournal->descriptions = "Sold item(s) by Cash";
         $newsalecreditjournal->gjchartaccountId = $creditonsalechart;
         $newsalecreditjournal->debit = 0;
         $newsalecreditjournal->credit = $request['amount'];


         $newsalecreditjournal->save();

          // START GENERAL LEDGER FOR SALES ACCOUNT
            // Get the id of the above journal entry
              $salecreditledgerId = \DB::table('general_journals')
                           ->where('gjchartaccountId',$creditonsalechart)
                           ->orderBy('gjournalId','desc')
                           ->limit(1)
                           ->get();

                     // last inserted journal where id is specified
                        foreach($salecreditledgerId as $ke=>$value)
                        {
                          $salecreditlastid = $value->gjournalId;

                        }


              //Then check if the sale account already exist in ledger
              $creditsalegeneralLeger  = \DB::table('general_ledgers')
                                              ->where('transactionId',$creditonsalechart) // sales account
                                              ->orderBy('ledgerId','desc')
                                              ->limit(1)
                                              ->get();

              if(count($creditsalegeneralLeger))
              {
                // account exist, so take the balance and adjust it.
                   foreach($creditsalegeneralLeger as $key=>$value)
                   {
                     $cashb = $value->glbalance;

                     // cash account is increased by crediting and reduced when debited
                     $newcashb = $cashb + $request['amount'];

                     $updatecashaccount = new GeneralLedger;

                     $updatecashaccount->gdate = $saledate;
                     $updatecashaccount->transactionId = $creditonsalechart;
                     $updatecashaccount->journalId   = $salecreditlastid;
                     $updatecashaccount->gltotaldebit = 0;
                     $updatecashaccount->gltotalcredit = $request['amount'];
                     $updatecashaccount->glbalance = $newcashb;


                     $updatecashaccount->save();

                     // sales account has credit nature in Trial Balance
                     TrialBalance::where('chartsaccountId',$creditonsalechart)->update(['trialbcredit'=>$newcashb]);

                   }
              }else{
                // account does not exist in the ledger, so create its new ledger

                    $cresalenewledger = new GeneralLedger;

                  
                    $cresalenewledger->gdate = $saledate;
                    $cresalenewledger->transactionId = $creditonsalechart;
                    $cresalenewledger->journalId   = $salecreditlastid;
                    $cresalenewledger->gltotaldebit = 0;
                    $cresalenewledger->gltotalcredit = $request['amount'];
                    $cresalenewledger->glbalance = $request['amount'];


                    $cresalenewledger->save();

                    // create new trial balance for sales account
                    $newsaletrialbalance = new TrialBalance;

                    $newsaletrialbalance->chartsaccountId = $creditonsalechart;
                    $newsaletrialbalance->trialbdebit = 0;
                    $newsaletrialbalance->trialbcredit = $request['amount'];

                    $newsaletrialbalance->save();
              }




         

         

          // Record Second Transaction debit

          // Second transactions
         // debit cost of good sold
         $secondtrInv = ChartsOfAccount::where("accountName","Cost of Goods Sold")->select("chartaccId")->limit(1)->get();
          foreach($secondtrInv as $invacc=>$inventoraccount){
          $updateinvntorysale = $inventoraccount['chartaccId'];
         }
         
         


         $secondnewsalejournal = new GeneralJournal;

         $secondnewsalejournal->referenceNumber = $request['salereference'];
         $secondnewsalejournal->gjdate = $saledate;
         $secondnewsalejournal->descriptions = "cost of goods sold";
         $secondnewsalejournal->gjchartaccountId = $updateinvntorysale;
         $secondnewsalejournal->debit = $request['costofsolditems'];
         $secondnewsalejournal->credit = 0;


         $secondnewsalejournal->save();


         // GENERAL LEDGER for cost of goods sold
                   // Get the id of the above journal entry
                  $costofgoodsoldjornalid = \DB::table('general_journals')
                               ->where('gjchartaccountId',$updateinvntorysale)
                               ->orderBy('gjournalId','desc')
                               ->limit(1)
                               ->get();

                         // last inserted journal where id is specified
                            foreach($costofgoodsoldjornalid as $ke=>$value)
                            {
                              $costsoldid = $value->gjournalId;

                            }


                  //Then check if the sale account already exist in ledger
                  $costofgoodsgeneralLeger  = \DB::table('general_ledgers')
                                                  ->where('transactionId',$updateinvntorysale) // sales account
                                                  ->orderBy('ledgerId','desc')
                                                  ->limit(1)
                                                  ->get();

                  if(count($costofgoodsgeneralLeger))
                  {
                    // cost of goods sold available in the ledger, so create and update the balance
                     foreach($costofgoodsgeneralLeger as $kecv=>$value)
                     {
                         $costofgoodbalance = $value->glbalance;

                         // cost of goods account is debited to increase the balance
                         $newcostofgoodbalance = $costofgoodbalance + $request['costofsolditems'];

                         $newledgercostofgoods = new GeneralLedger;

                          $newledgercostofgoods->gdate = $saledate;
                          $newledgercostofgoods->transactionId = $updateinvntorysale;
                          $newledgercostofgoods->journalId   = $costsoldid;
                          $newledgercostofgoods->gltotaldebit = $request['costofsolditems'];
                          $newledgercostofgoods->gltotalcredit = 0;
                          $newledgercostofgoods->glbalance = $newcostofgoodbalance;//$request['costofsolditems'];


                          $newledgercostofgoods->save();

                          // update trial balance for costof good sold account
                          TrialBalance::where('chartsaccountId',$updateinvntorysale)->update(['trialbdebit'=>$newcostofgoodbalance]);

                     }
                  }else{
                    // cost of goods sold not available in the ledger so create one

                    $costofgoodsnewledger = new GeneralLedger;

                  
                    $costofgoodsnewledger->gdate = $saledate;
                    $costofgoodsnewledger->transactionId = $updateinvntorysale;
                    $costofgoodsnewledger->journalId   = $costsoldid;
                    $costofgoodsnewledger->gltotaldebit = $request['costofsolditems'];
                    $costofgoodsnewledger->gltotalcredit = 0;
                    $costofgoodsnewledger->glbalance = $request['costofsolditems'];


                    $costofgoodsnewledger->save();
                    
                    // New trial balance for cost of good sold
                    $newcostgsold = new TrialBalance;
                     
                    $newcostgsold->chartsaccountId = $updateinvntorysale;
                    $newcostgsold->trialbdebit = $request['costofsolditems'];
                    $newcostgsold->trialbcredit = 0;

                    $newcostgsold->save();

                  }

         // END GENERAL LEDGER for cost of goods sold
       





 

         
         // credit inventory
         $secondtrCostOfgoodsSold = ChartsOfAccount::where("accountName","inventory account")->select("chartaccId")->limit(1)->get();
          foreach($secondtrCostOfgoodsSold as $costSold=>$costofgoods){
          $costofgoodsold = $costofgoods['chartaccId'];
         }

        // Record Second Transaction credit
         $secondnewsalejournalcredit = new GeneralJournal;

         $secondnewsalejournalcredit->referenceNumber = $request['salereference'];
         $secondnewsalejournalcredit->gjdate = $saledate;
         $secondnewsalejournalcredit->descriptions = "Sold item(s) by Cash (inventory)";
         $secondnewsalejournalcredit->gjchartaccountId = $costofgoodsold;
         $secondnewsalejournalcredit->debit = 0;
         $secondnewsalejournalcredit->credit = $request['costofsolditems'];


        $secondsavesaledebit = $secondnewsalejournalcredit->save();

        // GENERAL LEDGER INVENTORY ACCOUNT
             // Get the id of the above journal entry
                  $inventoryjornalid = \DB::table('general_journals')
                               ->where('gjchartaccountId',$costofgoodsold)
                               ->orderBy('gjournalId','desc')
                               ->limit(1)
                               ->get();

                         // last inserted journal where id is specified
                            foreach($inventoryjornalid as $key=>$value)
                            {
                              $inventorycostid = $value->gjournalId;

                            }


                  //Then check if the sale account already exist in ledger
                  $costofgoodsgeneralLeger  = \DB::table('general_ledgers')
                                                  ->where('transactionId',$costofgoodsold) // inventory account
                                                  ->orderBy('ledgerId','desc')
                                                  ->limit(1)
                                                  ->get();
               
               if(count($costofgoodsgeneralLeger))
               {
                // Invetory account exist in the ledger, so create new and update the balance
                  foreach($costofgoodsgeneralLeger as $key=>$value)
                  {
                       $invebalance = $value->glbalance;
                         
                        // Inventory account is increase by debiting, here we are crediting it to reduce.
                       $newinvebalance  = $invebalance - $request['costofsolditems'];

                       $newinvetoledger  = new GeneralLedger;
                    
                      $newinvetoledger->gdate = $saledate;
                      $newinvetoledger->transactionId = $costofgoodsold;
                      $newinvetoledger->journalId   = $inventorycostid;
                      $newinvetoledger->gltotaldebit = 0;
                      $newinvetoledger->gltotalcredit = $request['costofsolditems'];
                      $newinvetoledger->glbalance = $newinvebalance;


                      $newinvetoledger->save();

                      //inventory account has  a nature of debiting, so in trial balance total balance will be debit
                      TrialBalance::where('chartsaccountId',$costofgoodsold)->update(['trialbdebit'=>$newinvebalance]);
                  }

               }else{
                // inventory account does not exist int the ledger so create new 
                   $invetoledger  = new GeneralLedger;
                    
                    $invetoledger->gdate = $saledate;
                    $invetoledger->transactionId = $costofgoodsold;
                    $invetoledger->journalId   = $inventorycostid;
                    $invetoledger->gltotaldebit = 0;
                    $invetoledger->gltotalcredit = $request['costofsolditems'];
                    $invetoledger->glbalance = $request['costofsolditems'];


                    $invetoledger->save();


                    // New trial balance for inventory account
                    $newinventorytrialbalance = new TrialBalance;
                     
                    $newinventorytrialbalance->chartsaccountId = $costofgoodsold;
                    $newinventorytrialbalance->trialbdebit = $request['costofsolditems'];
                    $newinventorytrialbalance->trialbcredit = 0;

                    $newinventorytrialbalance->save();

               }
        // END GENERAL LEDGER INVENTORY ACCOUNT


          // if($secondsavesaledebit)
          // {
            return response()->json(['data' => "successaccounting"],200);
          // }else{
          //   return response()->json(['data' => "error"],400);
          // }

          // }else{
          //   return response()->json(['data' => "error"],400);
          // }
    }







    /**
     * Remove the specified account
     */
    public function deleteSingleAccount(Request $request)
    {

        $this->validate($request,[
            'accountname'=>'required'
            ]);
  
        // check if id exist
        // $checkIfExist = ChartsOfAccount::findOrFail($request['accountname']);

        // if($checkIfExist)
        // {
           $deleteacc  =  ChartsOfAccount::where('chartaccId',$request['accountname'])->delete();

                  if($deleteacc){
                  return response(['msg'=>'account deleted', 'status'=>'success']);
                  }else{
                  return response(['msg'=>'Failed deleting the account', 'status'=>'failed']);
                  }

        // }else{

        // }
    }



    // delete multiple account at once
    public function deleteMultipleAccount(Request $request)
    {
        $this->validate($request, [
                       'accountname' => 'required',
                  ]);
         $ids = $request['accountname'];

         //dd($data);


         $delp = \DB::table('charts_of_accounts')->whereIn('chartaccId', $ids)->delete();
      

          if($delp){
            return response(['msg'=>'Account(s) deleted', 'status'=>'success']);
          }else{
          return response(['msg'=>'Failed deleting the account', 'status'=>'failed']);
          }
    }


    // return account to edit ajax request
    public function getAccoutToEdit(Request $request)
    {
        $this->validate($request,['accountid'=>'required']);
        $accid = $request->accountid;

        $getaccount = \DB::table('charts_of_accounts')->where('chartaccId', $accid)
                                                     ->join('accountTypes', 'charts_of_accounts.accountTypeId', '=', 'accountTypes.acctypeId')
                                                     ->get();


        return response()->json(["data"=>$getaccount]);
    }


  // update edited account
    public function updateEditedAccount(Request $request)
    {
        $id = $request['accid'];

        $updateacc  = \DB::table('charts_of_accounts')->where('chartaccId',$id)
                                                    ->update([
                                                            'accountTypeId' =>$request['accountcat'],
                                                            'accountName'   =>$request['newaccountname'],
                                                            'accIdenitifier'=>$request['identifier'],
                                                            'descriptions'  =>$request['descripts']
                                                         ]);

        if($updateacc){
            return response(['msg'=>'Account updated', 'status'=>'success']);
          }else{
          return response(['msg'=>'Failed updating the account', 'status'=>'failed']);
          }
    }


 // journal debited account(This is from accounting where you can debit and credit any account)
  public function createNewJournal(Request $request)
  {
      $this->validate($request,[
                  'debitedaccount'=>'required',
                  'debitedamount'=>'required',
                  'reference'=>'required',
         ]);

           // CREATE JOURNAL FOR DEBITED ACCOUNT
         $newjournal = new GeneralJournal;

         $newjournal->referenceNumber = $request['reference'];
         $newjournal->gjdate = $request['datecreated'];
         $newjournal->descriptions = $request['debitdescript'];
         $newjournal->gjchartaccountId = $request['debitedaccount'];
         $newjournal->debit = $request['debitedamount'];
         $newjournal->credit = 0;


        $savedebit = $newjournal->save();

        // START LEGDER BOOK FOR DEBITED ACCOUNT

                    // fetch the id of the above transaction from general journal
                      $debitedaccountsid = \DB::table('general_journals')->where('gjchartaccountId',$request['debitedaccount'])->max('gjournalId');

                    // Check if the account exist in the general ledger
                       $existLeger  = \DB::table('general_ledgers')
                                ->where('transactionId',$request['debitedaccount'])
                                ->orderBy('ledgerId','desc')
                                ->limit(1)
                                ->get();

                      if(count($existLeger)){
                              // the account already exist in the journal so create new and the update the balance
                             foreach($existLeger as $key=>$va)
                             {
                                $latestbalance = $va->glbalance;

                                // all expenses accounts are increased by debiting
                                $newlatestbalance = $latestbalance + $request['debitedamount'];

                                $gledger  = new GeneralLedger;
                                
                                $gledger->gdate = $request['datecreated'];
                                $gledger->transactionId = $request['debitedaccount'];
                                $gledger->journalId   = $debitedaccountsid;
                                $gledger->gltotaldebit = $request['debitedamount'];
                                $gledger->gltotalcredit = 0;
                                $gledger->glbalance = $newlatestbalance;
                               
                                $gledger->save();

                                
                                //update trial balance for this account
                                TrialBalance::where('chartsaccountId',$request['debitedaccount'])->update(['trialbdebit'=>$newlatestbalance]);

                             }
                      }else{

                            // The account does not exist in the general ledger, so create new ledger
                              $newledgertransaction = new GeneralLedger;

                              $newledgertransaction->gdate = $request['datecreated'];
                              $newledgertransaction->transactionId = $request['debitedaccount'];
                              $newledgertransaction->journalId   = $debitedaccountsid;
                              $newledgertransaction->gltotaldebit = $request['debitedamount'];
                              $newledgertransaction->gltotalcredit = 0;
                              $newledgertransaction->glbalance = $request['debitedamount'];
                               
                               $newledgertransaction->save();

                               // create new trial balance for this account
                               $newtrialbalanceany = new TrialBalance;

                               $newtrialbalanceany->chartsaccountId = $request['debitedaccount'];
                               $newtrialbalanceany->trialbdebit = $request['debitedamount'];
                               $newtrialbalanceany->trialbcredit = 0;

                               $newtrialbalanceany->save();
                      }
        //END LEGDER BOOK FOR DEBITED ACCOUNT








          // CREATE JOURNAL FOR CREDITED ACCOUNT
         $newcreditjournal = new GeneralJournal;

         $newcreditjournal->referenceNumber = $request['reference'];
         $newcreditjournal->gjdate = $request['datecreated'];
         $newcreditjournal->descriptions = $request['creditdescript'];
         $newcreditjournal->gjchartaccountId = $request['crediteaccount'];
         $newcreditjournal->debit = 0;
         $newcreditjournal->credit = $request['creditedamount'];


         $savecreditedjournal = $newcreditjournal->save();

          // START GENERAL LEDGER FOR CREDITED ACCOUNT

                      // fetch the id of the above transaction from general journal
                      $maxcreditedaccountsid = \DB::table('general_journals')->where('gjchartaccountId',$request['crediteaccount'])->max('gjournalId');

                    // Check if the account exist in the general ledger
                       $creditedexistLeger  = \DB::table('general_ledgers')
                                ->where('transactionId',$request['crediteaccount'])
                                ->orderBy('ledgerId','desc')
                                ->limit(1)
                                ->get();

                       if(count($creditedexistLeger)){

                              // The account exist in the ledger, so create new and then update the balance
                             foreach($creditedexistLeger as $k=>$va)
                             {
                                $recentcreditbalance = $va->glbalance;

                                //
                                $newrcreditbalance = $recentcreditbalance - $request['creditedamount'];


                                $updatecreditbalance = new GeneralLedger;

                                $updatecreditbalance->gdate = $request['datecreated'];
                                $updatecreditbalance->transactionId = $request['crediteaccount'];
                                $updatecreditbalance->journalId   = $maxcreditedaccountsid;
                                $updatecreditbalance->gltotaldebit = 0;
                                $updatecreditbalance->gltotalcredit = $request['creditedamount'];
                                $updatecreditbalance->glbalance = $newrcreditbalance;
                               
                                $updatecreditbalance->save();

                               //update trial balance for this account
                                TrialBalance::where('chartsaccountId',$request['crediteaccount'])->update(['trialbcredit'=>$newrcreditbalance]);
                             }

                       }else{

                              // The account does not exist in the ledger, so create new one
                              $crediaccount = new GeneralLedger;
                             
                              $crediaccount->gdate = $request['datecreated'];
                              $crediaccount->transactionId = $request['crediteaccount'];
                              $crediaccount->journalId   = $maxcreditedaccountsid;
                              $crediaccount->gltotaldebit = 0;
                              $crediaccount->gltotalcredit = $request['creditedamount'];
                              $crediaccount->glbalance = $request['creditedamount'];
                               
                              $crediaccount->save();


                               // create new trial balance for this credited account
                               $newtrialbalancecreditany = new TrialBalance;

                               $newtrialbalancecreditany->chartsaccountId =$request['crediteaccount'];
                               $newtrialbalancecreditany->trialbdebit = 0;
                               $newtrialbalancecreditany->trialbcredit = $request['creditedamount'];

                               $newtrialbalancecreditany->save();
                       }

                       return response()->json(['data' => "success"],200);
          // if($savecreditedjournal)
          // {
          //   return response()->json(['data' => "success"],200);
          // }else{
          //   return response()->json(['data' => "error"],400);
          // }
         //END GENERAL LEDGER FOR CREDITED ACCOUNT
       
  }




  // journal debited account(NOT USED BY ANY FACE, NOT SURE..)
  public function newJournalcreditedaccount(Request $request)
  {
      $this->validate($request,[
                  'creditedaccount'=>'required',
                  'creditedamount'=>'required',
                  'reference'=>'required',
         ]);

         $newcreditjournal = new GeneralJournal;

         $newcreditjournal->referenceNumber = $request['reference'];
         $newcreditjournal->gjdate = $request['datecreated'];
         $newcreditjournal->descriptions = $request['debitdescript'];
         $newcreditjournal->gjchartaccountId = $request['debitedaccount'];
         $newcreditjournal->debit = 0;
         $newcreditjournal->credit = $request['creditedamount'];


         $savecreditedjournal = $newcreditjournal->save();
        if($savecreditedjournal){
           return response()->json(['data' => "success"],200);
          }else{
            return response()->json(['data' => "error"],400);
          }
  }
// Not sure, this is not used




//  Record accepted expenses in the jornal and update in the ledger
  public function acceptExpenses(Request $request)
  {

     $this->validate($request,[
                  'expensecreditaccount'=>'required',
                  'expenseduedate'=>'required',
                  'expensereference'=>'required',
                  'expensedescriptions'=>'required',
                  'originalamount'=>'required',
                  'expensedebitaccount'=>'required'
         ]);



         $newexpensejournal = new GeneralJournal;

         $newexpensejournal->referenceNumber = $request['expensereference'];
         $newexpensejournal->gjdate = $request['expenseduedate'];
         $newexpensejournal->descriptions = $request['expensedescriptions'];
         $newexpensejournal->gjchartaccountId = $request['expensedebitaccount'];
         $newexpensejournal->debit = $request['originalamount'];
         $newexpensejournal->credit = 0;


         $newexpensejournal->save();

        // START LEGDER BOOK FOR DEBITED ACCOUNT
          // fetch the id of the above transaction
                      $debitedaccountsid = \DB::table('general_journals')
                                   ->where('gjchartaccountId',$request['expensedebitaccount'])
                                   ->orderBy('gjournalId','desc')
                                   ->limit(1)
                                   ->get();
                                   
                     // last inserted journal where id is specified
                        foreach($debitedaccountsid as $k=>$value)
                        {
                          $debitedlastid = $value->gjournalId;

                        }

            // Check if the account exist in the ledger
                         $existanceingeneralLeger  = \DB::table('general_ledgers')
                                  ->where('transactionId',$request['expensedebitaccount']) // any expense account
                                  ->orderBy('ledgerId','desc')
                                  ->limit(1)
                                  ->get();

                         if(count($existanceingeneralLeger))
                         {
                            // the account already exist in the journal so create new and the update the balance
                             foreach($existanceingeneralLeger as $key=>$va)
                             {
                                $expbalance = $va->glbalance;

                                // all expenses accounts are increased by debiting
                                $newexpbalance = $expbalance + $request['originalamount'];

                                $debexpenseledger  = new GeneralLedger;
                                
                                $debexpenseledger->gdate = $request['expenseduedate'];
                                $debexpenseledger->transactionId = $request['expensedebitaccount'];
                                $debexpenseledger->journalId   = $debitedlastid;
                                $debexpenseledger->gltotaldebit = $request['originalamount'];
                                $debexpenseledger->gltotalcredit = 0;
                                $debexpenseledger->glbalance = $newexpbalance;
                               
                                $debexpenseledger->save();

                                // expenses has debit nature, so in trial balance they will have total debit
                                TrialBalance::where('chartsaccountId',$request['expensedebitaccount'])->update(['trialbdebit'=>$newexpbalance]);

                             }

                         }else{
                           // The account does not exist in the ledger, so create new ledger
                              $newexpensedledger = new GeneralLedger;

                              $newexpensedledger->gdate = $request['expenseduedate'];
                              $newexpensedledger->transactionId = $request['expensedebitaccount'];
                              $newexpensedledger->journalId   = $debitedlastid;
                              $newexpensedledger->gltotaldebit = $request['originalamount'];
                              $newexpensedledger->gltotalcredit = 0;
                              $newexpensedledger->glbalance = $request['originalamount'];
                               
                               $newexpensedledger->save();

                               // new trial balance for expense account
                               $expensetrialbalance = new TrialBalance;
                               
                               $expensetrialbalance->chartsaccountId = $request['expensedebitaccount'];
                               $expensetrialbalance->trialbdebit = $request['originalamount']; 
                               $expensetrialbalance->trialbcredit = 0;

                               $expensetrialbalance->save();


                         }
        // END LEGDER BOOK
       // if($saveexpensedebit){

           //return response()->json(['data' => "success"],200);
         $newexpensescreditjournal = new GeneralJournal;

         $newexpensescreditjournal->referenceNumber = $request['expensereference'];
         $newexpensescreditjournal->gjdate = $request['expenseduedate'];
         $newexpensescreditjournal->descriptions = $request['expensedescriptions'];
         $newexpensescreditjournal->gjchartaccountId = $request['expensecreditaccount'];
         $newexpensescreditjournal->debit = 0;
         $newexpensescreditjournal->credit = $request['originalamount'];


         $newexpensescreditjournal->save();

         // START LEDGER FOR CREDITED ACCOUNT
                  $creditedaccountsid = \DB::table('general_journals')
                                   ->where('gjchartaccountId',$request['expensecreditaccount'])
                                   ->orderBy('gjournalId','desc')
                                   ->limit(1)
                                   ->get();
                                   
                       // last inserted journal where id is specified
                        foreach($creditedaccountsid as $key=>$value)
                        {
                          $creditedlastid = $value->gjournalId;

                        }

                        // Check if the account exist in the ledger
                         $existcreditingeneralLeger  = \DB::table('general_ledgers')
                                  ->where('transactionId',$request['expensecreditaccount']) // any account where money can come from in order to pay expense
                                  ->orderBy('ledgerId','desc')
                                  ->limit(1)
                                  ->get();

                         if(count($existcreditingeneralLeger))
                         {
                             // The account exist in the ledger, so create new and then update the balance
                             foreach($existcreditingeneralLeger as $k=>$va)
                             {
                                $currentcreditbalance = $va->glbalance;

                                //
                                $newcurrentcreditbalance = $currentcreditbalance - $request['originalamount'];

                                $updatecreditbalance = new GeneralLedger;

                                $updatecreditbalance->gdate = $request['expenseduedate'];
                                $updatecreditbalance->transactionId = $request['expensecreditaccount'];
                                $updatecreditbalance->journalId   = $creditedlastid;
                                $updatecreditbalance->gltotaldebit = 0;
                                $updatecreditbalance->gltotalcredit = $request['originalamount'];
                                $updatecreditbalance->glbalance = $newcurrentcreditbalance;
                               
                                $updatecreditbalance->save();
                                 
                                 // I am assuming that cash and payable accounts are the only accounts used for paying expenses
                    TrialBalance::where('chartsaccountId',$request['expensecreditaccount'])->update(['trialbdebit'=>$newcurrentcreditbalance]); 

                             }

                         }else{
                             // The account does not exist in the ledger, so create new one
                              $creditingaccount = new GeneralLedger;
                             
                              $creditingaccount->gdate = $request['expenseduedate'];
                              $creditingaccount->transactionId = $request['expensecreditaccount'];
                              $creditingaccount->journalId   = $creditedlastid;
                              $creditingaccount->gltotaldebit = 0;
                              $creditingaccount->gltotalcredit = $request['originalamount'];
                              $creditingaccount->glbalance = $request['originalamount'];
                               
                              $creditingaccount->save();


                              // create new trial for this account
                              $expenwscredittrial = new TrialBalance;

                              $expenwscredittrial->chartsaccountId = $request['expensecreditaccount'];
                              $expenwscredittrial->trialbdebit = $request['originalamount'];
                              $expenwscredittrial->trialbcredit = 0;

                               $expenwscredittrial->save();

                         }

         // END LEFGER FOR CREDOTED ACCOUNT

         //$request['acceptedexpenseid'];
         $saveacceptedexpensesjournal = Expense::where('expenseid',$request['acceptedexpenseid'])->update([
            'expenseStatus'=>1,
          ]);

          if($saveacceptedexpensesjournal)
          {
             return response(['msg'=>'Transaction Accepted', 'status'=>'success']);
          }else{
            return response(['msg'=>'Transaction Failed', 'status'=>'failed']);
          }

          // }else{
          //   return response()->json(['data' => "error"],400);
          // } 
  }




  // Record new purchases journal book
  public function newPurchasejournal(Request $request)
  {
       $this->validate($request,[
                  'amount'=>'required',
                  'reference'=>'required',
                  'paymentdate'=>'required',
                  'methodtopay'=>'required'
         ]);
          
        // Check payment method if cash we debit Purchase Account and credit Cash Account
       // if method is credit, debit Purchase Account and credit Payable Account
       if($request['methodtopay'] == 1) // cash terms
       {
   
                  // debit Inventory account
             $purchasedebitacc = ChartsOfAccount::where("accountName","inventory account")->select("chartaccId")->limit(1)->get();

              foreach($purchasedebitacc as $pdebitacc=>$pdebacc){
              $purcdchart = $pdebacc['chartaccId'];
             }
               


             // create journal
             $newpurchaasejournal = new GeneralJournal;

             $newpurchaasejournal->referenceNumber = $request['reference'];
             $newpurchaasejournal->gjdate = $request['paymentdate'];
             $newpurchaasejournal->descriptions = "Purchase of item(s)";
             $newpurchaasejournal->gjchartaccountId = $purcdchart;
             $newpurchaasejournal->debit = $request['amount'];
             $newpurchaasejournal->credit = 0;


             $newpurchaasejournal->save();


                           // CREATE NEW LEDGER FOR inventory account
              $payablelstjId = \DB::table('general_journals')
                                   ->where('gjchartaccountId',$purcdchart)
                                   ->orderBy('gjournalId','desc')
                                   ->limit(1)
                                   ->get();
                                   
                     // last inserted journal where id is specified
                        foreach($payablelstjId as $ke=>$value)
                        {
                          $payablelastid = $value->gjournalId;

                        }


              //Then check if the account already exist in ledger
              $opayablegeneralLeger  = \DB::table('general_ledgers')
                                  ->where('transactionId',$purcdchart) // inventory account
                                  ->orderBy('ledgerId','desc')
                                  ->limit(1)
                                  ->get();

        
           if(count($opayablegeneralLeger))
            {
              // insert and update the balance in ledger
              foreach($opayablegeneralLeger as $v=>$values)
              {

                $paybnewblance = $values->glbalance;
                
                // when inventory account is debited, means we increase it
                $newpaybalance = $paybnewblance + $request['amount'];


                 // Create ledger
                $payableupdatenewledger = new GeneralLedger;

                  
                $payableupdatenewledger->gdate = $request['paymentdate'];
               // $newledger->referenceNumber = $lastinsertId;
                $payableupdatenewledger->transactionId = $purcdchart;
                $payableupdatenewledger->journalId   = $payablelastid;
                $payableupdatenewledger->gltotaldebit = $request['amount'];
                $payableupdatenewledger->gltotalcredit = 0;
                $payableupdatenewledger->glbalance = $newpaybalance;

                $payableupdatenewledger->save();

                //update the trial balance for inventory account, it has debit balance
                TrialBalance::where('chartsaccountId',$purcdchart)->update(['trialbdebit'=>$newpaybalance]);
              }
              
            }else{

              // create new ledger for this account
                  $newledger = new GeneralLedger;

                  
                  $newledger->gdate = $request['paymentdate'];
                  $newledger->transactionId = $purcdchart;
                  $newledger->journalId   = $payablelastid;
                  $newledger->gltotaldebit = $request['amount'];
                  $newledger->gltotalcredit = 0;
                  $newledger->glbalance = $request['amount'];


                  $newledger->save();

                  // create new tria balance for inventory account, it has debit balance
                  $inventtrialbalance = new TrialBalance;

                  $inventtrialbalance->chartsaccountId = $purcdchart;
                  $inventtrialbalance->trialbdebit = $request['amount'];
                  $inventtrialbalance->trialbcredit = 0;

                  $inventtrialbalance->save();

            }

            // END CREATE LEDGDER inventory account when account exist








            // credit account
             $purchasecreditacc = ChartsOfAccount::where("accountName","cash account")->select("chartaccId")->limit(1)->get();
             
              foreach($purchasecreditacc as $pcreditacc=>$pcredacc){
              $purchasecchart = $pcredacc['chartaccId'];
             }
           

              // create journal
             $newpurchasecreditjournal = new GeneralJournal;

             $newpurchasecreditjournal->referenceNumber = $request['reference'];
             $newpurchasecreditjournal->gjdate = $request['paymentdate'];
             $newpurchasecreditjournal->descriptions = "Purchase of item(s)";
             $newpurchasecreditjournal->gjchartaccountId = $purchasecchart;
             $newpurchasecreditjournal->debit = 0;
             $newpurchasecreditjournal->credit = $request['amount'];


             $newpurchasecreditjournal->save();

                $accpayablelstjId = \DB::table('general_journals')
                           ->where('gjchartaccountId',$purchasecchart)
                           ->orderBy('gjournalId','desc')
                           ->limit(1)
                           ->get();

                     // last inserted journal where id is specified
                        foreach($accpayablelstjId as $ke=>$value)
                        {
                          $accpayablelastid = $value->gjournalId;

                        }


              //Then check if the account already exist in ledger
              $accopayablegeneralLeger  = \DB::table('general_ledgers')
                                              ->where('transactionId',$purchasecchart) //  cash account
                                              ->orderBy('ledgerId','desc')
                                              ->limit(1)
                                              ->get();

              // Check if account exist
              if(count($accopayablegeneralLeger))
              {
                // account already exist in the ledger, so insert it and update the balance
                // cash account is increased when debited, and reduced when credited
                
                foreach($accopayablegeneralLeger as $keve=>$value)
                {
                     $balanc = $value->glbalance;

                     // Here the cash account has been credited, so we reduce the balance in the ledger
                     $newbalanc = $balanc - $request['amount'];

                     // now insert and update the ledger
                     $ccnewpledger = new GeneralLedger;

                  
                    $ccnewpledger->gdate = $request['paymentdate'];
                    $ccnewpledger->transactionId = $purchasecchart;
                    $ccnewpledger->journalId   = $accpayablelastid;
                    $ccnewpledger->gltotaldebit = 0;
                    $ccnewpledger->gltotalcredit = $request['amount'];
                    $ccnewpledger->glbalance = $newbalanc;

                    $ccnewpledger->save();

                    //update trial balance for cash account, usually has debit balance.
                    TrialBalance::where('chartsaccountId',$purchasecchart)->update(['trialbdebit'=>$newbalanc]);

                }

              }else{
                // account not exist in the ledger, so create new ledger
                  $newpledger = new GeneralLedger;

                  
                  $newpledger->gdate = $request['paymentdate'];
                  $newpledger->transactionId = $purchasecchart;
                  $newpledger->journalId   = $accpayablelastid;
                  $newpledger->gltotaldebit = 0;
                  $newpledger->gltotalcredit = $request['amount'];;
                  $newpledger->glbalance = $request['amount'];;


                  $newpledger->save();

                  // create new trial balance for cash account, usually has debite balance in trial balance
                  $cashnewtrialbalance = new TrialBalance;
                  
                  $cashnewtrialbalance->chartsaccountId = $purchasecchart;
                  $cashnewtrialbalance->trialbdebit = $request['amount'];
                  $cashnewtrialbalance->trialbcredit = 0;

                  $cashnewtrialbalance->save();

              }
                 // if($savepurchasecredit){
                       return response(['msg'=>'Receiving Purchase completed', 'status'=>'success']);
                 // }else{
                 //        return response(['msg'=>'Failed to receive purchase', 'status'=>'failed']);
                 // }


          

       }

       elseif($request['methodtopay'] == 2) // credit terms payments
       {

            
               
               // debit account
             $purchasedebitacc = ChartsOfAccount::where("accountName","inventory account")->select("chartaccId")->limit(1)->get();
              foreach($purchasedebitacc as $pdebitacc=>$pdebacc){
              $dchart = $pdebacc['chartaccId'];
             }



             // create debit inventory account journal
             $newpurchaasejournal = new GeneralJournal;

             $newpurchaasejournal->referenceNumber = $request['reference'];
             $newpurchaasejournal->gjdate = $request['paymentdate'];
             $newpurchaasejournal->descriptions = "Purchase of item(s)";
             $newpurchaasejournal->gjchartaccountId = $dchart;
             $newpurchaasejournal->debit = $request['amount'];
             $newpurchaasejournal->credit = 0;


             $newpurchaasejournal->save();


              // CREATE NEW LEDGER FOR inventory account
              $xpayablelstjId = \DB::table('general_journals')
                                   ->where('gjchartaccountId',$dchart)
                                   ->orderBy('gjournalId','desc')
                                   ->limit(1)
                                   ->get();
                                   
                     // last inserted journal where id is specified
                        foreach($xpayablelstjId as $ke=>$value)
                        {
                          $xpayablelastid = $value->gjournalId;

                        }


              //Then check if the account already exist in ledger
              $opayablegeneralLeger  = \DB::table('general_ledgers')
                                  ->where('transactionId',$dchart) // inventory account
                                  ->orderBy('ledgerId','desc')
                                  ->limit(1)
                                  ->get();
               
              // Check if account exist
               if(count($opayablegeneralLeger))
               {
                // account inventory exist, create new ledger with updated balance

                foreach($opayablegeneralLeger as $v=>$values)
                {
                   $paybnewblance = $values->glbalance;
                  
                  // when inventory account is debited, means we increase it
                  $newpaybalance = $paybnewblance + $request['amount'];


                   // Create ledger
                  $payableupdatenewledger = new GeneralLedger;

                    
                  $payableupdatenewledger->gdate = $request['paymentdate'];
                 // $newledger->referenceNumber = $lastinsertId;
                  $payableupdatenewledger->transactionId = $dchart;
                  $payableupdatenewledger->journalId   = $xpayablelastid;
                  $payableupdatenewledger->gltotaldebit = $request['amount'];
                  $payableupdatenewledger->gltotalcredit = 0;
                  $payableupdatenewledger->glbalance = $newpaybalance;

                   $payableupdatenewledger->save();

                   //update trial balance for inventory, usually has debit balance
                   TrialBalance::where('chartsaccountId',$dchart)->update(['trialbdebit'=>$newpaybalance]);

                }

               }else{
                // account inventory does not exist, so create new ledger

                  $xnewledger = new GeneralLedger;

                  
                  $xnewledger->gdate = $request['paymentdate'];
                  $xnewledger->transactionId = $dchart;
                  $xnewledger->journalId   = $xpayablelastid;
                  $xnewledger->gltotaldebit = $request['amount'];
                  $xnewledger->gltotalcredit = 0;
                  $xnewledger->glbalance = $request['amount'];


                  $xnewledger->save();

                  //create new trial balance for inventory account, usually has debit balance
                  $newinvtrialtb = new TrialBalance;

                  $newinvtrialtb->chartsaccountId  =  $dchart;
                  $newinvtrialtb->trialbdebit = $request['amount'];
                  $newinvtrialtb->trialbcredit = 0;

                  $newinvtrialtb->save();
               }
             // END DEBITING INVENTORY ACCOUNT






             
              // credit account payable
             $purchasecreditacc = ChartsOfAccount::where("accountName","accounts payable")->select("chartaccId")->limit(1)->get();
              foreach($purchasecreditacc as $pcreditacc=>$pcredacc){
              $cchart = $pcredacc['chartaccId'];
             }

             // create credit journal
             $newpurchasecreditjournal = new GeneralJournal;

             $newpurchasecreditjournal->referenceNumber = $request['reference'];
             $newpurchasecreditjournal->gjdate = $request['paymentdate'];
             $newpurchasecreditjournal->descriptions = "Purchase of item(s)";
             $newpurchasecreditjournal->gjchartaccountId = $cchart;
             $newpurchasecreditjournal->debit = 0;
             $newpurchasecreditjournal->credit = $request['amount'];


            $newpurchasecreditjournal->save();
                 // if($savepurchasecredit){
                 //       return response(['msg'=>'Receiving Purchase completed', 'status'=>'success']);
                 // }else{
                 //        return response(['msg'=>'Failed to receive purchase', 'status'=>'failed']);
                 // }
            
              // START GENERAL LEDGER FOR ACCOUNT PAYABLE
            // Get the id of the above journal entry
              $accpayablelstjId = \DB::table('general_journals')
                           ->where('gjchartaccountId',$cchart)
                           ->orderBy('gjournalId','desc')
                           ->limit(1)
                           ->get();

                     // last inserted journal where id is specified
                        foreach($accpayablelstjId as $ke=>$value)
                        {
                          $accpayablelastid = $value->gjournalId;

                        }


              //Then check if the account already exist in ledger
              $accopayablegeneralLeger  = \DB::table('general_ledgers')
                                              ->where('transactionId',$cchart) // account payable
                                              ->orderBy('ledgerId','desc')
                                              ->limit(1)
                                              ->get();

          
               // Check if account payable already exist in the ledger
                if(count($accopayablegeneralLeger))
                {
                    // Account payable exist in the ledger, so create new ledger and update the balance
                    // Account payable is increased by CREDITING, and reduced by DEBITING, here we are crediting it
                    foreach($accopayablegeneralLeger as $keve=>$value)
                  {
                       $balanc = $value->glbalance;

                       // Here the account has been credited, so we increase the balance in the ledger
                       $newbalanc = $balanc + $request['amount'];

                       // now insert and update the ledger
                       $ccnewpledger = new GeneralLedger;

                    
                      $ccnewpledger->gdate = $request['paymentdate'];
                      $ccnewpledger->transactionId = $cchart;
                      $ccnewpledger->journalId   = $accpayablelastid;
                      $ccnewpledger->gltotaldebit = 0;
                      $ccnewpledger->gltotalcredit = $request['amount'];
                      $ccnewpledger->glbalance = $newbalanc;

                      $ccnewpledger->save();

                      // update trial balance for account payable, usually has credit balance
                      TrialBalance::where('chartsaccountId',$cchart)->update(['trialbcredit'=>$newbalanc]);

                  }

                }else{

                      // account not exist in the ledger, so create new ledger
                      $newpledger = new GeneralLedger;

                      
                      $newpledger->gdate = $request['paymentdate'];
                      $newpledger->transactionId = $cchart;
                      $newpledger->journalId   = $accpayablelastid;
                      $newpledger->gltotaldebit = 0;
                      $newpledger->gltotalcredit = $request['amount'];
                      $newpledger->glbalance = $request['amount'];;


                      $newpledger->save();

                      //create new trial balance for account payable, usually has credit balance
                      $newaccpayabletrialbalance  = new TrialBalance;

                      $newaccpayabletrialbalance->chartsaccountId = $cchart;
                      $newaccpayabletrialbalance->trialbdebit = 0;
                      $newaccpayabletrialbalance->trialbcredit = $request['amount'];

                      $newaccpayabletrialbalance->save();
                }

                return response(['msg'=>'Receiving Purchase completed', 'status'=>'success']);

       }
  }


  //Delete single expense claimis
  public function deleteSingleExpenseClaim(Request $request)
  {
     $this->validate($request,[
          'expenseid'=>'required'
      ]);

     $deleteexpense = Expense::where('expenseid',$request['expenseid'])->delete();

     if($deleteexpense == True){
         return response(['msg'=>'Claim deleted successfully', 'status'=>'success']);
     }else{
        return response(['msg'=>'couldnt delete claim', 'status'=>'failed']);
     }
  }

}
