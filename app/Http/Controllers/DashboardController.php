<?php

namespace App\Http\Controllers;

use App\User;
use App\Customer;
use App\Supplier;
use App\Product;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Sale;
use App\Purchase;

class DashboardController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function index()
    {  
        
             // get authenticated user
        $userdata = \Auth::user();
         
        //count total customer
        $customerdata  = Customer::all();

        // Count total suppliers
        $supplier = Supplier::all();

        $products  = Product::all();
        $totalpurchase = Purchase::all()->count();

        $user =  User::all();
        
        $todaydate = date('Y:m:d');
        $totalsales = Sale::where('solddate',$todaydate)->get();

        return view('dashboard.index', compact('userdata', 'customerdata', 'products', 'supplier', 'user','totalsales','totalpurchase'));
    }


public function getDefaultTopSellingItems()
{
    $todaydate = date('Y-m-d');

    $gettopselling = Sale::where("solddate",$todaydate)
                                      ->join("products",'sales.product_id','=','products.id')
                                      ->select(\DB::raw('sum(quantity) as totalquantity,products.productName'))
                                      ->groupBy("product_id")
                                       ->OrderBy("totalquantity","DESC")
                                      ->limit(10)
                                      ->get();

    return response()->json(["data"=>$gettopselling]);

}

public function getTopSellingItems(Request $request)
{
    $gettopselling = Sale::whereBetween("solddate",array($request['startdate'], $request['enddate']))
                                      ->join("products",'sales.product_id','=','products.id')
                                      //->select(\DB::raw('sum(quantity) as totalquantity,products.*,sales.*'))
                                       ->select(\DB::raw('sum(quantity) as totalquantity,products.productName'))
                                       ->groupBy("product_id")
                                      ->OrderBy("totalquantity","DESC")
                                      ->limit(10)
                                      ->get();

    return response()->json(["data"=>$gettopselling]);

}



}