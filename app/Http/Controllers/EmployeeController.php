<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Customer;
use App\Supplier;
use App\Product;
use App\Role;
use App\Permission;
use App\Perm_general;

class EmployeeController extends Controller
{

 public function getUsers()
    {

         $userdata = \Auth::user();
         
         //count total customer
         $customerdata  = Customer::all();

         // Count total products
        $products  = Product::all();

        // count total supplier
        $supplier = Supplier::all();

        // return users
        //$user  = User::all()->roles;
        $user = User::join('role_user','role_user.user_id','=','users.id')->join('roles','role_user.role_id','=','roles.id')->select('users.*','role_user.*','roles.rolename')->get();

        $user3  = User::all();

        // return permission
        $perms = Permission::all();
        //$perms =  Perm_general::join('permissions', 'permissions.permgid','=','perm_generals.permgid')->select('permissions.*','perm_generals.*')->get();

        // return   Roles
        $roles = Role::all();
        $myroles = \DB::table('roles')->get();

       
        
        return view('auth.user', compact('user3','userdata', 'user',  'customerdata', 'products', 'supplier','perms','roles', 'myroles'));
    }




    // Get permissions based on the selected role. 
    public function getPermissionsBasedOnSelectedRole(Request $request){
      $this->validate($request,["selectedroleid"=>'required|integer']);

      $perms = \DB::table('permission_role')
                  ->where('role_id',$request['selectedroleid'])
                  ->join('permissions','permission_role.permission_id','=','permissions.id')
                  ->get();
      //if($perms){
        return response()->json(["data"=>$perms]);
     // }
     // else{
       // return response()->json(["data"=>"error"]);
     // }



      
    }

  // Update  assigned roles
    public function UpdateEditedAssignedRoles(Request $request){
        $this->validate($request, [
           'editedrole_name'=>'required',
        ]);
       //dd($request['editedrole_name']);
        
        // Check if there are any checked permissions
         if($request['editedpermission_name'] != ''){ 

            $editedperms = $request['editedpermission_name'];
            $editedroleid = $request['editedrole_name'];
             // Delete all permissions assigned to role in request to prevent dupblicate
            $deletepermission = \DB::table('permission_role')
                                    ->where('role_id',$editedroleid)
                                    ->delete();
            //dd($deletepermission);
            // if($deletepermission){
            //   return response()->json(["data"=>$deletepermission]);
            // }
          
         // Check if delete is true
          if($deletepermission){
             // Insert to update the permissions
             foreach ($editedperms as $key) {
                $updateroleassigned = \DB::insert("INSERT into permission_role(role_id,permission_id) VALUES($editedroleid, $key)");
                return response()->json(["data"=>"perms_updated"]);
               }
              
             
          }else{
            return response()->json(["data"=>"deletepermissionproblem"]);
          }
         }else{
          return response()->json(["data"=>"nopermission"]);
         }
      
          //$perms = $request->all();
         /* $editedperms = $request['editedpermission_name'];
          $editedroleid = $request['editedrole_name'];
          
        
          foreach ($editedperms as $key) {

            $deleteperms = \DB::table('permission_role')->where()->delete();

            $updateroleassigned = \DB::insert("INSERT into permission_role(role_id,permission_id) VALUES($editedroleid, $key)");
*/

         // }
    }






   public function createUsers(Request $request){

       $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]); 

       //return response()->json(['data' => 'hello']);

       $newuser = User::create([
            'name'     => $request['name'],
            'email'    => $request['email'],
            'password' => bcrypt($request['password']),
            'phone'    =>$request['phone'],
            'address'  =>$request['address'],
            'country'  =>$request['country'],
        ]);
       // if($newuser){
       //  return response();
       // }


   }


    
    /**
     * Create new role
     */
    public function createRole(Request $request)
    {

         //validate the inputs
        $this->validate($request, [
                 'rolename' => 'required',
                // 'description'=>'required',
            ]);

        $role = $request->all();


         Role::create($role);

        // session()->flash('flash_message', 'Role has been added Successfully!');
        // return redirect('employee');
        
    }

    /**
     * create new permission
     */
    public function createPermission(Request $request)
    {

        $this->validate($request, [
                    'permission'=> 'required',
            ]);
       
         
           $perm = $request->all();
           
           return $perm;

    }

   /*
   * Assign Roles to permissions
   */
    public function assignRolePermissions(Request $request)
    {
       $this->validate($request, [
           'role'=>'required',
           'permission'=>'required',
        ]);
      
          //$perms = $request->all();
          $permids = $request['permission'];
          $roleid = $request['role'];
          
         
          foreach ($permids as $key) {
            $newroleperm = \DB::insert("INSERT into permission_role(role_id,permission_id) VALUES($roleid, $key)");
          }

         // if($newroleperm){
         //   return response()->json(['data' => "success"],200);
         //  }else{
         //    return response()->json('error',400);
         //  }

    }


    // Update 

  /*
  * Assign Role to users
  */
    public function assignRoleUsers(Request $request)
    {
       $this->validate($request, [
          'role'=>'required',
          'user'=>'required'
        ]);

       $rolename = $request['role'];
       $username = $request['user'];

       $newuserole = \DB::insert("INSERT INTO role_user(role_id,user_id) VALUES($rolename, $username)");

       if($newuserole){
           return response()->json(['data' => "success"],200);
          }else{
            return response()->json(['data' => "error"],400);
          }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteRole(Request $request)
    {
         $this->validate($request,['roleid'=>'required']);
         $checkid = Role::findOrFail($request['roleid']);

       if(! is_null($checkid)){
        $deleterole = Role::where('id', $request['roleid'])->delete();

        if($deleterole){
          return response()->json(['data' => "success"],200);
        }else{
            return response()->json(['data' => "error"],400);
          }
       }

    }

    /**
     * Show the form for editing roles
     */
    public function editRole(Request $request)
    {
      // Varidate inputs
      $this->validate($request,['roleid'=>'required']);
      // check if exist
       $checkid = Role::findOrFail($request['roleid']);

      if(! is_null($checkid)){
        $role = Role::where('id',$request['roleid'])->get();
        return response()->json(["data"=>$role]);
      }else{
        return response()->json(["data"=>"error"]);
      }

    }

    // Update Edited Role
    public function UpdateEditedRole(Request $request)
    { 
       // Validate inputs
       $this->validate($request, ["roleid"=>"required","rolename"=>"required","descripts"=>"required"]);
       //Check if role exist
       $checkid = Role::findOrFail($request['roleid']);
       if(! is_null($checkid)){
          $updaterole = Role::where("id",$request['roleid'])->update(["rolename"=>$request["rolename"],"description"=>$request["descripts"]]);

          if($updaterole){
            return response()->json(["data"=>"success"],200);
          }else{ return response()->json(["data"=>"error"],400);}
       }else{
        die();
       }

    } 

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $userupdate  = $request->all();
        $id = $userupdate['usrid'];
        $role = $userupdate['role'];

         $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'password' => 'required|min:6',
            'role'   =>'required'
        ]);

        $checkid = User::findOrFail($id);
        //echo $id;
        //die;
        if(! is_null($checkid))
        {
         $updateinfo = User::where('id', $id)->update([
                 'name' => $userupdate['name'],
                 'email' =>$userupdate['email'],
                 'password' =>bcrypt($userupdate['password']),
                 'phone' =>$userupdate['empphonenumber'],
                 'address' =>$userupdate['empaddress'],
                 'country' =>$userupdate['empcountry'],
                 'photo' =>$userupdate['empphoto'],
                 'remember_token' =>$userupdate['_emptoken'],

            ]);

          $updaterole = \DB::update("UPDATE role_user SET role_id=$role WHERE user_id=$id");

          if($updateinfo)
          {
            session()->flash('flash_message', 'User Updated Successfully!');

            return redirect('employee');
          }
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $checkid = User::findOrFail($id);

       if(! is_null($checkid)){
        User::where('id', $id)->delete();
       }
       
       session()->flash('flash_message', 'User Deleted Successfully!');

       return redirect('employee');
    }
}
