<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use App\Supplier;
use App\User;
use App\Customer;
use App\Product;
use App\Carts;
use App\Sale;
use App\Setting;
use App\Stock;
use App\Store;

class SalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
          // get authenticated user
          $userdata = \Auth::user();
          
          $supplier = Supplier::all();
          $suppliers = Supplier::all()->count();

          //count total customer
          $customerdata  = Customer::all();

          $user =  User::all();

          // Return total products
          $products = Product::all();

          // Return all store
          $storez = Store::all();


          // $quicklistitemsold  = \DB::table("sales")
          //                            ->orderBy("id","DESC")
          //                             //->select(\DB::raw('sum(quantity) as soldquantity,sales.*'))
          //                             ->groupBy("soNumber")
                                      
          //                             ->get();

        return view('sales.index', compact('userdata', 'supplier', 'suppliers', 'customerdata', 'products', 'user','storez'));
    }


    /*
      GET All sold receipts
    */
  public function getSoldItemReceipts()
  {

       $quicklistitemsold  = \DB::table("sales")
                                     ->orderBy("id","DESC")
                                      //->select(\DB::raw('sum(quantity) as soldquantity,sales.*'))
                                      ->groupBy("soNumber")
                                      
                                      ->get();
      return response()->json(["data"=>$quicklistitemsold]); 
  }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
      
         $data  = $request->all();
         $sale = $data['mysale'];
         $storname = $data['storename'];

        /* //check if the product aleady exist in the product_cart
         $checkIfExist = Carts::where('productName',$sale)
                                 ->orWhere('productBarcode',$sale)
                                 ->get();
        // if the product is already exist increment the quantity and then return to use                         
        if($checkIfExist->count()){
            // Update the quantity cart for this products only
            foreach ($checkIfExist as $key => $quantityValue) {
                //Increment quantity
               //$newquantity = $quantityValue['productQuantity']+1;
              $newquantity=1;

               // update total price
               $totalprice = $newquantity*$quantityValue['productSellingPrice'];
            }
            $incrementQuantity = Carts::where('productBarcode', $sale)
                                               ->orWhere('productName', $sale)
                                               ->update(['productQuantity'=>$newquantity,'total'=>$totalprice]);
                                               
           if($incrementQuantity){
            
                $resultIncrement = Carts::where('productBarcode', $sale)
                           ->orWhere('productName', $sale)
                           ->get();
                           //print_r($resultIncrement);
                          // die();
            //$resultIncrement = $resultIncrement->add($newquantity);
            //$resultIncrement = array_push($newquantity,$resultIncrement); 
                return response()->json(['data' => $resultIncrement]);

           }else{
            return response()->json(['data' => "no match"]);
           }
            
        }
       // The product does not exist in the cart so fetch it from the product table then return to user and also insert into the products_cart.
        else{

            $result = Product::where('productBarcode', $sale)
                           ->orWhere('productName', $sale)
                           ->get();
            // check if exist in the product table
                    if($result->count()){
                   // Insert product into cart then return
                       foreach ($result as $key => $value) {
                        $tprice = $value['productSellingPrice']*1; 
                         $saveIntoCart = Carts::create([
                            'productName'=>$value['productName'],
                            'productBarcode'=>$value['productBarcode'],
                            'productSellingPrice'=>$value['productSellingPrice'],
                            'productQuantity'=>1,
                            'total'=>$tprice,
                            'productCategory'=>$value['productCategory'],
                            'productBuyingPrice'=>$value['productBuyingPrice']
                            ]);
                       }
                        $res = Carts::where('productBarcode', $sale)
                           ->orWhere('productName', $sale)
                           ->get();
                    return response()->json(['data' => $res]);
                      }
                  if($result->isEmpty()){
                        return response()->json(['data' => "no match"]);
                      }
           
        }*/

          $result = Product::where('productBarcode', $sale)
                           ->orWhere('productName', $sale)
                           ->get();
          if($result->count()){
           // Insert product into cart
            return response()->json(['data' => $result]);
          }
          if($result->isEmpty()){
            return response()->json(['data' => "no match"]);
          }

          
    }


    /**
     * Create new sale.
     */
    public function createNewSale(Request $request)
    {
        $sale = $request->all();
        $userdata = \Auth::user();

           $this->validate($request,[
              'product'=>'required',
              'itemprice'=>'required',
              'quantitysold'=>'required',
              'total'=>'required',
              'saleordernumber'=>'required',
              
          ]);
          
       // before record the sale check if the item exists in products
        $checkitem = Product::where("id",$sale['product'])->get();
        //var_dump($checkitem);
       

         if($checkitem->count()){

          foreach ($checkitem as $key => $value) {
                  $qtyavailabletosell = $value['availableQuantity'];

                  if($qtyavailabletosell > $sale['quantitysold']){

                        // create sale
                        $newsale = new Sale;
                        $newsale->user_id      = $userdata->id;
                        $newsale->customer_id  = $sale['customer'];
                        $newsale->product_id   = $sale['product'];
                        $newsale->unitPrice    = $sale['itemprice'];
                        $newsale->discount     = 0;
                        $newsale->vat          = $sale['vat'];
                        $newsale->quantity     = $sale['quantitysold'];
                        $newsale->total        = $sale['total'];
                        $newsale->soNumber     = $sale['saleordernumber'];
                        $newsale->solddate     = date('Y-m-d');

                        $c  = $newsale->save();

                         if($c){
                          // reduce quantity in stock and product table based on the id product
                          $getqty = Product::where("id",$sale['product'])->get();

                              foreach ($getqty as $key => $value) {
                                $onhandqty = $value['onhandQuantity'];
                                $allocatedqty = $value['allocatedQuantity'];

                                //allocatedquantity sum with quantitysold
                              $newallocatedqty = $allocatedqty + $sale['quantitysold'];

                              // calculate the new available quantity
                              $newavailableqty = $onhandqty - $newallocatedqty;

                            // Update the qtytosell in products table
                              $updateproduct = Product::where("id",$sale['product'])->update(['availableQuantity'=>$newavailableqty,'allocatedQuantity'=>$newallocatedqty]);
                             // return response()->json(["data"=>"salecomplete"]);

                               //update stock table;
                              $updatestock = Stock::where("product_id",$sale['product'])->update(['availableQuantity'=>$newavailableqty,'allocatedQuantity'=>$newallocatedqty]);

                              }

                         }else{
                      //sale could not be created due to database error
                      return response()->json(["data"=>"errorsavesale"]);
                        }


                   }else{

                    //STOCK UNSUFFICIENT, GIVE A WARNING BUT CONTINUE TO SALE
                    // create sale
                        $newsale = new Sale;
                        $newsale->user_id      = $userdata->id;
                        $newsale->customer_id  = $sale['customer'];
                        $newsale->product_id   = $sale['product'];
                        $newsale->unitPrice    = $sale['itemprice'];
                        $newsale->discount     = 0;
                        $newsale->vat          = $sale['vat'];
                        $newsale->quantity     = $sale['quantitysold'];
                        $newsale->total        = $sale['total'];
                        $newsale->soNumber     = $sale['saleordernumber'];
                        $newsale->solddate     = date('Y-m-d');

                        $c  = $newsale->save();

                         if($c){
                          // reduce quantity in stock and product table based on the id product
                          $getqty = Product::where("id",$sale['product'])->get();

                              foreach ($getqty as $key => $value) {
                                $onhandqty = $value['onhandQuantity'];
                                $allocatedqty = $value['allocatedQuantity'];

                                //allocatedquantity sum with quantitysold
                              $newallocatedqty = $allocatedqty + $sale['quantitysold'];

                              // calculate the new available quantity
                              $newavailableqty = $onhandqty - $newallocatedqty;

                            // Update the qtytosell in products table
                              $updateproduct = Product::where("id",$sale['product'])->update(['availableQuantity'=>$newavailableqty,'allocatedQuantity'=>$newallocatedqty]);
                             // return response()->json(["data"=>"salecomplete"]);

                               //update stock table;
                              $updatestock = Stock::where("product_id",$sale['product'])->update(['availableQuantity'=>$newavailableqty,'allocatedQuantity'=>$newallocatedqty]);

                              }

                         }else{
                      //sale could not be created due to database error
                      return response()->json(["data"=>"errorsavesale"]);
                        }
                  return response()->json(["data"=>"enteredqtyisgrt"]);
                 }//end if quantity is great available is greater than to sell
         } //end foreach
         return response()->json(["data"=>"salecomplete"]);


         }else{
           
            //item does not exist in product table
            return response()->json(["data"=>"itemnotfound"]);
        }

        
    }




   // print receipt after sale
    public function printReceipt($sordernumber)
    {
          //echo $saleitemid;
         // get authenticated user
          $userdata = \Auth::user();
          
          $supplier = Supplier::all();
          $suppliers = Supplier::all()->count();

          //count total customer
          $customerdata  = Customer::all();

          $user =  User::all();

          // Return total products
          $products = Product::all();
           
           //sale order number
          $snumber = $sordernumber;
          // company details
          $companydetails = Setting::all();

          // get receipt according based on entered sale order number
          $solddate = date('Y:m:d');
          $saleorders = \DB::table("sales")->where("soNumber",$sordernumber)->where("solddate",$solddate)->join("products",'sales.product_id','=','products.id')->select('products.productName','sales.quantity','sales.unitPrice','sales.total')->get();
          
          // Get payment due
          $paymentdue = \DB::table("sales")->where("soNumber",$sordernumber)->select('created_at','vat')->LIMIT(1)->get();

           $vat = \DB::table("sales")->where("soNumber",$sordernumber)->select('vat')->LIMIT(1)->get();

      return view(
             'sales.receipt', 
             compact(
                   'userdata', 
                   'supplier',
                   'suppliers',
                   'customerdata', 
                   'products',
                   'user',
                   'saleorders',
                   'snumber',
                   'companydetails',
                   'paymentdue',
                   'vat'
                   )
             );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function customerAutocomplete(Request $request)
    {

  $term = $request->get('w');
    
    // $return_array = array();

    // $customers = Customer::where('customerName','LIKE', '%'.$term.'%')
    //                       ->orWhere('customerCompanyName','LIKE', '%'.$term.'%')
    //                       ->get();


    //        foreach ($customers as $key) {
    //          $return_array[] = ['id'=>$key->id,'value'=>$key->customerName];
    //        }
           return response()->json($term);

    }
}
