<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Customer;
use App\Supplier;
use App\Product;
use App\Store;
use App\Stock;
use App\Purchase;
use App\Setting;

class PurchasesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userdata = \Auth::user();
         
         //count total customer
         $customerdata  = Customer::all();

         // Count total products
        $products  = Product::all();


        // count total supplier
        $supplier = Supplier::all();
        $supp = Supplier::all();

        // return users
        $user  = User::all();

        //return all store locations
        $stores  = Store::all();
        
        
        // return all purchases orders
        $purchases = \DB::table("purchases")->join('suppliers','purchases.vendorId','=','suppliers.id')
                                            ->join('purchaseItemsDetails','purchases.purchId','=','purchaseItemsDetails.purchasesId')
                                            ->join('stores','purchases.destinationLocation','=','stores.id')
                                            ->select(\DB::raw('sum(purchaseItemsDetails.itemQuantity) as sumorderq, sum(itemQuantity *  itemPrice) as totalcost,purchases.*,suppliers.*,stores.*'))
                                            ->groupBy("poNumber")
                                            ->orderBy('purchases.purchid','desc')
                                            ->get();

        // return company settings
        $settings = Setting::all();
        

        
        return view('purchases.index', compact('userdata', 'user', 'customerdata', 'products', 'supplier','supp','stores','purchases','settings'));
    }


   /**
     * Stock paymennt ovucher
     */
    public function purchasePaymentVoucher($poNumber)
    {
           // get authenticated user
          $userdata = \Auth::user();
          
          $supplier = Supplier::all();
          $suppliers = Supplier::all()->count();

          //count total customer
          $customerdata  = Customer::all();

          $user =  User::all();

          // Return total products
          $products = Product::all();
          $settings = Setting::all();
         //return all store locations
          $stores  = Store::all();

          $thispNumber = $poNumber;
          
       
         //get purchase details
         $purchasedetails = \DB::table("purchases")->where('poNumber',$poNumber)
                                                ->join('suppliers','purchases.vendorId','=','suppliers.id')
                                                  ->join('purchaseItemsDetails','purchases.purchId','=','purchaseItemsDetails.purchasesId')
                                                  ->join('products','purchaseItemsDetails.itemId','=','products.id')
                                                  ->join('stores','purchases.destinationLocation','=','stores.id')
                                                  
                                               // ->select(\DB::raw('sum(orderquantity) as sumorderq,purchases.*,suppliers.*,stores.*,products.*'))
                                               // ->groupBy("poNumber")
                                               ->get();

     
          $purchaseSharedDetails = \DB::table("purchases")->where("poNumber",$poNumber)
                                                          ->join('suppliers','purchases.vendorId','=','suppliers.id')
                                                          ->join('stores','purchases.destinationLocation','=','stores.id')
                                                          ->LIMIT(1)->get();


    


       return view('purchases.voucher',compact('purchaseSharedDetails','thispNumber','userdata','settings','suppliers','supplier','customerdata','user','products','purchasedetails'));
    }


    /**
     * Delete single purchase.
     */
    public function purchaseDelete(Request $request)
    {
        $purchId = $request['delpurch'];
        $deletepurchase = Purchase::where('purchId',$purchId)->delete();

        if($deletepurchase){
            return response(['msg'=>'Purchase deleted', 'status'=>'success']);
          }else{
          return response(['msg'=>'Failed deleting the purchase', 'status'=>'failed']);
          }
    }

    /*
    * Delete multiple purchase
    */
    public function purchaseDeleteMultiple(Request $request)
    {
        $this->validate($request, [
                       'checkedpurchase' => 'required',
                  ]);
         $data = $request->all();

        //var_dump($data['checkedproduct']);

          foreach($data['checkedpurchase'] as $id){
          $delp = Purchase::where('purchId', $id)->delete();
          }

          if($delp){
            return response(['msg'=>'Purchase(s) deleted', 'status'=>'success']);
          }else{
            return response(['msg'=>'Failed deleting the purchase', 'status'=>'failed']);
          }
      }




public function receiveViewPrintPurchaseOrders($poNumber)
    {
         
         

         //echo $saleitemid;
         // get authenticated user
          $userdata = \Auth::user();
          
          $supplier = Supplier::all();
          $suppliers = Supplier::all()->count();

          //count total customer
          $customerdata  = Customer::all();

          $user =  User::all();

          // Return total products
          $products = Product::all();
          $settings = Setting::all();
         //return all store locations
          $stores  = Store::all();

          $thispNumber = $poNumber;
       
        //get purchase details
        $purchasedetails = \DB::table("purchases")->join('suppliers','purchases.vendorId','=','suppliers.id')
                                                  ->join('purchaseItemsDetails','purchases.purchId','=','purchaseItemsDetails.purchasesId')
                                                  ->join('products','products.id','=','purchaseItemsDetails.itemId')
                                                  ->join('stores','purchases.destinationLocation','=','stores.id')
                                                  ->where('poNumber',$poNumber)
                                               // ->select(\DB::raw('sum(orderquantity) as sumorderq,purchases.*,suppliers.*,stores.*,products.*'))
                                               // ->groupBy("poNumber")
                                               ->get();

     
          $purchaseSharedDetails = \DB::table("purchases")->where("poNumber",$poNumber)
                                                          ->join('suppliers','purchases.vendorId','=','suppliers.id')
                                                          ->join('stores','purchases.destinationLocation','=','stores.id')
                                                          ->LIMIT(1)->get();

          // get receive details if exists
           $itemreceivedetails = \DB::table('goodsReceiveNote')->where("poNumber",$poNumber)
                                                           ->join('purchases','goodsReceiveNote.purchaseorderid','=','purchases.purchId')
                                                         
                                                            ->get();
          $paymentmethods = \DB::table('payments_methods')->get();
           
         return view('purchases.purchasedetails', compact('purchaseSharedDetails','thispNumber','userdata','settings','suppliers','supplier','customerdata','user','products','purchasedetails','itemreceivedetails','paymentmethods'));
        
    }




    // update purchase, increment the product stock according to product id and store location from purchase order table
    public function receivePurchaseOrders(Request $request)
    {
      
        $delpurchase = $request->all();
        $userdata = \Auth::user();

      
        
       //check if ordernumber exist
        $sumOrderquantity =\DB::table('purchases')
                             ->where('poNumber', $request['poNumber'])
                             ->join('purchaseItemsDetails','purchases.purchId','=','purchaseItemsDetails.purchasesId')
                             ->select(\DB::raw('sum(itemQuantity) as sumorderq,purchases.deliveryDate'))
                            ->get();

         if($sumOrderquantity)
         {

           foreach ($sumOrderquantity as $key => $va) {
            $totalqty = $va->sumorderq;
            $deriverydate = $va->deliveryDate;
           }

              //check if entered item quantity is equal to what is in the purchase number
               if($totalqty != $request['deliveredquantity'])
               {
                  // Return error entered order quantity does not match with the total ordered quantity in the purchase
                   return response()->json(["data"=>"qtyerror"]);
               }
               elseif ($deriverydate != $request['delivereddate']) {
                 // Return error, entered delivery date does not match with expected delivery in the purchase
                return response()->json(["data"=>"deliverydatenotmatch"]);
               }else{
                // everything matches in the purchase
                 $checkifOrderexistPerItem = \DB::table('purchases')->where('poNumber', $request['poNumber'])->join('purchaseItemsDetails','purchases.purchId','=','purchaseItemsDetails.purchasesId')->get();

                   foreach ($checkifOrderexistPerItem as $key => $itemvalue) {
                                  $product  = $itemvalue->itemId;
                                  //$deriverydate = $value->expectedDelivery;
                                  $purchid   = $itemvalue->purchId;

                                  //Create gnr and return the id
                                  $creategrn = \DB::table('goodsReceiveNote')->insertGetId(array('purchaseorderid'=>$purchid,'processedBy'=> $userdata->id));

                                  // update  system  delivery note informations
                                  $delivery = \DB::table('deliveryNoteDetails')->insert(
                                                         array(
                                                        'deliveryNumber'=> $request['deliverynotenumber'],
                                                        'grnNumber'=>$creategrn,
                                                        //'purchaseNumber'=>$request['poNumber'],
                                                        'deliveredQty'=>$request['deliveredquantity'],
                                                        'deliveredDamagedQty'=>$request['delivereddamaged'],
                                                        'deliveredby'=> $request['deliveredby'],
                                                        'delivererContact'=>$request['deliverercontact'],
                                                        'checkedBy'=>$request['deliverycheckedby'],
                                                        'deliveryNoteDate'=>$request['delivereddate']
                                                          ));


                                           
                                              
                                            // get products to update in the inventory
                                            $productToUpate = Product::where("id",$itemvalue->itemId)->get();
                                            
                                            //print_r($productToUpate);
                                           foreach($productToUpate as $key=>$myval)
                                           {
                                                 $quantityinproduct =  $myval->onhandQuantity;
                                                 $quantitytosell    =  $myval->availableQuantity;


                                                //increment stock
                                                  $newquantityinproduct = $quantityinproduct + $itemvalue->itemQuantity;
                                                  $newquantitytosell    = $quantitytosell + $itemvalue->itemQuantity;

                                                  // update the quantitytoSell and productQuantity in product table
                                                  $updateQuantityInproducts = Product::where("id",$itemvalue->itemId)
                                                                                      ->where("store_id",$itemvalue->destinationLocation)
                                                                                      ->update(['onhandQuantity'=>$newquantityinproduct,'availableQuantity'=>$newquantitytosell]);
                                                 
                      

                                                 //update also stock table;
                                                  $updatestock = Stock::where("product_id",$itemvalue->itemId )->update(['onhandQuantity'=>$newquantityinproduct,'availableQuantity'=>$newquantitytosell]);

                                                  // update set the Itemstatus = 1 so that to show that the item has been received and waiting for payment
                                                  $updatestockstatus = Purchase::where("poNumber",$itemvalue->poNumber )
                                                                                 ->where("destinationLocation",$itemvalue->destinationLocation)
                                                                                 ->update(['purchaseStatus'=>2]);
                                                 
                                             
                                           }
                                   // $p = $request['poNumber'];
                                    //return redirect('purchase/purchase/'.$p);
                                        
                                    // return response()->json(["data"=>"deliverycomplete"]);
                                               

                                          // echo $newquantityinproduct = $quantityinproduct + $itemvalue->orderquantity;
                                                  //$newquantitytosell    = $quantitytosell + $itemvalue->orderquantity;
                   }
                   return response()->json(["data"=>"deliverycomplete"]);
               }

         }else{
          //purchase order number does not exist
          return response()->json(['data'=>'purchaseordernumbernotexist']);
         }

    }




    /**
     * create new purchase order items details.
     */
     public function postPurchaseitemsDetails(Request $request)
    {
       $inpurchase = $request;
       $this->validate($request,[
                     'itemid'=>'required',
                     'orderqty'=>'required',
                     'itemtotalcost'=>'required',
              ]);

       // get the last inserted id in purchase table
       $purchaselastId = \DB::table('purchases')->max('purchId');
      
       // insert purchase items  details into purchaseItemsDetails
        $insertpurchaseItemDetails = \DB::table('purchaseItemsDetails')->insert(
                             array(
                                    'itemId'      =>$inpurchase['itemid'],
                                    'itemQuantity'=>$inpurchase['orderqty'],
                                    'itemPrice'   =>$inpurchase['itemcost'],
                                    'purchasevat' =>$inpurchase['purchaseitemvat'],
                                    'purchasesId' =>$purchaselastId
                            
                              ));

      if($insertpurchaseItemDetails)
      {
        return response()->json(["data"=>"success"]);
      }else{
        return response()->json(["data"=>"errorinsertdetails"]);
      }

    }


    /**
     * edit created new purchase order items details.
     */
     public function editpostedPurchaseitemsDetails(Request $request)
    {

       
       $this->validate($request,[
                     'edititemid'=>'required',
                     'editorderqty'=>'required',
                     'edititemtotalcost'=>'required',
                     'currentupdatedpurchaseId'=>'required',
              ]);
         $itemId = $request['edititemid'];
         $purchIsd = $request['currentupdatedpurchaseId'];
       
        $checkexistance = \DB::table('purchaseItemsDetails')->where('purchasesId',$purchIsd)->where('itemId', $itemId)->get();
        if(count($checkexistance))
        {
             
             $updatedet = \DB::table('purchaseItemsDetails')->where('purchasesId',$purchIsd)
                                                                  ->where('itemId', $itemId)
                                                                  ->update([
                                                                  //'itemId'      =>$request['edititemid'],
                                                                  'itemQuantity'=>$request['editorderqty'],
                                                                  'itemPrice'   =>$request['edititemcost'],
                                                                  'purchasevat' =>$request['editpurchaseitemvat'],
                                                                  ]);
       

                  if($updatedet)
                  {
                    return response()->json(["data"=>"success"]);
                  }else{
                    return response()->json(["data"=>"errorinsertdetails"]);
                  }


        }else{

          // item not exist in this order number, so add it
            $updatedetsetnew = \DB::table('purchaseItemsDetails')->insert(
                             array(
                                    'itemId'      =>$request['edititemid'],
                                    'itemQuantity'=>$request['editorderqty'],
                                    'itemPrice'   =>$request['edititemcost'],
                                    'purchasevat' =>$request['editpurchaseitemvat'],
                                    'purchasesId' =>$purchIsd
                            
                              ));
       

                  if($updatedetsetnew)
                  {
                    return response()->json(["data"=>"success"]);
                  }else{
                    return response()->json(["data"=>"errorinsertdetails"]);
                  }

        }
                    

     
                  



   

      

    }




    /**
     * create new purchase order.
     */
    public function postPurchaseDetails(Request $request)
    {

        $inpurchase = $request;
        $userdata = \Auth::user();

         $this->validate($request,[
                     'ordernumber'=>'required',
                     'supplierId'=>'required',
                     'storedestination'=>'required',
                     //'itemtotalcost'=>'required',
                     'stockdue'=>'required',
                     'expectedDelivery'=>'required'
              ]);
 

        $newpurchase = new Purchase;

        $newpurchase->poNumber = $inpurchase['ordernumber'];
        //$newpurchase->product_id = $inpurchase['itemid'];
        $newpurchase->vendorId = $inpurchase['supplierId'];
        $newpurchase->destinationLocation = $inpurchase['storedestination'];
        $newpurchase->deliveryDate = $inpurchase['expectedDelivery'];
        //$newpurchase->orderquantity = $inpurchase['orderqty'];
        //$newpurchase->itemcost = $inpurchase['itemcost'];
        //$newpurchase->purchaseTax = $inpurchase['purchaseitemvat'];
       //$newpurchase->purchaseTotalCost  = $inpurchase['itemtotalcost']; 
        $newpurchase->purchaseDueDate = $inpurchase['stockdue'];
        $newpurchase->messageToVendor = $inpurchase['suppliermessage'];
        $newpurchase->purchaseStatus = 0;
        $newpurchase->createdBy  = $userdata->id;

        $createpo = $newpurchase->save();

        

      
        

        // if($purchaselastId){
        //     return response()->json(["data"=>$purchaselastId]);
        // }else{
        //     return response()->json(["data"=>"error"]);
        // }

    }



    /**
     * Edit created new purchase order.
     */
    public function editpostedPurchaseDetails(Request $request)
    {

        $updateinpurchase = $request;
        $userdata = \Auth::user();

         $this->validate($request,[
                     'editordernumber'=>'required',
                     'editsupplierId'=>'required',
                     'editstoredestination'=>'required',
                     //'itemtotalcost'=>'required',
                     'editstockdue'=>'required',
                     'editexpectedDelivery'=>'required'
              ]);
     

       //  $newpurchase = new Purchase;

       //  $newpurchase->poNumber = $inpurchase['editordernumber'];
       //  //$newpurchase->product_id = $inpurchase['itemid'];
       //  $newpurchase->vendorId = $inpurchase['editsupplierId'];
       //  $newpurchase->destinationLocation = $inpurchase['editstoredestination'];
       //  $newpurchase->deliveryDate = $inpurchase['editexpectedDelivery'];
       //  //$newpurchase->orderquantity = $inpurchase['orderqty'];
       //  //$newpurchase->itemcost = $inpurchase['itemcost'];
       //  //$newpurchase->purchaseTax = $inpurchase['purchaseitemvat'];
       // //$newpurchase->purchaseTotalCost  = $inpurchase['itemtotalcost']; 
       //  $newpurchase->purchaseDueDate = $inpurchase['editstockdue'];
       //  $newpurchase->messageToVendor = $inpurchase['editsuppliermessage'];
       //  $newpurchase->purchaseStatus = 0;
       //  $newpurchase->createdBy  = $userdata->id;

       //  $createpo = $newpurchase->update();

        $updatepurchae = Purchase::where('poNumber',$updateinpurchase['editordernumber'])
                                   ->update([
                                     'vendorId'=>$updateinpurchase['editsupplierId'],
                                     'destinationLocation'=>$updateinpurchase['editstoredestination'],
                                     'deliveryDate'=>$updateinpurchase['editexpectedDelivery'],
                                     'purchaseDueDate' => $updateinpurchase['editstockdue'],
                                     'messageToVendor' => $updateinpurchase['editsuppliermessage'],
                                     'createdBy'  => $userdata->id
                                     ]);

        // if($updatepurchae)
        // {
        //   echo "we are ok";
        // }

      
        

        // if($purchaselastId){
        //     return response()->json(["data"=>$purchaselastId]);
        // }else{
        //     return response()->json(["data"=>"error"]);
        // }

    }




    /*******************
    * GET PURCHASE TO EDIT USING MODAL
    ***********************
    */
    public function getPurchaseEditModal(Request $request)
    {
       $this->validate($request,['purchaseid'=>'required']);
       $purchid = $request->purchaseid;
       // $purchaseinfo = \DB::table("purchases")->where('purchId',$purchid )
       //                                        ->join('suppliers','purchases.vendorId','=','suppliers.id')
       //                                        ->join('purchaseItemsDetails','purchases.purchId','=','purchaseItemsDetails.purchasesId')
       //                                        ->join('products','purchaseItemsDetails.itemId','=','products.id')
       //                                        ->join('stores','purchases.destinationLocation','=','stores.id')
       //                                        ->select(\DB::raw('sum(purchaseItemsDetails.itemQuantity) as sumorderq, sum(itemQuantity *  itemPrice) as totalcost,purchases.*,suppliers.*,stores.*,products.*'))
       //                                        ->groupBy("poNumber")
       //                                        ->orderBy('purchases.purchid','desc')
       //                                        ->get();
        $purchaseinfo = \DB::table('purchaseItemsDetails')->where('purchasesId', $purchid)
         ->join('purchases','purchases.purchId','=','purchaseItemsDetails.purchasesId')
        // ->join('suppliers','purchases.vendorId','=','suppliers.id')
         ->join('products','purchaseItemsDetails.itemId','=','products.id')
        // ->join('stores','purchases.destinationLocation','=','stores.id')
        // ->select(\DB::raw('sum(purchaseItemsDetails.itemQuantity) as sumorderq, sum(itemQuantity *  itemPrice) as totalcost,purchases.*,suppliers.*,stores.*,products.*'))
        //                                       ->groupBy("poNumber")
                                            
                                            ->get();


        return response()->json(["data"=>$purchaseinfo]); 
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getSupplierInfo(Request $request)
    {
        $suppid = $request->supplierId;
        $suppdetails = Supplier::where('id',$suppid)->get();
        
        return response()->json(['data' => $suppdetails],200);
    }



    public function getItemsAutocomplete(Request $request)
    {
          $data = $request->all();
          $item = $data['searchedItem'];

          $checkIfExist = Product::where('productName',$item)
                                 ->orWhere('productBarcode',$item)
                                 ->get();
                                 
           if($checkIfExist->count()){
           // Insert product into cart
            return response()->json(['data' => $checkIfExist]);
          }
          if($checkIfExist->isEmpty()){
            return response()->json(['data' => "no match"]);
          }
    }



    // return all purchases orders
    public function ajaxgetPurchaseOrders()
    {
      
       
        $purchases = \DB::table("purchases")->join('suppliers','purchases.vendorId','=','suppliers.id')
                                             ->join('purchaseItemsDetails','purchases.purchId','=','purchaseItemsDetails.purchasesId')
                                             ->join('stores','purchases.destinationLocation','=','stores.id')
                                             ->select(\DB::raw('sum(purchaseItemsDetails.itemQuantity) as sumorderq, sum(itemQuantity *  itemPrice) as totalcost,purchases.*,suppliers.*,stores.*'))
                                             ->groupBy("poNumber")
                                            ->orderBy('purchases.purchid','desc')
                                            ->get();

             return response()->json(["data"=>$purchases]);                                

    }


}
