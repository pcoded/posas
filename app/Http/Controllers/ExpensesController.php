<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Customer;
use App\Product;
use App\User;
use App\Supplier;
use App\ChartsOfAccount;
use App\Expense;
use App\ValueAddedTaxe;

class ExpensesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
              // get authenticated user
          $userdata = \Auth::user();
          
          $supplier = Supplier::all();

          $supplier2 = Supplier::all();

          ///$suppliers = Supplier::all()->count();

          //count total customer
         $customerdata  = Customer::all();

         $user =  User::all();

          // Return total products
          $products = Product::all();

          // return list of account category
          $cataccouting = \DB::table('accountTypes')->where('accName',"Expense accounts")->get();

          // charts of account where account type is expense account
          $chartsofacc = ChartsOfAccount::where("accountTypeId",5)->whereNotIn('accountName', array("Cost of Goods Sold"))->get();

        // return method of payments
        $paymentmethods = \DB::table('payments_methods')->limit(1)->get();
        $vat = ValueAddedTaxe::all();
          
         return view('expenses.index', compact('userdata','vat','cataccouting','paymentmethods','supplier','supplier2', 'customerdata', 'products', 'user','chartsofacc'));
    }

    /**
    * Return list of expenses via ajax
    */
    public function getListOfExpenses()
    {
        $userdata = \Auth::user();

        $expenses = Expense::where('expenseCreatedby',$userdata->id)->get();
        return response()->json(["data"=>$expenses]);
    }

    /**
     * Create new expense
     */
    public function createnewExpense(Request $request)
    {   
        $userdata = \Auth::user();

        $this->validate($request,[
               'datecreated'=>'required',
               'methodname'=>'required',
               'expenseaccount'=>'required',
               'expenseamount'=>'required',
               'expedescripts'=>'required',
            ]);
        
        // calculate vat
        $postedvat = $request['expensevat'];
        $vatinpercent = $postedvat/100;
        $postedamount = $request['expenseamount'];



         $newvat = $postedamount + ($postedamount * $vatinpercent);
        
        
        $newexpense  = new Expense;
        $newexpense->expensedatecreated = $request['datecreated'];
        $newexpense->paymentmethod = $request['methodname'];
        $newexpense->expensechartaccountId = $request['expenseaccount'];
        $newexpense->amount = $request['expenseamount'];
        $newexpense->expensedescriptions = $request['expedescripts'];
        $newexpense->expenseReceiptNumber = $request['receiptnumber'];
        $newexpense->expenseCreatedby = $userdata->id;
        $newexpense->expenseStatus = 0;
        $newexpense->expensevat  = $newvat;

        $saveexpense = $newexpense->save();

         if($saveexpense){
                  return response(['msg'=>'Expense recorded, waiting for approval', 'status'=>'success']);
                  }else{
                  return response(['msg'=>'Failed to record expense', 'status'=>'failed']);
                  }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
