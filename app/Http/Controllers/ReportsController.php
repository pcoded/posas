<?php

namespace App\Http\Controllers;

use App\User;
use App\Customer;
use App\Supplier;
use App\Product;
use App\Sale;
use App\Purchase;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Setting;

class ReportsController extends Controller
{
    // private $sales;
    // public function __construct($gpreport)
    // {
    //   $this->sales = $gpreport;
    // }
    public function index()
    {

         $userdata = \Auth::user();
         
         //count total customer
         $customerdata  = Customer::all();

         // Count total products
        $products  = Product::all();

        // count total supplier
        $supplier = Supplier::all();

        // return users
        $user  = User::all();

        //count total sales
        $totalsales  = Sale::all()->count();
        //count total purchase
        $totalpurchase = Purchase::all()->count();
        //count total customers
        //count total items
        $reportsales='';

        $companydetails = Setting::select('companyName')->get();

        // if($sales)
        // {
            return view('reports.index', compact('userdata', 'user',  'customerdata', 'products', 'supplier','totalsales','totalpurchase','reportsales',''));

        // }else{

        //     return view('reports.index', compact('userdata', 'user',  'customerdata', 'products', 'supplier','totalsales','totalpurchase'));
        // }
        
    }

    /*
     ****************************************************
     * Get general sales profit report between dates
     ****************************************************
     */
    public function getGeneralProfitReport(Request $request)
    {

        $this->validate($request, [
                 'from' => 'required',
                 'to' => 'required',
            ]);

         $userdata = \Auth::user();
         
         //count total customer
         $customerdata  = Customer::all();

         // Count total products
        $products  = Product::all();

        // count total supplier
        $supplier = Supplier::all();

        // return users
        $user  = User::all();

        //count total sales
        $totalsales  = Sale::all()->count();
        //count total purchase
        $totalpurchase = Purchase::all()->count();


         $from = $request['from'];
         $to =  $request['to'];
         $filter = $request['filter'];

         $reportsales  = \DB::table("sales")->whereBetween("solddate",array($from, $to))
                                     // ->where("solddate","<=",$to)
                                      ->join("products",'sales.product_id','=','products.id')
                                      ->select(\DB::raw('sum(quantity) as totalquantity,sum(total) as totalcost,products.*,sales.*'))
                                      ->groupBy("product_id")
                                      ->get();

         if($reportsales)
         {
            return response()->json(["data"=>$reportsales]);
         }
         else{
            return response()->json(["data"=>"no match"]);
         }
        //return view('reports.index', compact('userdata', 'user',  'customerdata', 'products', 'supplier','totalsales','totalpurchase','reportsales'));
    }

    /**
     * Return view finance reports
     */
    public function financeReports()
    {


         $userdata = \Auth::user();
         
         //count total customer
         $customerdata  = Customer::all();

         // Count total products
        $products  = Product::all();

        // count total supplier
        $supplier = Supplier::all();

        // return users
        $user  = User::all();

        //count total sales
        $totalsales  = Sale::all()->count();
        //count total purchase
        $totalpurchase = Purchase::all()->count();
        //count total customers
        //count total items
        $reportsales='';
         $companydetails = Setting::select('companyName')->get();

        return view('reports/finance/index', compact('userdata', 'user',  'customerdata', 'products', 'supplier','totalsales','totalpurchase','reportsales','companydetails'));
    }

    /**
     * Income statement report
     */
    public function incomeStatmentReport(Request $request)
    {
        //$this->validate($request,['incomedatefrom'=>'required','incomedateto'=>'requiredd']);
        /*$incomedata = \DB::table('general_ledgers')
                      ->whereBetween("gdate",array($request['incomedatefrom'], $request['incomedateto']))
                      ->whereNotIn('transactionId', array(59, 60, 61,62,63,))
                      ->join('charts_of_accounts','general_ledgers.transactionId','=','charts_of_accounts.chartaccId')
                      ->join('accountTypes','charts_of_accounts.accountTypeId','=','accountTypes.acctypeId')
                      ->select(\DB::raw('sum(glbalance) as totalbalances,general_ledgers.transactionId,charts_of_accounts.accountName'))
                      ->groupBy('transactionId')
                      ->get();*/
         
         $incomeArray = array();

  // TOTAL SALES
         $incomedata = \DB::table('general_ledgers')
                      ->whereBetween("gdate",array($request['incomedatefrom'], $request['incomedateto']))
                      ->select(\DB::raw('sum(gltotalcredit) as salesbalance,general_ledgers.transactionId'))
                      ->whereIn("transactionId",array(67))
                     // ->whereNotIn('transactionId', array(59, 60, 61,62,63,))
                      ->get();

        foreach ($incomedata as $key => $value) {
            $salestotal = array();
            $salestotal['totalsales'] = $value->salesbalance;

            array_push($incomeArray, $salestotal);
        }

    /// COst OF GOODS SOLD
            $cogsdata = \DB::table('general_ledgers')
                      ->whereBetween("gdate",array($request['incomedatefrom'], $request['incomedateto']))
                      ->select(\DB::raw('sum(gltotaldebit) as cogsbalance,general_ledgers.transactionId'))
                      ->whereIn("transactionId",array(72))
                      //->whereNotIn('transactionId', array(59, 60, 61,62,63,))
                      ->get();

        foreach ($cogsdata as $key => $value) {
            $cogstotal = array();
            $cogstotal['totalcogs'] = $value->cogsbalance;

            array_push($incomeArray, $cogstotal);
        }

 
 // TOTAL EXPENSES

                $expensesdata = \DB::table('general_ledgers')
                      ->whereBetween("gdate",array($request['incomedatefrom'], $request['incomedateto']))
                      ->select(\DB::raw('sum(gltotaldebit) as expensesbalance,general_ledgers.transactionId'))
                      ->whereIn("transactionId",array(71,70,69,68,66,65,64))
                      //->whereNotIn('transactionId', array(59, 60, 61,62,63,))
                      ->get();

        foreach ($expensesdata as $key => $value) {
            $expensestotal = array();
            $expensestotal['totalexpenses'] = $value->expensesbalance;

            array_push($incomeArray, $expensestotal);
        }


        return response()->json(["data"=>$incomeArray]);
    }

    /**
     * query balance sheet report
     */
    public function balanceSheetReport(Request $request)
    {
        $this->validate($request,[
                  'balanceopeningdate'=>'required',
                  'balanceclosingdate'=>'required'
            ]);
        $openingdate = $request['balanceopeningdate'];
        $closingdate = $request['balanceclosingdate'];

        
    }

    /**
     * Sales Summary Report by Date Range
     */
    public function salesSummaryReport(Request $request)
    {
       // $salesArray = array();


         $summary = \DB::table('sales')
                      ->whereBetween("solddate",array($request['fromdate'], $request['todate']))
                      ->select(\DB::raw('sum(total) as salestotal,sales.solddate'))
                      ->groupBy('solddate')
                      //->orderBy('salestotal','DESC')
                      ->get();

        // foreach ($summary as $key => $value) {
        //     $salestotalsummary = array();
        //     $salestotalsummary['salestotal'] = $value->salestotal;

        //     array_push($salesArray, $salestotalsummary);
        // }

       return response()->json(["data"=>$summary]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
