<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Customer;
use App\Supplier;
use App\Product;
use App\Store;
use App\Stock;
use App\Purchase;
use App\Setting;

class StockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userdata = \Auth::user();
         
         //count total customer
         $customerdata  = Customer::all();

         // Count total products
        $products  = Product::all();

        // count total supplier
        $supplier = Supplier::all();

        // return users
        $user  = User::all();

        //return all store locations
        $stores  = Store::all();
        
        // return all purchases orders
        $purchases = \DB::table("purchases")->join('suppliers','purchases.vendorId','=','suppliers.id')
                                             ->join('purchaseItemsDetails','purchases.purchId','=','purchaseItemsDetails.purchasesId')
                                             ->join('stores','purchases.destinationLocation','=','stores.id')
                                             ->select(\DB::raw('sum(purchaseItemsDetails.itemQuantity) as sumorderq, sum(itemQuantity *  itemPrice) as totalcost,purchases.*,suppliers.*,stores.*'))
                                             ->groupBy("poNumber")
                                            ->orderBy('purchases.purchid','desc')
                                            ->get();

        // return company settings
        $settings = Setting::all();
        //$purchases = Purchase::all();

        return view('stock_inventory.index', compact('userdata', 'user',  'customerdata', 'products', 'supplier','stores','purchases','settings'));
    }





    // Get items to transfer ajax request
    public function getItemstoTransfer(Request $request)
    {
        
          $data = $request->all();
          $item = $data['searchedItem'];
          $fromstore = $data['storefrom'];
          $storeto = $data['storeto'];

          $checkIfExist = Product::where('productName',$item)
                                  ->where('store_id', $fromstore)
                                 ->orWhere('productBarcode',$item)
                                 ->get();
                                 
           if($checkIfExist->count()){
           // Insert product into cart
            return response()->json(['data' => $checkIfExist]);
          }
          if($checkIfExist->isEmpty()){
            return response()->json(['data' => "no match"]);
          }

    }



 
// Create inventory to transfer
    public function createItemstoTransfer(Request $request)
    {
        // validate
        $this->validate($request,[
                  'storesource'=>'required',
                  'storedestination'=>'required',
                  'transferquantity'=>'required',
                  'transferdate'=>'required',
                  'transferitemid'=>'required'
            ]);

       $userdata = \Auth::user();
       $createtransfer = \DB::table('stockTransfers')->insert(array(
             'itemtoTranfer'=>$request['transferitemid'],
             'itemtransferQantity'=>$request['transferquantity'],
             'transferCreatedBy'=>$userdata->id,
             'transferSource'=>$request['storesource'],
             'transferDestination'=>$request['storedestination'],
             'transferdate'=>$request['transferdate'],

        ));
    

    if($createtransfer)
      {
        return response()->json(["data"=>"success"]);
      }else{
        return response()->json(["data"=>"errorinsertdetails"]);
      }











    

    }






    
    




    




    

      
}
