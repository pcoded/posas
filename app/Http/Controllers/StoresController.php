<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use App\Product;
use App\Customer;
use App\Supplier;
use App\User;
use App\Store;
use Excel;

class StoresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         // get authenticated user
         $userdata = \Auth::user();

         
          //count total customer
         $customerdata  = Customer::all();

         $user =  User::all();

          // Return total products
          $products = Product::all();


          $supplier = Supplier::all();

          // get store locations
          $stores = Store::all();

        return view('location_stores.index', compact('userdata', 'products', 'customerdata', 'supplier', 'user', 'stores'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
          
        //validate inputs
        $this->validate($request, ['name'=>'required']);
        $storeinput = $request;
        $userdata = \Auth::user();
           
        
      
       $stores = new Store;

       $stores->storeName = $storeinput['name'];

       $stores->storeEmail = $storeinput['email'];

       $stores->storeAddress = $storeinput['address'];

       $stores->storePhoneNumber = $storeinput['phonenumber'];
       $stores->user_id = $userdata->id;

       $stores->save();
       
      session()->flash('flash_message', 'New store created Successfully!');
       return redirect('stores');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return $id;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
       $storedit = $request;

       //store id
       $storeId = $storedit['asdhgasha'];

      Store::where('id', $storeId)->update( [ 'storeName'=>$storedit['name'], 'storeEmail'=>$storedit['email'],  'storeAddress'=>$storedit['address'], 'storePhoneNumber'=> $storedit['phonenumber']  ] );

     session()->flash('flash_message', 'Store has been updated Successfully!');

      return redirect('stores');
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        Store::where('id', $id)->delete();

       session()->flash('flash_message', 'Store Deleted Successfully!');

        return redirect('stores');
    }




/**
 * Delete multiple supplier from chechbox
 */
public function destroymultiple(Request $request)
{

   $this->validate($request, [
                 'checkedstore' => 'required',
            ]);
 $data = $request->all();
//return $data['checkedValues'] ;
 // Customer::where('id', $data)->delete([]);
 // return response()->json(['responseText' => 'Success!'], 200);
 


  foreach($data['checkedstore'] as $id){
  Store::where('id', $id)->delete();
  }

  //return Response::json(['data' => array('message' => 'Customer deleted', 'redirecturl' => '/customer')]);
  session()->flash('deleteajax_message', 'Store Deleted Successfully!');
   return response()->json(['responseText' => 'Success!'], 200);
//return Response::json(array('responseText' => 'Success!'))->session()->flash('deleteajax_message', 'Customer Deleted Successfully!');
 

}





/**
 * Upload store lists
 * @param  Request $request [description]
 * @return [type]           [description]
 */
    public function getFile(Request $request)
{  
    // Validate file
     $this->validate($request, [
                 'uploadfile' => 'required | mimes:xls,xlsx,ods,csv,xml,xlt',

            ]);

     // Read the input file
    $fileToUpload = $request->file('uploadfile');

    /**
     * Start import the file
     */
    Excel::load($fileToUpload, function($reader){

    $reader->each(function($row){
   foreach ($row as $value) {
       Store::create([
      'storeName'               => $value['name'],
      'storeEmail'                => $value['email'],
      'storePhoneNumber'   => $value['phone'],
      'storeAddress'            => $value['address'],
  
        ]);
   }
});

    });

    session()->flash('flash_message', 'store has been added Successfully!');
    return redirect('stores');
}


/**
 * Export store to excel
 * @return [type] [description]
 */
public function exportExcel()
{
   // $customerdata = Customer::all();
    Excel::create('store_sheet', function($excel){
        
        $excel->sheet('store', function($sheet){
            $sheet->fromArray(Store::all(['storeName as NAME',  'storeEmail as EMAIL', 'storePhoneNumber as PHONE', 'storeAddress as ADDRESS']));
        });
    })->export('xlsx');

    return redirect('stores');
}






}
