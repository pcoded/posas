<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Product;
use App\User;
use App\Supplier;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
//use Maatwebsite\Excel\Facades\Excel;
use Excel;
use Gate;
use Auth;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
     


          // get authenticated user
          $userdata = \Auth::user();

          $customerdata = Customer::all();

          // count total product
          $products = Product::all();

          // count total uses
          $user  = User::all();

          // Return total suplier 
          $supplier = Supplier::all();


        return view('customers.index', compact('userdata', 'customerdata', 'products', 'supplier', 'user'));
   

      
       
    }




    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate the inputs
        $this->validate($request, [
                 'customerName' => 'required',
            ]);


        $customerinput = $request->all();
        Customer::create($customerinput);

         session()->flash('flash_message', 'Customer has been added Successfully!');

          return redirect('customer');
    }

    public function onSaleNewCustomer(Request $request)
    {
       $onsalenewcustomer = $request->all();

       $this->validate($request, [
                 'customerName' => 'required',
            ]);
       
       $create = Customer::create($onsalenewcustomer);

       if($create)
       {
         return response()->json(["data"=>"success"],200);
       }else{
         return response()->json(["data"=>"error"],402);
       }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       return $id;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
              
              $customerinput = $request;
              $productId = $customerinput['asdgasdga'];

              Customer::where('id', $productId)->update([
                    'customerName'=>$customerinput['customerName'],
                    'customerCompanyName'=>$customerinput['customerCompanyName'],
                    'customerEmail'=>$customerinput['customerEmail'],
                    'customerPhoneNumber'=>$customerinput['customerPhoneNumber'],
                    'customerAddress' => $customerinput['customerAddress'],
                    'customerCountry' => $customerinput['customerCountry'],
                ]);

              session()->flash('flash_message', 'customer updated!');
              return redirect('customer');
    }



    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $checkid = Customer::findOrFail($id);

       if(! is_null($checkid)){
        Customer::where('id', $id)->delete();
       }
       
       session()->flash('flash_message', 'Customer Deleted Successfully!');

       return redirect('customer');

    }

/**
 * Destroy multiple id's using ajax form subumition from a checkbox
 *
 * @return array of id checked from the view
 */
public function destroymultiple(Request $request)
{

   $this->validate($request, [
                 'checkedValues' => 'required',
            ]);
 $data = $request->all();
//return $data['checkedValues'] ;
 // Customer::where('id', $data)->delete([]);
 // return response()->json(['responseText' => 'Success!'], 200);
 


  foreach($data['checkedValues'] as $id){
  Customer::where('id', $id)->delete();
  }

  //return Response::json(['data' => array('message' => 'Customer deleted', 'redirecturl' => '/customer')]);
  session()->flash('deleteajax_message', 'Customer Deleted Successfully!');
   return response()->json(['responseText' => 'Success!'], 200);
//return Response::json(array('responseText' => 'Success!'))->session()->flash('deleteajax_message', 'Customer Deleted Successfully!');
 

}

  /**
     * Upload customer excel sheet to customer table
     * First import the Excel class above
     * @var getFIle
     */
public function getFile(Request $request)
{  
    // Validate file
     $this->validate($request, [
                 'uploadfile' => 'required | mimes:xls,xlsx,ods,csv,xml,xlt',

            ]);

     // Read the input file
    $fileToUpload = $request->file('uploadfile');

    /**
     * Start import the file
     */
    Excel::load($fileToUpload, function($reader){

    $reader->each(function($row){
   foreach ($row as $value) {
       Customer::create([
      'customerName'               => $value['customername'],
      'customerCompanyName' =>  $value['customercompanyname'],
      'customerEmail'                => $value['customeremail'],
      'customerPhoneNumber'   => $value['customerphonenumber'],
      'customerAddress'            => $value['customeraddress'],
      'customerCountry'            => $value['customercountry']
        ]);
   }
});

    });

    session()->flash('flash_message', 'Customer has been added Successfully!');
    return redirect('customer');
}

/**
 * Export excel file
 * @return [type] [description]
 */
public function exportExcel()
{
   // $customerdata = Customer::all();
    Excel::create('Customers_sheet', function($excel){
        
        $excel->sheet('customers', function($sheet){
            $sheet->fromArray(Customer::all(['customerName', 'customerCompanyName', 'customerEmail', 'customerPhoneNumber', 'customerAddress', 'customerCountry']));
        });
    })->export('xlsx');

    return redirect('customer');
}


public function passmanage(Request $request)
  {

    $userdata = \Auth::user();
    $useremail = $userdata->email;
    
    $this->validate($request, [
       //'email'=>'required|max:255',
       'password'=>'required|confirmed|min:6'
      ]);

     // $useremail = $request['email'];

     if(User::where('email','=',$useremail)->count() > 0){
       
       $password = bcrypt($request['password']);

       $passchange = User::where('email',$useremail)->update(['password'=>$password]);
        return response()->json(['data'=>'success'], 200);
     }else{
      return response()->json(['data' => 'Email does not exist'], 400);  
     }
    
  }




}
