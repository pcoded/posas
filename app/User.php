<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password','phone','address','country'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

/**
 * Get user roles
 * Every time user login to the system we want to get his/her role e.g Admin, Manager or Normal User
 *  One use can have many roles
 */
    public function roles()
    {
       return $this->belongsToMany('App\Role');
    }

  //CHeck if user has role e.g manager, admin
    public function hasRole($role)
    {
                if(is_string($role))
                {
                    return $this->roles->contains('name', $role);
                }

                return !! $role->intersect($this->roles)->count();

                foreach ($role as $r) {
                     if($this->hasRole($r->name))
                     {
                        return true;
                     }
                }

                return false;


    }

//Assign role to user
    public function assign($role)
    {
        if(is_string($role))
        {
            return $this->roles()->save(
                    Role::whereName($role)->firstOrFail()
                );
        }

        return $this->roles()->save($role);
    }

 // user can create many expenses
  public function expense()
  {
    return $this->hasMany('App\Expense');
  }

}
