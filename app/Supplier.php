<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    protected $fillable = [
       'supplierName',
       'supplierEmail',
       'supplierPhoneNumber',
       'supplierPhoto',
       'supplierAddress',
       'supplierCountry',
       'supplierAccountNumber',
       'taxNumber',
       'wensite',
       'descriptions',
       'taxType',
       'status',
    ];

    /**
     * One supplier can have many products
     * @return \Illuminate\Datebase\Eloquent\Relations\Hasmany
     */
    public function products()
    {
      return $this->hasMany('App\Product');
    }
}
