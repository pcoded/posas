@if(count($errors) > 0)
   <div class="row">
       <div class="col-md-3"></div>
       <div class="col-md-6">
       	
       	<div class="alert alert-danger text-center">
       	 <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
             <strong>Whoops!</strong> There were some problems with your inputs
       <ul>
           @foreach($errors->all() as $error)
             <li>{{ $error }}</li>
           @endforeach
       </ul>
   </div>
       </div>
        <div class="col-md-3"></div>	
   </div>
@endif