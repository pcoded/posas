    @if(Session::has('deleteajax_message'))
        <section>
           <div class="row">
                 <div class="col-md-4"></div>
                 <div class="col-md-4 alert alert-success text-center informuser">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      {{ session('deleteajax_message') }}
                 </div>
                 <div class="col-md-4"></div>
           </div>
        </section>
      @endif