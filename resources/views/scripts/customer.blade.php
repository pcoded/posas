<script  type="text/javascript">
  
  // Delete customer with multiple ids
            $('#submitform').click(function(){
                   // FIre comfirmation from user
                         swal({ 
                         title: "Are you sure?", 
                        text: "You will not be able to recover Customer (s)!",  
                        type: "warning",   
                        showCancelButton: true, 
                        confirmButtonColor: "#DD6B55", 
                        confirmButtonText: "Yes, delete it!",
                        cancelButtonText: "No, cancel plx!", 
                        closeOnConfirm: false, 
                       closeOnCancel: false 
                     },
                        function(isConfirm){ 
                              if (isConfirm) { 

                                     // get the items
                                        var checkedValues = $('input:checkbox:checked').map(function() {
                                        return this.value;
                                        }).get();
                   
                                    // Process the array items
                                         $.ajax({
                                          type:"POST",
                                          url:'/customer/deletemultiple',
                                          data:{checkedValues, _token: $('input[name=_token]').val()},
                                          dataType: 'json',
                                          cache : false,
                                          success: function(data){
                                         window.location.reload();
                                            //swal("Deleted!", "Your imaginary file has been deleted.", "success"); 
                                          },
                                          error: function(data){

                                            swal({
                                                title: "Error",
                                                text: "Select atleast one customer",
                                                type: "error",
                                                confirmButtonText: "Ok"
                                            });


                                          }
                                      }); 
                                   //  window.setTimeout(function(){location.reload()}, 3000);
                                    
                                     //window.location.reload();
                              } else { 
                                     swal("Cancelled", "Your imaginary file is safe :)", "error"); 
                                     document.getElementById("idinput").checked = false;
                                     //window.setTimeout(function(){location.reload()}, 3000);
                                    
                                       }
                         });

               
            });

</script>