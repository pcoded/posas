@extends('master')
@section('title', 'Products')
@section('content')
  @include('includes.header')
   @include('includes.sidebar')

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <i class="fa fa-th"></i> Products
            <small>Control panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="/home"><i class="fa fa-home"></i> Home</a></li>
            <li class="/home">Dashboard</li>
            <li class="active"><a href="/product">Products</a></li>
          </ol>
        </section>
        <div  class="box box-primary"></div> <!-- blue line  on top-->
@include('errors.flash')
@include('errors.formerrors')
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
                        <div class="col-md-12">
                           <div class="btn-group pull-right" role="group" aria-label="...">
                                <button type="button" class="btn btn-success btn-flat" data-toggle="modal" data-target=".bs-example-modal-lg"><i class="fa fa-plus"></i> New Product</button>
                                 <button type="button" class="btn bg-red btn-flat"><i class="fa fa-trash"></i> Delete</button>
                                <button type="button" class="btn bg-orange btn-flat" data-toggle="modal" data-target="#excel"><i class="fa fa-reply-all"></i> Import From Excel</button>
                                <button type="button" class="btn bg-maroon btn-flat"><i class="fa fa-location-arrow"></i> Export To Excel</button>
                            </div>
                        </div>
         	         </div><!-- endcol-12 -->
             <div  class="box box-default" style="margin-bottom:0px;"></div> <!-- blue line  on top-->

            <div class="row">
                  <div class="col-xs-12">
                    @if(count($products))
                      <div class="box">
                        <div class="box-header">
                          <h3 class="box-title">List of Products</h3>
                          
                        </div><!-- /.box-header -->
                        <div class="box-body table-responsive no-padding">
                          <table id="getposdataTable" class="table table-bordered table-hover">
                           
                        <thead>
                            <tr>
                            <th><i class="fa fa-th"></i></th>
                              <th>Product Name</th>
                              <th>Quantity</th>
                              <th>Category</th>
                              <th>Size</th>
                              <th>Cost Price</th>
                              <th>Selling Price</th>
                              <th>Inventory</th>
                              <th>Edit</th>
                              <th>Delete</th>
                            </tr>
                            </thead>
                           <tbody>
                          @foreach($products as $product)
                            <tr>
                                 <td><label><input type="checkbox" class="flat-red"></label></td>
                              <td><a href="/product/show/{{$product->id}}" data-toggle="tooltip" data-placement="top" title="Click to view more details">{{$product->productName}}</a></td>
                              <td>
                                   @if($product->productQuantity > 3)
                                         <span class="label label-info">{{$product->productQuantity}}</span>
                                         @else
                                          <span class="label label-danger" data-toggle="tooltip" data-placement="top" title="Inventory is almost over, you consider add more">{{$product->productQuantity}}</span>
                                   @endif
                              </td>
                              <td>{{$product->productCategory}}</td>
                              <td>{{$product->productSize}}</td>
                              <td>{{$product->productBuyingPrice}}</td>
                              <td>{{$product->productSellingPrice}}</td>
                              <td><a href="/stock/show/{{$product->id}}" data-toggle="tooltip" data-placement="top" title="Update Inventory">Inventory</a></td>

                              <td><a href="#" data-toggle="modal" data-target="#{{ $product->id}}"><i class="fa fa-edit"></i> Edit</a></td>
                              <td><a href="/product/delete/{{$product->id}}" onclick="return confirm('Are you sure you want to delete this product?');"><i class="fa fa-trash-o" style="color:red"></i> elete</a></td>
                            </tr>
                        @endforeach 
                          </tbody></table>
                        </div><!-- /.box-body -->
                        <div class="box-footer clearfix no-border">
                         <button class="btn btn-default pull-right" data-toggle="modal" data-target=".bs-example-modal-lg"><i class="fa fa-plus"></i> Add New Product</button>
                       </div>
                      </div><!-- /.box -->
                      @else
                      <hr>
                         <!-- <div class="col-xs-6 col-md-push-3">
                              <div class="alert alert-info" role="alert">
                                 <h5 class="text-center">Currently there is no products.</h5>
                               </div>
                         </div> -->

                         <div class="alert alert-warning alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <center><strong>Warning!</strong> Currently there is no products.</center>
                        </div>
                      @endif


            </div>

           </div> <!-- Main row -->
         
        </section><!-- /.content -->

        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
             <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                             <div class="modal-header">
                                   <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <h4 class="modal-title" id="myModalLabel">New Product Form</h4>
                             </div>
                             <div class="modal-body">
                                  

                                <form role="form" method="POST" action="/product/store">
                                {!! csrf_field() !!}
                                          <div class="box-body">

                                        

                                                    <!-- Custom Tabs -->
                                                    <div class="nav-tabs-custom">
                                                              <ul class="nav nav-tabs">
                                                                      <li class="active"><a href="#productInformation" data-toggle="tab" aria-expanded="true"><strong>Product Information</strong></a></li>
                                                                       <li class=""><a href="#PricingInventory" data-toggle="tab" aria-expanded="true"><strong>Pricing & Inventory</strong></a></li>
                                                                      <li class=""><a href="#DefaultInfo" data-toggle="tab" aria-expanded="false"><strong>Default</strong></a></li>
                                                                  
                                                              </ul>
                                                              <div class="tab-content">


                                                                   <!-- CREAT ROLES TAB -->
                                                             
                                                                        <div class="tab-pane active" id="productInformation">
                                                                              <div class="form-group">
                                                                                  <label for="barcode">Product Id</label>
                                                                                  <input type="text" name="barcode" placeholder="Barcoce Number" id="barcode" class="form-control">
                                                                            </div>

                                                               
                                                                             <div class="form-group">
                                                                                  <label for="name" style="color:red">Product Name</label>
                                                                                  <input type="text" name="name" placeholder="Product Name" id="name" class="form-control" required>
                                                                            </div>

                                                                              <div class="form-group">
                                                                                  <label for="category">Product Category</label>
                                                                                  <input type="text" name="category" placeholder="Product Category" id="category" class="form-control">
                                                                             </div>
                                                           
                                                                            
                                                                            <div class="form-group">
                                                                                    <label for="size">Size</label>
                                                                                    <input type="text" name="size" placeholder="Product size" id="size" class="form-control">
                                                                            </div>


                                                                              <div class="form-group">
                                                                                      <label for="supplier">Supplier</label>
                                                                                     <select name="supplier" class="form-control">
                                                                                       @if(count($supplier))
                                                                                       <option value="#">Choose Supplier</option>
                                                                                          @foreach($supplier as $supplier)
                                                                                                <option value="{{$supplier->id}}">{{$supplier->supplierName}}</option>
                                                                                          @endforeach
                                                                                       @else
                                                                                          <option value="">No supplier added yet</option>}
                                                                                        @endif
                                                                                     </select>
                                                                              </div>


                                                                             <div class="form-group">
                                                                              <label for="reorderlevel">Re-Order Level</label>
                                                                              <input  name ="reorderlevel" id="reorderlevel" type="text"  placeholder="At which point of stock you would like to re-order this product" class="form-control">
                                                                            </div>

                                                                            <div class="form-group">
                                                                                <label for="descriptions">Product Descriptions</label>
                                                                                <textarea name="descriptions" placeholder="Give any description if available" class="form-control"></textarea>
                                                                          </div>

                                                                      
                                                                              

                                                                        </div><!-- /.Role tab-pane -->
                                         



                                                                       <!--  CREATE PERMISSIONS TAB -->
                                                                        <div class="tab-pane" id="PricingInventory">
                                                                           <div class="form-group">
                                                                                  <label for="unitprice" style="color:red">Cost Price</label>
                                                                                  <input type="text" name="unitprice" placeholder="Unit price" id="unitprice" class="form-control" required>
                                                                          </div>

                                                                          <div class="form-group">
                                                                                  <label for="sellingprice" style="color:red">Selling Price</label>
                                                                                  <input type="text" name="sellingprice" placeholder="Selling Price" id="sellingprice" class="form-control" required>
                                                                          </div>

                                                                        </div> <!-- END PERMISSION TAB -->

                                                                        <!-- ASSIGN PERMISSION TO ROLE TAB -->
                                                                        <div class="tab-pane" id="DefaultInfo">
                                                                         
                                                                            <div class="form-group">
                                                                                  <label for="quantity">Product Quantity</label>
                                                                                  <input type="text" name="quantity" placeholder="Product Quantity" id="quantity" class="form-control" required>
                                                                            </div>


                                                                            <div class="form-group">
                                                                                    <label for="supplier">Location at Store</label>
                                                                                     <input type="text" name="locationatStore" placeholder="Location at store" id="quantity" class="form-control" required>
                                                                            </div>

                                                                        </div><!-- /.ASSIGN PERMISSION TO ROLE tab-pane -->

                                                              </div><!-- /.tab-content -->
                                                    </div><!-- nav-tabs-custom -->
                          






                                          </div><!-- /.box-body -->

                                        <div class="modal-footer">
                                           <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                           <button type="submit" class="btn btn-primary">Save</button>
                                        </div>
                             </form>



                              </div>

                          
                    </div>
             </div>
       </div>




      @foreach($products as $product)
       <div class="modal fade" id="{{ $product->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
             <div class="modal-dialog modal-lg">
                     <div class="modal-content">
                             <div class="modal-header">
                                   <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <h4 class="modal-title" id="myModalLabel">Edit {{ $product->productName }}</h4>
                             </div>
                                 <div class="modal-body">
                                        

                                            <form role="form" method="POST" action="/product/update/">
                                               {!! csrf_field() !!}
                                          <div class="box-body">

                                                   <div class="form-group">
                                                          <label for="name">Product Name</label>
                                                          <input type="text" name="name" value="{{ $product->productName }}" placeholder="Product Name" id="name" class="form-control" required>
                                                    </div>

                                                    <div class="form-group">
                                                          <label for="quantity">Product Quantity</label>
                                                          <input type="text" name="quantity" value="{{ $product->productQuantity }}" placeholder="Product Quantity" id="quantity" class="form-control" required>
                                                    </div>

                                                    <div class="form-group">
                                                            <label for="size">Size</label>
                                                            <input type="text" name="size" value="{{ $product->productSize }}" placeholder="Product size" id="size" class="form-control">
                                                    </div>

                                                    <div class="form-group">
                                                          <label for="barcode">Barcoce Number</label>
                                                          <input type="text" name="barcode"  value="{{ $product->productBarcode}}" placeholder="Barcoce Number" id="barcode" class="form-control">
                                                    </div>

                                                    <div class="form-group">
                                                            <label for="category">Product Category</label>
                                                            <input type="text" name="category" value="{{ $product->productCategory }}" placeholder="Product Category" id="category" class="form-control">
                                                    </div>

                                                    <div class="form-group">
                                                            <label for="unitprice">Unit Price</label>
                                                            <input type="text" name="unitprice" value="{{ $product->productBuyingPrice }}" placeholder="Unit price" id="unitprice" class="form-control" required>
                                                    </div>

                                                    <div class="form-group">
                                                            <label for="sellingprice">Selling Price</label>
                                                            <input type="text" name="sellingprice" value="{{ $product->productSellingPrice }}" placeholder="Selling Price" id="sellingprice" class="form-control" required>
                                                    </div>

                                                    
                                                    <div class="form-group">
                                                            <label for="supplier">Supplier</label>
                                                           <select name="supplier" class="form-control" required>
                                                             @if(count($supplier2))
                                                             <option value="{{ $product->supplier_id }}"></option>
                                                                @foreach($supplier2 as $supplier)
                                                                      <option value="{{$supplier->id}}">{{$supplier->supplierName}}</option>
                                                                @endforeach
                                                             @else
                                                                <option value="">No supplier added yet</option>}
                                                              @endif
                                                           </select>
                                                    </div>

                                                    <div class="form-group">
                                                            <label for="store">product Location</label>
                                                           <select name="store" class="form-control" required>
                                                             @if(count($stores))
                                                             <option value="{{$product->store_id}}"></option>
                                                                @foreach($stores as $store)
                                                                      <option value="{{$store->id}}">{{$store->storeName}}</option>
                                                                @endforeach
                                                             @else
                                                                <option value="">No store  added yet</option>}
                                                              @endif
                                                           </select>
                                                    </div>
                                                       
                                                          <div class="form-group">
                                                            <label for="descriptions">Product Descriptions</label>
                                                            <textarea name="descriptions"  class="form-control">{{ $product->productDescriptions }}</textarea>
                                                      </div>

                                                    <div class="form-group">
                                                      <label for="photo">Product Photo</label>
                                                      <input  name ="photo" id="productPhoto" type="file" class="form-control">
                                                    </div>
                                                     <input type="hidden" name="asdhgasha" value="{{ $product->id }}">
                                                       
                                                  <div class="form-group">
                                                      <label for="reorderlevel">Re-Order Level</label>
                                                      <input  name ="reorderlevel" value="{{ $product->reOrderLevel }}" id="reorderlevel" type="text"  placeholder="At which point of stock you would like to re-order this product" class="form-control">
                                                    </div>
                                                     

                                            </div>

                                            <div class="modal-footer">
                                                   <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                   <button type="submit" class="btn btn-primary">Save</button>
                                            </div>
                                          </form>



                                 </div>

                     
                     </div>

               </div>
       </div>
 @endforeach
       
  <!-- form to upload -->     
<!-- <form method="POST" action="/product/importExcel"  enctype="multipart/form-data">
    {!! csrf_field() !!}
 </form> -->
 {!! Form::open(['url' => '/product/importExcel', 'files'=>true]) !!}
       
       @include('partials.uploadform')
     
 {!! Form::close() !!}

</div><!-- /.content-wrapper -->

            
        @include('includes.footer')
         
@stop
