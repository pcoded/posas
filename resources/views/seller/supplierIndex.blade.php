@extends('master')
@section('title', 'Suppliers')
@section('content')
  @include('includes.header')
   @include('includes.sidebar')

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <i class="fa fa-paper-plane"></i> Suppliers
            <small>Control panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="/home"><i class="fa fa-home"></i> Home</a></li>
            <li class="/home">Dashboard</li>
            <li class="active"><a href="/supplier">Suppliers</a></li>
          </ol>
        </section>
        <div  class="box box-primary"></div> <!-- blue line  on top-->
        
        @include('errors.flash')
        @include('errors.formerrors')
        @include('errors.deleteajax')
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
                   <div class="col-md-12">

                             <div class="btn-group pull-right" role="group" aria-label="...">
                                <button type="button" class="btn btn-success btn-flat" data-toggle="modal" data-target=".bs-example-modal-lg"><i class="fa fa-plus"></i> New Supplier</button>
                                 <button type="button" class="btn bg-red btn-flat" id="supplier" ><i class="fa fa-trash"></i> Delete</button>
                                <button type="button" class="btn bg-orange btn-flat" data-toggle="modal" data-target="#excel"><i class="fa fa-reply-all"></i> Excel Import</button>
                                <a href="supplier/export"><button type="button" class="btn bg-maroon btn-flat"><i class="fa fa-location-arrow"></i> Excel Export</button></a>
                                
                            </div>
                      
                   </div><!-- endcol-12 -->
            </div>

            <div  class="box box-default" style="margin-bottom:0px;"></div> <!-- blue line  on top-->

            <div class="row">
                  <div class="col-xs-12">
                    @if(count($supplier))
                      <div class="box">
                        <div class="box-header">
                          <h3 class="box-title">List of Suppliers</h3>
                          
                        </div><!-- /.box-header -->
                        <div class="box-body table-responsive no-padding">
                          <table id="getposdataTable" class="table table-bordered table-hover">
                           
                           <thead>
                            <tr>
                             <th><i class="fa fa-th"></i></th>
                              <th>Supplier Name</th>
                              <th>Email</th>
                              <th>Phone Number</th>
                              <th>Address</th>
                              <th>Country</th>
                              <th>Account Number</th>
                              <th>Edit</th>
                              <th>Delete</th>
               
                            </tr>
                            </thead>
                             <tbody>
                         @foreach($supplier as $supplier)
                            <tr>
                                   <td>
                                            <input type="checkbox" name="supplierIds[]"  id="idinput" value="{{$supplier->id}}"  class="flat-red">
                                    </td>
                              <td><a href="/supplier/edit/{{$supplier->id}}" data-toggle="tooltip" data-placement="top" title="Click to Edit">{{$supplier->supplierName}}</a></td>
                              <td>{{$supplier->supplierEmail}}</td>
                              <td>{{$supplier->supplierPhoneNumber}}</td>
                              <td>{{$supplier->supplierAddress}}</td>
                              <td>{{$supplier->supplierCountry}}</td>
                              <td>{{$supplier->supplierAccountNumber}}</td>
                              <td><a href="#" data-toggle="modal" data-target="#{{ $supplier->id}}"><i class="fa fa-edit"></i> Edit</a></td>
                              <td><a href="/supplier/delete/{{$supplier->id}}" onclick="return confirm('Are you sure you want to delete this product?');"><i class="fa fa-trash-o" style="color:red"></i> Delete</a></td>
                             
                            </tr>
                            @endforeach
                          </tbody></table>
                          <div class="box-footer clearfix no-border">
                     <button class="btn btn-default pull-right" data-toggle="modal" data-target=".bs-example-modal-lg"><i class="fa fa-plus"></i> Add New Supplier</button>
                </div>
                        </div><!-- /.box-body -->
                      </div><!-- /.box -->
                      @else
                      <hr>
                         <div class="col-xs-6 col-md-push-3">
                              <div class="alert alert-danger" role="alert">
                                 <h4 class="text-center"> No Supplier have been added  yet.</h4>
                               </div>
                         </div>
                      @endif


            </div>



           </div> <!-- Main row -->
         
        </section><!-- /.content -->

        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
             <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                             <div class="modal-header">
                                   <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <h4 class="modal-title" id="myModalLabel">New Supplier Form</h4>
                             </div>
                             <div class="modal-body">
                                  

                                <form role="form" method="POST" action="/supplier/store">
                                {!! csrf_field() !!}
                                          <div class="box-body">
                                                   <div class="form-group">
                                                          <label for="name">Supplier Name</label>
                                                          <input type="text" name="name" placeholder="Supplier Name" id="supplierName" class="form-control" required>
                                                    </div>

                                                    <div class="form-group">
                                                          <label for="email">Supplier Email</label>
                                                          <input type="email" name="email" placeholder="Supplier Email" id="supplierEmail" class="form-control">
                                                    </div>

                                                    <div class="form-group">
                                                            <label for="phonenumber">Phone Number</label>
                                                            <input type="text" name="phonenumber" placeholder="Supplier Phone Number" id="supplierPhoneNumber" class="form-control" required>
                                                    </div>

                                                    <div class="form-group">
                                                          <label for="supplieraddress">Address</label>
                                                          <input type="text" name="supplieraddress" placeholder="Supplier Address" id="supplierAddress" class="form-control">
                                                    </div>

                                                    <div class="form-group">
                                                            <label for="country">Country</label>
                                                            <input type="text" name="country" placeholder="Country" id="country" class="form-control">
                                                    </div>

                                                    <div class="form-group">
                                                            <label for="accountnumber">Account Number</label>
                                                            <input type="text" name="accountnumber" placeholder="Account Number" id="accountnumber" class="form-control">
                                                    </div>

                                                    <div class="form-group">
                                                      <label for="supplierPhoto">Product Photo</label>
                                                      <input  name ="photo" id="supplierPhoto" type="file" class="form-control">
                                                    </div>
                                                    
                                          </div><!-- /.box-body -->

                                        <div class="modal-footer">
                                           <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                           <button type="submit" class="btn btn-primary">Save</button>
                                        </div>
                             </form>



                              </div>

                          
                    </div>
             </div>
       </div>





@if(count($supplier2))
@foreach($supplier2 as $supplier)
<div class="modal fade" id="{{ $supplier->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
             <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                             <div class="modal-header">
                                   <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <h4 class="modal-title" id="myModalLabel">Edit {{ $supplier->supplierName}}</h4>
                             </div>
                             <div class="modal-body">
                                  

                                <form role="form" method="POST" action="/supplier/update">
                                {!! csrf_field() !!}
                                          <div class="box-body">
                                                   <div class="form-group">
                                                          <label for="name">Supplier Name</label>
                                                          <input type="text" name="name" value="{{$supplier->supplierName}}" placeholder="Supplier Name" id="supplierName" class="form-control" required>
                                                    </div>

                                                    <div class="form-group">
                                                          <label for="email">Supplier Email</label>
                                                          <input type="email" name="email"  value="{{$supplier->supplierEmail}}" placeholder="Supplier Email" id="supplierEmail" class="form-control">
                                                    </div>

                                                    <div class="form-group">
                                                            <label for="phonenumber">Phone Number</label>
                                                            <input type="text" name="phonenumber" value="{{$supplier->supplierPhoneNumber}}" placeholder="Supplier Phone Number" id="supplierPhoneNumber" class="form-control" required>
                                                    </div>

                                                    <div class="form-group">
                                                          <label for="supplieraddress">Address</label>
                                                          <input type="text" name="supplieraddress" value="{{$supplier->supplierAddress}}" placeholder="Supplier Address" id="supplierAddress" class="form-control">
                                                    </div>

                                                    <div class="form-group">
                                                            <label for="country">Country</label>
                                                            <input type="text" name="country" value="{{$supplier->supplierCountry}}" placeholder="Country" id="country" class="form-control">
                                                    </div>

                                                    <div class="form-group">
                                                            <label for="accountnumber">Account Number</label>
                                                            <input type="text" name="accountnumber"  value="{{$supplier->supplierAccount}}" placeholder="Account Number" id="accountnumber" class="form-control">
                                                    </div>

                                                    <div class="form-group">
                                                      <label for="supplierPhoto">Product Photo</label>
                                                      <input  name ="photo" value="{{$supplier->supplierPhoto}}" id="supplierPhoto" type="file" class="form-control">
                                                      <input  name ="asdgasdga"  value = "{{ $supplier->id}}" type="hidden">
                                                    </div>
                                                    
                                          </div><!-- /.box-body -->

                                        <div class="modal-footer">
                                           <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                           <button type="submit" class="btn btn-primary">Update</button>
                                        </div>
                             </form>



                              </div>

                          
                    </div>
             </div>
       </div>
@endforeach
@endif

{!! Form::open(['url'=>'supplier/importExcel', 'files'=>true]) !!}

   @include('partials.uploadform')

{!! Form::close() !!}



      </div><!-- /.content-wrapper -->

            
        @include('includes.footer')
         
@stop
