@extends('master')
@section('title', 'Stores')
@section('content')
  @include('includes.header')
   @include('includes.sidebar')

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <i class="fa fa-cubes"></i> Stores
            <small>Control panel</small>
          </h1>

     <ol class="breadcrumb">
            <li><a href="/home"><i class="fa fa-home"></i> Home</a></li>
            <li class="/home">Dashboard</li>
            <li class="active"><a href="/stores">Stores</a></li>
          </ol>
        </section>
        <div  class="box box-primary"></div> <!-- blue line  on top-->
    
          @include('errors.flash')
          @include('errors.formerrors')
          @include('errors.deleteajax')

        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
                   <div class="col-md-12">

                             <div class="btn-group pull-right" role="group" aria-label="...">
                                <button type="button" class="btn btn-success btn-flat" data-toggle="modal" data-target=".bs-example-modal-lg"><i class="fa fa-plus"></i> Add New Store</button>
                                 <button type="button" class="btn bg-red btn-flat" id="store"><i class="fa fa-trash"></i> Delete</button>
                                <button type="button" class="btn bg-orange btn-flat" data-toggle="modal" data-target="#excel"><i class="fa fa-reply-all"></i> Import From Excel</button>
                                <button type="button" class="btn bg-maroon btn-flat"><i class="fa fa-location-arrow"></i> Export To Excel</button>
                            </div>
                        
         	         </div><!-- endcol-12 -->
            </div>

              <div  class="box box-default" style="margin-bottom:0px;"></div> <!-- blue line  on top-->

            <div class="row">
                  <div class="col-xs-12">
                    @if(count($stores))

                <div class="box box-default">
                <div class="box-header ui-sortable-handle" style="cursor: move;">
                
                  <h3 class="box-title">List of Stores</h3>
                </div><!-- /.box-header -->
          
                <div class="box-body table-responsive no-padding">
                
                  <table  id="getposdataTable" class="table table-bordered table-hover">
                    
                    <thead>
                      <tr>
                      <th><i class="fa fa-th"></i></th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Address</th>
                        <th>Phone Number</th>
                        <th>Edit</th>
                         <th>Delete</th>
                      </tr>
                    </thead>
                    <tbody>
                    @foreach($stores as $store)
                      <tr>
                          <td>
                                  <input type="checkbox" name="storeIds[]"  id="idinput" value="{{$store->id}}"  class="flat-red">
                                
                          </td>
                        <td>{{$store->storeName}}</td>
                        <td>{{$store->storeEmail}}</td>
                        <td>{{$store->storeAddress}}</td>
                        <td>{{$store->storePhoneNumber}}</td>
                        <td><a href="#" data-toggle="modal" data-target="#{{ $store->id}}"><i class="fa fa-edit"></i> Edit</a></td>
                        <td><a href="/stores/delete/{{$store->id}}" onclick="return confirm('Are you sure you want to delete this store?');"><i class="fa fa-trash-o" style="color:red"></i> elete</a></td>
                      </tr>
                    @endforeach
                    </tbody>
                  </table>

                </div><!-- /.box-body -->
                <div class="box-footer clearfix no-border">
                  <button class="btn btn-default pull-right" data-toggle="modal" data-target=".bs-example-modal-lg"><i class="fa fa-plus"></i> Add New Store</button>
                </div>
              </div>


                      @else
                      <hr>
                         <div class="col-xs-6 col-md-push-3">
                              <div class="alert alert-danger" role="alert">
                                 <h4 class="text-center"> No Store have been added  yet.</h4>
                               </div>
                         </div>
                      @endif


            </div>



           </div> <!-- Main row -->
         
        </section><!-- /.content -->

        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
             <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                             <div class="modal-header">
                                   <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <h4 class="modal-title" id="myModalLabel">New Store Form</h4>
                             </div>
                             <div class="modal-body">
                                  

                                   <form  method="post" action="/stores/store">
                                        {!! csrf_field() !!}
                                          <div class="form-group">
                                            <input type="text" name="name" class="form-control" placeholder="Name of store or Location">
                                            
                                          </div>
                                          <div class="form-group">
                                            <input type="email" name="email" class="form-control" placeholder="Email">
                                           
                                          </div>
                                          <div class="form-group">
                                            <input type="text" name="address" class="form-control" placeholder="Address: e.g 123 Mirambo Street">
                                           
                                          </div>
                                          <div class="form-group">
                                            <input type="text" name="phonenumber" class="form-control" placeholder="Phone Number">
                                           
                                          </div>

                                                    
                                          </div><!-- /.box-body -->
                                
                                        <div class="modal-footer">
                                           <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                           <button type="submit" class="btn btn-primary">Save</button>
                                        </div>
                             </form>

                              </div>

                    </div>
             </div>
       </div>


    @foreach($stores as $store)
       <div class="modal fade" id="{{ $store->id }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
             <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                             <div class="modal-header">
                                   <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                  <h4 class="modal-title" id="myModalLabel">Edit {{ $store->storeName }}</h4>
                             </div>
                             <div class="modal-body">
                                  

                                   <form method="POST" action="/stores/update">
                                       {!! csrf_field() !!}
                                          <div class="form-group">
                                            <input type="text" name="name" value="{{ $store->storeName}}" class="form-control" placeholder="Name of store or Location">
                                            
                                          </div>
                                          <div class="form-group">
                                            <input type="email" name="email" value="{{ $store->storeEmail}}"  class="form-control" placeholder="Email">
                                           
                                          </div>
                                          <div class="form-group">
                                            <input type="text" name="address" value="{{ $store->storeAddress}}" class="form-control" placeholder="Address: e.g 123 Mirambo Street">
                                           
                                          </div>
                                          <div class="form-group">
                                            <input type="text" name="phonenumber" value="{{ $store->storePhoneNumber}}" class="form-control" placeholder="Phone Number">
                                            <input type="hidden" name="asdhgasha" value="{{ $store->id }}">
                                          </div>
                                                 
                                                  <div class="modal-footer">
                                           <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                           <button type="submit" class="btn btn-primary">Save</button>
                                        </div>
                                                    
                                          </form></div><!-- /.box-body -->
                            

                              </div>
                    </div>
             </div>
         @endforeach

        {{!! Form::open(['url'=>'stores/importExcel', 'files'=>true]) !!}

            @include('partials.uploadform')

       {!! Form::close() !!}

      </div><!-- /.content-wrapper -->

            
        @include('includes.footer')
         
@stop

