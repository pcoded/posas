
@extends('master')
@section('title', 'Settings')
@section('content')
@include('includes.header')
@include('includes.sidebar')
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <i class="fa fa-cog"></i> Settings
            <small>Control panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="/home"><i class="fa fa-home"></i> Home</a></li>
            <li class="/home">Dashboard</li>
            <li class="active"><a href="/settings">Settings</a></li>
          </ol>
        </section>
         <div  class="box box-primary"></div> <!-- blue line  on top-->

 <section class="content">
          <div class="row">
  
            Settings goes here....

          </div><!-- end row -->
          </section>
         </div>

@include('includes.footer')
@stop
