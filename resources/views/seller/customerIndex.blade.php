
@extends('master')
@section('title', 'Customers')
@section('content')
  @include('includes.header')
   @include('includes.sidebar')

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <i class="fa fa-users"></i> Customer
            <small>Control panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="/home"><i class="fa fa-home"></i> Home</a></li>
            <li class="/home">Dashboard</li>
            <li class="active"><a href="/customer">Customer</a></li>
          </ol>
        </section>
         <div  class="box box-primary"></div> <!-- blue line  on top-->

          @include('errors.flash')
          @include('errors.formerrors')
          @include('errors.deleteajax')

      <div id="deleting"></div>
        <div class="content">
             <!-- Add new, Delete multiple, Export & import to Ex -->
          <div class="row">
                   <div class="col-md-12">
                             <div class="btn-group pull-right " role="group" aria-label="...">
                           
                                <button type="button" class="btn btn-success btn-flat" data-toggle="modal" data-target=".bs-example-modal-lg"><i class="fa fa-plus"></i> Add New Customer</button>
                            
                            
                            
                                 <button type="button" class="btn bg-red btn-flat" id="submitform" ><i class="fa fa-trash"></i> Delete</button>
                    
                                <button type="button" class="btn bg-orange btn-flat" data-toggle="modal" data-target="#excel"><i class="fa fa-reply-all"></i> Import From Excel</button>
                                <a href="/customer/export/"><button type="button" class="btn bg-maroon btn-flat"><i class="fa fa-location-arrow"></i> Export To Excel</button></a>
                            </div>
                   </div><!-- endcol-12 -->
            </div><!-- endrow -->
  <div  class="box box-default" style="margin-bottom:0px;"></div> <!-- blue line  on top-->

            <div class="row">
                  <div class="col-xs-12">
                    @if(count($customerdata))
                      <div class="box">
                        <div class="box-header">
                          <h3 class="box-title">List of Customer</h3>
                          
                        </div><!-- /.box-header -->
                        <div class="box-body table-responsive no-padding">
                          <table id="getposdataTable" class="table table-bordered table-hover" >

                                 <thead>
                                      <tr>
                                      <th><i class="fa fa-th"></i></th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Address</th>
                                        <th>Phone Number</th>
                                        <th>Edit</th>
                                        <th>Delete</th>
                                      </tr>
                                  </thead>
                            <tbody>

                         @foreach($customerdata as $customer)
                            <tr>
                              <td>
                                      <form  id = "checkAll"  method= "POST"  action="/customer/deletemultiple">
                         
                                             <input type="checkbox" name="customerIds[]"  id="idinput" value="{{$customer->id}}"  class="flat-red">
                                          </form>
                              </td>
                              <td><a href="/customer/show/{{$customer->id}}" data-toggle="tooltip" data-placement="top" title="Click to view more details"> {{$customer->customerName}}</a></td>
                             <td>{{$customer->customerEmail}}</td>
                             <td>{{$customer->customerAddress}}</td>
                             <td>{{$customer->customerPhoneNumber}}</td>
                             <td><a href="#" data-toggle="modal" data-target="#{{ $customer->id}}"><i class="fa fa-edit"></i> Edit</a></td>
                              <td><a href="/customer/delete/{{$customer->id}}" onclick="return confirm('Are you sure you want to delete this customer?');"><i class="fa fa-trash-o" style="color:red"></i> elete</a></td>
                             
                            </tr>
                            @endforeach
                          </tbody></table>
                        </div><!-- /.box-body -->
                        <div class="box-footer clearfix no-border">
                     <button class="btn btn-default pull-right" data-toggle="modal" data-target=".bs-example-modal-lg"><i class="fa fa-plus"></i> Add New customer</button>
                </div>
                      </div><!-- /.box -->
                      @else
                      <hr>
                         <div class="col-xs-6 col-md-push-3">
                              <div class="alert alert-danger" role="alert">
                                 <h4 class="text-center"> No customer have been added  yet.</h4>
                               </div>
                         </div>
                      @endif


            </div>



           </div> <!-- Main row -->
         
        </div><!-- /.content -->



        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
             <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                             <div class="modal-header">
                                   <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <h4 class="modal-title" id="myModalLabel">New Customer Form</h4>
                             </div>
                             <div class="modal-body">
                                  

                                <form role="form" method="POST" action="/customer/store">
                                {!! csrf_field() !!}
                                          <div class="box-body">
                                                   <div class="form-group">
                                                          <label for="customerName">Customer Name</label>
                                                          <input type="text" name="customerName" placeholder="Customer Name" id="customerName" class="form-control" required>
                                                    </div>

                                                    <div class="form-group">
                                                          <label for="customerCompanyName">Customer Company Name</label>
                                                          <input type="text" name="customerCompanyName" placeholder="Customer Company Name" id="customerCompanyName" class="form-control">
                                                    </div>

                                                    <div class="form-group">
                                                            <label for="customerEmail">Customer Email</label>
                                                            <input type="email" name="customerEmail" placeholder="Customer Email" id="customerEmail" class="form-control">
                                                    </div>

                                                    <div class="form-group">
                                                            <label for="customerPhoneNumber">Customer Phone Number</label>
                                                            <input type="text" name="customerPhoneNumber" placeholder="Customer Phone Number" id="customerPhoneNumber" class="form-control">
                                                    </div>

                                                   
                                                           
                                                    <div class="form-group">
                                                      <label for="exampleInputFile">Customer Photo</label>
                                                      <input  name ="customerPhoto" id="exampleInputFile" type="file">
                                                    </div>

                                                     <div class="form-group">
                                                            <label for="customerAddress">Customer Address</label>
                                                            <input type="text" name="customerAddress" placeholder="Customer Address" id="customerAddress" class="form-control">
                                                    </div>

                                                    <div class="form-group">
                                                            <label for="customerCountry">Customer Country</label>
                                                            <input type="text" name="customerCountry" placeholder="Customer Country" id="customerCountry" class="form-control">
                                                    </div>

                                                    
                                          </div><!-- /.box-body -->

                                        <div class="modal-footer">
                                           <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                           <button type="submit" class="btn btn-primary">Save</button>
                                        </div>
                             </form>



                              </div>

                          
                    </div>
             </div>
       </div>



  @if(count($customerdata))
  @foreach($customerdata as $customer)
     <div class="modal fade" id="{{ $customer->id }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
             <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                             <div class="modal-header">
                                   <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <h4 class="modal-title" id="myModalLabel">Edit {{ $customer->customerName }}</h4>
                             </div>
                             <div class="modal-body">
                                  

                                <form role="form" method="POST" action="/customer/update">
                                {!! csrf_field() !!}
                                          <div class="box-body">
                                                   <div class="form-group">
                                                          <label for="customerName">Customer Name</label>
                                                          <input type="text" name="customerName" value = "{{ $customer->customerName }}" placeholder="Customer Name" id="customerName" class="form-control">
                                                    </div>

                                                    <div class="form-group">
                                                          <label for="customerCompanyName">Customer Company Name</label>
                                                          <input type="text" name="customerCompanyName"  value = "{{ $customer->customerCompanyName }}" placeholder="Customer Company Name" id="customerCompanyName" class="form-control">
                                                    </div>

                                                    <div class="form-group">
                                                            <label for="customerEmail">Customer Email</label>
                                                            <input type="email" name="customerEmail"  value = "{{ $customer->customerEmail }}" placeholder="Customer Email" id="customerEmail" class="form-control">
                                                    </div>

                                                    <div class="form-group">
                                                            <label for="customerPhoneNumber">Customer Phone Number</label>
                                                            <input type="text" name="customerPhoneNumber"  value = "{{ $customer->customerPhoneNumber }}" placeholder="Customer Phone Number" id="customerPhoneNumber" class="form-control">
                                                    </div>

                                                    <div class="form-group">
                                                            <label for="customerAddress">Customer Address</label>
                                                            <input type="text" name="customerAddress"  value = "{{ $customer->customerAddress }}" placeholder="Customer Address" id="customerAddress" class="form-control">
                                                    </div>

                                                    <div class="form-group">
                                                            <label for="customerCountry">Customer Country</label>
                                                            <input type="text" name="customerCountry"  value = "{{ $customer->customerCountry }}" placeholder="Customer Country" id="customerCountry" class="form-control">
                                                    </div>

                                                    <div class="form-group">
                                                      <label for="exampleInputFile">Customer Photo</label>
                                                      <input  name ="customerPhoto"  value = "{{ $customer->customerPhoto }}" id="exampleInputFile" type="file">
                                                    </div>
                                                    <input  name ="asdgasdga"  value = "{{ $customer->id}}" type="hidden">
                                                    
                                          </div><!-- /.box-body -->

                                        <div class="modal-footer">
                                           <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                           <button type="submit" class="btn btn-primary">Save</button>
                                        </div>
                             </form>



                              </div>

                          
                    </div>
             </div>
       </div>
@endforeach
@endif


 {!! Form::open([ 'url' => '/customer/importExcel', 'files'=>true]) !!}
       
       @include('partials.uploadform')

{!! Form::close() !!}

      </div><!-- /.content-wrapper -->

        
        @include('includes.footer')

     
@stop
