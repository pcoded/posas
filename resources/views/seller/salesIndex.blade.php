@extends('master')
@section('title', 'Sales')
@section('content')
  @include('includes.header')
   @include('includes.sidebar')

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <i class="fa fa-shopping-cart"></i> Sales
            <small>Control panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="/home"><i class="fa fa-home"></i> Home</a></li>
            <li class="active"><a href="/sales">Sales</a></li>
          </ol>
        </section>
      
      <section>
         <div class="col-md-12">
              <div class="col-md-4"></div>
              <div class="col-md-4">@include('flash::message')</div>
              <div class="col-md-4"></div>
         </div>     
      </section>
      <div  class="box box-primary"></div> <!-- blue line  on top-->
      
    <section class="content">
    <div class="row">
        <!-- Left col -->
        <section class="col-lg-7">
          <!-- Chat box -->
          <div class="box box-solid">
            <div  class="box-body">
               
                 
              <div class="input-group">
              <div class="input-group-btn">
                  <button type="button" class="btn btn-primary"><i class="fa fa-plus"></i></button>
                </div>
                <input class="form-control" id="salesValue" name="itemName" type="text"  placeholder="Enter item name or scan barcode">  
                <input type="hidden" id="formtoken" name="_token" value="{{ csrf_token() }}">              
              </div>
            
          
          </div>
          </div>
          <!-- /.box (chat box) -->

           <div class="box box-solid">
              <div  class="box-body">
                <table id="register" class="table table-hover">

                  <thead>
                    <tr class="register-items-header">
                      <th>Action</th>
                      <th class="item_name_heading">Item Name</th>
                      <th class="sales_price">Price</th>
                      <th class="sales_quantity">Qty.</th>
                      <th class="sales_discount">Disc %</th>
                      <th>Total</th>
                    </tr>
                  </thead>
                
                     <tr id="messagerow"></tr>
                      <!--  <tr class="">
                           <td><a href="" style="color: #ff7474;"><i class="icon ion-android-cancel"></i></a></td>
                          <td class="text-center">Ciment</td>
                          <td class="text-center">Tsh 18,500</td>
                          <td class="text-center">1</td>
                          <td class="text-center">0%</td>
                           <td class="text-center">Tsh 18,500</td>
                          <td colspan="6">
                            <div class="text-center text-warning"> <h3>There are no items in the cart<span class="flatGreenc"> [Sales]</span></h3></div>
                          </td>
                        </tr> -->

                 
          </table>
             </div>
          </div>
          <!-- /.box (chat box) -->


        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        <section class="col-lg-5">

          <div class="box box-solid">
            <div class="box-body border-radius-none">
              <div class="form-group">
                <div class="btn-group">
                <button type="button" class="btn btn-warning"  aria-expanded="false">
                  <i class="ion-pause"></i> Suspend Sale </button>
              </div>

               <div class="btn-group">
                  <button type="button" class="btn btn-danger"  aria-expanded="false">
                  <i class="ion-close-circled"></i> Cancel Sale </button>
              </div>
            </div>
            <hr>

             <div class="form-group">
        
                <div class="input-group">
                  <div class="input-group-btn">
                  <button type="button" class="btn btn-primary"><i class="ion-person-add"></i></button>
                </div>
                  <input class="form-control pull-right" placeholder="Type customer name..." type="text">

                </div>
                <!-- /.input group -->
              </div>

            </div>
          </div>
          <!-- /.box -->

          <div class="box box-solid">
            <div class="box-body border-radius-none" style="display: block;">
               <div class="row" style="margin-top: 5px;">
                 <div class="col-md-8">Discount all Items by Percent:</div>
                 <div class="col-md-4">50%</div>
               </div>
               <br>
               <div class="row" style="margin-top: 5px;">
                 <div class="col-md-8">Discount Entire Sale: </div>
                 <div class="col-md-4">50%</div>
               </div>
                  <br>
               <div class="row" style="background-color: #f1ffec;">
                 <div class="col-md-8">Sub Total:</div>
                 <div class="col-md-4" id="subtotal"><strong>Tsh 0.00</strong></div>
               </div>
                
                <br>
                 <div class="row" style="margin-top: 5px;  border-top: 1px dashed #d0d3d8; border-bottom: 1px dashed #d0d3d8; font-weight: 300px; font-size: 13px;">
                 <div class="col-md-6 total-amount" style="line-height: 26px; border-right: 1px dashed #d0d3d8;">
                   <div class="side-heading" style="font-weight: 400px; color: #67676c; font-size: 15px;">Total</div>
                       <div class="row">
                          <div class="col-md-6" style="font-size: 22px; text-align: center; color: #ff9e28">Tsh</div>
                          <div class="col-md-6" id="totalAmount" style="color: #6fd64b; text-align: center; font-size: 22px;">0.00</div>
                       </div>
                        
                 </div>
                 <div class="col-md-6" style="line-height: 26px;">
                    <div class="side-heading" style="font-weight: 400px; color: #67676c; font-size: 15px;">Amount Due</div>
                       <div class="amount total-amount" id="amountDue" style="font-size: 22px; text-align: center; color: #ff9e28">Tsh 0.00</div>
                 </div>
                 </div>
             <div id="pay">
                 <div class="row payment">
                    
                     <div class="col-md-12">
                     <br>
                     Add Payment<br>
                       <button class="btn btn-default cashbutton">Cash</button>
                       <button class="btn btn-default checkbutton">Check</button>
                       <button class="btn btn-default giftcardbutton">Gift Card</button>
                     </div>
                 </div>
                 <br>
                 <div class="row">
                   <div class="col-md-12">
                         <div class="input-group cash">
                              <input class="form-control" id="paidAmount" type="text"> 
                              <div class="input-group-btn">
                                <button type="button" class="btn btn-primary">Complete Sale</button>
                              </div>               
                        </div>

                        <div class="input-group check">
                              <input class="form-control" type="text" placeholder="Enter check number"> 
                              <div class="input-group-btn">
                                <button type="button" class="btn btn-primary">Complete Sale</button>
                              </div>               
                        </div>

                        <div class="input-group giftCard">
                              <input class="form-control" type="text" placeholder="Enter gift card"> 
                              <div class="input-group-btn">
                                <button type="button" class="btn btn-primary">Complete Sale</button>
                              </div>               
                        </div>

                      </div>
                 </div>
                 <br>
                 <div class="row comment">
                     <div class="col-md-12">
                        <fieldset>Comments:</fieldset>
                        <textarea class="form-control"></textarea>
                     </div>
                 </div>


            </div><!-- endpay div -->

            </div>
          </div>
          <!-- /.box -->

  

        </section>
        <!-- right col -->
      </div>

    </section>



      </div><!-- /.content-wrapper -->

            
        @include('includes.footer')
         
@stop


