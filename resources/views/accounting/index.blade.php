@extends('master')
@section('title', 'Accouting')
@section('content')
  @include('includes.header')
   @include('includes.sidebar')

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
           <i class="fa fa-dashboard"></i> Accouting
            <small>Control panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Accouting</li>
          </ol>
        </section>
        <div  class="box box-primary"></div> <!-- blue line  on top-->

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="ion ion-ios-cart-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Cash Account</span>
              <span class="info-box-number">90<small>%</small></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="ion-ios-folder-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Inventory</span>
              <span class="info-box-number">41,410</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Sales</span>
              <span class="info-box-number">760</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total Customers</span>
              <span class="info-box-number">2,000</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
      </div><!-- /.row -->
          <!-- Main row -->
          <!-- summary reports -->


      <!-- Main content -->
    <section class="content">
      <div class="row">
        

         
      <!--  <div class="col-md-12"> -->
          <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a aria-expanded="true" href="#journalentry" data-toggle="tab">Journal Entry</a></li>
              <li class=""><a aria-expanded="false" href="#chartsofaccount" data-toggle="tab">Charts of Accounts</a></li>
              <li class=""><a aria-expanded="false" href="#expenseclaims" data-toggle="tab">Expense claims</a></li>
      <!--         <li class="dropdown">
                <a aria-expanded="false" class="dropdown-toggle" data-toggle="dropdown" href="#">
                  Dropdown <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Action</a></li>
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Another action</a></li>
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Something else here</a></li>
                  <li role="presentation" class="divider"></li>
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Separated link</a></li>
                </ul>
              </li> -->
              <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
            </ul>

            <div class="tab-content">



              <!-- /.tab-pane -->
              <div class="tab-pane active" id="journalentry">
                      <h1 class="text-center"><small>Journal Entry</small></h1>
                      <hr>

                      <div class="form-group">
                          <label for="journdescriptions">Descriptions</label>
                          <textarea class="form-control" name="journadescrpt" placeholder="Tell about this record..."></textarea>
                      </div>

                      <div class="form-group">
                       <label for="journdate">Date</label>

                          <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                          <input class="form-control pull-right active" name="journaldate" id="journaldate" type="text">
                        </div>
                        
                      </div>

                     <table id="journalbooktable" class="table table-bordered">
                              <thead>
                                  <tr>
                                       <th>Account</th>
                                       <th>Descriptions</th>
                                       <th>Debit</th>
                                       <th>Credit</th>
                                  </tr>
                              </thead>

                                  <tbody>
                                        <tr class="accdebit">
                                           <td>
                                              <select name="debitaccount" id="debitaccount" class="form-control select2" style="width: 100%;">
                                               @if(count($charts))
                                                 @foreach($charts as $chart)
                                                  <option value="{{ $chart->chartaccId }}">{{ $chart->accountName }}</option>
                                                 @endforeach
                                                @endif
                                              </select>
                                           </td>
                                           <td><input class="form-control" type="text" name="debitdescriptions" placeholder="e.g: Trip to Rock city"></td>
                                           <td><input class="form-control text-right jdebit" type="number" name="debitaccountdebitamount" placeholder="800.00"></td>
                                           <td><input class="form-control text-right" type="number" name="debitaccountcreditamount" placeholder="0.00" value="0" disabled="true"></td>
                                        </tr>


                                        <tr class="accdebit">
                                           <td>
                                              <select name="creditaccount" id="creditaccount" class="form-control select2" style="width: 100%;">
                                                  @if(count($charts))
                                                 @foreach($charts as $chart)
                                                  <option value="{{ $chart->chartaccId }}">{{ $chart->accountName }}</option>
                                                 @endforeach
                                                @endif
                                              </select>
                                           </td>
                                           <td><input class="form-control" type="text" name="creditdescriptions" placeholder="e.g: Air RockCity"></td>
                                           <td><input class="form-control text-right" type="number" value="0" name="creditaccountdebitamount"  placeholder="0.00" disabled="true"></td>
                                           <td><input class="form-control text-right jcredit" type="number" name="creditaccountcreditamount" placeholder="800.00"> <input type="hidden" name="_creditdebittoken" value="<?php echo csrf_token(); ?>"></td>
                                        </tr>  

                                        <tr>
                                          <td colspan="1"></td>
                                          <td><strong style="float:right;font-size:30px">Total</strong></td>
                                          <td id="totaldebit" class="text-right" style="font-weight:bold; font-size:30px">R 0.00</td>
                                          <td id="totalcredit" class="text-right" style="font-weight:bold; font-size:30px">R 0.00</td>
                                        </tr>  


                                  </tbody>
                            
                          </table>


                          <div class="modal-footer">
                                       <button type="reset" class="btn btn-default">Reset</button>
                                       <button type="submit" id="savejournal" class="btn btn-primary">Save Transaction</button>
                                    </div>

              </div>

              <div class="tab-pane" id="chartsofaccount">

                        <div class="row">
                              <div class="col-md-12">
                                 <div class="btn-group pull-right" role="group" aria-label="...">
                                      <button type="button" class="btn btn-success btn-flat" data-toggle="modal" data-target="#newaccountingModal"><i class="fa fa-plus"></i> New Account</button>
                                     @can('delete-expenses')
                                       <button type="button" class="btn bg-red btn-flat" id="deletechartsaccount"><i class="fa fa-trash"></i> Delete</button>
                                    @endcan
                                  </div>
                              </div>
                         </div><!-- endcol-12 -->
                   <div  class="box box-default" style="margin-bottom:0px;"></div> <!-- blue line  on top-->


                  
                   <h1 class="text-center"><small>Charts of Accounting</small></h1>
                       

                          <table id="chartsofaccoutingtabledata" class="table table-bordered">
                              <thead>
                                  <tr>
                                       <th><i class="fa fa-th"></i></th>
                                       <th>Account Number</th>
                                       <th>Account Name</th>
                                       <th>Account Category</th>
                                       <th>Edit</th>
                                       <th>Delete</th>
                                  </tr>
                              </thead>

                                  <tbody>
                                                                                
                                  </tbody>
                            
                          </table>

              </div>


             <br />
              <!-- /.tab-pane -->
              <div class="tab-pane" id="expenseclaims">
                 <h1 class="text-center"><small>Expense claims List</small></h1>
                 <table id="claimsexpensetable" class="table table-bordered">
                              <thead>
                                  <tr>
                                      <th>#</th>
                                       <th>Payment To</th>
                                       <th>Date submitted</th>
                                       <th>Total Amount</th>
                                       <th>Status</th>
                                       <th>View more</th>
                                       <th>Edit</th>
                                       <th>Delete</th>
                                  </tr>
                              </thead>

                                   <tbody>
               
                                                                                
                                  </tbody>
                            
                          </table>
                           



                                      <!-- new accounting -->
        <div class="modal fade in" id="viewexpensemodal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
         <div class="modal-dialog modal-lg">
                <div class="modal-content">
                         <div class="modal-header">
                               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <h4 class="modal-title" id="myModalLabel">More details</h4>
                         </div>
                         <div class="modal-body">
                                   
                                    <div class="box-body">
                                   <!--  <strong>Expense Claim Submitted <span class="submittedon"></span></strong>
                                    <hr> -->
                                     <div class="row">
                                     <!--    <div class="col-md-12">
                                           <div class="col-md-4">
                                               <button type="button" class="btn btn-primary btn-xs">Approve</button>
                                               <button type="button" class="btn btn-warning btn-xs">Decline</button>
                                               <button type="button" class="btn btn-danger btn-xs">Delete</button>
                                           </div>
                                           <div class="col-md-4"></div>
                                           <div class="col-md-4"></div>
                                        </div> -->
                                     
                                        <div class="col-md-12">
                                           <div class="col-md-4">Prepared By:
                                              <div class="viewpreparedby" style="font-size:20px; font-weight:bold;"></div>
                                            </div>
                                           <div class="col-md-4">Payment To:
                                             <div class="viewpaidto" style="font-size:20px; font-weight:bold;"></div>
                                           </div>
                                           <div class="col-md-4">Amount Paid:
                                                 <div class="viewamountpaid" style="font-size:20px; font-weight:bold;"></div>
                                            </div>
                                        </div>
                                     </div>
                                     <br>
                                     <div class="row">
                                        <div class="col-md-12">
                                          <div class="col-md-6">Descriptions: 
                                              <div class="viewreason" style="font-weight:bold;"></div>
                                         </div>
                                          <div class="col-md-6 col-md-push-2">Status:
                                          <div class=" viewstatus label label-success"></div>
                                            
                                          </div>
                                        </div>
                                     </div><br>

                                     <div class="row">
                                        <div class="col-md-12 jumbotron">
                                          <div class="col-md-4">
                                             
                                          <div class="form-group">
                                            <label for="inputEmail3" class="control-label">Date Paid</label>
                                              <div class="input-group">
                                              <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                              </div>
                                              <input class="form-control pull-right active" name="expensedatepaid" id="expensedatepaid" type="text">
                                              <input type="text" name="acceptedexpenseid" value="" style="display:none;">
                                            </div>
                                           
                                          </div>

                                          </div>

                                          <div class="col-md-4">
                                             <div class="form-group">
                                               <label>Paid From Account</label>
                                                 <select name="paidfromaccount" id="paidfromaccount" class="form-control">
                                                   @if(count($chartofaccountexpense))
                                                     @foreach($chartofaccountexpense as $paidfrom)
                                                    <option value="{{$paidfrom->chartaccId}}">{{$paidfrom->accountName}}</option>
                                                     @endforeach
                                                  @else 
                                                     No Account created
                                                  @endif
                                                 </select>
                                             </div>
                                          </div>

                                          <div class="col-md-4">
                                            
                                            <div class="form-group">
                                               <label>Paid To Account</label>
                                                 <select name="paidtoccount" id="paidtoaccount" class="form-control">
                                                <!--  @if(count($chartsexpens))
                                                   @foreach($chartsexpens as $chartsexpens)
                                                    <option value="{{$chartsexpens->chartaccId}}">{{$chartsexpens->accountName}}</option>
                                                    @endforeach
                                                  @else
                                                   No account created
                                                   @endif -->
                                                 </select>
                                             </div>

                                          </div>
                                       
                                        </div>
                                     </div>
                                      
                                    </div>



                                    <div class="modal-footer" id="expensebuttoncontrol">
                                       <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                       <button type="button" class="btn btn-danger deleteexpensebtn">Delete</button>
                                       <button type="button" class="btn btn-warning rejectexpensebtn">Reject</button>
                                       <button type="submit"  class="btn btn-primary approveexpensebtn">Accept</button>
                                    </div>
               
                          </div>

                      
                </div>
         </div>
   </div>





              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
       <!--  </div> -->


      
      
        <!-- /.col (RIGHT) -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->

  
    </section><!-- /.content -->


           <!-- new accounting -->
        <div class="modal fade in" id="newaccountingModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
         <div class="modal-dialog modal-lg">
                <div class="modal-content">
                         <div class="modal-header">
                               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <h4 class="modal-title" id="myModalLabel">New Accounting</h4>
                         </div>
                         <div class="modal-body">
                                   
                                    <div class="box-body">
                                      <div class="form-group">
                                        <label for="exampleInputEmail1">Account Name</label>
                                        <input class="form-control" name="nameofaccount" id="accountname" placeholder="Account name" type="text">
                                        <input type="hidden" name="_newaccounttoken" value="<?php echo csrf_token(); ?>">
                                      </div>

                                      <div class="form-group">
                                         <label fore="accidentifier">Account Number</label>
                                         <input class="form-control" type="number" name="accidentifier" id="accidentifier" placeholder="Account number">
                                      </div>

                                      <div class="form-group">
                                        <label for="exampleInputPassword1">Account Category</label>
                                        <select name="accountcategory" class="form-control" id="accountcategory">

                                             @foreach($cataccouting as $cataccouting)
                                               <option value="{{ $cataccouting->acctypeId }}"> {{ $cataccouting->accName }}</option>
                                             @endforeach
                                        </select>
                                      </div>


                                      <div class="form-group">
                                        <label>Descriptions</label>
                                        <textarea class="form-control" name="accountdescriptions" id="accountdescriptions" rows="3" placeholder="Enter account descriptions ..."></textarea>
                                      </div>
                                      
                                      
                                    </div>



                                    <div class="modal-footer">
                                       <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                       <button type="submit"  class="btn btn-primary submitnewaccount">Save</button>
                                    </div>
               
                          </div>

                      
                </div>
         </div>
   </div>




   <!--EDIT ACCOUNTING MODAL-->
        <div class="modal fade in" id="editaccountingModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
         <div class="modal-dialog modal-lg">
                <div class="modal-content">
                         <div class="modal-header">
                               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <h4 class="modal-title" id="myModalLabel">Edit Account</h4>
                         </div>
                         <div class="modal-body">
                                   
                                    <div class="box-body">
                                      <div class="form-group">
                                        <label for="exampleInputEmail1">Account Name</label>
                                        <input class="form-control" name="editnameofaccount" value="" id="editaccountname" placeholder="Account name" type="text">
                                        <input type="hidden" name="_editaccounttoken" value="<?php echo csrf_token(); ?>">
                                        <input type="text" name="editaccountId" id="editaccountId" value="" style="display:none;">
                                      </div>


                                      <div class="form-group">
                                         <label fore="accidentifier">Account Number</label>
                                         <input class="form-control" type="number" name="editaccidentifier" value="" id="editaccidentifier" placeholder="Account number">
                                      </div>

                                      <div class="form-group">
                                        <label for="exampleInputPassword1">Account Category</label>
                                        <select name="editaccountcategory" class="form-control" id="editaccountcategory">
                                           @if(count($editcataccouting))
                                             @foreach($editcataccouting as $editcataccouting)
                                               <option value="{{ $editcataccouting->acctypeId }}"> {{ $editcataccouting->accName }}</option>
                                             @endforeach
                                          @endif
                                        </select>
                                      </div>


                                      <div class="form-group">
                                        <label>Descriptions</label>
                                        <textarea class="form-control" name="editaccountdescriptions" id="editaccountdescriptions" rows="3" placeholder="Enter account descriptions ..."></textarea>
                                      </div>
                                      
                                      
                                    </div>



                                    <div class="modal-footer">
                                       <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                       <button type="submit" id="editaccount" class="btn btn-primary">Save</button>
                                    </div>
               
                          </div>

                      
                </div>
         </div>
   </div>





 </div><!-- /.content-wrapper -->
 
            
        @include('includes.footer')

        


@stop

