@extends('master')
@section('title', 'Products')
@section('content')
  @include('includes.header')
   @include('includes.sidebar')

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <i class="fa fa-th"></i> Products
            <small>Control panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="/home"><i class="fa fa-home"></i> Home</a></li>
            <li class="/home">Dashboard</li>
            <li class="active"><a href="/product">Products</a></li>
          </ol>
        </section>
        <div  class="box box-primary"></div> <!-- blue line  on top-->
       <!--  <div class="dataloader">
              <img src="{{asset("bower_components/admin-lte/dist/img/preloader.GIF")}}" alt="dataloader">
        </div> -->
@include('errors.flash')
@include('errors.formerrors')
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
                         <div class="col-md-12">
              <h4>Initial products setup</h4>
                  <!-- Custom Tabs -->
                  <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs" id="createNotTab">
                              <li class="active"><a href="#productlist"  data-toggle="tab" aria-expanded="true"><strong>Product list</strong></a></li>
                                <li class=""><a href="#create_user" data-toggle="tab" aria-expanded="false"><strong>Create New</strong></a></li>
                                <!-- <li class=""><a href="#create_permissionTab" data-toggle="tab" aria-expanded="true"><strong>Create Permissions</strong></a></li> -->
                                <li class=""><a href="#assign_permissionTroleTable" data-toggle="tab" aria-expanded="false"><strong>Categories & Brand</strong></a></li>
                              
                               <!--  <li class=""><a href="#assignUser_toRoleTab" data-toggle="tab" aria-expanded="false"><strong>Settings4</strong></a></li> -->
                            </ul>
                            <div class="tab-content">


                                  <!-- TAB1 -->
                                      <div class="tab-pane active" id="productlist">
                                          


                                        <section class="content">
                                        <!-- Small boxes (Stat box) -->
                                        <div class="row">
                                                      <div class="col-md-12">
                                                         <div class="btn-group pull-right" role="group" aria-label="...">
                                                             <!--  <button type="button" class="btn btn-success btn-flat" data-toggle="modal" data-target=".bs-example-modal-lg"><i class="fa fa-plus"></i> New Product</button> -->
                                                             @can('delete-products')
                                                               <button type="button" class="btn bg-red btn-flat" id="deleteTriger"><i class="fa fa-trash"></i> Delete</button>
                                                            @endcan
                                                            @can('import-products')
                                                              <button type="button" class="btn bg-orange btn-flat" data-toggle="modal" data-target="#excel"><i class="fa fa-reply-all"></i> Import From Excel</button>
                                                            @endcan
                                                            @can('export-products')
                                                              <a href="/product/exportExcel"><button type="button" class="btn bg-maroon btn-flat"><i class="fa fa-location-arrow"></i> Export To Excel</button></a>
                                                            @endcan
                                                          </div>
                                                      </div>
                                                 </div><!-- endcol-12 -->
                                           <div  class="box box-default" style="margin-bottom:0px;"></div> <!-- blue line  on top-->

                                          <div class="row">
                                                <div class="col-xs-12">
                                                
                                                    <div class="box">
                                                      <div class="box-header">
                                                        <h3 class="box-title" style="color:red"><strong>* Row with RED color indicate the item has expired <br></strong></h3>
                                                         <h3 class="box-title" style="color:#FF851B"><strong>* Row with ORANGE color indicate the item will expire within 2 days</strong></h3>
                                                        
                                                      </div><!-- /.box-header -->
                                                      <div class="box-body table-responsive no-padding">
                                                        <table id="csmsproductsdataTable" class="table table-bordered table-hover">
                                                         
                                                      <thead>
                                                          <tr>
                                                          <th><i class="fa fa-th"></i></th>
                                                            <th>Name</th>
                                                           <!--  <th>Store Name</th> -->
                                                            <th>Available Qty</th>
                                                           <!--  <th>Category</th>
                                                            <th>Size</th> -->
                                                            <th>BuyPrice</th>
                                                            <th>SellPrice</th>
                                                            <th>Inventory</th>
                                                            <th>Edit</th>
                                                            <th>Delete</th>
                                                          </tr>
                                                          </thead>
                                                         <tbody>
                                                                                                      
                                                        </tbody>

                                                        </table>
                                                      </div><!-- /.box-body -->
                                                      <!-- <div class="box-footer clearfix no-border">
                                                       <button class="btn btn-default pull-right" data-toggle="modal" data-target=".bs-example-modal-lg"><i class="fa fa-plus"></i> Add New Product</button>
                                                     </div> -->
                                                    </div><!-- /.box -->
                                        
                                                   


                                          </div>

                                         </div> 
                                       
                                      </section><!-- /.content -->



                                      </div><!-- /.Role tab-pane -->








              <!--TAB2 -->
              <div class="tab-pane" id="create_user">
                             <div class="row">
                      
                                     <div class="col-md-12">
                                           
                                                 <form role="form"  action="/product/store" method="post">
                                                  {!! csrf_field() !!}

                                                   <div class="col-md-4">
                                                  <h1><small>Product Details</small></h1>
                                                  <hr>
                                                   <div class="form-group">
                                                            <label for="name" style="color:red">Product Name</label>
                                                            <input type="text" name="name" placeholder="Product Name" id="name" class="form-control" required>
                                                      </div>

                                              
                                                        <div class="form-group">
                                                            <label for="supplier">Supplier</label>
                                                           <select class="form-control select2" name="supplier"  style="width: 100%;">
                                                             @if(count($supplier))
                                                            <!--  <option value="#">Choose Supplier</option> -->
                                                                @foreach($supplier as $supplier)
                                                                      <option value="{{$supplier->id}}">{{$supplier->supplierName}}</option>
                                                                @endforeach
                                                             @else
                                                                <option value="">No supplier added yet</option>}
                                                              @endif
                                                           </select>
                                                          </div>

                                                  
                                                          <div class="form-group">
                                                              <label for="category">Product Category</label>
                                                                <select class="form-control select2" name="category" style="width: 100%;">
                                                              
                                                               @if(count($categories))
                                                            <!--  <option value="#">Choose Supplier</option> -->
                                                                @foreach($categories as $category)
                                                                      <option value="{{$category->catName}}">{{$category->catName}}</option>
                                                                @endforeach
                                                             @else
                                                                <option value="">No category added yet</option>}
                                                              @endif
                                                              </select>
                                                         </div>


                                                         <div class="form-group">
                                                              <label for="barcode">SKU</label>
                                                              <input type="text" class="form-control" name="barcode" placeholder="e.g Barcoce Number" id="barcode" class="form-control">
                                                           </div>

                                                        <div class="form-group">
                                                             <label for="tags">Initial Stock Location</label>
                                                           <select class="form-control select2" name="store" style="width: 100%;" required>
                                                             @if(count($stores))
                                                             <option value="">Choose store</option>
                                                                @foreach($stores as $store)
                                                                      <option value="{{$store->id}}">{{$store->storeName}}</option>
                                                                @endforeach
                                                             @else
                                                                <option value="">No store  added yet</option>}
                                                              @endif
                                                           </select>
                                                         </div>      

                                                   </div> 



                                                    <div class="col-md-4">
                                                       <div class="OptionalDetails">
                                                          <h1><small>Pricing Details</small></h1>
                                                          <hr>



                                                          <div class="form-group">
                                                             <label for="tags">Initial Cost Price(include handling cost)</label>
                                                             <div class="input-group">
                                                             <input type="number" class="form-control" name="totalPurchasingCost" placeholder="e.g 10.00">
                                                             <span class="input-group-addon">.00</span>
                                                             </div>
                                                         </div>

                                                         <div class="form-group">
                                                             <label for="tags">Buy Price(price from supplier)</label>
                                                             <div class="input-group">
                                                             <input type="number" class="form-control" name="supplierPricelist" placeholder="e.g 10.00">
                                                             <span class="input-group-addon">.00</span>
                                                             </div>
                                                         </div>

                                                         <div class="form-group">
                                                                  <label for="sellingprice" style="color:red">Retailer Price(selling price)</label>
                                                                  
                                                                  <div class="input-group">
                                                                  <input type="number" name="sellingprice" placeholder="Selling Price" id="sellingprice" class="form-control" required>
                                                                     <span class="input-group-addon">.00</span>
                                                                  </div>
                                                          </div>

                                                           <div class="form-group">
                                                                  <label for="sellingprice" style="color:red">Prices Include VAT</label>
                                                                    <!-- <select class="form-control select2" name="vat" style="width: 100%;" required>
                                                                      <option value="1">INCLUSIVE</option>
                                                                       <option value="9">EXCLUSIVE</option>
                                                                    </select> -->
                                                                   <!--  <input type="number" name="vat" class="form-control"> -->
                                                               
                                                                    <div class="input-group">
                                                                    <input class="form-control" type="number" placeholder="e.g 18" name="vat">
                                                                    <span class="input-group-addon">%</span>
                                                                  </div>
                                                          </div>



                                                                      <div class="form-group">
                                                                         <label for="purchasemethodofpayments">Payment method</label>
                                                                         <select class="form-control select2" style="width: 100%;" name="productpaymentmethod">
                                                                         @if(count($paymentmethods))
                                                                          @foreach($paymentmethods as $paymentmethods)
                                                                           <option value="{{$paymentmethods->methodid}}">{{$paymentmethods->paymentmethodname}}</option>
                                                                          @endforeach
                                                                           No account created
                                                                        @endif
                                                                         </select>
                                                                      </div>


                                                        </div>
                                                   </div> 




                                                   <div class="col-md-4">
                                                     <h1><small>Quantity Values</small></h1>
                                                     <hr>
                                                        <div class="form-group">
                                                             <label for="tags">On Hand Quantity</label>
                                                             <input type="number" class="form-control" name="onhandquantity" placeholder="Initial quantity">
                                                         </div>


                                                         <div class="form-group">
                                                             <label for="tags">Allocated Quantity</label>
                                                             <input type="number" class="form-control" name="allocatedquantity" placeholder="Quantity you already sold">
                                                         </div>

<!-- 
                                                         <div class="form-group">
                                                             <label for="tags">Available Quantity</label>
                                                             <input type="number" class="form-control" name="availablequantity" placeholder="on">
                                                         </div> -->
                                                              

                                                   </div>  

 
                                                  
                                     </div>


                                      <div class="col-md-12">
                                        
                                       <div class="box box-default collapsed-box">
                                          <div class="box-header with-border">
                                              <h3 class="box-title"  data-widget="collapse">Optional Item Details</h3>

                                                <div class="box-tools pull-right">
                                              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                              </button>
                                              </div>
                                            <!-- /.box-tools -->
                                          </div>
                                              <!-- /.box-header -->
                                                <div style="display: none;" class="box-body">
                                                  
                                                        <div class="OptionalDetails">
                                                          
                                                           <div class="form-group">
                                                                 <label for="supplier">Expiration Date</label>

                                                                    <div class="input-group">
                                                                    <div class="input-group-addon">
                                                                      <i class="fa fa-calendar"></i>
                                                                    </div>
                                                                    <input class="form-control pull-right active" name="expiredata" id="purchaseOrderdata" type="text">
                                                                  </div>
                                                                  
                                                                </div>


                                                             <div class="form-group">
                                                              <label for="category">Brand</label>
                                                            <select class="form-control select2" name="brand" style="width: 100%;">
                                                               
                                                               @if(count($brands))
                                                            <!--  <option value="#">Choose Supplier</option> -->
                                                                @foreach($brands as $brands)
                                                                      <option value="{{$brands->brandName}}">{{$brands->brandName}}</option>
                                                                @endforeach
                                                             @else
                                                                <option value="">No brand added yet</option>}
                                                              @endif
                                                              </select>
                                                         </div>


                                                         <div class="form-group">
                                                             <label for="tags">Tags</label>
                                                             <input type="text" class="form-control" name="tag" placeholder="e.g Summer">
                                                         </div>

                                                         <div class="form-group">
                                                                <label for="size">Size</label>
                                                                <input type="text" name="size" placeholder="e.g 5ml, medium" id="size" class="form-control">
                                                        </div>
                                                         
                                                         <div class="form-group">
                                                                <label for="color">Color</label>
                                                                <input type="text" name="color" placeholder="Product color" id="color" class="form-control">
                                                        </div>

                                                          <div class="form-group">
                                                            <label for="descriptions">Product Descriptions</label>
                                                            <textarea name="descriptions" placeholder="Give any description if available" class="form-control"></textarea>
                                                          </div>
                                                      </div>     
                                                         
                                                 </div>
                                                <!-- /.box-body -->
                                      </div>
                                        

                                             <div class="modal-footer">
                                                   <button type="reset" class="btn btn-default">Reset</button>
                                                   <button type="submit" id="producsSubmit" class="btn btn-success">Save</button>
                                            </div>

                                      </div>

                                              
                                          </form>
                             </div><!-- end row -->
                      
                      </div><!-- /.CREATE USER tab-pane -->
                               
                      

                                

                                      <!-- TAB3 -->
                                      <div class="tab-pane" id="assign_permissionTroleTable">
                                         <div class="row">
                                             <div class="col-md-12">
                                                            <div class="row">
                                                      <div class="col-md-12">
                                                         <div class="btn-group pull-right" role="group" aria-label="...">
                                                              <button type="button" class="btn btn-success btn-flat" data-toggle="modal" data-target=".categorynew"><i class="fa fa-plus"></i> New Category</button>
                                                              <button type="button" id="deletemultiplecat" onclick="return confirm('Are you sure you want to delete this category(s)?');" class="btn bg-red btn-flat"><i class="fa fa-trash"></i> Delete</button>
                                                              <button type="button" class="btn bg-orange btn-flat" data-toggle="modal" data-target="#excel"><i class="fa fa-reply-all"></i> Import From Excel</button>
                                                              <button type="button" class="btn bg-maroon btn-flat"><i class="fa fa-location-arrow"></i> Export To Excel</button>
                                                          </div>
                                                      </div>
                                                 </div><!-- endcol-12 -->
                                           <div  class="box box-default" style="margin-bottom:0px;"></div> <!-- blue line  on top-->

                                          <div class="row">
                                                <div class="col-xs-12">
                                                  @if(count($categories))
                                                    <div class="box">
                                                      <div class="box-header">
                                                        <h3 class="box-title text-center">List of Products Categories</h3>
                                                        
                                                      </div><!-- /.box-header -->
                                                      <div class="box-body table-responsive no-padding">
                                                        <table id="categorydataTable" class="table table-bordered table-hover">
                                                         
                                                      <thead>
                                                          <tr>
                                                          <th><i class="fa fa-th"></i></th>
                                                            <th>Name</th>
                                                  
                                                            <th>Edit</th>
                                                            <th>Delete</th>
                                                          </tr>
                                                          </thead>
                                                         <tbody>
                                                        @foreach($categories as $category)
                                                          <tr>
                                                               <td><label><input type="checkbox" name="categoryids[]" value="{{$category->id}}" class="flat-red"></label></td>
                                                               <td>{{$category->catName}}</td>
                                              
                                                      
                                                            <td><a href="#" data-toggle="modal" data-target="#{{$category->id}}"><i class="fa fa-edit"></i> Edit</a></td>
                                                            <td><a href="/product/deletecategory/{{$category->id}}" onclick="return confirm('Are you sure you want to delete this category?');"><i class="fa fa-trash-o" style="color:red"></i> Delete</a></td>
                                                          </tr>
                                                      @endforeach 
                                                        </tbody></table>
                                                      </div><!-- /.box-body -->
                                       
                                                    </div><!-- /.box -->
                                                    @else
                                                    <hr>
                                              

                                                       <div class="alert alert-warning col-xs-6 col-xs-push-3" role="alert">
                                                          <button type="button" class="close"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                          <center><strong>Warning!</strong> No! Categories now.</center>
                                                      </div>
                                                    @endif


                                          </div>
                                             </div>
                                         </div>
                                         </div>

                                        
                                      </div><!-- /.ASSIGN PERMISSION TO ROLE tab-pane -->

                                      <!-- TAB4 -->
                                     <!--  <div class="tab-pane" id="assignUser_toRoleTab">
                                                settings4
                                       </div> --><!-- /.ASSIGN USER ROLE tab-pane -->

    </div><!-- /.tab-content -->
  </div><!-- nav-tabs-custom -->
  </div><!-- end col-md-12 for custom tabs -->
           </div> <!-- Main row -->
         
        </section><!-- /.content -->

   

   <!-- EDIT PRODUCTS -->


       <div class="modal fade" id="editproductmodal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
             <div class="modal-dialog modal-lg">
                     <div class="modal-content">
                             <div class="modal-header">
                                   <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <h4 class="modal-title" id="myModalLabel">Edit Item</h4>
                             </div>
                                 <div class="modal-body">
                                        

                                           
                                        
                                          <div class="box-body">
                                              <div class="col-md-12">
                                                
                                                    <div class="col-md-4">
                                                             <h1><small>Product Details</small></h1>
                                                  <hr>
                                                   <div class="form-group">
                                                            <label for="name" style="color:red">Product Name</label>
                                                            <input type="text" name="editproductname" value=""  placeholder="Product Name" id="name" class="form-control" required>
                                                      </div>


                                                          <div class="form-group">
                                                              <label for="category">Product Category</label>
                                                                <select class="form-control select2" name="editcategory" id="editcategory" style="width: 100%;">
                                                              
                                                               @if(count($categories))
                                                            <!--  <option value="#">Choose Supplier</option> -->
                                                                @foreach($categories as $category)
                                                                      <option value="{{$category->catName}}">{{$category->catName}}</option>
                                                                @endforeach
                                                             @else
                                                                <option value="">No category added yet</option>}
                                                              @endif
                                                              </select>
                                                         </div>

                                              
                                                       
                                                    <div class="form-group">
                                                            <label for="supplier">Supplier</label>
                                                           <select name="editsupplier" id="editsupplier" class="form-control" required>
                                                             @if(count($supplier2))
                                                             <option value="{{ $supplier->supplier_id }}"></option>
                                                                @foreach($supplier2 as $supplier)
                                                                      <option value="{{$supplier->id}}">{{$supplier->supplierName}}</option>
                                                                @endforeach
                                                             @else
                                                                <option value="">No supplier added yet</option>}
                                                              @endif
                                                           </select>
                                                    </div>

                                                    <div class="form-group">
                                                              <label for="barcode">SKU</label>
                                                              <input type="text" class="form-control" value="" name="editbarcode" placeholder="e.g Barcoce Number" id="editbarcode" class="form-control">
                                                      </div>


                                                     <div class="form-group">
                                                            <label for="store">product Location</label>
                                                           <select name="editstore" class="form-control" id="editstore" required>
                                                             @if(count($stores))
                                                             <option value="{{$store->store_id}}"></option>
                                                                @foreach($stores as $store)
                                                                      <option value="{{$store->id}}">{{$store->storeName}}</option>
                                                                @endforeach
                                                             @else
                                                                <option value="">No store  added yet</option>}
                                                              @endif
                                                           </select>
                                                    </div>
                                                   </div> 


                                                    <div class="col-md-4">
                                                      
                                                            <h1><small>Pricing Details</small></h1>
                                                          <hr>



                                                          <div class="form-group">
                                                             <label for="tags">Initial Cost Price(include handling cost)</label>
                                                             <div class="input-group">
                                                             <input type="number" class="form-control" name="edittotalPurchasingCost" value="" placeholder="e.g 10.00">
                                                             <span class="input-group-addon">.00</span>
                                                             </div>
                                                         </div>

                                                         <div class="form-group">
                                                             <label for="tags">Buy Price(price from supplier)</label>
                                                             <div class="input-group">
                                                             <input type="number" class="form-control" name="editsupplierPricelist" value=""   placeholder="e.g 10.00">
                                                             <span class="input-group-addon">.00</span>
                                                             </div>
                                                         </div>

                                                         <div class="form-group">
                                                                  <label for="editsellingprice" style="color:red">Retailer Price(selling price)</label>
                                                                  
                                                                  <div class="input-group">
                                                                  <input type="number" name="editsellingprice" value=""  placeholder="Selling Price" id="sellingprice" class="form-control" required>
                                                                     <span class="input-group-addon">.00</span>
                                                                  </div>
                                                          </div>

                                                           <div class="form-group">
                                                                  <label for="sellingprice" style="color:red">Prices Include VAT</label>
                                                                    <!-- <select class="form-control select2" name="vat" style="width: 100%;" required>
                                                                      <option value="1">INCLUSIVE</option>
                                                                       <option value="9">EXCLUSIVE</option>
                                                                    </select> -->
                                                                   <!--  <input type="number" name="vat" class="form-control"> -->
                                                               
                                                                    <div class="input-group">
                                                                    <input class="form-control" type="number" value=""  placeholder="e.g 18" name="editvat">
                                                                    <span class="input-group-addon">%</span>
                                                                  </div>
                                                          </div>

                                                    </div>

                                                    <div class="col-md-4">
                                                      

                                                        <h1><small>Quantity Values</small></h1>
                                                     <hr>
                                                        <div class="form-group">
                                                             <label for="tags">On Hand Quantity</label>
                                                             <input type="number" class="form-control" name="editonhandquantity" value=""  placeholder="Initial quantity">
                                                         </div>


                                                         <div class="form-group">
                                                             <label for="tags">Allocated Quantity</label>
                                                             <input type="number" class="form-control" name="editallocatedquantity" value="" placeholder="Quantity you already sold">
                                                         </div>


                                                         <div class="form-group">
                                                             <label for="tags">Available Quantity</label>
                                                             <input type="number" class="form-control" name="editavailablequantity" value="" placeholder="available quantity">
                                                         </div>
                                                    </div>
                                              
                                              </div>



                                  <div class="col-md-12 productOptions">
                                                 
                                  <div class="box box-default collapsed-box">
                                    <div class="box-header with-border">
                                        <h3 class="box-title"  data-widget="collapse">Optional Item Details</h3>

                                          <div class="box-tools pull-right">
                                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                        </button>
                                        </div>
                                      <!-- /.box-tools -->
                                        </div>
                                        <!-- /.box-header -->
                                          <div style="display: none;" class="box-body">
                                            
                                                           <div class="OptionalDetails">
                                                           <div class="form-group" >
                                                                 <label for="supplier">Expiration Date</label>

                                                                    <div class="input-group">
                                                                    <div class="input-group-addon">
                                                                      <i class="fa fa-calendar"></i>
                                                                    </div>
                                                                    <input  class="form-control pull-right active editproduct" value="" name="editexpiredata" id="editproductdate" type="text">
                                                                  </div>
                                                                  
                                                                </div>


                                                             <div class="form-group">
                                                              <label for="category">Brand</label>
                                                            <select class="form-control select2" name="editbrand" style="width: 100%;">
                                                               
                                                               @if(count($brands))
                                                            <!--  <option value="#">Choose Supplier</option> -->
                                                                @foreach($brands as $brands)
                                                                      <option value="{{$brands->brandName}}">{{$brands->brandName}}</option>
                                                                @endforeach
                                                             @else
                                                                <option value="">No brand added yet</option>}
                                                              @endif
                                                              </select>
                                                         </div>


                                                         <div class="form-group">
                                                             <label for="tags">Tags</label>
                                                             <input type="text" class="form-control" name="edittag" value=""  placeholder="e.g Summer">
                                                         </div>

                                                         <div class="form-group">
                                                                <label for="size">Size</label>
                                                                <input type="text" name="editsize" placeholder="e.g 5ml, medium" value=""  id="editsize" class="form-control">
                                                        </div>
                                                         
                                                         <div class="form-group">
                                                                <label for="color">Color</label>
                                                                <input type="text" name="editcolor" placeholder="Product color" value=""  id="editcolor" class="form-control">
                                                        </div>

                                                          <div class="form-group">
                                                            <label for="descriptions">Product Descriptions</label>
                                                            <textarea name="editdescriptions" placeholder="Give any description if available" value=""  class="form-control"></textarea>

                                                            <input type="hidden" name="editasdhgasha" value="" style="display:none">
                                                            <input type="hidden" name="_editproductstoken" value="<?php echo csrf_token(); ?>">
                                                          </div>
                                                          
                                                      </div>
                                           </div>
                                              <!-- /.box-body -->
                                  </div><!-- END TABLE FOR SHOWING DETAILS ALREADY ENTERED -->
                              </div>

                                                     

                                            </div>

                                            <div class="modal-footer">
                                                   <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                   <button type="button" class="btn btn-primary" id="saveeditedproducts">Save</button>
                                            </div>
                                         



                                 </div>

                     
                     </div>

               </div>
       </div>









 <!-- VIEW INVENTORY MODAL -->

    <div class="modal fade" id="viewinventorymodal" tabindex="-1" role="dialog" aria-labelledby="myinventoryLargeModalLabel">
             <div class="modal-dialog modal-lg">
                     <div class="modal-content">
                             <div class="modal-header">
                                   <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <h4 class="modal-title" id="myModalLabel invproductname">Inventory Details</h4>
                             </div>
                                 <div class="modal-body">
                                        
                                        <div class="row">
                                        <div class="col-md-12">
                                           <div class="panel panel-default">
                                                <div class="panel-body">
                                                 <div class="col-md-4">
                                                      <span style="color:#889096"><i class="fa fa-user margin-r-5"></i>Item supplier by:</span><br>
                                                     <small class="label bg-yellow invsuppliername"></small>
                                                 </div>

                                                 <div class="col-md-4">
                                                      <span style="color:#889096"><i class="fa fa-clock-o margin-r-5"></i>Added On</span><br>
                                            
                                                      <small class="label bg-green invecreatedat"></small>
                                                 </div>

                                                 <div class="col-md-4">
                                                      <span style="color:#889096"><i class="fa fa-book margin-r-5"></i>Expiration Date</span><br>
                                                      <small class="label bg-red investorename"></small>
                                                 </div>

                                                </div>
                                              </div>
                                        </div>
                                            
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                            <div class="box-body table-responsive no-padding">
                                                <table class="table viewinvtable">
                                                    <thead>
                                                          <tr>
                                                          <!-- <th><i class="fa fa-th"></i></th> -->
                                                            <th>SKU</th>
                                                            <th>Name</th>
                                                            <!-- <th>Supplier</th> -->
                                                            <th>Retail Price</th>
                                                            <th>Buy Price</th>
                                                            <th>Total Cost</th>
                                                            <th>Available</th>
                                                            <th>On Hand</th>
                                                            <th>Last Updated</th>
                                                          </tr>
                                                          </thead>

                                                          <tr>
                                                             <td class="invebarcode"></td>
                                                             <td class="inveproname"></td>
                                                             <td class="invesell"></td>
                                                             <td class="invebuy"></td>
                                                             <td class="invepurchprice"></td>
                                                             <td class="inveavailable"></td>
                                                             <td class="invehandqty"></td>
                                                             <td class="invelastupdate"></td>
                                                          </tr>
                                                </table>
                                                </div>
                                            </div>
                                        </div>

                                 </div>

                     
                     </div>

               </div>
       </div>
      






            <!-- NEW CATEGORY MODAL -->
         <div class="modal fade categorynew" id="categorynew" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
             <div class="modal-dialog modal-lg">
                     <div class="modal-content">
                             <div class="modal-header">
                                   <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <h4 class="modal-title" id="myModalLabel">Create New Category</h4>
                             </div>
                                 <div class="modal-body">
                                      <form class="form-group" action="product/createcategory" method="post">
                                       {!! csrf_field() !!}
                                           <div class="form-group">
                                              <label class="">Category Name</label>
                                              <input type="text" name="catName" class="form-control" required="true">
                                           </div>
                                     

                                      <div class="modal-footer">
                                                   <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                   <button type="submit" class="btn btn-primary">Save</button>
                                            </div> 
                                            </form>
                                 </div>
                        </div>


              </div>
          </div>

      @if(count($categories))
      @foreach($categories as $categ)
          <div class="modal fade category" id="{{$categ->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
             <div class="modal-dialog modal-lg">
                     <div class="modal-content">
                             <div class="modal-header">
                                   <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <h4 class="modal-title" id="myModalLabel">Create New Category</h4>
                             </div>
                                 <div class="modal-body">
                                      <form class="form-group" action="product/updatecategory" method="post">
                                       {!! csrf_field() !!}
                                           <div class="form-group">
                                              <label class="">Category Name</label>
                                              <input type="text" name="catName" value="{{$categ->catName}}" class="form-control" required="true">
                                               <input type="hidden" name="catId" value="{{$categ->id}}" class="form-control" required="true">
                                           </div>
                                     

                                      <div class="modal-footer">
                                                   <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                   <button type="submit" class="btn btn-primary">Save</button>
                                            </div> 
                                            </form>
                                 </div>
                        </div>


              </div>
          </div>
          @endforeach
          @endif

 <!-- END INVENTORY MODAL -->
       
  <!-- form to upload -->     
<!-- <form method="POST" action="/product/importExcel"  enctype="multipart/form-data">
    {!! csrf_field() !!}
 </form> -->
 {!! Form::open(['url' => '/product/importExcel', 'files'=>true]) !!}
       
       @include('partials.uploadform')
     
 {!! Form::close() !!}

</div><!-- /.content-wrapper -->

            
        @include('includes.footer')
         
@stop
