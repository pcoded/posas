@extends('master')
@section('title', 'csms-voucher')
@section('content')
@include('includes.header')
@include('includes.sidebar')

             <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <i class="fa fa-shopping-cart"></i> Stock
            <small>Control panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="/home"><i class="fa fa-home"></i> Home</a></li>
            <li class="active"><a href="/stock">Stock</a></li>
          </ol>
        </section>
         <div  class="box box-primary"></div> <!-- blue line  on top-->


         <div class="pad margin no-print">
         <div class="panel panel-default">
         <div class="panel-body">
            <div class="row">
                 <div class="col-md-12">
                     <div class="col-md-1"></div>
                     
                   <div class="col-md-8" >
                      <a class="btn btn-app bg-orange" onclick="purchasePrint()">
                               <i class="fa fa-print"></i> Print
                          </a>

                          

                          <a class="btn btn-app btn bg-navy" href="#">
                               <i class="fa fa-envelope-o"></i> Email Voucher
                          </a>

                          <a class="btn btn-app btn bg-maroon" href="#">
                               <i class="fa fa-file-pdf-o"></i> PDF
                          </a>

                          <a class="btn btn-app btn bg-maroon"  data-toggle="modal" data-target=".supplierReceipt" onclick="history.go(-1)" href="#">
                               <i class="fa fa-repeat"></i> Previous Page
                          </a>

                          <a class="btn btn-app btn bg-orange" href="{{$thispNumber}}">
                               <i class="fa  fa-file-text-o"></i> Payment Voucher
                          </a>

                          <a class="btn btn-app btn bg-olive" href="/stock">
                               <i class="fa  fa-cart-plus"></i> New Purchase
                          </a>
                   </div>
                   <div class="col-md-2 text-center" style="font-weight:60px; margin-top:20px;">
                       @foreach($purchaseSharedDetails as $purchaseshare)
                         <button class="btn btn-primary"> Approve Payment</button>
                       @endforeach
                   </div>
                 </div>
            </div>
            </div>
            </div>
         </div>


         <section class="invoice purchaseInvoice">

                <!-- title row -->
            <div class="row">
              <div class="col-xs-12">
                <h2 class="page-header">
                  <i class="fa fa-globe"></i> csms, Inc.
                 <small class="pull-right">Printed on: <?php echo date('d-m-Y')?></small>
                </h2>
              </div>
              <!-- /.col -->
            </div>




    <div class="row">
        <div class="col-xs-12">
            <div class="text-center">
                <h2><small><strong>PAYMENT VOUCHER</strong></small></h2>
            </div>
            <hr>
            <div class="row">
                <div class="col-xs-12 col-md-3 col-lg-3 pull-left">
                    <div class="panel panel-default height">
                        <div class="panel-heading"><strong>Payment To:</strong></div>
                        <div class="panel-body">
                            <strong>{{$purchaseshare->supplierName}}</strong><br>
                            Address: {{$purchaseshare->supplierAddress}}<br>
                            Phone: {{$purchaseshare->supplierPhoneNumber}}<br>
                            Email: {{$purchaseshare->supplierEmail}}<br>
                          
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-3 col-lg-3">
                    <div class="panel panel-default height">
                        <div class="panel-heading"><strong>Supplier Invoice #</strong></div>
                        <div class="panel-body">
                           <input type="text" name="supplierInvoiceNumber" class="form-control" placeholder="Enter Supplier Invoice Number">
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-3 col-lg-3">
                    <div class="panel panel-default height">
                        <div class="panel-heading"><strong>Payment Method</strong></div>
                        <div class="panel-body">
                            <select class="form-control">
                                <option>Cash</option>
                                <option>Credit</option>
                                <option>M-Pesa</option>
                                <option>Tigo-Pesa</option>
                            </select>
                        </div>
                    </div>
                </div> 
                 @if(count($settings))
                <div class="col-xs-12 col-md-3 col-lg-3 pull-right">
                    <div class="panel panel-default height">
                        <div class="panel-heading"><strong>Payment From:</strong></div>
                        <div class="panel-body">
                        @foreach($settings as $setting)
                            <strong>{{$setting->companyName}}</strong><br>
                            {{$setting->companyAddress1}}<br>
                           Phone: {{$setting->companyContact1}}<br>
                           E-mail: {{$setting->companyEmail}}<br>
                           
                        @endforeach
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>

  <!--   <div class="row">
         <div class="text-center">
                <h2>PAYMENT VOUCHER</h2>
            </div>
      
    </div> -->


    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="text-center"><strong>Order summary</strong></h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-condensed" id="vouchertable">
                            <thead>
                                <tr>
                                    <td><strong>Item Name</strong></td>
                                     <td class="text-center"><strong>Item Quantity</strong></td>
                                    <td class="text-center"><strong>Item Price</strong></td>
                                   
                                    <td class="text-right"><strong>Total</strong></td>
                                </tr>
                            </thead>
                            <tbody>
                           @foreach($purchasedetails as $purchasez)
                                <tr>
                                    <td>{{$purchasez->productName}}</td>
                                    <td class="text-center">{{$purchasez->itemQuantity}}</td>
                                    <td class="text-center">{{$purchasez->itemPrice}}</td>
                                    <td class="text-right subtotalvoucher">{{$purchasez->itemQuantity * $purchasez->itemPrice}}</td>
                                </tr>
                                @endforeach



                               <!--  <tr>
                                    <td class="highrow"></td>
                                    <td class="highrow"></td>
                                    <td class="highrow text-center"><strong>Subtotal</strong></td>
                                    <td class="highrow text-right">$958.00</td>
                                </tr>
                                <tr>
                                    <td class="emptyrow"></td>
                                    <td class="emptyrow"></td>
                                    <td class="emptyrow text-center"><strong>Shipping</strong></td>
                                    <td class="emptyrow text-right">$20</td>
                                </tr>
                                <tr>
                                    <td class="emptyrow"><i class="fa fa-barcode iconbig"></i></td>
                                    <td class="emptyrow"></td>
                                    <td class="emptyrow text-center"><strong>Total</strong></td>
                                    <td class="emptyrow text-right">$978.00</td>
                                </tr> -->


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>



                                  <div class="row">
                                           <div class="col-md-12">
                                                  <div class="col-md-6"></div>
                                                  <div class="col-md-6">
                                             <ul style="list-style-type:none;">
                                
                                    <input type="hidden" name="purchasetaxreceiptvoucher" value="{{$purchasez->purchasevat}}">
                                    <li class="row-fluid">
                                    <span class="span6"><strong>SubTotal</strong></span>
                                    <span class="span6 purchasereceiptsubtotalvoucher" style="float:right;font-size:20px; font-weight:bold;">Tsh 0.00</span>
                                   </li> <hr> 
                                                           
                                    <li class="row-fluid">
                                    <span class="span6"><strong>Plus VAT</strong></span>
                                    <span class="span6 purchasevatreceiptvoucher" style="float:right;font-size:20px; font-weight:bold;">{{$purchasez->itemPrice * $purchasez->purchasevat}}</span>
                                   </li> <hr> 
    
                                    <li class="row-fluid">
                                    <span class="span6"><strong>Total Cost</strong></span>
                                    <span class="span6 purchasesupertotalvoucher" style="float:right;font-size:20px; font-weight:bold;">{{$purchasez->itemPrice * $purchasez->purchasevat + $purchasez->itemPrice}}</span>
                                   </li>

                                   <hr> 
                                </ul>
                               </div>
                                          
                            </div>
                          </div>

                    <br>

                          <div class="row">
                             <div class="col-xs-12 col-md-3 col-lg-4">
                               ...............................................<br>
                               <strong>Prepared by</strong>
                             </div>

                             <div class="col-xs-12 col-md-4 col-lg-4">
                               .................................................<br>
                               <strong>Approved by</strong>
                             </div>

                             <div class="col-xs-12 col-md-4 col-lg-4">
                               ..................................................<br>
                               <strong>Received by</strong>
                             </div>
                          </div>




      </section>

         </div>
            
@include('includes.footer')
         
@stop
