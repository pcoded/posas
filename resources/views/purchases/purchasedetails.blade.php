@extends('master')
@section('title', 'Stock-Purchase')
@section('content')
  @include('includes.header')
   @include('includes.sidebar')


              <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <i class="fa fa-shopping-cart"></i> Purchase
            <small>Control panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="/home"><i class="fa fa-home"></i> Home</a></li>
            <li class="active"><a href="/purchase">Purchase</a></li>
          </ol>
        </section>
         <div  class="box box-primary"></div> <!-- blue line  on top-->




     
         <div class="pad margin no-print">
         <div class="panel panel-default">
         <div class="panel-body">
            <div class="row">
                 <div class="col-md-12">
                     <div class="col-md-1"></div>
                     
                 	 <div class="col-md-8" >
                 	 	  <a class="btn btn-app bg-orange" onclick="purchasePrint()">
                               <i class="fa fa-print"></i> Print
                          </a>

                          

                          <a class="btn btn-app btn bg-navy" href="#">
                               <i class="fa fa-envelope-o"></i> Email Purchase
                          </a>

                          <a class="btn btn-app btn bg-maroon" href="#">
                               <i class="fa fa-file-pdf-o"></i> PDF
                          </a>

                          <a class="btn btn-app btn bg-maroon"  data-toggle="modal" data-target=".supplierReceipt" href="#">
                               <i class="fa fa-file-pdf-o"></i> Receive
                          </a>

                        <!--   <a class="btn btn-app btn bg-orange" href="voucher/{{$thispNumber}}">
                               <i class="fa  fa-file-text-o"></i> Payment Voucher
                          </a> -->

                          <a class="btn btn-app btn bg-olive" href="/purchases">
                               <i class="fa  fa-cart-plus"></i> New Purchase
                          </a>
                 	 </div>
                 	 <div class="col-md-2 text-center" style="font-weight:60px; margin-top:20px;">
                       @foreach($purchaseSharedDetails as $purchaseshare)
                          @if($purchaseshare->purchaseStatus == 0)
                             <h4><span class="label label-primary">Order Active</span></h4>
                          
                               @elseif($purchaseshare->purchaseStatus == 1)
                                <h4><span class="label label-info">Awaiting Payment</span></h4>
                                @else
                                 <h4><span class="label label-success">Order Closed</span></h4>
                               @endif
                       @endforeach
                   </div>
                 </div>
            </div>
            </div>
            </div>
         </div>
       
        <section class="invoice purchaseInvoice">

          <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-globe"></i> csms, Inc.
           <small class="pull-right">Printed on: <?php echo date('d-m-Y')?></small>
          </h2>
        </div>
        <!-- /.col -->
      </div>

                    <div class="row invoice-info">
                    <div class="text-center">
                        <h1>Purchase Order</h1>
                    </div>
                   </div>

                                  <div class="row">
                                           <div class="col-xs-12">
                                           
                                           @if(count($settings))
                                             <div class="col-xs-6">
                                              <strong>From:</strong>
                                             @foreach($settings as $setting)
                                              <h4><small>{{$setting->companyName}}</small></h4>
                                              
                                              <h4><small>{{$setting->companyAddress1}}</small></h4>
                                              
                                              <h4><small>Phone: {{$setting->companyContact1}}</small></h4>

                                              <h4><small>E-mail: {{$setting->companyEmail}}</small></h4>
                                             @endforeach

                                             </div>
                                             @endif
                                             <div class="col-xs-6 text-right">
                                           
                                              <table class="table" >
                                             
                                                <tr>
                                                  <td style="border:none;"><h4><small><strong>Order #:&nbsp;&nbsp;&nbsp;</strong> {{$thispNumber}}</small></h4></td>
                                                  
                                                </tr>

                                                <tr>
                                                @foreach($purchaseSharedDetails as $purchaseshare)
                                                  <td style="border:none;"><h4><small><strong>Order Date:</strong> {{$purchaseshare->purchaseDueDate}}</small></h4></td>
                                                  
                                                </tr>
                                                <tr>
                                                  <td style="border:none;"><h4><small><strong>Exp. Delivery Date:</strong> {{$purchaseshare->deliveryDate}}</small></h4></td>
                                                </tr>
                                               
                                        
                                             
                                              </table>
                                             

                                             </div>
                                           </div>
                                        </div>
                                        <hr>

                                

                                          <div class="row">
                                           <div class="col-xs-12">
                                            <div class="col-xs-4">
                                              <h4>
                                                    <small>
                                                        <strong>Vendor:</strong>
                                                        
                                                        <br>{{$purchaseshare->supplierName}}
                                                        <br>Address: {{$purchaseshare->supplierAddress}}
                                                        <br>Phone: {{$purchaseshare->supplierPhoneNumber}}
                                                        <br>Phone: {{$purchaseshare->supplierEmail}}
                                                     
                                                           
                                                    </small>
                                               </h4>
                                            </div>
                                            <div class="col-xs-4">
                                              <h4>
                                                  <small>
                                                       <strong>Bill to:</strong>
                                                           <br>{{$setting->companyName}}
                                                           <br>{{$setting->companyAddress1}}
                                                           <br>{{$setting->companyContact1}}
                                                   </small>
                                               </h4>
                                            </div>
                                            <div class="col-xs-4">
                                              <h4>
                                                  <small>
                                                       <strong>Ship to:</strong>
                                                           <br>{{$purchaseshare->storeName}}
                                                           <br>{{$purchaseshare->storeAddress}}
                                                           <br>{{$purchaseshare->storePhoneNumber}}
                                                   </small>
                                               </h4>
                                            </div>
                                           </div>
                                        </div>
                                        <hr>
                                     @endforeach

                                              <div class="row">
                                                <div class="col-xs-12 table-responsive">
                                                    <table class="table table-bordered" id="purchasereceiptTable">
                                                        <thead>
                                                              <tr>
                                                              <!-- <th><i class="fa fa-th"></i></th> -->
                                                                <th>Item Name</th>
                                                                <th>Quantity</th>
                                                                <!-- <th>After</th> -->
                                                                <th>Item Price</th>
                                                                <!-- <th>Tax</th> -->
                                                                <th>Total</th>
                                                            
                                                              </tr>
                                                              </thead>
                                                              @foreach($purchasedetails as $purchasez)
                                                              <tr>
                                                                 <td>{{$purchasez->productName}}</td>
                                                                 <td>{{$purchasez->itemQuantity}}</td>
                                                                 <td>{{$purchasez->itemPrice}}</td>
                                                                 
                                                                 <td class="purchaseitemsubtotalcost">{{$purchasez->itemQuantity * $purchasez->itemPrice}}</td>
                                                                
                                                              </tr>

                                                              @endforeach
                                                    </table>
                                                </div>
                                            </div>


                                  <div class="row">
                                           <div class="col-xs-12">
                                                  <div class="col-xs-6"></div>
                                                  <div class="col-xs-6">
                                             <ul style="list-style-type:none;">
                                
                                    <input type="hidden" name="purchasetaxreceipt" value="{{$purchasez->purchasevat }}">
                                    <li class="row-fluid">
                                    <span class="span6"><strong>SubTotal</strong></span>
                                    <span class="span6 purchasereceiptsubtotal" style="float:right;font-size:20px; font-weight:bold;">Tsh 0.00</span>
                                   </li> <hr> 
                                                           
                                    <li class="row-fluid">
                                    <span class="span6"><strong>Plus VAT</strong></span>
                                    <span class="span6 purchasevatreceipt" style="float:right;font-size:20px; font-weight:bold;">{{$purchasez->itemPrice * $purchasez->purchasevat }}</span>
                                   </li> <hr> 
    
                                    <li class="row-fluid">
                                    <span class="span6"><strong>Total Cost</strong></span>
                                    <span class="span6 purchasesupertotal" style="float:right;font-size:20px; font-weight:bold;">{{$purchasez->itemPrice * $purchasez->purchasevat  + $purchasez->itemPrice}}</span>
                                   </li>

                                   <hr> 
                                </ul>
                               </div>
                                          
                            </div>
                          </div>



             </section>




        

    <!-- START confirm order MODAL -->
                    <div class="modal fade supplierReceipt" id="supplierReceipt" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                     <div class="modal-dialog modal-lg">
                  
                            <div class="modal-content">
                                     <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                          <h4 class="modal-title" id="myModalLabel">Receiving Goods Guidance</h4>
                                     </div>
                                     
                                     <div class="modal-body">
                                         <div class="tab-content">
                                           <div class="tab-pane active" id="deliveryDetails">
                                               @if(count($itemreceivedetails))
                                <!-- TABLE FOR SHOWING DETAILS ALREADY ENTERED -->
                                <div class="box box-default collapsed-box">
                                    <div class="box-header with-border">
                                    <h3 class="box-title"  data-widget="collapse">View Receiving Details</h3>

                                      <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                    </button>
                                    </div>
                                      <!-- /.box-tools -->
                                        </div>
                                        <!-- /.box-header -->
                                          <div style="display: none;" class="box-body">
                                            The body of the box
                                           </div>
                                              <!-- /.box-body -->
                                            </div><!-- END TABLE FOR SHOWING DETAILS ALREADY ENTERED -->
                                             @else 

                                                 <div class="box box-default collapsed-box">
                                              <div class="box-header with-border">
                                                <h3 class="box-title"  data-widget="collapse">New Delivery Details</h3>

                                                <div class="box-tools pull-right">
                                                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                                  </button>
                                                </div>
                                                 <!-- /.box-tools -->
                                              </div>
                                              <!-- /.box-header -->
                                               <div style="display: none;" class="box-body">
                                                <div class="row">

                                                  <div class="col-md-12">
                                                                            


                                                                       
                                                                              
                                                                           
                                                  <!--  start basic details deliver note -->
                                                    <div class="col-md-6">
                                                      <h1><small>Basic Details</small></h1><hr>
                                                        <div class="form-group">
                                                            <label class="">Delivery Note #</label>
                                                      <input type="text" name="deliveryNumber" class="form-control" placeholder="Enter Delivery Note Number" required="true">
                                                      <input type="hidden" id="formtoken" name="_deliverdetailstoken" value="{{ csrf_token() }}">

                                                        <input type="hidden" name="deliverypoNumber" value="{{$thispNumber}}">  
                                                        </div>

                                                       <div class="form-group">
                                                          <label class="">Quantity Delivered</label>
                                                            <input type="number" name="deliveredQty" class="form-control" placeholder="Enter Quantity Delivered" required="true">
                                                      </div>

                                                        <div class="form-group">
                                                          <label class="">Deliverer Name</label>
                                                            <input type="text" name="deliveredby" class="form-control" placeholder="Enter Delivered By">
                                                        </div>

                                                        <div class="form-group">
                                                        <label class="">Deliverer Contact</label>
                                                          <input type="text" name="deliverercontact" class="form-control" placeholder="Enter Deliverer Contact">
                                                          </div>

                                                            <div class="form-group">
                                                              <label class="">Delivered Date</label>

                                                                                              
                                                               <div class="input-group">
                                                                 <div class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <input class="form-control pull-right active delivernoteDate" name="delivereddate" id="deliverynotedate" type="text">
                                                             </div>
                                                                                                 
                                                            </div>




                                                       </div><!-- end basic details deliver note -->
                                                                                    
                                                        <!-- start item condition details -->
                                                          <div class="col-md-6">
                                                            <h1><small>Item Conditional  Details</small></h1><hr>

                                                            <div class="form-group">
                                                            <label class="">Damaged Quantity</label>
                                                            <input type="number" name="delivereddamaged" class="form-control deliverydamaged" placeholder="Enter Damaged quanty">
                                                            </div>

                                                          <!--  <div class="form-group">
                                                            <input type="checkbox" name="overqty" id="overqty" /> <strong>Over Quantity</strong>
                                                              <input type="number" name="deliveryoverquantity" class="form-control" placeholder="Enter exceeded Quantity">
                                                              </div>

                                                                <div class="form-group">
                                                              <input type="checkbox" name="belowqty" id="belowqty" /> <strong>Below Quantity</strong>
                                                             <input type="number" name="deliverybelowquantity" class="form-control" placeholder="Enter remaining Quantity">
                                                              </div>-->

                                                                <div class="form-group">
                                                                  <label class="">Checked By</label>
                                                                    <input type="text" name="deliverycheckedby" class="form-control" placeholder="Delivered checked by">
                                                                </div>

                                                                    <div class="form-group">
                                                                    <label class="">More Descriptions</label>
                                                                    <textarea name="deliverydescriptions" class="form-control" placeholder="Enter any details regarding this delivery.."></textarea>
                                                                    </div>

                                                                      <div class="form-group">
                                                                         <label for="purchasemethodofpayments">Payment method</label>
                                                                         <select class="form-control select2" id="purchasemethodofpayments" style="width: 100%;" name="paymentmethod">
                                                                         @if(count($paymentmethods))
                                                                          @foreach($paymentmethods as $paymentmethods)
                                                                           <option value="{{$paymentmethods->methodid}}">{{$paymentmethods->paymentmethodname}}</option>
                                                                          @endforeach
                                                                           No account created
                                                                        @endif
                                                                         </select>
                                                                      </div>


                                                            <div class="form-group">
                                                              <label class="">Payment Date</label>

                                                                                              
                                                               <div class="input-group">
                                                                 <div class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <input class="form-control pull-right active delivernoteDate" name="purchasepaymentdate" id="purchasepaymentdate" type="text">
                                                             </div>
                                                                                                 
                                                            </div>


                                                                                         
                                                                </div><!-- end item condition details -->
                                                                <!-- <div class="col-md-4">Other </div> -->
                                                                <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                               <button type="submit" id="receiveAndConfirmPurchase" class="btn btn-primary">Confirm Purchase</button>
                                                                </div>
                                                                </div>

                                                                            
                                                                               
                                                              </div>
                                                                </div>
                                                                <!-- /.box-body -->
                                                              </div>

                                             @endif
                                           </div>
                                         </div>
                                     </div>
                             </div>
                        </div>
                        </div>













        </div>

    @include('includes.footer')
         
@stop