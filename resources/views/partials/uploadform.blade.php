        <div class="modal fade" id="excel" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
             <div class="modal-dialog">
                    <div class="modal-content">
                             <div class="modal-header">
                                   <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <h4 class="modal-title" id="myModalLabel">Choose file to upload</h4>
                             </div>
                             <div class="modal-body">
                              
                                  <input type="file" name="uploadfile" required>
                             </div>

                             <div class="modal-footer">
                                 <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                 <button type="submit" class="btn btn-primary">Import</button>
                             </div>
                            
                    </div>
            </div>
      </div>