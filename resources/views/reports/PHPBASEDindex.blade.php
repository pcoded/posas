@extends('master')
@section('title', 'Reports')
@section('content')
@include('includes.header')
@include('includes.sidebar')

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
           <i class="fa fa-bar-chart"></i> Reports
          
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol>
        </section>
        <div  class="box box-primary"></div> <!-- blue line  on top-->
       
             <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="ion ion-ios-cart-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total Sales</span>
              <span class="info-box-number">{{ $totalsales }}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="ion-ios-folder-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total Items</span>
              <span class="info-box-number">{{ count($products) }}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Purchases</span>
              <span class="info-box-number">{{$totalpurchase}}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total Customer</span>
              <span class="info-box-number">{{ count($customerdata) }}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
      </div><!-- /.row -->
          <!-- Main row -->
          <!-- summary reports -->


      <!-- Main content -->
    <section class="content">
      <div class="row">
           <!-- <div class="col-md-12"> -->
                <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs" id="createNotTab">
                              <li class="active"><a href="#generalReport" data-toggle="tab" aria-expanded="true"><strong>Profit Report</strong></a></li>
                              <li class=""><a href="#reports" data-toggle="tab" aria-expanded="false"><strong>Dev report</strong></a></li>
                            </ul>
                            <div class="tab-content">


                                      <div class="tab-pane active" id="generalReport">
                                        
                  
                                         <!--report settings row -->
                                        <!--  <div class="row"> -->
                                               <div class="breadcrumb">
                                                <h4><strong>Configure report</strong></h4><hr>
                                                       <div class="row">
                                                               <form action="generalProfitReport" method="post">
                                                                 {!! csrf_field() !!}

                                                                  <div class="col-md-12">
                                                                           <div class="col-md-4">
                                                                                  <div class="form-group">
                                                                                        <label for="inputEmail3">From:</label>

                                                                                          <div class="input-group">
                                                                                          <div class="input-group-addon">
                                                                                            <i class="fa fa-calendar"></i>
                                                                                          </div>
                                                                                          <input class="form-control pull-right active" name="reportdatefrom" id="reportdatefrom" type="text" required>
                                                                                        </div>
                                                                                       
                                                                                    </div>
                                                                           </div>

                                                                           <div class="col-md-4">
                                                                                    <div class="form-group">
                                                                                        <label for="inputEmail3">To:</label>

                                                                                          <div class="input-group">
                                                                                          <div class="input-group-addon">
                                                                                            <i class="fa fa-calendar"></i>
                                                                                          </div>
                                                                                          <input class="form-control pull-right active" name="reportdateto" id="reportdateto" type="text" required>
                                                                                        </div>
                                                                                       
                                                                                    </div>
                                                                           </div>

                                                                           <div class="col-md-4">
                                                                                   <div class="form-group">
                                                                                       <label for="inputEmail3">Order By:</label>
                                                                                       <select name="reportorderby" id="reportorderby" class="form-control">
                                                                                            <option value="">Choose option</option>
                                                                                            <option value="1">Item</option>
                                                                                            <option value="2">Sales Person</option>
                                                                                            <option value="3">Customer</option>
                                                                                            <option value="4">Supplier</option>
                                                                                            
                                                                                       </select>
                                                                                      <input type="hidden" id="gpreport" name="_gprtoken" value="{{ csrf_token() }}"> 
                                                                                     </div>
                                                                           </div>
                                                                           <br><br>
                                                                           <div style="margin-left:13px;"><button id="btnsubmitreport" type="submit" class="btn btn-primary btn-lg">Generate Report</button></div>
                                                                           
                                                                  </div>


                                                                 
                                                               </form>
                                                              
                                                       </div>
                                               </div>
                                        <!--  </div> -->
                                           <!-- /.report settings row -->
                                           <br>
                                           <div class="tablar">
                                                  <table id="createGPR" class="table table-hover table-bordered">
                                                      <thead>
                                                              <tr class="register-items-header">
                                                               
                                                                <th>Item Name</th>
                                                                <th>Quantity Sold</th>
                                                                <th>Total Purchase Price</th>
                                                                <th>Total Selling Price</th>
                                                                <th>vat</th>
                                                                <th>Total</th>
                                                                <th>Profit</th>
                                                              </tr>
                                                        </thead>
                                                         <tbody>
                                                          @if($reportsales != '')
                                                            @foreach($reportsales as $report)
                                                               <tr class="itemsalelist"> 
                                                                <td class="itemName">{{$report->productName}}</td> 
                                                                <td>{{$report->totalquantity}}</td> 
                                                                <td>{{$report->totalquantity * $report->productBuyingPrice}}</td>  
                                                                <td>{{$report->totalquantity * $report->productSellingPrice}}</td>
                                                                <td> {{$report->vat* $report->productSellingPrice*$report->totalquantity}}</td> 
                                                                <td class="totalcost">{{$report->totalcost}}</td> 
                                                                <td class="profit">{{$report->totalquantity*$report->productSellingPrice - $report->totalquantity * $report->productBuyingPrice}}</td>
                                                                </tr>
                                                            @endforeach
                                                          @else
                                                          <tr><td colspan="8"><div class="text-center text-warning"><h3>' + "Nothing to show" + '</h3></div></td></tr>
                                                         @endif
                                                        </tbody>
                                                               
                                            
                                                           
                                                    </table>
                                            </div>
                                           <br>

                                           <div class="graphical">
                                                 <h1 class="text-center"><small>Graphical Presentations</small></h1>

                                                              <!-- BAR CHART -->
                                                    <div class="box box-success">
                                                      <div class="box-header with-border">
                                                        <h3 class="box-title">Bar Chart</h3>

                                                        <div class="box-tools pull-right">
                                                          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                                          </button>
                                                          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                                        </div>
                                                      </div>
                                                      <div class="box-body">
                                                        <div class="chart">
                                                          <canvas id="barChart" style="height:230px"></canvas>
                                                        </div>
                                                      </div>
                                                      <!-- /.box-body -->
                                                    </div>
                                                    <!-- /.box -->

                                           </div>

                                      </div>
                                        
                                        <!-- Start another table -->
                                      <div class="tab-pane" id="reports">
                                             

                                      ANOTHER TAB
                                      </div>

                            </div>
                </div>
                  <!-- /.tabs -->
            
          <!--  </div> -->
             <!-- /.col-md-12 -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->

  
        </section><!-- /.content -->
        
        </div><!-- end content-wrapper -->
            
@include('includes.footer')
         
@stop
