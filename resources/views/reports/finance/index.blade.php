@extends('master')
@section('title', 'Reports')
@section('content')
@include('includes.header')
@include('includes.sidebar')

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
           <i class="fa fa-bar-chart"></i> Reports
          
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol>
        </section>
        <div  class="box box-primary"></div> <!-- blue line  on top-->
       
             <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="ion ion-ios-cart-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total Sales</span>
              <span class="info-box-number">{{ $totalsales }}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="ion-ios-folder-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total Items</span>
              <span class="info-box-number">{{ count($products) }}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Purchases</span>
              <span class="info-box-number">{{$totalpurchase}}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total Customer</span>
              <span class="info-box-number">{{ count($customerdata) }}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
      </div><!-- /.row -->
          <!-- Main row -->
          <!-- summary reports -->


      <!-- Main content -->
    <section class="content">
      <div class="row">
           <!-- <div class="col-md-12"> -->
                <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs" id="createNotTab">
                              <li class="active"><a href="#profitandloss" data-toggle="tab" aria-expanded="true"><strong>Profit & Loss</strong></a></li>
                              <li><a href="#balancesheet" data-toggle="tab" aria-expanded="true"><strong>Balance Sheet</strong></a></li>
                            </ul>
                            <div class="tab-content">


                                <!-- Start account reports tabs -->
                                <!-- profit and loss statment -->
                                <div class="tab-pane active" id="profitandloss">
                                    <div class="row">
                                         <div class="col-xs-12">
                                            <div class="breadcrumb">
                                                <h4><strong>Profit & Loss Report Configuration</strong></h4><hr>
                                                       <div class="row">
                                                    

                                                                  <div class="col-md-12">
                                                                           <div class="col-md-5">
                                                                                  <div class="form-group">
                                                                                        <label for="inputEmail3">From:</label>

                                                                                          <div class="input-group">
                                                                                          <div class="input-group-addon">
                                                                                            <i class="fa fa-calendar"></i>
                                                                                          </div>
                                                                                          <input class="form-control pull-right active" name="profitlossfrom" id="profitlossfrom" type="text">
                                                                                        </div>
                                                                                       
                                                                                    </div>
                                                                           </div>

                                                                           <div class="col-md-5">
                                                                                    <div class="form-group">
                                                                                        <label for="inputEmail3">To:</label>

                                                                                          <div class="input-group">
                                                                                          <div class="input-group-addon">
                                                                                            <i class="fa fa-calendar"></i>
                                                                                          </div>
                                                                                          <input class="form-control pull-right active" name="profitlossto" id="profitlossto" type="text">
                                                                                        </div>
                                                                                       
                                                                                    </div>
                                                                           </div>
                                                                             <br>
                                                                           <div class="col-md-2">
                                                                                  <button id="profitlossreportsbtn" type="submit" class="btn btn-primary btn-lg">Update</button>
                                                                                      <input type="hidden" id="profitlosstoken" name="_profitlosstoken" value="{{ csrf_token() }}"> 
                                                                                    
                                                                           </div>
                                                                          <!--  <br>
                                                                           <div style="margin-left:13px;"><button id="btnsubmitreport" type="submit" class="btn btn-primary btn-lg">Generate Report</button></div> -->
                                                                           
                                                                  </div>


                                                                 
                                                              <!--  </form> -->
                                                              
                                                       </div>
                                               </div>
                                         </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="col-xs-2"></div>
                                            <div class="col-xs-8 text-center">
                                               <small style="font-size:15px;"><strong>
                                                 @if(count($companydetails))
                                                   @foreach($companydetails as $key=>$value)
                                                      {{ $value->companyName}}
                                                   @endforeach
                                                 @endif
                                               </strong></small><br>
                                               <small style="font-size:15px;"><strong>Profit & Loss Statement</strong></small><br>
                                               <small style="font-size:15px;"><strong><div id="headerdate"> </div></strong></small>
                                            </div>
                                            <div class="col-xs-2"></div>
                                        </div>
                                    </div>
                                     
                                     <div class="row">
                                       <div class="col-xs-12">
                                           <h4><strong><a href="#">Tranding Income</a></strong></h4><hr style="height:1px;border:none;color:#333;background-color:#333;margin-top:0px;">
                                           <div class="col-xs-6 text-left" style="margin-top:0px;"><strong>Total Sales</strong></div>
                                           <div class="col-xs-6 text-right" style="margin-top:0px; font-weight:bold;font-size: 22px;" id="totalincomesales">R 0.00</div>

                                       </div> 
                                       
                                       <!-- <div class="col-xs-12">
                                          <hr style="margin-top:0px; padding:0px;"><div class="col-xs-6"><strong>Total Trading Income</strong></div>
                                           <div class="col-xs-6 text-right">23534365</div>
                                       </div> -->


                                       <div class="col-xs-12">
                                            <h4><hr style="margin-top:0px;"><strong><a href="#">Cost of Sales</a></strong></h4><hr style="height:1px;border:none;color:#333;background-color:#333;margin-top:0px;">
                                           <div class="col-xs-6 text-left"><strong>Total COGS</strong></div>
                                           <div class="col-xs-6 text-right" style="margin-top:0px; font-weight:bold;font-size: 22px;" id="totalincomecogs">R 0.00</div>
                                       </div> 
                                       
                                     <!--   <div class="col-xs-12" style="margin-top:0px;">
                                           <hr style="margin-top:0px;"><div class="col-xs-6 text-left"><strong>Total Cogs</strong></div>
                                           <div class="col-xs-6 text-right">23534365</div>
                                       </div> -->

                                      
                                       <div class="col-xs-12" style="color:#00A65A">
                                      <hr style="margin-top:0px;">
                                         <div class="col-xs-6 pull-left" style="font-size: 22px;"><strong>Gross Profit</strong></div>
                                          <div class="col-xs-6 text-right" style="margin-top:0px; font-weight:bold;font-size: 22px;" id="totalgrossprofit">R 0.00</div>
                                       
                                      </div>

                                       <div class="col-xs-12">
                                        <hr style="height:4px;border:none;color:#333;background-color:#333;margin-top:0px;">
                                            <h4><strong><a href="#">Expenses</a></strong></h4><hr style="height:1px;border:none;color:#333;background-color:#333;margin-top:0px; ">
                                           <div class="col-xs-6 text-left"><strong>Total Expenses</strong></div>
                                           <div class="col-xs-6 text-right" style="margin-top:0px; font-weight:bold;font-size: 22px;" id="totalexpensesincomestat">R 0.00</div>
                                       </div> 


                                       <div class="col-xs-12" style="color:#00A65A">
                                          <hr style="margin-top:0px;">
                                           <div class="col-xs-6 pull-left" style="font-size: 22px; font-weight:bold;" id="netprofitloss">Net Profit</div>
                                           <div class="col-xs-6 text-right" style="margin-top:0px; font-weight:bold;font-size: 22px;" id="totalnetprofit">R 0.00</div>
                                       
                                      </div>
                                    
                                      <div class="col-xs-12">
                                        <hr style="height:4px;border:none;color:#333;background-color:#333;margin-top:0px;">
                                      </div>

                                    </div>
                                </div>
                                <!--/. profit and loss statment -->
                                   












                                <!-- Balance Sheet -->
                                 <div class="tab-pane" id="balancesheet">
                                       <div class="row">
                                         <div class="col-xs-12">
                                            <div class="breadcrumb">
                                                <h4><strong>Balance Sheet Report Configuration</strong></h4><hr>
                                                       <div class="row">
                                                    
                                                                  <div class="col-md-12">
                                                                           <div class="col-md-5">
                                                                                  <div class="form-group">
                                                                                        <label for="inputEmail3">Balance Opening Date:</label>

                                                                                          <div class="input-group">
                                                                                          <div class="input-group-addon">
                                                                                            <i class="fa fa-calendar"></i>
                                                                                          </div>
                                                                                          <input class="form-control pull-right active" name="balanceopeningdate" id="balanceopeningdate" type="text">
                                                                                        </div>
                                                                                       
                                                                                    </div>
                                                                           </div>

                                                                           <div class="col-md-5">
                                                                                    <div class="form-group">
                                                                                        <label for="inputEmail3">Balance Closind Date:</label>

                                                                                          <div class="input-group">
                                                                                          <div class="input-group-addon">
                                                                                            <i class="fa fa-calendar"></i>
                                                                                          </div>
                                                                                          <input class="form-control pull-right active" name="balanceclosingdate" id="balanceclosingdate" type="text">
                                                                                        </div>
                                                                                       
                                                                                    </div>
                                                                           </div>
                                                                             <br>
                                                                           <div class="col-md-2">
                                                                            <button id="balancesheetbtn" type="submit" class="btn btn-primary btn-lg">Update</button>
                                                                            <input type="hidden" id="balancesheettoken" name="_balancesheettoken" value="{{ csrf_token() }}">        
                                                                           </div>
                                                              
                                                                  </div>
                                                              <!--  </form> -->
                                                              
                                                       </div>

                                               </div>
                                         </div>
                                    </div>


                                        <div class="row">
                                          <div class="col-xs-12">
                                              <div class="col-xs-2"></div>
                                              <div class="col-xs-8 text-center">
                                                 <small style="font-size:15px;"><strong>
                                                   @if(count($companydetails))
                                                     @foreach($companydetails as $key=>$value)
                                                        {{ $value->companyName}}
                                                     @endforeach
                                                   @endif
                                                 </strong></small><br>
                                                 <small style="font-size:15px;"><strong>Balance Sheet</strong></small><br>
                                                 <small style="font-size:15px;"><strong><div id="headerdate">As at 30 September 2016 </div></strong></small>
                                              </div>
                                              <div class="col-xs-2"></div>
                                          </div>
                                      </div>
                                    


                                    <!-- display balancesheet here.. -->
                                     <div class="row">
                                         <div class="col-xs-12">
                                           <h4><strong><a href="#">Assets</a></strong></h4><hr style="height:1px;border:none;color:#333;background-color:#333;margin-top:0px;">
                                          <!--  <div class="col-xs-6 text-left" style="margin-top:0px;"><small>Cash account</small></div>
                                           <div class="col-xs-6 text-right" style="margin-top:0px;" id="totalincomesales">R 0.00</div> -->

                                           <table class="table table-condensed">
                                             <tr>
                                               <td>Cash account</td>
                                               <td class="text-right">R 0.00</td>
                                             </tr>

                                              <tr>
                                               <td>Account Receivable</td>
                                               <td class="text-right">R 0.00</td>
                                             </tr>

                                             <tr>
                                               <td>Inventory Account</td>
                                               <td class="text-right">R 0.00</td>
                                             </tr>

                                             <tr style="color:#00A65A ">
                                               <td><strong>Total Assets</strong></td>
                                               <td class="text-right" ><strong>R 0.00</strong></td>
                                             </tr>

                                           </table>

                                       </div> 



                                         <div class="col-xs-12">
                                           <h4><strong><a href="#">Liabilities</a></strong></h4><hr style="height:1px;border:none;color:#333;background-color:#333;margin-top:0px;">
                                          <!--  <div class="col-xs-6 text-left" style="margin-top:0px;"><small>Cash account</small></div>
                                           <div class="col-xs-6 text-right" style="margin-top:0px;" id="totalincomesales">R 0.00</div> -->

                                           <table class="table table-condensed">
                                             <tr>
                                               <td>Accounts Payable</td>
                                               <td class="text-right">R 0.00</td>
                                             </tr>

                                             <tr style="color:#00A65A">
                                               <td><strong>Total Liabilities</strong></td>
                                               <td class="text-right" ><strong>R 0.00</strong></td>
                                             </tr>

                                           </table>

                                       </div>



                                     <div class="col-xs-12">
                                           <h4><strong><a href="#">Equity</a></strong></h4><hr style="height:1px;border:none;color:#333;background-color:#333;margin-top:0px;">
                                          <!--  <div class="col-xs-6 text-left" style="margin-top:0px;"><small>Cash account</small></div>
                                           <div class="col-xs-6 text-right" style="margin-top:0px;" id="totalincomesales">R 0.00</div> -->

                                           <table class="table table-condensed">
                                            <tr>
                                               <td>Net Income</td>
                                               <td class="text-right">R 0.00</td>
                                             </tr>

                                             <tr>
                                               <td>Owners capital</td>
                                               <td class="text-right">R 0.00</td>
                                             </tr>

                                             <tr style="color:#00A65A">
                                               <td><strong>Total Equity</strong></td>
                                               <td class="text-right" ><strong>R 0.00</strong></td>
                                             </tr>

                                              <tr style="color:#00A65A">
                                               <td><strong>Equity + Liabilities</strong></td>
                                               <td class="text-right" ><strong>R 0.00</strong></td>
                                             </tr>

                                           </table>

                                       </div>



                                     </div>


                                </div>
                                <!-- /. balane sheet -->

                                <!-- /.Accounting reports tabs-content -->



                            </div>
                </div>
                  <!-- /.tabs -->
            
          <!--  </div> -->
             <!-- /.col-md-12 -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->

  
        </section><!-- /.content -->
        
        </div><!-- end content-wrapper -->
            
@include('includes.footer')
         
@stop
