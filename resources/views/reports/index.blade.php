@extends('master')
@section('title', 'Reports')
@section('content')
@include('includes.header')
@include('includes.sidebar')

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
           <i class="fa fa-bar-chart"></i> Reports
          
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol>
        </section>
        <div  class="box box-primary"></div> <!-- blue line  on top-->
       
             <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="ion ion-ios-cart-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total Sales</span>
              <span class="info-box-number">{{ $totalsales }}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="ion-ios-folder-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total Items</span>
              <span class="info-box-number">{{ count($products) }}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Purchases</span>
              <span class="info-box-number">{{$totalpurchase}}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total Customer</span>
              <span class="info-box-number">{{ count($customerdata) }}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
      </div><!-- /.row -->
          <!-- Main row -->
          <!-- summary reports -->


      <!-- Main content -->
    <section class="content">
      <div class="row">
           <!-- <div class="col-md-12"> -->
                <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs" id="createNotTab">
                              <li class="active"><a href="#generalReport" data-toggle="tab" aria-expanded="true"><strong>Detailes Sales Summary</strong></a></li>
                              <li class=""><a href="#reports" data-toggle="tab" aria-expanded="false"><strong>Inventory</strong></a></li>

                              <li class=""><a href="#accountingReports" data-toggle="tab" aria-expanded="false"><strong>Graphical Sales Summary</strong></a></li>
                            </ul>
                            <div class="tab-content">


                                      <div class="tab-pane active" id="generalReport">
                                        
                  
                                         <!--report settings row -->
                                        <!--  <div class="row"> -->
                                               <div class="breadcrumb">
                                                <h4><strong>Configure report</strong></h4><hr>
                                                       <div class="row">
                                                              <!--  <form action="reports/generalProfitReport" method="post">
                                                                 {!! csrf_field() !!} -->

                                                                  <div class="col-md-12">
                                                                           <div class="col-md-4">
                                                                                  <div class="form-group">
                                                                                        <label for="inputEmail3">From:</label>

                                                                                          <div class="input-group">
                                                                                          <div class="input-group-addon">
                                                                                            <i class="fa fa-calendar"></i>
                                                                                          </div>
                                                                                          <input class="form-control pull-right active" name="reportdatefrom" id="reportdatefrom" type="text">
                                                                                        </div>
                                                                                       
                                                                                    </div>
                                                                           </div>

                                                                           <div class="col-md-4">
                                                                                    <div class="form-group">
                                                                                        <label for="inputEmail3">To:</label>

                                                                                          <div class="input-group">
                                                                                          <div class="input-group-addon">
                                                                                            <i class="fa fa-calendar"></i>
                                                                                          </div>
                                                                                          <input class="form-control pull-right active" name="reportdateto" id="reportdateto" type="text">
                                                                                        </div>
                                                                                       
                                                                                    </div>
                                                                           </div>

                                                                           <div class="col-md-4">
                                                                                   <div class="form-group">
                                                                                       <label for="inputEmail3">Order By:</label>
                                                                                       <select name="reportorderby" id="reportorderby" class="form-control">
                                                                                            <option value="">Choose option</option>
                                                                                            <option value="1">Item</option>
                                                                                            <option value="2">Sales Person</option>
                                                                                            <option value="3">Customer</option>
                                                                                            <option value="4">Supplier</option>
                                                                                            
                                                                                       </select>
                                                                                      <input type="hidden" id="gpreport" name="_gprtoken" value="{{ csrf_token() }}"> 
                                                                                     </div>
                                                                           </div>
                                                                           <br><br>
                                                                           <div style="margin-left:13px;"><button id="btnsubmitreport" type="submit" class="btn btn-primary btn-lg">Generate Report</button></div>
                                                                           
                                                                  </div>


                                                                 
                                                              <!--  </form> -->
                                                              
                                                       </div>
                                               </div>
                                        <!--  </div> -->
                                           <!-- /.report settings row -->
                                           <br>
                                           <div class="tablar">
                                                  <table id="createGPR" class="table table-hover table-bordered">
                                                      <thead>
                                                              <tr class="register-items-header">
                                                               
                                                                <th>Item Name</th>
                                                                <th>Quantity Sold</th>
                                                                <th>Total Purchase Price</th>
                                                                <th>Total Selling Price</th>
                                                                <th>vat</th>
                                                                <th>Total</th>
                                                                <th>Profit</th>
                                                              </tr>
                                                        </thead>
                                                         <tbody>
                                                      
                                                          </tbody>
                                                               
                                            
                                                           
                                                    </table>
                                            </div>
                                           

                                                  <br>

                                           <div class="graphical">
                                                 <h1 class="text-center"><small>Graphical Presentations</small></h1>

                                                              <!-- BAR CHART -->
                                                    <div class="box box-success">
                                                      <div class="box-header with-border">
                                                        <h3 class="box-title">Item against Profit Bar Chart</h3>

                                                        <div class="box-tools pull-right">
                                                          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                                          </button>
                                                          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                                        </div>
                                                      </div>
                                                      <div class="box-body">
                                                        <div class="chart">
                                                          <canvas id="barChart" style="height:810px"></canvas>
                                                        </div>


                                                      </div>
                                                      <!-- /.box-body -->
                                                    </div>
                                                    <!-- /.box -->

                                           </div>

                                      </div>
                                        
                                        <!-- Start another table -->
                                      <div class="tab-pane" id="reports">
                                         <div>
                                              <a class="btn btn-app bg-orange" onclick="productPrint()">
                                                 <i class="fa fa-print"></i> Print
                                               </a>
                                         </div>
                                               
                                                 <div class="row">
                                                <div class="col-xs-12">
                                                
                                                    <div class="box">
                                                      <div class="box-header">
                                                        <h3 class="box-title">List of Products</h3>
                                                        
                                                      </div><!-- /.box-header -->
                                                      <div class="box-body table-responsive no-padding">
<!--                                                         <table id="reportsproductsdataTable" class="table table-bordered table-hover productPrint">
                                                         
                                                      <thead>
                                                          <tr>
                                                         
                                                            <th>Name</th>
                                                            <th>Onhand Qty</th>
                                                            <th>Cost Price</th>
                                                            <th>Total</th>
                                                            <th>Selling Price</th>
                                                            <th>Total</th>
                
                                                          </tr>
                                                          </thead>
                                                         <tbody>
                                                         @foreach($products as $prot)
                                                          <tr class="trproducts">
                                                              <td>{{ $prot->productName}}</td>
                                                               <td class="onhandq">{{ $prot->onhandQuantity}}</td>
                                                               <td class="totpurchprice">{{ $prot->productBuyingPrice}}</td>
                                                               <td>{{$prot->onhandQuantity * $prot->productBuyingPrice}}</td>
                                                               <td class="totalsellprice">{{ $prot->productSellingPrice}}</td>
                                                                <td class="tota">{{$prot->onhandQuantity * $prot->productSellingPrice}}</td>
                                                          </tr>
                                                        @endforeach
                                                        <tr>
                                                        <td><strong>Total</strong></td>
                                                         <td id="totalitems"></td>
                                                          <td id="totalprucprice">50</td>
                                                          <td id="totalpruc" style="font-weight:bold;">500</td>
                                                          <td id="totbuy">30</td>
                                                          <td id="totbs" style="font-weight:bold;">30</td>
                                                        </tr>
                                                                                                      
                                                        </tbody>

                                                        </table> -->
                                                      </div><!-- /.box-body -->
                                                     
                                                    </div><!-- /.box -->
                                        
                                                   


                                          </div>

                                         </div> 


                                      </div>



                                       <!-- Start sales summary reports tab -->
                                      <div class="tab-pane" id="accountingReports">
                                          <div class="row">
                                              <div class="col-md-12">
                                                <div class="breadcrumb">
                                                <h4><strong>Sales Summary Report Configuration</strong></h4><hr>
                                                       <div class="row">
                                                    

                                                                  <div class="col-md-12">
                                                                           <div class="col-md-5">
                                                                                  <div class="form-group">
                                                                                        <label for="inputEmail3">From:</label>

                                                                                          <div class="input-group">
                                                                                          <div class="input-group-addon">
                                                                                            <i class="fa fa-calendar"></i>
                                                                                          </div>
                                                                                          <input class="form-control pull-right active" name="salesummaryfrom" id="salesummaryfrom" type="text">
                                                                                        </div>
                                                                                       
                                                                                    </div>
                                                                           </div>

                                                                           <div class="col-md-5">
                                                                                    <div class="form-group">
                                                                                        <label for="inputEmail3">To:</label>

                                                                                          <div class="input-group">
                                                                                          <div class="input-group-addon">
                                                                                            <i class="fa fa-calendar"></i>
                                                                                          </div>
                                                                                          <input class="form-control pull-right active" name="salesummaryto" id="salesummaryto" type="text">
                                                                                        </div>
                                                                                       
                                                                                    </div>
                                                                           </div>
                                                                             <br>
                                                                           <div class="col-md-2">
                                                                              <button id="salessummarybutton" type="submit" class="btn btn-primary btn-lg">Update</button>
                                                                              <input type="hidden" id="salessummarytoken" name="_salessummarytoken" value="{{ csrf_token() }}"> 
                                                                                    
                                                                           </div>
                                               
                                                                  </div>                                                                 
                                                              <!--  </form> -->
                                                              
                                                       </div>
                                               </div>
                                              </div>
                                          </div>

                                           <div class="row">
                                              <div class="col-xs-12">
                                                  <div class="col-xs-2"></div>
                                                  <div class="col-xs-8 text-center">
                                                
                                                     <small style="font-size:15px;"><strong>Sales Summary Report</strong></small><br>
                                                     <small style="font-size:15px;"><strong><div id="headerdate"> </div></strong></small>
                                                  </div>
                                                  <div class="col-xs-2"></div>
                                              </div>
                                        </div>



                                            <div class="row">
                                               <div class="col-md-12">
                                                 <div id="canvasline">
                                                  <canvas id="salessummaryline" style="height:300px;"></canvas>
                                                </div>
                                               </div>
                                            </div>


                                      </div>
                  <!-- /.Sales Summary reports tab-content -->



                            </div>
                </div>
                  <!-- /.tabs -->
            
          <!--  </div> -->
             <!-- /.col-md-12 -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->

  
        </section><!-- /.content -->
        
        </div><!-- end content-wrapper -->
            
@include('includes.footer')
         
@stop
