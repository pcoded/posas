@extends('master')
@section('title', 'Settings')
@section('content')
  @include('includes.header')
   @include('includes.sidebar')

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <i class="fa fa-users"></i> Settings
            <small>Control panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="/home"><i class="fa fa-home"></i> Home</a></li>
            <li class="active"><a href="/settings">Settings</a></li>
          </ol>
        </section>
        <div  class="box box-primary"></div> <!-- blue line  on top-->

          @include('errors.flash')
          @include('errors.formerrors')
          @include('errors.deleteajax')
        <section class="content">



        


       <div class="row">
                                                        
         <div class="col-md-12">
              <h4>Settup company details and other informations</h4>
                  <!-- Custom Tabs -->
                  <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs" id="createNotTab">
                              <li class="active"><a href="#currentsettings" data-toggle="tab" aria-expanded="true"><strong>Shop Settings</strong></a></li>
                                <li class=""><a href="#newsettings" data-toggle="tab" aria-expanded="false"><strong>New Shop Settings</strong></a></li>
                                <!-- <li class=""><a href="#create_permissionTab" data-toggle="tab" aria-expanded="true"><strong>Create Permissions</strong></a></li> -->
                                <li class=""><a href="#othersettings" data-toggle="tab" aria-expanded="false"><strong>Other settings</strong></a></li>
                              
                               
                            </ul>
                            <div class="tab-content">




								       <!--TAB1 -->
								       <div class="tab-pane active" id="currentsettings">
								                 
                       
						                          <table id="listofsettings" class="table table-bordered">
						                              <thead>
						                                  <tr>
						                                       <th><i class="fa fa-th"></i></th>
						                                       <th>Company Name</th>
						                                       <th>Address</th>
						                                       <th>Contact</th>
						                                       <th>Email</th>
						                                       <th>Edit</th>
						                                       <th>Delete</th>
						                                  </tr>
						                              </thead>

						                                  <tbody>
						                                                                                
						                                  </tbody>
						                            
						                          </table>
								       </div><!-- /.CREATE USER tab-pane -->
                               
                      

                                      <!-- TAB2 -->
                                      <div class="tab-pane" id="newsettings">
                                        <div class="row">
					                   {!! Form::open(['url'=>'settings/create']) !!}
					                   {!! csrf_field() !!}
					                           <div class="col-md-12">
					                                 <h1><small>Basic Company Informations</small></h1>
					                                 <hr>

					                       

					                                          <div class="form-group">
					                                            <label for="FullName">Company Name<span style="color:red">*</span></label>
					                                            <input type="text" name="compName" class="form-control" id="CompanyName" placeholder="Company Name">
					                                            <input type="hidden" name="_settingstoken" value="<?php echo csrf_token(); ?>">
					                                          </div>

					                        

					                                          <div class="form-group">
					                                            <label for="companyaddress1">Company Address 1<span style="color:red">*</span></label>
					                                            <input type="text" name="compAddress1" class="form-control" id="companyaddress1" placeholder="Address 1">
					                                          </div>

					                                          <div class="form-group">
					                                            <label for="address">Company Address 2</label>
					                                            <input type="text" name="compAddress2" class="form-control" id="companyaddress2" placeholder="Address 2">
					                                          </div>

					                                          <div class="form-group">
					                                            <label for="country">Company Contact 1<span style="color:red">*</span></label>
					                                            <input type="text" name = "compContact1" class="form-control" id="companycontact1" placeholder="Contact 1">
					                                          </div>

					                                          <div class="form-group">
					                                            <label for="country">Company Contact 2</label>
					                                            <input type="text" name = "compContact2" class="form-control" id="companycontact2" placeholder="Contact 2">
					                                          </div>

					                                          <div class="form-group">
					                                            <label for="country">Company Website</label>
					                                            <input type="text" name = "compWebsite" class="form-control" id="companywebsite" placeholder="Website link">
					                                          </div>

					                                          <div class="form-group">
					                                            <label for="userfile">Company Logo</label>
					                                            <input type="file" id="userfile" name="logo">
					                                  
					                                          </div>
					                           
					                                        <div class="row">
					                                        <div class="col-md-2 col-md-push-10">
					                                          <button type="button" id="compSubmit" class="btn btn-success btn-block">Save</button>
					                                        </div>
					                                          
					                                       </div>
					                           </div>
					                   </div><!-- end row -->
					                   {!! Form::close() !!}
                                      </div><!-- /.Role tab-pane -->

									    <!-- TAB3 -->
									    <div class="tab-pane" id="othersettings">
									     settings3
									    </div><!-- /.ASSIGN PERMISSION TO ROLE tab-pane -->

								      

    </div><!-- /.tab-content -->
  </div><!-- nav-tabs-custom -->
  </div><!-- end col-md-12 for custom tabs -->
  </div>




     <div class="modal fade editcompanydetails" id="editcompanydetails" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
             <div class="modal-dialog modal-lg">
                     <div class="modal-content">
                             <div class="modal-header">
                                   <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <h4 class="modal-title" id="myModalLabel">Edit Company Details</h4>
                             </div>
                                 <div class="modal-body">
                                     
                                      <div class="row">
                                      	  <div class="col-md-12">
                                      	  	  <div class="col-md-6">

                                      	  	      <div class="form-group">
		                                              <label class="">Company Name(*)</label>
		                                              <input type="text" name="companyname" value="" class="form-control" required="true">
		                                               <input type="hidden" name="companyid" value="" class="form-control" required="true">
		                                               {!! csrf_field() !!}
		                                           </div>

		                                            <div class="form-group">
		                                              <label class="">Company Address1(*)</label>
		                                              <input type="text" name="companyaddress1" value="" class="form-control">
		                                           </div>

		                                           <div class="form-group">
		                                              <label class="">Company Address2</label>
		                                              <input type="text" name="companyaddress2" value="" class="form-control">
		                                           </div>


		                                           <div class="form-group">
		                                              <label class="">Company Email</label>
		                                              <input type="text" name="companyemail" value="" class="form-control">
		                                           </div>
                                      	  	  	
                                      	  	  </div>




                                      	  	  <div class="col-md-6">

                                      	  	       <div class="form-group">
		                                              <label class="">Company contact1(*)</label>
		                                              <input type="text" name="companycontact1" value="" class="form-control">
		                                           </div>

                                      	  	  	    <div class="form-group">
		                                              <label class="">Company contact2</label>
		                                              <input type="text" name="contact2" value="" class="form-control">
		                                           </div>

		                                           <div class="form-group">
		                                              <label class="">Company Website</label>
		                                              <input type="text" name="website" value="" class="form-control">
		                                           </div>

		                                           <div class="form-group">
		                                              <label class="">Company Logo</label>
		                                              <input type="file" id="userfile" name="logo">
		                                           </div>


                                      	  	  </div>

                                      	  </div>
                                      </div>
                                           
                                     

                                      <div class="modal-footer">
                                                   <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                   <button type="submit" id="updatecompanysettingsbutton" class="btn btn-primary">Update</button>
                                            </div> 
                                          
                                 </div>
                        </div>


              </div>
          </div>

          
</section><!-- /.content -->


</div><!-- /.content-wrapper -->
         
@include('includes.footer')       
@stop
