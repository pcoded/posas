@extends('master')
@section('title', 'Expenses')
@section('content')
@include('includes.header')
@include('includes.sidebar')


      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
           <i class="fa fa-dashboard"></i> Expenses
            <small>Control panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Expenses</li>
          </ol>
        </section>
        <div  class="box box-primary"></div> <!-- blue line  on top-->

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->



      
          <!-- summary reports -->


      <!-- Main content -->
    <section class="content">
      <div class="row">
        

         
      <!--  <div class="col-md-12"> -->
          <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a aria-expanded="false" href="#chartsofaccount" data-toggle="tab">Current expenses</a></li>
              <li class=""><a aria-expanded="false" href="#createnewepxpens" data-toggle="tab">New expense</a></li>
            <!--   <li class="dropdown">
                <a aria-expanded="false" class="dropdown-toggle" data-toggle="dropdown" href="#">
                  Dropdown <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Action</a></li>
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Another action</a></li>
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Something else here</a></li>
                  <li role="presentation" class="divider"></li>
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Separated link</a></li>
                </ul>
              </li> -->
              <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
            </ul>

            <div class="tab-content">





              <div class="tab-pane active" id="chartsofaccount">

                        <div class="row">
                              <div class="col-md-12">
                                 <div class="btn-group pull-right" role="group" aria-label="...">
                                 <!--      <button type="button" class="btn btn-success btn-flat" data-toggle="modal" data-target="#newexpense"><i class="fa fa-plus"></i>Expenses</button> -->
                                      <button type="button" class="btn btn-info btn-flat" data-toggle="modal" data-target="#newaccountingModal"><i class="fa fa-plus"></i> New Expense Account</button>
                                     @can('delete-products')
                                       <button type="button" class="btn bg-red btn-flat" id="deletechartsaccount"><i class="fa fa-trash"></i> Delete</button>
                                    @endcan
                                  </div>
                              </div>
                         </div><!-- endcol-12 -->
                   <div  class="box box-default" style="margin-bottom:0px;"></div> <!-- blue line  on top-->


                  
                     <h1 class="text-center"><small>List of all Expenses</small></h1>
                      <table id="listofexpensestabledata" class="table table-bordered">
                              <thead>
                                  <tr>
                                       <th><i class="fa fa-th"></i></th>
                                       <th>Date</th>
                                       <th>Descriptions</th>
                                       <th>Total Amount</th>
                                       <th>Status</th>
                                       <!-- <th>Edit</th>
                                       <th>Delete</th> -->
                                  </tr>
                              </thead>

                                  <tbody>
               
                                                                                
                                  </tbody>
                            
                          </table>
                       

                   
              </div>



              <!-- /.tab-pane create new expenses -->
              <div class="tab-pane" id="createnewepxpens">
                    <h1 class="text-center"><small>Create Expenses</small></h1><hr>
                      
                  <div class="row">
				         	 <div class="col-md-12">
                    <div class="col-md-6">
                           <div class="form-group">
                               <label for="exampleInputEmail1">Receipt From</label>   
                               <input class="form-control pull-right active" name="expensereceipt" id="expensereceipt" type="text">
                               <input type="hidden" name="_newexpensetoken" value="<?php echo csrf_token(); ?>">
                           </div>
                    </div>
                     

				         	    <div class="col-md-6">
					                 <div class="form-group">
                                        <label for="exampleInputEmail1">Date</label>
                                        <div class="input-group">
						                  <div class="input-group-addon">
						                    <i class="fa fa-calendar"></i>
						                  </div>
						                  <input class="form-control pull-right active" name="newexpensedatepicker" id="newexpensedatepicker" type="text">
						                </div>
                                        
                            </div>
                      </div>

				         	 </div>
					     </div>

					     <div class="row">
					     	 <div class="col-md-12">

					     	 	 <div class="col-md-4">
						     	 	<div class="form-group">
                      		 <label for="expenseaccount">Expense category</label>
                      		 <select class="form-control select2" id="expensecategory" style="width: 100%;" name="expensecategory">
                      		 @if($chartsofacc)
                      		  @foreach($chartsofacc as $chartsofacc)
                      		 	 <option value="{{$chartsofacc->chartaccId}}">{{$chartsofacc->accountName}}</option>
                      		  @endforeach
                      		  @else
                      		  No account created
                      		  @endif
                      		 	 
                      		 </select>
                      	</div>
					     	 	 </div>

					     	 	 <div class="col-md-4">
				     	         <div class="form-group">
				     	         	<label for="expenseAmount">Amount</label>
				     	         	<input type="number" name="expenseamount" class="form-control" placeholder="Amount used" />
				     	         </div>	 	
					     	 	 </div>

                    <div class="col-md-4">
                       <div class="form-group">
                        <label for="expenseAmount">Vat(%)</label>
                       
                             <select class="form-control select2" id="expensevat" style="width: 100%;" name="expensevat">
                           @if($chartsofacc)
                            @foreach($vat as $vat)
                             <option value="{{$vat->vid}}">{{$vat->vname}}%</option>
                            @endforeach
                            @else
                            No vat created
                            @endif
                             
                           </select>
                       </div>   
                   </div>
					     	 	 
					     	 </div>
					     </div>

               <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                         <label for="paymentmethod">Payment method</label>
                         <select class="form-control select2" id="paymentmethod" style="width: 100%;" name="paymentmethod">
                         @if(count($paymentmethods))
                          @foreach($paymentmethods as $paymentmethods)
                           <option value="{{$paymentmethods->methodid}}">{{$paymentmethods->paymentmethodname}}</option>
                          @endforeach
                           No account created
                        @endif
                         </select>
                      </div>
                  </div>
               </div>

					     <div class="row">
					     	<div class="col-md-12">
					     	   <div class="form-group">
					     	     <label for="expense reason">Expense reason</label>
					     		 <textarea class="form-control" name="expensereason" placeholder="Give a little descriptions..."></textarea>
					     	   </div>
					     	</div>
					     </div>

       <!--       <div class="row">
                 <div class="col-md-12">
                    <div class="col-md-4"></div>
                    <div class="col-md-4"></div>
                    <div class="col-md-4">
                           <li class="row-fluid">
                            <span class="span6"><strong>Amount</strong></span>
                            <span class="span6 expensesubtotal" style="float:right;font-size:15px; font-weight:bold;">Tsh 0.00</span>
                           </li> <hr> 
                   

                            <li class="row-fluid">
                            <span class="span6"><strong>Total Cost + Vat</strong></span>
                            <span class="span6 expensesupertotal" style="float:right;font-size:15px; font-weight:bold;">Tsh 0.00</span>
                           </li>
                    </div>
                 </div>
             </div> -->

                          
	              <div class="row">
					         	 <div class="col-md-12">
					         	 	 <div class="box-footer">
						                <button type="button" id="newexpensebutton" class="btn btn-success pull-right" aria-expanded="false"> <i class="ion-plus-circled"></i> Save Expense</button>
						              </div>
					         	 </div>
							   </div>
	                      
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
       <!--  </div> -->


      
      
        <!-- /.col (RIGHT) -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->

  
    </section><!-- /.content -->


           <!-- new expense-->
        <div class="modal fade in" id="newexpense" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
         <div class="modal-dialog modal-lg">
                <div class="modal-content">
                         <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <h4 class="modal-title" id="myModalLabel">New Accounting</h4>
                         </div>
                         <div class="modal-body">
                                   
                                   <div class="box-body">
                                      <div class="form-group">
						                  <label for="inputEmail3" class="col-sm-2 control-label">Date</label>
						                    <div class="input-group">
							                  <div class="input-group-addon">
							                    <i class="fa fa-calendar"></i>
							                  </div>
							                  <input class="form-control pull-right active editpurcdate" name="newexpensedatepicker" id="newexpensedatepicker" type="text">
							                </div>
						                
						                </div>


						                 <div class="form-group">
	                                        <label for="exampleInputPassword1">Payment method</label>
	                                        <select name="accountcategory" class="form-control" id="accountcategory">
                                               
	                                        </select>
                                      </div>


                                      </div>


                                    <div class="modal-footer">
                                       <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                       <button type="submit" id="submitnewexpense" class="btn btn-primary">Save</button>
                                    </div>
               
                          </div>

                      
                </div>
         </div>
   </div>




   <!-- new accounting -->
        <div class="modal fade in" id="newaccountingModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
         <div class="modal-dialog modal-lg">
                <div class="modal-content">
                         <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <h4 class="modal-title" id="myModalLabel">New Accounting</h4>
                         </div>
                                    <div class="modal-body">
                                   
                                    <div class="box-body">
                                      <div class="form-group">
                                        <label for="exampleInputEmail1">Account Name</label>
                                        <input class="form-control" name="nameofaccount" id="accountname" placeholder="Account name" type="text">
                                        <input type="hidden" name="_newaccounttoken" value="<?php echo csrf_token(); ?>">
                                      </div>

                                      <div class="form-group">
                                         <label fore="accidentifier">Account Number</label>
                                         <input class="form-control" type="number" name="accidentifier" id="accidentifier" placeholder="Account number">
                                      </div>

                                      <div class="form-group">
                                        <label for="exampleInputPassword1">Account Category</label>
                                        <select name="accountcategory" class="form-control" id="accountcategory">

                                             @foreach($cataccouting as $cataccouting)
                                               <option value="{{ $cataccouting->acctypeId }}" selected="true"> {{ $cataccouting->accName }}</option>
                                             @endforeach
                                        </select>
                                      </div>


                                      <div class="form-group">
                                        <label>Descriptions</label>
                                        <textarea class="form-control" name="accountdescriptions" id="accountdescriptions" rows="3" placeholder="Enter account descriptions ..."></textarea>
                                      </div>
                                      
                                      
                                    </div>



                                    <div class="modal-footer">
                                       <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                       <button type="submit" class="btn btn-primary submitnewaccount">Save</button>
                                    </div>
               
                          </div>

                      
                </div>
         </div>
   </div>











 </div><!-- /.content-wrapper -->
            
@include('includes.footer')
         
@stop
