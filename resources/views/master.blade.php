<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>CSMS |  @yield('title')</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta http-equiv="cache-control" content="private, max-age=0, no-cache">
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="expires" content="0">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="{{ asset("bower_components/admin-lte/bootstrap/css/bootstrap.min.css") }}">
    <!-- Font Awesome -->
    <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">-->
    <!-- for local developement -->
    <link rel="stylesheet" href="{{ asset("bower_components/admin-lte/plugins/font-awesome/css/font-awesome.min.css") }}">
    <!-- Ionicons -->
    <!-- <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"> -->
 <!-- for local development -->
 <link rel="stylesheet" href="{{ asset("bower_components/admin-lte/plugins/ionicons/css/ionicons.min.css") }}">
  <!-- Morris chart -->
 <!--  <link rel="stylesheet" href="{{ asset("bower_components/admin-lte/plugins/morris/morris.css")}}"> -->

        <!-- DataTable css -->
     <!-- DataTables -->
    <link rel="stylesheet" href="{{  asset("bower_components/admin-lte/plugins/datatables/dataTables.bootstrap.css") }}">

     <!-- Select2 -->
  <link rel="stylesheet" href="{{ asset("bower_components/admin-lte/plugins/select2/select2.min.css")}}">


    <!-- datepicker range -->
    <link rel="stylesheet" href="{{ asset("bower_components/admin-lte/plugins/daterangepicker/daterangepicker-bs3.css") }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset("bower_components/admin-lte/dist/css/AdminLTE.min.css") }}">



      <!-- Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{asset("bower_components/admin-lte/dist/css/skins/_all-skins.min.css")}}">
      
        <!-- jvectormap -->
    <link rel="stylesheet" href="{{asset("bower_components/admin-lte/plugins/jvectormap/jquery-jvectormap-1.2.2.css")}}">
    <!-- Date Picker -->
    <link rel="stylesheet" href="{{asset("bower_components/admin-lte/plugins/datepicker/datepicker3.css")}}">


    <!-- Daterange picker -->
    <link rel="stylesheet" href="{{asset("bower_components/admin-lte/plugins/daterangepicker/daterangepicker-bs3.css")}}">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="{{asset("bower_components/admin-lte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css")}}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- iCheck for checkboxes and radio inputs -->
   <!--  <link rel="stylesheet" href="{{  asset("bower_components/admin-lte/plugins/iCheck/minimal/blue.css") }}"> -->
   <link rel="stylesheet" href="{{  asset("bower_components/admin-lte/plugins/iCheck/all.css") }}">

   <!-- Sweet alert message css -->
   <link rel="stylesheet" href="{{  asset("bower_components/admin-lte/plugins/sweetalert/dist/sweetalert.css") }}">
   <link rel="stylesheet" href="{{  asset("bower_components/admin-lte/plugins/pnotify/pnotify.css") }}">

   <!--EasyAutocomplete -->
  <!--  <link rel="stylesheet" href="{{  asset("bower_components/admin-lte/plugins/EasyAutocomplete/easy-autocomplete.min.css") }}"> -->

    <!-- local development -->
    <link rel="stylesheet" type="text/css" href="{{ asset("bower_components/admin-lte/plugins/jQueryUI/smoothness_jquery-ui.css")}}">


    <!-- toastr notification -->
    <link rel="stylesheet" type="text/css" href="{{ asset("bower_components/admin-lte/plugins/toastr/build/toastr.min.css")}}">
    <link rel="stylesheet" type="text/css" href="{{ asset("bower_components/admin-lte/plugins/autocomplete/auto-complete.css")}}">


   <style type="text/css">


#field {
    margin-bottom:20px;

}
.delivernoteDate{z-index:1151 !important;}
.editproduct{z-index:1151 !important;}

.editpurcdate{z-index:1050 !important;}
.nav-tabs{font-size: 16px;}
   </style>

   <!--  <script src="{{asset("bower_components/admin-lte/plugins/firebug-lite/firebug-lite.js")}}"></script> -->

    </head>




    @yield('content')






    <!-- jQuery 2.1.4 -->
    <script src="{{ asset("bower_components/admin-lte/plugins/jQuery/jQuery-2.1.4.min.js") }}"></script>
     <!-- jQuery UI 1.11.4 -->
     <!--<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>-->

    <!-- local development -->
     <script src="{{ asset("bower_components/admin-lte/plugins/jQueryUI/jquery-ui.min.js")}}"></script>

    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>

    <!-- Bootstrap 3.3.5 -->
    <script src="{{ asset("bower_components/admin-lte/bootstrap/js/bootstrap.min.js") }}"></script>
    
    <!-- Morris.js charts -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script> -->
<!-- <script src="{{ asset("bower_components/admin-lte/plugins/morris/morris.min.js")}}"></script> -->
<script src="{{ asset("bower_components/admin-lte/plugins/chartjs/Chart.min.js")}}"></script>


 <!-- DataTables -->
    <script src="{{ asset("bower_components/admin-lte/plugins/datatables/jquery.dataTables.min.js") }}"></script>
    <script src="{{  asset("bower_components/admin-lte/plugins/datatables/dataTables.bootstrap.min.js") }}"></script>


     <script src="{{ asset("bower_components/admin-lte/plugins/select2/select2.full.min.js") }}"></script>



    <!-- Sparkline -->
    <script src="{{asset("bower_components/admin-lte/plugins/sparkline/jquery.sparkline.min.js")}}"></script>
    <!-- jvectormap -->
    <script src="{{asset("bower_components/admin-lte/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js")}}"></script>
    <script src="{{asset("bower_components/admin-lte/plugins/jvectormap/jquery-jvectormap-world-mill-en.js")}}"></script>
    <!-- jQuery Knob Chart -->
    <script src="{{asset("bower_components/admin-lte/plugins/knob/jquery.knob.js")}}"></script>
    
    <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>-->
    <!-- moment for local development -->
     <script src="{{asset("bower_components/admin-lte/plugins/momentJs/moment.min.js")}}"></script>

     <!-- daterangepicker -->
    <script src="{{asset("bower_components/admin-lte/plugins/daterangepicker/daterangepicker.js")}}"></script>
    <!-- datepicker -->
    <script src="{{asset("bower_components/admin-lte/plugins/datepicker/bootstrap-datepicker.js")}}"></script>

    
   
    <!-- Bootstrap WYSIHTML5 -->
    <script src="{{asset("bower_components/admin-lte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js")}}"></script>
    <!-- Slimscroll -->
    <script src="{{asset("bower_components/admin-lte/plugins/slimScroll/jquery.slimscroll.min.js")}}"></script>

    <script src="{{asset("bower_components/admin-lte/plugins/pnotify/pnotify.js")}}"></script>
<!--     <script src="{{asset("bower_components/admin-lte/plugins/Typehead/bootstrap3-typeahead.min.js")}}"></script>
     <script src="{{asset("bower_components/admin-lte/plugins/Typehead/typeahead.bundle.js")}}"></script> -->



 <!-- iCheck 1.0.1 -->
    <script src="{{  asset("bower_components/admin-lte/plugins/iCheck/icheck.min.js") }}"></script>
 <!-- Sweet alert message js -->
 <script src="{{  asset("bower_components/admin-lte/plugins/sweetalert/dist/sweetalert.min.js") }}"></script>
    <!-- FastClick -->
    <script src="{{asset("bower_components/admin-lte/plugins/fastclick/fastclick.min.js")}}"></script>
    <!-- AdminLTE App -->
    <script src="{{asset("bower_components/admin-lte/dist/js/app.min.js")}}"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
     <script src="{{asset("bower_components/admin-lte/dist/js/pages/dashboard.js")}}"></script>
    <!-- AdminLTE for demo purposes -->
     <script src="{{asset("bower_components/admin-lte/dist/js/demo.js")}}"></script>
    <script src="{{asset("bower_components/admin-lte/dist/js/sales.js")}}"></script>
    <script src="{{asset("bower_components/admin-lte/dist/js/acl.js")}}"></script>
    <script src="{{asset("bower_components/admin-lte/dist/js/purchases.js")}}"></script>
    <script src="{{asset("bower_components/admin-lte/dist/js/reports.js")}}"></script>
    <script src="{{asset("bower_components/admin-lte/dist/js/products.js")}}"></script>
    <script src="{{asset("bower_components/admin-lte/dist/js/stores.js")}}"></script>
    <script src="{{asset("bower_components/admin-lte/dist/js/accounting.js")}}"></script>
    <script src="{{asset("bower_components/admin-lte/dist/js/expenses.js")}}"></script>
    <script src="{{asset("bower_components/admin-lte/dist/js/settings.js")}}"></script>
     


     <script src="{{asset("bower_components/admin-lte/plugins/toastr/build/toastr.min.js")}}"></script>

    <!--  print element -->
     <script src="{{asset("bower_components/admin-lte/plugins/printElement/jquery.printElement.min.js")}}"></script>
      
     <script src="{{asset("bower_components/admin-lte/plugins/autocomplete/auto-complete.min.js")}}"></script>

     <!-- EasyAutocomplete -->
   <!-- <link rel="stylesheet" href="{{  asset("bower_components/admin-lte/plugins/EasyAutocomplete/jquery.easy-autocomplete.min.js") }}"> -->



<script>
// var zIndex = parseInt(this.element.parents().filter(function() {
//                             return $(this).css('z-index') != 'auto';
//                         }).first().css('z-index'))+1151;
 
PNotify.prototype.options.styling = "fontawesome";
       //Initialize Select2 Elements
        $(function(){
          $(".select2").select2();
        });

        function ePrint()
        {
          var vv  = $(".myprint");
          window.print(vv);
        }

        function purchasePrint()
        {
          var purchase  = $(".purchaseInvoice");
          window.print(purchase);
        }


        function productPrint()
        {
          var products  = $(".productPrint");
          window.print(products);
        }
        
        

        



       //iCheck for checkbox and radio inputs
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_flat-green'
        });
     //Red color scheme for iCheck
        $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
          checkboxClass: 'icheckbox_minimal-red',
          radioClass: 'iradio_minimal-red'
        });
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
          checkboxClass: 'icheckbox_flat-green',
          radioClass: 'iradio_flat-green'
        });


        // DataTables scripts
           $(function () {
                 $('#getposdataTable').DataTable();

           });

           $(function () {
                $("#permissionsTable").DataTable();

           });

           // Success alert slide up
           $('div.informuser').delay('3000').slideUp('300');


           // Date picker
      //      $(function () {
      //   //Date range picker
      //   $('#reservation').daterangepicker();
      //   //Date range picker with time picker
      //   $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
      //   //Date range as a button
      //   $('#daterange-btn').daterangepicker(
      //       {
      //         ranges: {
      //           'Today': [moment(), moment()],
      //           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
      //           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
      //           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
      //           'This Month': [moment().startOf('month'), moment().endOf('month')],
      //           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      //              },
      //         startDate: moment().subtract(29, 'days'),
      //         endDate: moment()
      //       },
      //   function (start, end) {
      //     $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
      //   }
      //   );
      // });
      

     


               // Delete customer with multiple ids
            $('#submitform').click(function(){
                   // FIre comfirmation from user
                         swal({ 
                         title: "Are you sure?", 
                        text: "You will not be able to recover Customer (s)!",  
                        type: "warning",   
                        showCancelButton: true, 
                        confirmButtonColor: "#DD6B55", 
                        confirmButtonText: "Yes, delete it!",
                        cancelButtonText: "No, cancel!", 
                        closeOnConfirm: false, 
                       closeOnCancel: false 
                     },
                        function(isConfirm){ 
                              if (isConfirm) { 

                                     // get the items
                                        var checkedValues = $('input:checkbox:checked').map(function() {
                                        return this.value;
                                        }).get();
                   
                                    // Process the array items
                                         $.ajax({
                                          type:"POST",
                                          url:'/customer/deletemultiple',
                                          data:{checkedValues, _token: $('input[name=_token]').val()},
                                          dataType: 'json',
                                          cache : false,
                                          success: function(data){
                                         window.location.reload();
                                            //swal("Deleted!", "Your imaginary file has been deleted.", "success"); 
                                          },
                                          error: function(data){

                                            swal({
                                                title: "Error",
                                                text: "Ohh!, You did not select any Customer",
                                                type: "error",
                                                confirmButtonText: "Ok"
                                            });


                                          }
                                      }); 
                                   //  window.setTimeout(function(){location.reload()}, 3000);
                                    
                                     //window.location.reload();
                              } else { 
                                     swal("Cancelled", "", "error"); 
                                     document.getElementById("idinput").checked = false;
                                     //window.setTimeout(function(){location.reload()}, 3000);
                                    
                                       }
                         });

               
            });


 // Delete customer with multiple ids
            $('#supplier').click(function(){
                   // FIre comfirmation from user
                         swal({ 
                         title: "Are you sure?", 
                        text: "You will not be able to recover Supplier (s)!",  
                        type: "warning",   
                        showCancelButton: true, 
                        confirmButtonColor: "#DD6B55", 
                        confirmButtonText: "Yes, delete it!",
                        cancelButtonText: "No, cancel!", 
                        closeOnConfirm: false, 
                       closeOnCancel: false 
                     },
                        function(isConfirm){ 
                              if (isConfirm) { 

                                     // get the items
                                        var checkedsupplier = $('input:checkbox:checked').map(function() {
                                        return this.value;
                                        }).get();
                   
                                    // Process the array items
                                         $.ajax({
                                          type:"POST",
                                          url:'/supplier/deletemultiple',
                                          data:{checkedsupplier, _token: $('input[name=_token]').val()},
                                          dataType: 'json',
                                          cache : false,
                                          success: function(data){
                                         window.location.reload();
                                            //swal("Deleted!", "Your imaginary file has been deleted.", "success"); 
                                          },
                                          error: function(data){

                                            swal({
                                                title: "Error",
                                                text: "Ohh!, You did not select any supplier",
                                                type: "error",
                                                confirmButtonText: "Ok"
                                            });


                                          }
                                      }); 
                                   //  window.setTimeout(function(){location.reload()}, 3000);
                                    
                                     //window.location.reload();
                              } else { 
                                     swal("Cancelled", "", "error"); 
                                     document.getElementById("idinput").checked = false;
                                     //window.setTimeout(function(){location.reload()}, 3000);
                                    
                                       }
                         });

               
            });





// Delete Stores with multiple ids
            $('#store').click(function(){
                   // FIre comfirmation from user
                         swal({ 
                         title: "Are you sure?", 
                        text: "You will not be able to recover Store (s)!",  
                        type: "warning",   
                        showCancelButton: true, 
                        confirmButtonColor: "#DD6B55", 
                        confirmButtonText: "Yes, delete it!",
                        cancelButtonText: "No, cancel!", 
                        closeOnConfirm: false, 
                       closeOnCancel: false 
                     },
                        function(isConfirm){ 
                              if (isConfirm) { 

                                     // get the items
                                        var checkedstore = $('input:checkbox:checked').map(function() {
                                        return this.value;
                                        }).get();
                   
                                    // Process the array items
                                         $.ajax({
                                          type:"POST",
                                          url:'/stores/deletemultiple',
                                          data:{checkedstore, _token: $('input[name=_token]').val()},
                                          dataType: 'json',
                                          cache : false,
                                          success: function(data){
                                         window.location.reload();
                                            //swal("Deleted!", "Your imaginary file has been deleted.", "success"); 
                                          },
                                          error: function(data){

                                            swal({
                                                title: "Error",
                                                text: "Ohh!, You did not select any store",
                                                type: "error",
                                                confirmButtonText: "Ok"
                                            });


                                          }
                                      }); 
                                   //  window.setTimeout(function(){location.reload()}, 3000);
                                    
                                     //window.location.reload();
                              } else { 
                                     swal("Cancelled", "", "error"); 
                                     document.getElementById("idinput").checked = false;
                                     //window.setTimeout(function(){location.reload()}, 3000);
                                    
                                       }
                         });

               
            });




// EDIT PURCHASE
function editPurchase()
{
  $("#ListPurchases tr").not("thead tr").each(function(){
    purchas = $(this).closest("tr").find(".bulk").val();
     console.log(purchas);
  });
 
  //$('#editpurchasemodal').modal('show');
}


//Default top selling items
$(document).ready(function(){
  var todaysdate = new Date();
  var userdate = moment(todaysdate).format('MMMM D, YYYY');
  var formattodaydate = moment(todaysdate).format('YYYY-MM-DD');
  //console.log(formattodaydate);

 var labels = []; var chartdata = [];
    $("#salesChart").text();
    $("#topsellingdaterange").text(userdate);
    $("#topsale").remove();
    $("#topsaleschart").append('<canvas id="topsale"></canvas>');

    // call ajax to update top selling product
    $.ajax({
          type:"get",
          url: "/home/defaulttopselling",
          data:{"startdate":formattodaydate,_token: $('input[name=_token]').val()},
          success:function(data){
               $.each(data, function(i,productdata){
                 $.each(this, function(productdata, itemdata){
                    //console.log(itemdata.productName);
                    labels.push(itemdata.productName);
                    chartdata.push(itemdata.totalquantity);
                 });
               });

                            var productChartData = {
                                      labels: labels,
                                        datasets: [
                                            {
                                                label: labels,
                                                fillColor: '#37a7e8',
                                                color: "#f56954",
                                                highlight: "#f56954",
                                                data: chartdata
                                            },
                                            
                                        ]
                                    };
                                    



                                        //-------------
                                    //- BAR CHART -
                                    //-------------
                                    var myChartCanvas = $("#topsale").get(0).getContext("2d");
                                    
                                    // options
                                    var barChartOptions = {
                                           multiTooltipTemplate: "<%= datasetLabel %> - <%= value %>",                 
                                    //Boolean - whether to make the chart responsive
                                    responsive: true,
                                   // maintainAspectRatio: true
                                  };


                                  // Instantiate a new chart
                                    var myLineChart = new Chart(myChartCanvas).Bar(productChartData,barChartOptions);




          },error:function(data){
            console.log(data);
          }
    });



});

           

</script>





  </body>
</html>