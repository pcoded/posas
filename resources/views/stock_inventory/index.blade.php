@extends('master')
@section('title', 'Stock')
@section('content')
@include('includes.header')
@include('includes.sidebar')

<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <i class="fa fa-users"></i> Stock & Inventory Control
            <small>Control panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="/home"><i class="fa fa-home"></i> Home</a></li>
            <li class="active"><a href="/stock">Stock</a></li>
          </ol>
        </section>
        <div  class="box box-primary"></div> <!-- blue line  on top-->

          @include('errors.flash')
          @include('errors.formerrors')
          @include('errors.deleteajax')
        <section class="content">

       <div class="row">
                                                        
         <div class="col-md-12">
              <h4>Transfer Inventory  and directly adjust your stock.</h4>
                  <!-- Custom Tabs -->
                  <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs" id="createNotTab">
                           <!--    <li class="active"><a href="#firsttab" data-toggle="tab" aria-expanded="true"><strong>Purchase Order List</strong></a></li>
                         
                                <li class=""><a href="#thirdtab" data-toggle="tab" aria-expanded="false"><strong>New Purchase Orders</strong></a></li> -->
                              
                                <li class="active"><a href="#fourthtab" data-toggle="tab" aria-expanded="false"><strong>Stock Transfer</strong></a></li>
                            </ul>
                            <div class="tab-content">


					      <!-- TAB4 -->
					       <div class="tab-pane active" id="fourthtab">
	                             

	                                   
									         <div class="row">
									         	 <div class="col-md-12">
									         	 <h1 class="text-center"><small>Transfer Inventory Between Stores</small></h1>
									         	 <hr>
									         	 	<div class="col-md-4">
									         	 
								         	 		     <div class="form-group">
												                  <label for="inputEmail3" class="col-sm-2 control-label">From</label>
												                  <div class="col-sm-10">
												                   
												         	 			<select name="storefrom" class="form-control storeid" id="storefrom">
												         	 			<option value="">Choose Store</option>
												         	 			@if(count($stores))
												         	 			@foreach($stores as $store)
												         	 				<option value="{{$store->id}}">{{$store->storeName}}</option>
			                                                            @endforeach
			                                                            @endif
												         	 			</select>
												         	 		
												                  </div>
												             </div>

									         	 	</div>


									         	 	<div class="col-md-4">
									         	 	
									         	 		    <div class="form-group">
													                  <label for="inputEmail3" class="col-sm-2 control-label">To</label>
													                  <div class="col-sm-10">
													         	 			<select name="storeto" class="form-control storeid" id="storeto">
													         	 			<option value="">Choose Store</option>
													         	 			@if(count($stores))
													         	 			@foreach($stores as $store)
													         	 				<option value="{{$store->id}}">{{$store->storeName}}</option>
				                                                            @endforeach
				                                                            @endif
													         	 			</select>
													         	 		</div>
													                  
													         </div>
									         	 	</div>




									         	 	<div class="col-md-4">

									         	 	   <div class="form-group">
										                  <label for="inputEmail3" class="col-sm-2 control-label">Date</label>

										                  <div class="col-sm-10">
										                    <div class="input-group">
											                  <div class="input-group-addon">
											                    <i class="fa fa-calendar"></i>
											                  </div>
											                  <input class="form-control pull-right active" name="storetransferdate" id="storetransferdate" type="text">
											                </div>
										                  </div>
										                </div>
									         	 		
									         	 	</div>
									         	 </div>
									         </div>
									         <hr>

									  
                                           
									         <div class="row">
									   
									         	 <div class="col-md-12">
									         	 	     <div class="input-group">
									              <div class="input-group-btn">
									                  <button type="button" class="btn btn-primary"><i class="fa fa-plus"></i></button>
									                </div>
									                <input class="form-control" id="stocktransferitem" name="transferitemName" type="text"  placeholder="Enter item name or scan barcode">  
									                <input type="hidden" id="formstocktransfertoken" name="_stocktransfertoken" value="{{ csrf_token() }}">              
									              </div>

									              <br>
									               <table id="stocktranserTable" class="table table-hover table-bordered">

								                  <thead>
								                    <tr class="register-items-header">
								                      <th>Action</th>
								                      <th>Item Name</th>
								                      <th>Quantity</th>
								                      <th>Item Cost</th>
													  <th>Vat</th>
													  
								                    
								                    </tr>
								                  </thead>
								                <tbody>
								                	<tr id="transfersemessagerow"></tr>
								                </tbody>
								                     
								  
								                 
								                      </table>


									         	 </div>
									         </div>
                                             <br>
									   

									         <div class="row">
									         	 <div class="col-md-12">
									         	 	 <div class="box-footer">
									         	 	  <div class="btn-group">
										               <button type="button" class="btn btn-danger" aria-expanded="false">
                                                        <i class="ion-close-circled"></i> Cancel</button>
										             </div>
										                <button type="button" id="transferInventory" class="btn btn-success pull-right" aria-expanded="false"> <i class="ion-paper-airplane"></i> Transfer</button>
										              </div>
									         	 </div>
									         </div>

					       </div><!-- /.ASSIGN USER ROLE tab-pane -->

    </div><!-- /.tab-content -->
  </div><!-- nav-tabs-custom -->
  </div><!-- end col-md-12 for custom tabs -->
  </div>
          
</section><!-- /.content -->
          

    <div class="modal" id="editpurchasemodal">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Default Modal</h4>
              </div>
              <div class="modal-body">
                   
                     <div class="row">
			         	 <div class="col-md-12">
			         	<!--  <h1><small>Create new purchase</small></h1> -->
			         	 	<div class="col-md-4">
			         	 	
			         	 		<div class="form-group">
			         	 			<select name="supplierPurchase" id="supplierChange" class="form-control">
			         	 			<option value="">Choose Supplier</option>
			         	 			
			         	 				<option value="asdg">dfhsd</option>
                                   
			         	 			</select>
			         	 			<!-- <input type="hidden" name="_getsuppliertoken" value="<?php echo csrf_token(); ?>"> -->
			         	 		</div>
			         	 	
			         	 	</div>
			         	 	<div class="col-md-8 text-right">
			         	 		<div class="form-horizontal">
			         	 			Order # <input class="" type="text" name="orderNumber" value="" disabled="true"> 
			         	 		</div>
			         	 	</div>
			         	 </div>
			         </div>
			         <hr>




			            <div class="row">
				         	  <div class="col-md-12">
				         	  	  <div class="col-md-6">
				         	  	  	
								            
								                <div class="form-group">
								                  <label for="inputEmail3" class="col-sm-2 control-label">Address</label>

								                  <div class="col-sm-10">
								                    <input class="form-control" id="psupplieraddress" value="" placeholder="Supplier Address" type="text">
								                  </div>
								                </div>
                                                <br><br>
								                <div class="form-group">
								                  <label for="inputEmail3" class="col-sm-2 control-label">Email</label>

								                  <div class="col-sm-10">
								                    <input class="form-control" id="psupplieremail" value="" placeholder="Supplier Email" type="email">
								                  </div>
								                </div>
							
								          
								         
								            
				         	  	  </div>
				         	  	   <div class="col-md-6">
				         	  	   	     
								            
								                <div class="form-group">
								                  <label for="inputEmail3" class="col-sm-2 control-label">Due</label>

								                  <div class="col-sm-10">
								                    <div class="input-group">
									                  <div class="input-group-addon">
									                    <i class="fa fa-calendar"></i>
									                  </div>
									                  <input class="form-control pull-right active" name="porderdata" id="purchaseOrderdata" type="text">
									                </div>
								                  </div>
								                </div>
                                                 <br><br> 


								                <div class="form-group">
								                  <label for="inputEmail3" class="col-sm-2 control-label">Delivery</label>

								                  <div class="col-sm-10">
								                    <div class="input-group">
									                  <div class="input-group-addon">
									                    <i class="fa fa-calendar"></i>
									                  </div>
									                  <input class="form-control pull-right active" name="porderdeliverydate" id="purchaseOrderdelivery" type="text">
									                </div>
								                  </div>
								                </div>
								     
								                 <br><br>


								                
								         	 		<div class="form-group">
								         	 		<label for="inputStore" class="col-sm-2 control-label">Store</label>
								         	 		<div class="col-sm-10">
								         	 			<select name="storeLocation" class="form-control storeid" id="destination">
								         	 			<option value="">Choose Store</option>
								         	 			@if(count($stores))
								         	 			@foreach($stores as $store)
								         	 				<option value="{{$store->id}}">{{$store->storeName}}</option>
                                                        @endforeach
                                                        @endif
								         	 			</select>
								         	 		</div>
								         	 		</div>
								         	 	
							
								          
								         
								           
				         	  	   </div>
				         	  </div>
				         </div>
                          <hr>



                            <div class="row">
									   
					         	 <div class="col-md-12">
					         	 	     <div class="input-group">
					              <div class="input-group-btn">
					                  <button type="button" class="btn btn-primary"><i class="fa fa-plus"></i></button>
					                </div>
					                <input class="form-control" id="pruchaseValue" name="itemName" type="text"  placeholder="Enter item name or scan barcode">  
					                <input type="hidden" id="formpurchasetoken" name="_searchpurchasetoken" value="{{ csrf_token() }}">              
					              </div>

					              <br>
					               <table id="createPurchase" class="table table-hover table-bordered">

				                  <thead>
				                    <tr class="register-items-header">
				                      <th>Action</th>
				                      <th>Item Name</th>
				                      <th>Quantity</th>
				                      <th>After</th>
				                  
				                      <th>Item Cost</th>
									  <th>Tax</th>
									  <th>Total</th>
				                    
				                    </tr>
				                  </thead>
				                <tbody>
				                	<tr id="purchasemessagerow"></tr>
				                </tbody>
				                     
				  
				                 
				                      </table>


					         	 </div>
					         </div>
                             <br>
					         <div class="row">
					         	 <div class="col-md-12">
					         	 	 <div class="col-md-6">
					         	 	 	 <div class="form-group">
					         	 	 	 	  <textarea placeholder="Message to supplier" name="suppliermessage" id="suppliermessage" class="form-control"></textarea>
					         	 	 	 </div>
					         	 	 </div>

					         	 	 <div class="col-md-6"  >
					         	 	 	<ul style="list-style-type:none;">
					         	 	 	   <li class="row-fluid">
					         	 	 	   	<span class="span2"><strong>Total unit</strong></span>
					         	 	 	   	<span class="span2 totalunits lead" style="float:right;font-size:20px; font-weight:bold;">0</span>
					         	 	 	   </li> <hr>	

					         	 	 	    <li class="row-fluid">
					         	 	 	   	<span class="span6"><strong>SubTotal</strong></span>
					         	 	 	   	<span class="span6 subtotal" style="float:right;font-size:20px; font-weight:bold;">0</span>
					         	 	 	   </li> <hr>	

					         	 	 	    <li class="row-fluid">
					         	 	 	   	<span class="span6"><strong>Plus VAT</strong></span>
					         	 	 	   	<span class="span6 vat" style="float:right;font-size:20px; font-weight:bold;">0</span>
					         	 	 	   </li> <hr>	

					         	 	 	    <li class="row-fluid">
					         	 	 	   	<span class="span6"><strong>Total Cost</strong></span>
					         	 	 	   	<span class="span6 supertotal" style="float:right;font-size:20px; font-weight:bold;">0</span>
					         	 	 	   </li>

					         	 	 	   <hr>	
					         	 	 	</ul>
					         	 	 </div>
					         	 </div>
					         </div>
									         



              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
    </div>





</div><!-- /.content-wrapper -->
            
@include('includes.footer')
         
@stop
