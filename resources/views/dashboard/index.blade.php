@extends('master')
@section('title', 'Home')
@section('content')
  @include('includes.header')
   @include('includes.sidebar')

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
           <i class="fa fa-dashboard"></i> Dashboard
            <small>Control panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol>
        </section>
        <div  class="box box-primary"></div> <!-- blue line  on top-->

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="ion ion-ios-cart-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Today's Sales</span>
              <span class="info-box-number">{{count($totalsales)}}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="ion-ios-folder-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total Items</span>
              <span class="info-box-number">{{count($products)}}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total Purchases</span>
              <span class="info-box-number">{{$totalpurchase}}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total Customers</span>
              <span class="info-box-number">{{count($customerdata)}}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
      </div><!-- /.row -->
          <!-- Main row -->
          <!-- summary reports -->


      <!-- Main content -->
    <section class="content">
      <div class="row">
         <!--  <div class="col-md-12"> -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Top 10 Most Selling Items</h3>
               <input type="hidden" name="_topsellingtoken" value="<?php echo csrf_token(); ?>">

              <div class="box-tools pull-right">
            
               <button data-original-title="Date range" type="button" class="btn btn-primary btn-sm daterange pull-right" data-toggle="tooltip" title="">
                  <i class="fa fa-calendar"></i></button>
                <button data-original-title="Collapse" type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;">
                  <i class="fa fa-minus"></i></button>
               
              </div>
            </div>
            <!-- /.box-header -->
      
        
            
             

                      <div class="box-body">
                           <p class="text-center" id="topsellingdaterange"> </p>
                        <div id="topsaleschart">
                          <canvas id="topsale" style="height:400px"></canvas>
                        </div>


                      </div>
                
  
           
  
            <!-- ./box-body -->
           
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
       <!--  </div> -->
      </div>
      <!-- /.first row -->





    </section>
    <!-- /.content -->

  
    </section><!-- /.content -->


           <!-- Modal to edit user -->
          <!-- <div class="modal fade in" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
         <div class="modal-dialog modal-lg">
                <div class="modal-content">
                         <div class="modal-header">
                               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <h4 class="modal-title" id="myModalLabel">Edit</h4>
                         </div>
                         <div class="modal-body">
      



                                    <div class="modal-footer">
                                       <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                       <button type="submit" class="btn btn-primary">Save</button>
                                    </div>
               
                          </div>

                      
                </div>
         </div>
   </div> -->


 </div><!-- /.content-wrapper -->
 
            
        @include('includes.footer')

        


@stop

