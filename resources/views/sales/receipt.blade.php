@extends('master')
@section('title', 'Sales-Receipt')
@section('content')
  @include('includes.header')
   @include('includes.sidebar')


              <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <i class="fa fa-shopping-cart"></i> Sales
            <small>Control panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="/home"><i class="fa fa-home"></i> Home</a></li>
            <li class="active"><a href="/sales">Sales</a></li>
          </ol>
        </section>
         <div  class="box box-primary"></div> <!-- blue line  on top-->

<!--  @if(count($saleorders)) -->

         <div class="pad margin no-print">
            <div class="row">
                 <div class="col-md-12">
                     <div class="col-md-4"></div>
                     
                 	 <div class="col-md-6">
                 	 	  <a class="btn btn-app bg-orange" onclick="ePrint()">
                               <i class="fa fa-print"></i> Print
                          </a>

                          

                          <a class="btn btn-app btn bg-navy" href="#">
                               <i class="fa fa-envelope-o"></i> Email Receipt
                          </a>

                          <a class="btn btn-app btn bg-maroon" href="#">
                               <i class="fa fa-file-pdf-o"></i> PDF
                          </a>

                          <a class="btn btn-app btn bg-olive" href="/sales">
                               <i class="fa  fa-cart-plus"></i> New Sale
                          </a>
                 	 </div>
                 	 <div class="col-md-2"></div>
                 </div>
            </div>
         </div>



        <section class="invoice myprint" >
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-globe"></i> csms
           <small class="pull-right">Printed on: <?php echo date('d-m-Y')?></small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          @if(count($companydetails))
          @foreach($companydetails as $companydetail)
          <address>
            
            <strong>{{$companydetail->companyName}}.</strong><br>
            {{$companydetail->companyAddress1}}<br>
            <!-- Oysterbay, Dar es Salaam<br> -->
            Phone: {{$companydetail->companyContact1}}<br>
            Email: {{$companydetail->companyEmail}}
          </address>
          @endforeach
          @endif
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
        
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
        @if(count($snumber))
            
          <b>Receipt #:   {{$snumber}}</b><br>
          <br>
       
        @endif
          <!-- <b>Order ID:</b> 4F3S8J<br> -->
           
          <b>Payment Due:</b> @foreach($paymentdue as $paymentdue){{$paymentdue->created_at}}@endforeach
           <br>
          
          <b>Served by: </b>  {{ $userdata->name }}
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

       <div class="row invoice-info">
                <div class="text-center">
                    <h1>Receipt</h1>
                </div>
       </div>

      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-bordered" id="receiptTable">
            <thead>
            <tr>
              <th>Item</th>
               <th>Qty</th>
               <th>Price</th>
              <th>Subtotal</th>
            </tr>
            </thead>
            <tbody>
          
            @foreach($saleorders as $saleorder)
          
            <tr>
              <td>{{$saleorder->productName}}</td>
              <td>{{$saleorder->quantity}}</td>
              <td>{{$saleorder->unitPrice}}</td>
              <td class="itemUnitePriceTotal">{{$saleorder->quantity * $saleorder->unitPrice}}</td>

            </tr>
            @endforeach
            
            @foreach($vat as $vat)
          <input type="hidden" name="vatreceipt" class="vatreceipt" value="{{$vat->vat}}">
          @endforeach
            </tbody>
          </table>
           
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <div class="col-xs-6" >
        	 <textarea style="display:none;" class="form-control"></textarea>
        </div>
         
        <!-- /.col -->
        <div class="col-xs-6">
          <!-- <p class="lead">Amount Due 2/22/2014</p> -->

          <div class="table-responsive" id="calculateTable">
            <table class="table">
              <tbody><tr>
                <th style="width:50%">Subtotal:</th>
                <td class="receiptSubtotal">R 0.00</td>
              </tr>
              <tr>
                <th>Vat</th>
                <td class="calculateTableVAT">R 0.00</td>
              </tr>
              <tr>
           
              <tr>
                <th>Total:</th>
                <td class="calculatedTableSuperTotal">R 0.00</td>
              </tr>
            </tbody></table>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    @else


    <section class="content">
      <div class="error-page">
        

        <div class="error-content">
        <!-- <h2 class="headline text-yellow"> 404</h2> -->
          <h3><i class="fa fa-warning text-yellow"></i> Oops! Could't find the receipt</h3>

          <p>
            We could not find the receipt number you were looking for.
            Meanwhile, you may <a href="/sales">return to sale</a>.
          </p>

        </div>
        <!-- /.error-content -->
      </div>
      <!-- /.error-page -->
    </section>

    @endif

        </div>

    @include('includes.footer')
         
@stop