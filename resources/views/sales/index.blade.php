@extends('master')
@section('title', 'Sales')
@section('content')
  @include('includes.header')
   @include('includes.sidebar')

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <i class="fa fa-shopping-cart"></i> Sales
            <small>Control panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="/home"><i class="fa fa-home"></i> Home</a></li>
            <li class="active"><a href="/sales">Sales</a></li>
          </ol>
        </section>
      
      <section>
         <div class="col-md-12">
              <div class="col-md-4"></div>
              <div class="col-md-4">@include('flash::message')</div>
              <div class="col-md-4"></div>
         </div>     
      </section>
      <div  class="box box-primary"></div> <!-- blue line  on top-->
      
    <section class="content">

           <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs" id="createNotTab">
                              <li class="active"><a href="#newsale" data-toggle="tab" aria-expanded="true"><strong>New Sale</strong></a></li>
                              <li class=""><a href="#saleslist" data-toggle="tab" aria-expanded="false"><strong>Sales List</strong></a></li>
                            </ul>
                            <div class="tab-content">


                                      <div class="tab-pane active" id="newsale" style="background-color:#ECF0F5;padding:5px;">
                                              <div class="row">      
        <!-- Left col -->
        <section class="col-lg-7">
          <!-- Chat box -->
          <div class="box box-solid">
            <div  class="box-body">
               
                 
              <div class="input-group-lg">
              <!-- <div class="input-group-btn">
                  <button type="button" class="btn btn-primary"><i class="fa fa-plus"></i></button>
                </div> -->
                <input class="form-control" id="salesValue" name="itemName" type="text"  placeholder="Enter item name or scan barcode">  
                <input type="hidden" id="formtoken" name="_itemsaletoken" value="{{ csrf_token() }}">              
              </div>
            
          
          </div>
          </div>
          <!-- /.box (chat box) -->

           <div class="box box-solid">
              <div  class="box-body">
               <div class="box-body table-responsive no-padding">
                <table id="register" class="table table-hover">

                  <thead>
                    <tr class="register-items-header">
                      <th>Action</th>
                      <th class="item_name_heading">Item Name</th>
                      <th class="sales_quantity">Quantity</th>
                      <th class="sales_price">Price</th>
                      <th class="sales_tax">Vat</th>  
                      <th>Total</th>
                    </tr>
                  </thead>
                
                     <tr id="messagerow"></tr>
                      <!--  <tr class="">
                           <td><a href="" style="color: #ff7474;"><i class="icon ion-android-cancel"></i></a></td>
                          <td class="text-center">Ciment</td>
                          <td class="text-center">R 18,500</td>
                          <td class="text-center">1</td>
                          <td class="text-center">0%</td>
                           <td class="text-center">R 18,500</td>
                          <td colspan="6">
                            <div class="text-center text-warning"> <h3>There are no items in the cart<span class="flatGreenc"> [Sales]</span></h3></div>
                          </td>
                        </tr> -->

                 
          </table>
          </div>
             </div>
             
          </div>
          <!-- /.box (chat box) -->

          


        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        <section class="col-lg-5">

          <div class="box box-solid">
            <div class="box-body border-radius-none">
              <div class="form-group">
                <div class="btn-group">
                <button type="button" class="btn btn-warning"  aria-expanded="false">
                  <i class="ion-pause"></i> Suspend Sale </button>
              </div>
                 
               <div class="btn-group">
                  <button type="button" class="btn btn-danger"  aria-expanded="false">
                  <i class="ion-close-circled"></i> Cancel Sale </button>
              </div>
                 
              <!--  <div class="btn-group">
                  Sale No:<span style="font-size:30px;"><strong>200</strong></span>
              </div>
               -->
            

            </div>

             <div class="form-group">
                 <select name="storeName" class="form-control" id="storeNameonSale">
                  @if(count($storez))
                    <option value="">Choose store</option>
                    @foreach($storez as $store)
                     <option value="{{ $store->id }}">{{ $store->storeName }}</option>
                    @endforeach
                  @else
                     <option value="">No store(s) created</option>
                @endif
                 </select>
              </div>

            <hr>

             <div class="form-group">
        
                <div class="input-group">
                  <div class="input-group-btn">
                  <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".newcustomer"><i class="ion-person-add"></i></button>
                </div>
      
                 <select class="form-control" id="customerdetls" style="width: 100%;">
                 <option selected="selected" value=""></option>
                  @foreach($customerdata as $customers)
                  <option  value="{{ $customers->id}}">{{ $customers->customerName }}</option>
                  @endforeach
                </select>


               
                </div>
                <br>
              
             

                <!-- <div class="form-group">
                  <select class="form-control">
                     <option  selected="selected" value="0.18">Tax Inclusive</option>
                     <option>Tax Exclusive</option>
                  </select>
                    
                </div> -->


                <!-- /.input group -->
              </div>

              
               
            </div>
          </div>
          <!-- /.box -->

          <div class="box box-solid">
            <div class="box-body border-radius-none" style="display: block;">
               <!-- <div class="row" style="margin-top: 5px;">
                 <div class="col-md-8">Discount all Items by Percent:</div>
                 <div class="col-md-4">50%</div>
               </div>
               <br> -->
               <!-- <div class="row" style="margin-top: 5px;">
                
                 <center><div class="col-md-12 checkbox icheck"><strong>SALE INCLUDE TAX:</strong>     <input type="checkbox"  name="tax" class="flat-red saletax" value="0.18"></div></center>
               </div>
                  <br> -->
               <div class="row"  style="background-color: #f1ffec;">
                 <div class="col-md-4" style="text-align: center; font-size: 15px; margin-top: 10px;">Sub Total:</div>
                 <div class="col-md-8" id="subtotal" style="text-align: center; margin-top: 10px; font-size: 15px; font-weight: 400px;">R0.00</div>
               </div>
                
                <br>
                 <div class="row" style="margin-top: 5px;  border-top: 1px dashed #d0d3d8; border-bottom: 1px dashed #d0d3d8; font-weight: 300px; font-size: 13px;">
                 <div class="col-md-6 total-amount" style="line-height: 26px; border-right: 1px dashed #d0d3d8;">
                  <span style="font-size: 20px; font-weight: 400px;">Total</span>
                  <br>
                    <div id="totalAmount" style="color: #6fd64b; text-align: center; font-size: 22px;" data-decimals="2">R0.00</div>
                 </div>
                 <div class="col-md-6" style="line-height: 26px;">
                   <span style="font-size: 20px; font-weight: 400px;">Amount Due + VAT</span>
                   <br>
                   <div id="amountDue" style="color: #ff9e28; text-align: center; font-size: 22px;">R0.00</div>
                 </div>
                 </div>

                <div class="row"  style="backgroung-color:red; line-height: 26px; margin-top: 15px; border-bottom: 1px dashed #d0d3d8;">
                 <div class="col-md-6"  style="font-weight: 400px; color: #67676c; font-size: 20px;"> Return Change:</div>
                 <div class="col-md-6" id="change" style="color: #ff9e28; text-align: center; font-size: 22px;">R0.00</div>
               </div>

             <div id="pay">
                 <!-- <div class="row payment">
                    
                     <div class="col-md-12">
                     <br> 
                     Add Payment<br>-->
                       <!-- <button class="btn btn-default cashbutton">Change</button> -->
                       <!-- <button class="btn btn-default checkbutton">Check</button>
                       <button class="btn btn-default giftcardbutton">Gift Card</button> -->
                     <!-- </div>
                 </div> -->
                 <br>
                 <div class="row">
                   <div class="col-md-12">
                         <div class="input-group cash">

                              <input class="form-control" id="paidAmount" type="text" placeholder="Enter cash paid by customer"> 
                              <div class="input-group-btn">
                                <button type="button" class="btn btn-primary" id="completesalebutton"   disabled="true">Complete Sale</button>
                              </div>               
                        </div>

                        <!-- <div class="input-group check">
                              <input class="form-control" type="text" placeholder="Enter check number"> 
                              <div class="input-group-btn">
                                <button type="button" class="btn btn-primary">Complete Sale</button>
                              </div>               
                        </div>

                        <div class="input-group giftCard">
                              <input class="form-control" type="text" placeholder="Enter gift card"> 
                              <div class="input-group-btn">
                                <button type="button" class="btn btn-primary">Complete Sale</button>
                              </div>               
                        </div> -->

                      </div>
                 </div>
                 <br>
                 <div class="row comment">
                     <div class="col-md-12">
                        <fieldset>Comments:</fieldset>
                        <textarea class="form-control"></textarea>
                     </div>
                 </div>


            </div><!-- endpay div -->

            </div>
          </div>
          <!-- /.box -->

  

        </section>
        <!-- right col -->
      </div>
                                      </div>
                                      <div class="tab-pane" id="saleslist">

                                                <h1 class="text-center"><small>List of items sold</small></h1>
                                                <hr>

                                                <table id="mylistofitemsold" class="table table-bordered">
                                                    <thead>
                                                        <tr>
                                                             <th>#</th>
                                                             <th>SO#</th>
                                                             <th>Date</th>
                                                        </tr>
                                                    </thead>

                                                      <tbody>
                                                                                                      
                                                        </tbody>
                                                  
                                                </table>
                                      </div>
                            </div>
                     </div>
   
   

    </section>


          <!-- Modal new customer on sale -->
            <div class="modal fade newcustomer" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
             <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                             <div class="modal-header">
                                   <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <h4 class="modal-title" id="myModalLabel">New Customer Form</h4>
                             </div>
                             <div class="modal-body">
                                  

                               <!--  <form role="form" method="POST" action="/customer/store"> -->
                                
                                          <div class="box-body">
                                                   <div class="form-group">
                                                          <label for="customerName">Customer Name<span style="color:red">*</span></label>
                                                          <input type="text" name="customerNameonSale" placeholder="Customer Name" id="customerNameonsale" class="form-control" required>
                                                           <input type="hidden" id="formtoken" name="_onSaleNewCustomertoken" value="{{ csrf_token() }}">
                                                    </div>

                                                    <div class="form-group">
                                                          <label for="customerCompanyName">Customer Company Name</label>
                                                          <input type="text" name="customerCompanyName" placeholder="Customer Company Name" id="customerCompanyNameonsale" class="form-control">
                                                    </div>

                                                    <div class="form-group">
                                                            <label for="customerEmail">Customer Email</label>
                                                            <input type="email" name="customerEmail" placeholder="Customer Email" id="customerEmailonsale" class="form-control">
                                                    </div>

                                                    <div class="form-group">
                                                            <label for="customerPhoneNumber">Customer Phone Number</label>
                                                            <input type="text" name="customerPhoneNumber" placeholder="Customer Phone Number" id="customerPhoneNumberonsale" class="form-control">
                                                    </div>

                                                   
                                                           
                                                    <div class="form-group">
                                                      <label for="exampleInputFile">Customer Photo</label>
                                                      <input  name ="customerPhoto" id="customerPhotoonsale" type="file">
                                                    </div>

                                                     <div class="form-group">
                                                            <label for="customerAddress">Customer Address</label>
                                                            <input type="text" name="customerAddress" placeholder="Customer Address" id="customerAddressonsale" class="form-control">
                                                    </div>

                                                    <div class="form-group">
                                                            <label for="customerCountry">Customer Country</label>
                                                            <input type="text" name="customerCountry" placeholder="Customer Country" id="customerCountryonsale" class="form-control">
                                                    </div>

                                                    
                                          </div><!-- /.box-body -->

                                        <div class="modal-footer">
                                           <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                           <button type="button" id="onSalenewCustomer" class="btn btn-primary">Save</button>
                                        </div>
                             <!-- </form> -->



                              </div>

                          
                    </div>
             </div>
       </div>




  </div><!-- /.content-wrapper -->

            
    @include('includes.footer')
         
@stop


