/// MENU ///
1.view-users
2.add-users
3.edit-users
4.delete-users
5.view-rps(can create roles,permission,user roles)
6.


/// PRODUCTS ///
1.view-products
2.add-products
3.edit-products
4.delete-products


/// ORDERS ///
1.view-orders
2.add-orders
3.edit-orders
4.delete-orders
5.confirm-orders

/// PURCHASES ///
1.view-purchases
2.add-purchases
3.edit-purchases
4.delete-purchases

/// SALES ///
1.view-sales
2.add-sales
3.delete-sales
4.suspend-sales
5.cancel-sales
6.

/// STOCK & INVENTORY ///
1.view-inventory
2.add-inventory
3.edit-inventory
4.delete-inventory

/// CUSTOMERS ///
1.view-customers
2.add-customers
3.edit-customers
4.delete-customers
5.export-customers
6.import-customers

/// SUPPLIERS ///
1.view-suppliers
2.add-suppliers
3.edit-suppliers
4.delete-suppliers

/// EXPENSES ///
1. view-expenses
2. add-expenses
3. edit-expenses
4. delete-expenses

/// LOCATION STORES ///
1.view-location
2.add-location
3.edit-location
4.delete-location

/// REPORTS ///
1.view-reports
2.create-reports
3.edit-reports
4.delete-reports
