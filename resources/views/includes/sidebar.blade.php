
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="{{asset("bower_components/admin-lte/dist/img/admin.jpg")}}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p>{{ $userdata->name }}</p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- search form -->
          <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </form>
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            @can('view-dashboard')
            <li>
              <a href="/home">
                <i class="fa fa-dashboard"></i>
                    <span>DASHBOARD</span>
              </a>
            </li>
          @endcan

           @can('view-account')
             <li>
              <a href="/accounting">
                <i class="fa fa-money"></i> <span>ACCOUNTING</span>
                     
              </a>
            </li>
            @endcan
             

          @can('view-users')
             <li>
              <a href="/employee">
                <i class="fa fa-user"></i> <span>EMPLOYEES</span>
                     <small class="label pull-right bg-red">{{ count($user) }}</small>
              </a>
            </li>
          @endcan          
          
         
         @can('view-products')
            <li>
             
              <a href="/product">
                <i class="fa fa-tags"></i> <span>INVENTORY</span>
                     <small class="label pull-right bg-green">{{ count($products) }}</small>
              </a>
            
            </li>
       @endcan
   
           <!--  <li>
              <a href="/orders">
                <i class="fa fa-folder"></i> <span>Orders</span>
                     <small class="label pull-right bg-yellow">2</small>
              </a>
            </li> -->
         @can('view-purchases')
            <li disabled="true">
              <a href="/purchases" >
                <i class="fa fa-file-text"></i> <span>PURCHASES</span>
                     <small class="label pull-right bg-red">0</small>
              </a>
            </li>
       @endcan
         
       
       @can('view-sales')
           <li>
              <a href="/sales">
                <i class="fa fa-shopping-cart"></i> <span>SALES</span>
                     <!-- <small class="label pull-right bg-green">8</small> -->
              </a>
            </li>
      @endcan

            
          @can('view-stock_control')
           <li class="treeview">
              <a href="/stock">
                <i class="fa  fa-cubes"></i> <span>STOCK CONTROL</span>
              </a>
            </li>
          @endcan

         @can('view-customers')
              <li>
              <a href="/customer">
                  <i class="fa fa-users"></i>
                <span>CUSTOMERS</span>
         
                     <span class="label label-primary pull-right">{{ count($customerdata) }}</span>

              </a>
            </li>
        @endcan

          @can('view-suppliers')
             <li>
              <a href="/supplier">
                <i class="fa fa-share-alt"></i> <span>SUPPLIERS</span>
                     <small class="label pull-right bg-yellow">{{ count($supplier) }}</small>
              </a>
            </li>
          @endcan
           
           @can('view-expenses')
             <li class="treeview">
              <a href="/expenses">
                <i class="fa  fa-cubes"></i> <span>EXPENSES</span>
              </a>
            </li>
            @endcan
           
            @can('view-location')
             <li class="treeview">
              <a href="/stores">
                <i class="fa  fa-map-marker"></i> <span>STORES</span>
              </a>
            </li>
          @endcan

           
           <!-- @can('view-reports')
            <li class="treeview">
              <a href="/reports">
                <i class="fa fa-bar-chart"></i> <span>REPORTS</span>
              </a>
            </li>
      @endcan -->

    @can('view-reports')
      <li class="treeview">
          <a href="#">
            <i class="fa fa-bar-chart"></i> <span>REPORTS</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul style="display: none;" class="treeview-menu">
            <li><a href="/reports"><i class="fa fa-circle-o"></i> General Reports</a></li>
            <li><a href="/reports/finance"><i class="fa fa-circle-o"></i> Finance Reports</a></li>
         <!--    <li class="">
              <a href="#"><i class="fa fa-circle-o"></i>  Report one<i class="fa fa-angle-left pull-right"></i></a>
              <ul style="display: block;" class="treeview-menu menu-open">
                <li><a href="#"><i class="fa fa-circle-o"></i> Level Two</a></li>
                <li class="">
                  <a href="#"><i class="fa fa-circle-o"></i> Level Two <i class="fa fa-angle-left pull-right"></i></a>
                  <ul style="display: none;" class="treeview-menu">
                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                  </ul>
                </li>
              </ul>
            </li> -->
            
          </ul>
        </li>
      @endcan
      
         @can('view-settings')
            <li class="treeview">
              <a href="/settings">
                <i class="fa fa-cog"></i> <span>SETTINGS</span>
              </a>
            </li>
        @endcan


            <li class="header">Power</li>
            <li><a href="/auth/logout"><i class="fa fa-power-off"></i> <span>Logout</span></a></li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>