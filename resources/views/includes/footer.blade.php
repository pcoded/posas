    <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 1
        </div>
        <strong>Copyright &copy; <?php echo date('Y')?></strong> All rights reserved.
      </footer>
      