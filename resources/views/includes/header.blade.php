<header class="main-header">
        <!-- Logo -->
        <a href="#" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>CSMS</b></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>CSMS</b></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->
              <li class="dropdown messages-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-envelope-o"></i>
                  <span class="label label-success">0</span>
                </a>
                <ul class="dropdown-menu">
                  <li class="header">You have 0 message(s)</li>
                  <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu">
     
                      <!-- <li>
                        <a href="#"> 
                          <div class="pull-left">
                            <img src=" {{asset("bower_components/admin-lte/dist/img/admin.jpg")}}" class="img-circle" alt="User Image">
                          </div>
                          <h4>
                            Reviewers
                            <small><i class="fa fa-clock-o"></i> 2 days</small>
                          </h4>
                          <p>Why not buy a new awesome theme?</p>
                        </a>
                      </li> -->
                    </ul>
                  </li>
                 <!--  <li class="footer"><a href="#">See All Messages</a></li> -->
                </ul>
              </li>
              <!-- Notifications: style can be found in dropdown.less -->
              <li class="dropdown notifications-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-bell-o"></i>
                  <span class="label label-warning">0</span>
                </a>
                <ul class="dropdown-menu">
                  <li class="header">You have 0 notifications</li>
                  <li>
                    <!-- inner menu: contains the actual data -->
                   <!--  <ul class="menu">
                      <li>
                        <a href="#">
                          <i class="fa fa-shopping-cart text-green"></i> 25 sales made
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i class="fa fa-user text-red"></i> You changed your username
                        </a>
                      </li>
                    </ul> -->
                  </li>
                  <li class="footer"><a href="#">View all</a></li>
                </ul>
              </li>
              <!-- Tasks: style can be found in dropdown.less -->
              <li class="dropdown tasks-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-flag-o"></i>
                  <span class="label label-danger">0</span>
                </a>
                <ul class="dropdown-menu">
                  <li class="header">You have 0 tasks</li>
                  <li>
                    <!-- inner menu: contains the actual data -->
                   <!--  <ul class="menu">
        
                      <li>
                        <a href="#">
                          <h3>
                            New item order
                            <small class="pull-right">80%</small>
                          </h3>
                          <div class="progress xs">
                            <div class="progress-bar progress-bar-yellow" style="width: 80%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                              <span class="sr-only">80% Complete</span>
                            </div>
                          </div>
                        </a>
                      </li>
                    </ul>
                  </li> -->
                  <li class="footer">
                    <a href="#">View all tasks</a>
                  </li>
                </ul>
              </li>
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="{{asset("bower_components/admin-lte/dist/img/admin.jpg")}}" class="user-image" alt="User Image">
                  <span class="hidden-xs">{{ $userdata->name }}</span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="{{asset("bower_components/admin-lte/dist/img/admin.jpg")}}" class="img-circle" alt="User Image">
                    <p>
                      {{ $userdata->name }}
                     
                    </p>
                  </li>
                  <!-- Menu Body -->
             <!--      <li class="user-body">
                    <div class="col-xs-4 text-center">
                      <a href="#">Followers</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Sales</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Friends</a>
                    </div>
                  </li> -->
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="#" class="btn btn-default btn-flat" data-toggle="modal" data-target=".bs-example-modal-lg2">Change Password</a>
                    </div>
                    <div class="pull-right">
                      <a href="/auth/logout" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->
            <!--   <li>
                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
              </li> -->
            </ul>
          </div>
        </nav>
      </header>
      <body class="hold-transition skin-blue sidebar-mini">
      <div class="wrapper">


  
      <div class="modal fade bs-example-modal-lg2" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
             <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                             <div class="modal-header">
                                   <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <h4 class="modal-title" id="myModalLabel">Change password</h4>
                             </div>
                             <div class="modal-body">
                                  <form class="form-horizontal" action="/auth/passchange" method="post" >
                                   {!! csrf_field() !!}
                                        <div class="box-body">
                                        <!--   <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-2 control-label">Email</label>

                                            <div class="col-sm-10">
                                              <input class="form-control" name="inputnewEmail" placeholder="Email" type="email">
                                              <input type="hidden" name="_resetpasstoken" value="<?php echo csrf_token(); ?>">
                                            </div>
                                          </div> -->
                                          <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-2 control-label">New Password</label>
                                            <div class="col-sm-10">
                                              <input class="form-control" name="inputnewPassword" placeholder="Password" type="password">
                                              <input type="hidden" name="_resetpasstoken" value="<?php echo csrf_token(); ?>">
                                            </div>
                                          </div>

                                          <div class="form-group">
                                            <label for="inputPassword4" class="col-sm-2 control-label">Confirm New Password</label>
                                            <div class="col-sm-10">
                                              <input class="form-control" name="newconfirmpassword" id="inputresetPassword4" placeholder="Confirm New Password" type="password">
                                            </div>
                                          </div>

                                        </div>
                                        <!-- /.box-body -->
                                        <div class="box-footer">
                                          <button type="submit" id="resetpass" class="btn btn-success pull-right">Save</button>
                                        </div>
                                        <!-- /.box-footer -->
                                      </form>
                             </div>
                      </div>
                </div>
         </div>

