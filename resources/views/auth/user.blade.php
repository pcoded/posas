@extends('master')
@section('title', 'Employees')
@section('content')
  @include('includes.header')
   @include('includes.sidebar')

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <i class="fa fa-users"></i> Employees
            <small>Control panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="/home"><i class="fa fa-home"></i> Home</a></li>
            <li class="active"><a href="/home/user">Users</a></li>
          </ol>
        </section>
        <div  class="box box-primary"></div> <!-- blue line  on top-->

          @include('errors.flash')
          @include('errors.formerrors')
          @include('errors.deleteajax')
        <section class="content">



       <div class="row">
                                                        
         <div class="col-md-12">
              <h4>Roles, Permissions, &  User Registration</h4>
                  <!-- Custom Tabs -->
                  <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs" id="createNotTab">
                              <!-- <li class="active"><a href="#create_user" data-toggle="tab" aria-expanded="true"><strong>Create User</strong></a></li> -->
                                <li class="active"><a href="#create_roleTab" data-toggle="tab" aria-expanded="false"><strong>Create Roles</strong></a></li>
                                <!-- <li class=""><a href="#create_permissionTab" data-toggle="tab" aria-expanded="true"><strong>Create Permissions</strong></a></li> -->
                                <li class=""><a href="#assign_permissionTroleTable" data-toggle="tab" aria-expanded="false"><strong>Permission & Roles</strong></a></li>
                              
                                <li class="">
                                <a href="#assignUser_toRoleTab" data-toggle="tab" aria-expanded="false"><strong>User Roles</strong></a>
                                </li>

                                <li class="">
                                <a href="#userList" data-toggle="tab" aria-expanded="false"><strong>Users List</strong></a>
                                </li>
                            </ul>
                            <div class="tab-content">


                            <!-- CREAT ROLES TAB -->
                          <div class="tab-pane active" id="create_roleTab">

                          <div class="row">
                                <div class="col-md-12">
                                   <div class="btn-group pull-right" role="group" aria-label="...">
                                        <button type="button" class="btn btn-success btn-flat" data-toggle="modal" data-target=".rolemodal"><i class="fa fa-plus"></i> New Role</button>
                                    </div>
                                </div>
                           </div>
                            <div  class="box box-default" style="margin-bottom:0px;"></div> <!-- blue line  on top-->
                                <table id="rolezTable" class="display table" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                           <th style="display: none;">#</th>
                                            <th>Role Name</th>
                                            <th>Description</th>
                                            <th>Edit</th>
                                            <th>Delete</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                     @if($myroles)
                                       @foreach($myroles as $r)
                                         <tr>
                                           <td  style="display: none;" class="roleid">{{ $r->id }}</td>
                                           <td>{{ $r->rolename }}</td>
                                           <td>{{ $r->description }}</td>
                                           <td><button class="editrolebtn btn btn-success"><i class="fa fa-edit"></i> Edit</button></td>
                                           <td><button class="deleterolebtn btn btn-danger"><i class="fa fa-trash"></i> Delete</button></td>
                                         </tr>
                                       @endforeach
                                     @endif
                                    </tbody>
                                </table>

                            <div id="rolemodal" class="modal fade rolemodal">
                              <div class="modal-dialog modal-lg">
                                  <div class="modal-content">
                                      <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                                          <h4 class="modal-title">Create New Role</h4>
                                      </div>

                                      <div class="modal-body">
                                          <!-- {!! Form::open([ 'url'=> '/settings/createrole']) !!} -->
                                           
                                           <div class="form-group">
                                                <label for="role name">Role Name</label>
                                                <input type="text" class="form-control" name="rolename" placeholder="e.g: super admin or admin or manager. etc">
                                                <input type="hidden" name="_roletoken" value="<?php echo csrf_token(); ?>">
                                           </div>

                                            <div class="form-group">
                                                <label for="role description">Role Descriptions</label>
                                                <textarea id="roledescription" name="roledescription" class="form-control" placeholder="Write a little descriptions about the above role"></textarea>
                                           </div>

                                            <!-- <div class="form-group">
                                            <div class="row">
                                               
                                                <div class="col-md-2 col-md-push-10">
                                                  <button type="submit" id="roleSubmit" class="btn btn-success prevTab btn-block">save</button>
                                                  </div>
                                                </div>
                                            </div> -->
                                          <!--  {!! Form::close() !!} -->
                                      </div>

                                      <div class="modal-footer">

                                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                           <button type="submit" id="roleSubmit" class="btn btn-success prevTab">save</button>
                                      </div>
                                  </div>
                              </div>
                          </div>
                  </div><!-- /.Role tab-pane -->





    <!-- ASSIGN PERMISSION TO ROLE TAB -->
    <div class="tab-pane" id="assign_permissionTroleTable">

       <div class="nav-tabs-custom">
            <ul class="nav nav-tabs" id="createNotTab">
      
                <li class="active"><a href="#assignRoletoPermission" data-toggle="tab" aria-expanded="false"><strong>Assign Role to Permission</strong></a></li>
       
                <li class=""><a href="#manageRolesPermission" data-toggle="tab" aria-expanded="false"><strong>Manage Permissions</strong></a></li>
            </ul>
            <div class="tab-content">
            <div class="tab-pane active" id="assignRoletoPermission">
              <div class="form-group">
                     <select class="form-control" id="roles">
                          <option value="">Choose role name</option>
                          @foreach($roles as $role)
                          <option value="{{ $role->id }}">{{ $role->rolename }}</option>
                          @endforeach
                      </select><br><br>
                <div class="col-md-12">

                  <table id="permissionsTable" class="display table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                           <th>#</th>
                            <th>Permissions Name</th>
                            <th>Check Action</th>
                        </tr>
                    </thead>
                    <tbody>
                     @foreach($perms as $perm)
                        <tr>
                            <td>{{$perm->id}}</td>
                            <td><strong>{{$perm->name}}</strong></td>
                            <td>
                                <input type="checkbox" id="rolesperms" name="rolespermission[]"  value="{{$perm->id}}" class="flat-red" />
                                <input type="hidden" name="_permroletoken" value="<?php echo csrf_token(); ?>">
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                </div>

              <div class="row">
                <div class="col-md-2 col-md-push-10">
                  <button type="button" id="permissionRoleSubmit" class="btn btn-success btn-block">Save</button>
                </div>
               </div>

                  </div>

               </div>

                <div class=" tab-pane" id="manageRolesPermission">
                <div class="form-group">
                       <select class="form-control" id="editedrolesmanager">
                          @foreach($roles as $role)
                          <option value="{{ $role->id }}">{{ $role->rolename }}</option>
                          @endforeach
                      </select><br><br>


                 <!--  <div class="col-md-12"> -->

                  <table id="managepermissionsTable" class="display table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                           <th>#</th>
                            <th>Permissions Name</th>
                            <th>Check Action</th>
                        </tr>
                    </thead>
                    <!-- <tbody id="dynamictablebody">
                      
                    </tbody> -->
                    
                    
                </table>

                <!-- </div> -->

                <div class="row">
                <div class="col-md-2 col-md-push-10">
                  <button type="button" id="updatepermissionRoleSubmit" class="btn btn-success btn-block">Update</button>
                </div>
               </div>

                  </div>
                </div>
            </div>
        </div>



    </div><!-- /.ASSIGN PERMISSION TO ROLE tab-pane -->







    <!-- CREATE USER TAB -->
   <!--  <div class="tab-pane active" id="create_user">

    </div> --><!-- /.CREATE USER tab-pane -->
                               



      <!-- ASSIGN USER ROLE -->
      <div class="tab-pane" id="assignUser_toRoleTab">
      
        <div class="form-group">
         <input type="hidden" name="_roleusertoken" value="<?php echo csrf_token(); ?>">
            <select class="form-control" id="myuserid">
                <option value="">Choose user name</option>
                @foreach($user3 as $uss)
                <option value="{{ $uss->id }}">{{ $uss->name }}</option>
                @endforeach
            </select><br><br>

            <select class="form-control" id="rolesxx">
                <option value="">Choose role name</option>
                @foreach($roles as $role)
                <option value="{{ $role->id }}">{{ $role->rolename }}</option>
                @endforeach
            </select><br>

              <div class="row">
                <div class="col-md-2 col-md-push-10">
                  <button type="button" id="roleuserSubmit" class="btn btn-success btn-block">Save</button>
                </div>
                  
               </div>
        </div>
    </div><!-- /.ASSIGN USER ROLE tab-pane -->





    <!-- TAB2 -->
      <div class="tab-pane" id="userList">
              
               <!-- Small boxes (Stat box) -->
              <div class="row">
                       <div class="col-md-12">
                                 <div class="btn-group pull-right" role="group" aria-label="...">
                                 <button type="button" class="btn btn-success btn-flat" data-toggle="modal" data-target=".addnewuser"><i class="fa fa-plus"></i> Add New Employee</button> 
                                     <button type="button" class="btn bg-red btn-flat"><i class="fa fa-trash"></i> Delete</button>
                            <!--         <button type="button" class="btn bg-orange btn-flat"><i class="fa fa-reply-all"></i> Import From Excel</button> -->
                                    <button type="button" class="btn bg-maroon btn-flat"><i class="fa fa-location-arrow"></i> Export To Excel</button>
                                </div>
                     
                       </div><!-- endcol-12 -->
                </div>



            <div class="row">
                  <div class="col-md-12">
                    @if(count($user))

                <div class="box box-default">
                <div class="box-header ui-sortable-handle" style="cursor: move;">
                
                  <h3 class="box-title">List of Employees</h3>

                </div>
          
          
                  <table id="getposdataTable" class="table table-bordered table-hover">
                    
                    <thead>
                      <tr>
                      <th><i class="fa fa-th"></i></th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th>Edit</th>
                         <th>Delete</th>
                      </tr>
                    </thead>
                    <tbody>
                    @foreach($user as $user)
                      <tr>
                           <td><label><input type="checkbox" class="flat-red"></label></td>
                        <td>{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->rolename}}</td>
                        <td><a href="#" data-toggle="modal" data-target="#{{ $user->id}}"><i class="fa fa-edit"></i> Edit</a></td>
                        <td><a href="users/delete/{{$user->id}}"><i class="fa fa-trash-o" style="color:red"></i> Delete</a></td>
                      </tr>
                                 
                  <!-- Modal to edit user -->
                  <div class="modal fade" id="{{ $user->id }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                      <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <h4 class="modal-title" id="myModalLabel">Edit {{ $user->name }}</h4>
                              </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-4">
                                        <h1><small>Personal Information</small></h1>
                                        <hr>
                                                                        
                              <form action="users/update/" method="post">
                                  {!! csrf_field() !!}

                              <div class="form-group">
                                  <label for="FullName">Name</label>
                                  <input type="text" name="name" value="{{ $user->name }}" class="form-control"  placeholder="Full Name">
                                      <input type="hidden" name="_emptoken" value="<?php echo csrf_token(); ?>">

                                         <input type="hidden" name="usrid" value="{{ $user->id }}">
                                </div>

                                                                

                            <div class="form-group">
                              <label for="phonenumber">Phone Number</label>
                                <input type="text" name="empphonenumber" value="{{ $user->phone}}" class="form-control"  placeholder="Phone Number">
                             </div>

                                <div class="form-group">
                                  <label for="address">Address</label>
                                      <input type="text" name="empaddress" value="{{ $user->address }}" class="form-control"  placeholder="Address">
                                </div>

                                  <div class="form-group">
                                    <label for="country">Country</label>
                                      <input type="text" name = "empcountry" value="{{ $user->country }}" class="form-control"  placeholder="Country">
                                    </div>

                                      <div class="form-group">
                                          <label for="userfile">Employee Photo</label>
                                          <input type="file" id="userfile" name="empphoto">
                                        
                                         </div>
                                             </div>

                                             <!-- edit loging information -->
                                             <div class="col-md-4">

                                                 <h1><small>Login Informations</small></h1>
                                                    <hr>


                                                       <div class="form-group  has-feedback">
                                                        <label for="email">Email</label>
                                                        <input type="email" name="email"  value="{{ $user->email }}" class="form-control"  placeholder="email">
                                                           <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                                      </div>

                                              

                                                        <div class="form-group has-feedback">
                                                         <label for="password">Password</label>
                                                          <input type="password" name="password" class="form-control" placeholder="Password" required>
                                                          <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                                        </div>

                                                      <div class="form-group has-feedback">
                                                         <label for="password">Comfirm Password</label>
                                                        <input type="password" name="confirmed" class="form-control" placeholder="Retype password" required>
                                                        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                                                      </div>
                                                     
                                             </div>

                                              <!--  edit roles information -->
                                               <div class="col-md-4">
                                                         <h1><small>Role Informations</small></h1>
                                                         <hr>
                                                         <div class="form-group">
                                                         <label for="role">Role</label>
                                                         <select class="form-control" name="role" >
                                                          <option value="">Choose role name</option>
                                                          @foreach($roles as $role)
                                                          <option value="{{ $role->id }}">{{ $role->rolename }}</option>
                                                          @endforeach
                                                      </select>
                                                      </div>

                                               </div>
                                          </div>
                                      </div>

                                                                        
                                      <div class="modal-footer">
                                         <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                         <button type="submit" class="btn btn-primary">Save</button>
                                      </div>

                                      </form>
                       
                                  </div>

                              
                        </div>
                 </div>
           </div>


                    @endforeach
                    </tbody>
                  </table>

         
           
              </div>


            @else
            <hr>
               <div class="col-xs-6 col-md-push-3">
                    <div class="alert alert-danger" role="alert">
                       <h4 class="text-center"> No Supplier have been added  yet.</h4>
                     </div>
               </div>
            @endif


            </div>
           </div> <!-- Main row -->


                  <div id="addnewuser" class="modal fade addnewuser">
                      <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                              <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                                  <h4 class="modal-title">Create new user</h4>
                                </div>

                                <div class="modal-body">
                                      {!! Form::open(['url'=>'users/registeruser']) !!}
                                      {!! csrf_field() !!}
                                      <h1><small>Basic Personal Information</small></h1>
                                      <hr>
                                          <div class="form-group">
                                            <label for="FullName">Name</label>
                                            <input type="text" name="empname" class="form-control" id="FullName" placeholder="Full Name">
                                            <input type="hidden" name="_emptoken" value="<?php echo csrf_token(); ?>">
                                          </div>

                        

                                          <div class="form-group">
                                            <label for="phonenumber">Phone Number</label>
                                            <input type="text" name="empphonenumber" class="form-control" id="phonenumber" placeholder="Phone Number">
                                          </div>

                                          <div class="form-group">
                                            <label for="address">Address</label>
                                            <input type="text" name="empaddress" class="form-control" id="address" placeholder="Address">
                                          </div>

                                          <div class="form-group">
                                            <label for="country">Country</label>
                                            <input type="text" name = "empcountry" class="form-control" id="country" placeholder="Country">
                                          </div>

                                          <div class="form-group">
                                            <label for="userfile">Employee Photo</label>
                                            <input type="file" id="userfile" name="empphoto">
                                  
                                          </div>

                                             <h1><small>Login Informations</small></h1>
                                             <hr>
                                            <div class="form-group  has-feedback">
                                                <label for="email">Email</label>
                                                <input type="email" name="empemail" class="form-control" id="email" placeholder="email">
                                                   <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                              </div>

                                      

                                                <div class="form-group has-feedback">
                                                 <label for="password">Password</label>
                                                  <input type="password" name="emppassword" class="form-control" placeholder="Password" required>
                                                  <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                                </div>

                                              <div class="form-group has-feedback">
                                                 <label for="password">Comfirm Password</label>
                                                <input type="password" name="emppasswordconfirm" class="form-control" placeholder="Retype password" required>
                                                <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                                              </div>


                                     
                                </div>

                               

                              <div class="modal-footer">
                                       <button type="reset" class="btn btn-default">Reset</button>
                                       <button type="button" id="empSubmit" class="btn btn-success">Save</button>
                              </div>
                               {!! Form::close() !!}
                                
                            </div>
                              </div>
                          </div>

           </div><!-- /.Role tab-pane -->




    </div><!-- /.tab-content -->
  </div><!-- nav-tabs-custom -->
  </div><!-- end col-md-12 for custom tabs -->
  </div>


    <!--  MODAL TO EDIT ROLES -->
    <div id="Editrolemodal" class="modal fade">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                    <h4 class="modal-title">Edit Role</h4>
                </div>

                <div class="modal-body">
                     
                     <div class="form-group">
                          <label for="role name">Role Name</label>
                          <input type="text" class="form-control" name="editrolename">
                          <input type="hidden" name="_roletoken" value="<?php echo csrf_token(); ?>">
                          <input type="hidden" name="edited_id">
                     </div>

                      <div class="form-group">
                          <label for="role description">Role Descriptions</label>
                          <textarea id="roledescription" name="editroledescription" class="form-control"></textarea>
                     </div>

                </div>

                <div class="modal-footer">

                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                     <button type="submit" id="EditroleSubmit" class="btn btn-success prevTab">save</button>
                </div>
            </div>
        </div>
    </div>
     <!--  END MODAL TO EDIT ROLES -->

          
</section><!-- /.content -->


</div><!-- /.content-wrapper -->
         
@include('includes.footer')       
@stop
