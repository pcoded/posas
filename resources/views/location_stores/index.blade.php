@extends('master')
@section('title', 'Stores')
@section('content')
  @include('includes.header')
   @include('includes.sidebar')

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <i class="fa fa-users"></i> Stores Location
            <small>Control panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="/home"><i class="fa fa-home"></i> Home</a></li>
            <li class="active"><a href="/stores">Stores</a></li>
          </ol>
        </section>
        <div  class="box box-primary"></div> <!-- blue line  on top-->

          @include('errors.flash')
          @include('errors.formerrors')
          @include('errors.deleteajax')
        <section class="content">




       <div class="row">
                                                        
         <div class="col-md-12">
             <!--  <h4>Settup company details and other informations</h4> -->
                  <!-- Custom Tabs -->
                  <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs" id="createNotTab">
                              <li class="active"><a href="#create_user" data-toggle="tab" aria-expanded="true"><strong>Create Store</strong></a></li>
                               
                                <li class=""><a href="#assign_permissionTroleTable" data-toggle="tab" aria-expanded="false"><strong>Stores List</strong></a></li>
                              
                               <!--  <li class=""><a href="#assignUser_toRoleTab" data-toggle="tab" aria-expanded="false"><strong>Store4</strong></a></li> -->
                            </ul>
                            <div class="tab-content">




              <!--TAB1 -->
              <div class="tab-pane active" id="create_user">
                             <div class="row">
                             {!! Form::open(['url'=>'/stores/store']) !!}
                             {!! csrf_field() !!}
                                     <div class="col-md-12">
                                           <h1><small>Basic Store details</small></h1>
                                           <hr>

                                 
                                                <div class="form-group">
                                                 <label for="role name">Store Name</label>
                                                  <input type="text" name="name" class="form-control" placeholder="Name of store or Location">
                                                  
                                                </div>
                                                <div class="form-group">
                                                <label for="role name">Store Email</label>
                                                  <input type="email" name="email" class="form-control" placeholder="Email">
                                                 
                                                </div>

                                            <div class="form-group">
                                            <label for="role name">Store Address</label>
                                            <input type="text" name="address" class="form-control" placeholder="Address: e.g 123 Mirambo Street">
                                           
                                             </div>

                                              <div class="form-group">
                                              <label for="role name">Store Mobile</label>
                                            <input type="text" name="phonenumber" class="form-control" placeholder="Phone Number">
                                           
                                             </div>

                                              <div class="form-group">
                                              <label for="user name">Employees</label>
                                              <select name="usrid" class="form-control">
                                                <option value=""></option>
                                                @foreach($user as $usr)
                                                   <option value="$usr->id">{{$usr->name}}</option>
                                                @endforeach
                                              </select>
                                              </div>


                                     
                                                  <div class="row">
                                                  <div class="col-md-2 col-md-push-10">
                                                    <button type="submit" id="storeSubmit" class="btn btn-success btn-block">Save</button>
                                                  </div>
                                                    
                                                 </div>
                                     </div>
                             </div><!-- end row -->
                             {!! Form::close() !!}
                 </div><!-- /.CREATE store tab-pane -->
                               
                      

                                 

         <!-- TAB3 -->
          <div class="tab-pane" id="assign_permissionTroleTable">
                   <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
                   <div class="col-md-12">

                             <div class="btn-group pull-right" role="group" aria-label="...">
                               <!--  <button type="button" class="btn btn-success btn-flat" data-toggle="modal" data-target=".bs-example-modal-lg"><i class="fa fa-plus"></i> Add New Store</button> -->
                                 <button type="button" class="btn bg-red btn-flat" id="store"><i class="fa fa-trash"></i> Delete</button>
                                <button type="button" class="btn bg-orange btn-flat" data-toggle="modal" data-target="#excel"><i class="fa fa-reply-all"></i> Import From Excel</button>
                                <button type="button" class="btn bg-maroon btn-flat"><i class="fa fa-location-arrow"></i> Export To Excel</button>
                            </div>
                        
                   </div><!-- endcol-12 -->
            </div>

            <div  class="box box-default" style="margin-bottom:0px;"></div> <!-- blue line  on top-->

            <div class="row">
                  <div class="col-xs-12">
                    @if(count($stores))

                <div class="box box-default">
                <div class="box-header ui-sortable-handle" style="cursor: move;">
                
                  <h3 class="box-title">List of Stores</h3>
                </div><!-- /.box-header -->
          
                
                
                  <table  id="getposdataTable" class="table table-bordered table-hover">
                    
                    <thead>
                      <tr>
                      <th><i class="fa fa-th"></i></th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Address</th>
                        <th>Phone Number</th>
                        <th>Edit</th>
                         <th>Delete</th>
                      </tr>
                    </thead>
                    <tbody>
                    @foreach($stores as $store)
                      <tr>
                          <td>
                                  <input type="checkbox" name="storeIds[]"  id="idinput" value="{{$store->id}}"  class="flat-red">
                                
                          </td>
                        <td>{{$store->storeName}}</td>
                        <td>{{$store->storeEmail}}</td>
                        <td>{{$store->storeAddress}}</td>
                        <td>{{$store->storePhoneNumber}}</td>
                        <td><a href="#" data-toggle="modal" data-target="#{{ $store->id}}"><i class="fa fa-edit"></i> Edit</a></td>
                        <td><a href="/stores/delete/{{$store->id}}" onclick="return confirm('Are you sure you want to delete this store?');"><i class="fa fa-trash-o" style="color:red"></i> Delete</a></td>
                      </tr>
                    @endforeach
                    </tbody>
                  </table>

             
              </div>


                      @else
                      <hr>
                         <div class="col-xs-6 col-md-push-3">
                              <div class="alert alert-warning" role="alert">
                                 <h5 class="text-center"> No Store have been added  yet.</h5>
                               </div>
                         </div>
                      @endif


            </div>



           </div> <!-- Main row -->
         
        </section><!-- /.content -->
           </div><!-- /.ASSIGN PERMISSION TO ROLE tab-pane -->

                      <!-- TAB4 -->
                     <!--  <div class="tab-pane" id="assignUser_toRoleTab">
                                settings4
                       </div> --><!-- /.ASSIGN USER ROLE tab-pane -->

    </div><!-- /.tab-content -->
  </div><!-- nav-tabs-custom -->
  </div><!-- end col-md-12 for custom tabs -->
  </div>
          
</section><!-- /.content -->


@foreach($stores as $store)
       <div class="modal fade" id="{{ $store->id }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
             <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                             <div class="modal-header">
                                   <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                  <h4 class="modal-title" id="myModalLabel">Edit {{ $store->storeName }}</h4>
                             </div>
                             <div class="modal-body">
                                  

                                   <form method="POST" action="/stores/update">
                                       {!! csrf_field() !!}
                                          <div class="form-group">
                                            <input type="text" name="name" value="{{ $store->storeName}}" class="form-control" placeholder="Name of store or Location">
                                            
                                          </div>
                                          <div class="form-group">
                                            <input type="email" name="email" value="{{ $store->storeEmail}}"  class="form-control" placeholder="Email">
                                           
                                          </div>
                                          <div class="form-group">
                                            <input type="text" name="address" value="{{ $store->storeAddress}}" class="form-control" placeholder="Address: e.g 123 Mirambo Street">
                                           
                                          </div>
                                          <div class="form-group">
                                            <input type="text" name="phonenumber" value="{{ $store->storePhoneNumber}}" class="form-control" placeholder="Phone Number">
                                            <input type="hidden" name="asdhgasha" value="{{ $store->id }}">
                                          </div>
                                                 
                                                  <div class="modal-footer">
                                           <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                           <button type="submit" class="btn btn-primary">Save</button>
                                        </div>
                                                    
                                          </form></div><!-- /.box-body -->
                            

                              </div>
                    </div>
             </div>
         @endforeach

        {{!! Form::open(['url'=>'stores/importExcel', 'files'=>true]) !!}

            @include('partials.uploadform')

       {!! Form::close() !!}




</div><!-- /.content-wrapper -->
         
@include('includes.footer')       
@stop
