$(document).ready(function(){
  // get purchases data
 $("#ListPurchases").DataTable({
 	      scrollY: 400,
          paging: false,
          ordering:true,
          searching:true,
          processing: true,
          serverSide: false,
          ajax: "purchase/ajaxgetpurchaselist",
          dataSrc:"Data",
          columns:[
                  {data:'purchId',
                     render:function(data,type,row){
                      return '<label><input type="checkbox" class="bulkdel" name="purchaseids[]" value='+ data +' class="flat-red"></label>'
                     },
                    },

                   { data: 'poNumber', 
                     render:function(data,type,row){
                      return '<a href="purchase/purchase/'+ data +'" data-toggle="tooltip" data-placement="top" title="click to view more">'+ data +'</a>'
                     }
                   },
                   // {data:'supplierName',name:'supplierName'},
                   {data:'purchaseStatus',
                     render:function(data,type,now){
                     	if(data == 0)
                     	{
                           return '<h5><span class="label label-warning">Order Active</span></h5>';
                     	}
                     	else if(data == 1)
                     	{
                     		return '<h5><span class="label label-info">Awaiting Payment</span></h5>';
                     	}
                     	else{
                     		return '<h5><span class="label label-success">Order Closed</span></h5>';
                     	}
                     }
                   },
                   {data:'sumorderq', name:'sumorderq'},

                   {data:'totalcost',name:'totalcost'},

                   {data:'purchaseDueDate',name:'purchaseDueDate'},

                   {data:'deliveryDate',name:'deliveryDate'},

                   {data:'updated_at',name:'updated_at'},

                   {data:'purchId',
                     render:function(data,type,row){
                      return '<button class="purchaseEditbtn"><i class="fa fa-edit"></i> Edit</button>'
                     }
                    },

                    {data:'purchId',
                     render:function(data,type,row){
                      // return '<a href="/stock/delete/'+data+'" ><i class="fa fa-trash-o" style="color:red"></i> Delete</a>'
                      return '<button class="purchaseDeletebtn"><i class="fa fa-trash-o" style="color:red"></i> Delete</button>'
                     }
                    },
                ],
         "columnDefs": [ {
              "targets": [0,8,9],
              "orderable": false,
              "searchable": false
                
              } ],
              select:true,
 });


 // MUlitple row delete purchases
 //$("#bulkdelpurchase").attr('disabled',true);
   $(".bulkdel").on('click', function(){

       //$("#bulkdelpurchase").attr('disabled',false);

        var status = this.checked;
        $(".bulkdel").each(function(){
          $(this).prop("checked",status);
          
        });

      });


   // BULCK DELETE
       $("#bulkdelpurchase").on("click",function(event){
           if($('.bulkdel:checked').length > 0){
            var purcids = [];
              $('.bulkdel').each(function(i,prod){
                   if($(this).is(':checked')){
                    purcids.push($(this).val());

                   }

              });

              // call ajax method to delete
              var checkedpurchase = purcids.toString();
              $.ajax({
                type:"post",
                url:"purchase/deletePurchMultiple",
                data:{checkedpurchase:purcids,_token: $('input[name=_token]').val()},
                success:function(msg){
                     if(msg.status === 'success'){
                          toastr.success(msg.msg);
                          toastr.options.closeButton = true;
                          //t.draw(true);
                          setInterval(function(){
                            window.location.reload();
                          },1000);
                        }
                  //t.draw();
                },error:function(data){
                     if(data.status === 422){
                      toastr.error("Cannot delete the purchase(s)!", '');
                      toastr.options.closeButton = true;
                     }
                }
              });
           }else{
                    toastr.error('No purchase(s) selected');
                    toastr.options.closeButton = true;
           }
         
       });

   




     // Delete purchase
      $("#ListPurchases tbody").on("click","button[class=purchaseDeletebtn]",function(){
      	var deleteid =  $(this).closest("tr").find(".bulkdel").val();
       
       if(deleteid != '')
       {
         $.ajax({
         	type:"post",
         	url:"purchase/deleteSinglePurchase",
         	data:{"delpurch":deleteid,_token: $('input[name=_token]').val()},
         	success:function(msg)
         	{

         		   if(msg.status === 'success'){
                          toastr.success(msg.msg);
                          toastr.options.closeButton = true;
                          //t.draw(true);
                          setInterval(function(){
                            window.location.reload();
                          },1000);
                        }

         	},error:function(data)
         	{
                 if(data.status === 422){
                      toastr.error("Cannot delete the purchase(s)!", '');
                      toastr.options.closeButton = true;
                     }
         	}
         });
       }else{
            toastr.error("Opps!, Nothing to delete", '');
            toastr.options.closeButton = true;
       }
      });
      


      
    
	// Generate purchase order number
    function randomNumberRange(min, max)
    {
        return Math.floor(Math.random() * (max - min + 1) + min);
    }

    //var mynu = Math.floor(Math.random()*(max - min + 1) + min);
    var mynu ="";
    var len = 2;
    var po  = "PO";
    for(var i=0;i<len;i++)
    {
        mynu += randomNumberRange(1,1000);
    }
    
    var pordernumber = po+mynu;
    $("input[name=orderNumber]").val(pordernumber);


     
     // Purchase Date picker
	  $('#purchaseOrderdata').datepicker({
	  	format: 'yyyy-mm-dd'
	  });
	  $('#purchaseOrderdelivery').datepicker({
	  	//format: 'dd-mm-yyyy'
	  	format: 'yyyy-mm-dd'
	  });

	// RECEIVING PURCHASE
	 $('#deliverynotedate').datepicker({
		  	//format: 'dd-mm-yyyy'
		  	format: 'yyyy-mm-dd'
		  });

   $('#purchasepaymentdate').datepicker({
        //format: 'dd-mm-yyyy'
        format: 'yyyy-mm-dd'
      });




	  
      // var t = $("#purchaseTable").DataTable();

          // update totalunits, subtotal, vat, and supertotal
    	 function formatMoney(n, currency) {
            return currency + " " + n.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
         }
       
      
       // $("#addRow").on('click', function(){
       // 	t.row.add([
       //        '<input class="form-control" type="text" name="itemNames" id="autocomplete" placeholder="Type product name or enter barcode">',
       //        '0',
       //        '0',
       //        '0.00',
       //        '18%',
       //        '0.00',
       //        '<a hre="#" id="deleteRow"><small class="label bg-red">remove</small></a>'
       // 		]).draw(false);
       // });

       // $("#purchaseTable tbody").on('click','#deleteRow',function(){
       // 	t.row($(this).parents('tr')).remove().draw();
       // });






       // get the value of supplier and then append his/her details on other inputs
      
       $("#supplierChange").on("change", function(e){
       	e.preventDefault();
       	var optionSelected = $("option:selected", this);
       	var valueSelected = this.value;
       	 if(valueSelected == ''){
            toastr.error('Supplier is required', '');
		    toastr.options.closeButton = true;
       	 }else{
           $.ajax({
           	 type: "POST",
           	 url : "purchase/getSupplierDetails",
           	 data:{"supplierId":valueSelected,'_token':$('input[name=_getsuppliertoken]').val()},
           	 success:function(data){
                $.each(data, function(i, suppliers){
                    $.each(this, function(suppliers, supplier){
                    	  $("#psupplieraddress").val(supplier.supplierAddress);
                    	  $("#psupplieremail").val(supplier.supplierEmail);
                    });
                });
           	 },error:function(data){
           	 	console.log(data);
           	 }
           });
       	 }
       });






       //item lookup autocomplete
       // $("#autocomplete").autocomplete({
       // 	serviceUrl:'/stock/autocompleteItems',
       // 	onSelect:function(suggestion){
       // 		 alert('You selected: ' + suggestion.value + ', ' + suggestion.data);

       // 	}
       // });







        //NEW LIFE PURCHASE ORDER
          var purchaseTr = '';
           purchaseTr += '<td colspan="8"><div class="text-center text-warning"><h3>' + "There are no items in the pruchase cart" + '</h3></div></td>'
          $('#purchasemessagerow').append(purchaseTr);

        // listen to form input if changed then go grab the item details
          $("#pruchaseValue").change("input",function(e){
          	e.preventDefault();
          	var searchedItem = $(this).val();

		          	if(searchedItem == ''){
                          toastr.error('Item name is required', 'Error');
                          toastr.options.closeButton = true;
		          	}else{
                         
                         $.ajax({
                         	  type:'post',
                         	  url: '/purchase/autocompleteItems',
                         	  data:{searchedItem,_token: $("input[name=_searchpurchasetoken]").val()},
                         	  dataType: 'json',
                         	  success:function(data){
                         	  	$('#purchasemessagerow').remove();
	                                   $.each(data, function(i, items){
                                            if(items == "no match"){
                                               // console.log("item not found");
                                               toastr.error('Item not found', 'Error');
                                               toastr.options.closeButton = true;
                                            }else{
		                                            	//console.log("item found");
		                                            	toastr.success('You have successfully added item', 'Success');
		                                                toastr.options.closeButton = true;
		                                            	$.each(this, function(items, item){

		                                                    //console.log(item.productName);
		                                                    var total = '';
		                                                    total = 1*item.productBuyingPrice;

		                                                    var newpruchasetr = '';
		                                                    var itemclass = item.id;
		                                                    var qafterorder = parseInt(item.availableQuantity)+1;
		                                                    //alert(qafterorder);
                                                            
                                                           
		                                                    var itemvat  = item.vat*100;
		                                                    newpruchasetr+='<tr class="xyz"><td><button class="removeTR" style="color: #ff7474;"><i class="icon ion-android-cancel"></i></button></td><td id="itemrow">'+ item.productName +'</td><td><input type="number" class="form-control itqty"  value="1" name="orderquantity"/></td><td><input type="number" name="qbefore" class="qbefore" value="'+ qafterorder +'" disabled></td><td><input type="number" class="form-control itemscost" name="itemscost" value="'+ item.productBuyingPrice +'"></td><td><input type="number" value="'+ itemvat +'" name="purchaseitemvat" class="purchaseitemvat" disabled="true"></td><td><input type="number" name="totalprice" class="totalprice" value="'+ total +'"></td><td style="display:none"><input type="number" name="itemid" class="itemid" value="'+ item.id +'" hidded></td></tr>';
		                                                    $('#createPurchase').append(newpruchasetr);





		                                                        // Remove row
		                                                        $(".removeTR").click(function(){
		                                                        	$(this).closest("tr").remove();

		                                                    

		                                                       // calculate totalitems
		                                                         var totalitems= 0;
		                                                         //var qafterorder = 0;
								                                 $(".itqty").each(function(){
								                                   var value = $(this).val();
								                                   if(!isNaN(value) && value.length != 0){
								                                   	totalitems += parseFloat(value);

								                                   }

								                                 });
								                                 $(".totalunits").html(totalitems);


								                             

		                                                     
		                                                       // calculate subtotal
								                                 var subtotal = 0;
								                                 var vatonremoverow = 0;
								                                 $(".totalprice").each(function(){
								                                 	var value = $(this).val();
								                                 	if(!isNaN(value) && value.length !=0){
								                                 		subtotal += parseFloat(value);
								                                 		vatonremoverow = subtotal*item.vat;
								                                 	}
								                                 });
								                                 $(".subtotal").html(formatMoney(subtotal,"R"));

								                                // calculate VAT
								                               
								                                //var ttp = $(".subtotal").text();
								                                //var itemvat = $("#createPurchase tr").find("input[name=purchaseitemvat]").val();
								                                //console.log(itemvat);

								                               // var ttpOG = parseFloat(ttp.replace(/[^0-9-.]/g, ''));
								                                //vat = (itemvat/100 * ttpOG);
								                                $(".vat").html(formatMoney(vatonremoverow,"R"));

								                                 // calculate supertotal
								                                 var supertotal = 0;
								                                 var vat = $(".vat").text();
								                                 var vatOG = parseFloat(vat.replace(/[^0-9-.]/g, ''));
								                                 supertotal = vatOG + subtotal;
								                                 $(".supertotal").html(formatMoney(supertotal,"R"));
		                                                        });
		                                                       // End remove row







		                                                       




		                                                     

		                                                       // calculate totalitems
		                                                         var totalitems= 0;
								                                 $(".itqty").each(function(){
								                                   var value = $(this).val();
								                                   if(!isNaN(value) && value.length != 0){
								                                   	totalitems += parseFloat(value);

								                                   }

								                                 });
								                                
								                                 $(".totalunits").html(totalitems);
								                                
		                                                     
		                                                       // calculate subtotal
								                                 var subtotal = 0;
								                                 $(".totalprice").each(function(){
								                                 	var value = $(this).val();
								                                 	if(!isNaN(value) && value.length !=0){
								                                 		subtotal += parseFloat(value);
								                                 	}
								                                 });
								                                 $(".subtotal").html(formatMoney(subtotal,"R"));


								                                // calculate VAT
								                                var ttp = $(".subtotal").text();
								                               
								                               var itemvat = $("#createPurchase tr").find("input[name=purchaseitemvat]").val();
								                              
								                                var ttpOG = parseFloat(ttp.replace(/[^0-9-.]/g, ''));
								                                vat = (itemvat/100 * ttpOG);
								                                $(".vat").html(formatMoney(vat,"R"));

								                                 // calculate supertotal
								                                 var supertotal = 0;
								                                 var vat = $(".vat").text();
								                                 var vatOG = parseFloat(vat.replace(/[^0-9-.]/g, ''));
								                                 supertotal = vatOG + ttpOG;
								                                 $(".supertotal").html(formatMoney(supertotal,"R"));













		                                                    // Listen if order quantity is changed
		                                                    $(".xyz").on("input",function(){
		                                                    	itembfc = item.availableQuantity;
		                                                    	//var itemq = $(this).find("input[id='+itemclass+'").val();
		                                                    	var itemq = parseInt($(this).closest("tr").find(".itqty").val());
		                                                    	var itemcst = $(this).closest("tr").find(".itemscost").val();
		                                                    	var itemqafterorder = parseInt($(this).closest("tr").find(".qbefore").val());
		                                                    	
		                                                    	var itemqafterchange = itemq + itemqafterorder;
		                                                        if(!isNaN(itemqafterchange) && itemqafterchange.length !=0){
		                                                        	//console.log(itemqafterchange);
		                                                        	$(this).closest("tr").find(".qbefore").val(itemqafterchange);
		                                                        }else{
		                                                        	//console.log(item.availableQuantity);
		                                                        	$(this).closest("tr").find(".qbefore").val(itembfc);
		                                                        }
		                                                   
		                                                    	 
		                                                    	 //get total if quantity changed
		                                                    	 var newtotalcst = itemq * itemcst;
		                                                         var newtotalcstappedn = $(this).closest("tr").find(".totalprice").val(newtotalcst);
		                                                    	

		                                                    	

		                                                    	
		                                                        
		                                                        // sum tota items
								                                 var totalitems= 0;
								                                 $(".itqty").each(function(){
								                                   var value = $(this).val();
								                                   if(!isNaN(value) && value.length != 0){
								                                   	totalitems += parseFloat(value);

								                                   }

								                                 });
								                                 $(".totalunits").html(totalitems);
		                                                          
		                                                          // calculate subtotal
								                                 var subtotal = 0;
								                                 $(".totalprice").each(function(){
								                                 	var value = $(this).val();
								                                 	if(!isNaN(value) && value.length !=0){
								                                 		subtotal += parseFloat(value);
								                                 	}
								                                 });
								                                 $(".subtotal").html(formatMoney(subtotal,"R"));

								                                // calculate VAT
								                                var vat = 0;
								                                var ttp = $(".subtotal").text();
								                                var ttpOG = parseFloat(ttp.replace(/[^0-9-.]/g, ''));
								                                vat = (item.vat/100 * ttpOG);
								                                $(".vat").html(formatMoney(vat,"R"));

								                                 // calculate supertotal
								                                 var supertotal = 0;
								                                 var vat = $(".vat").text();
								                                 var vatOG = parseFloat(vat.replace(/[^0-9-.]/g, ''));
								                                 supertotal = vatOG + ttpOG;
								                                 $(".supertotal").html(formatMoney(supertotal,"R"));


								                                 
								                                 
								                                 

		                                                    }); //end when order quantity changed
                                                          

		                                                 
		                                            	});//end each item
                                                      

                                                     
                                              }//end else statement
	                                   });
                         	  },error:function(data){

                         	  }
                         });
                        $(this).val(""); // clearing the input
		          	}
          });// end scan items
       

           
      

          	 
          // CREATE PURCHASE ORDER
           $("#createpurchaseorder").click(function(e){
           	e.preventDefault();
            
        
          
           	  var ordernumber = $("input[name=orderNumber]").val();
           	  var stockdue = $("input[name=porderdata]").val();
           	  var expDelivery  = $("input[name=porderdeliverydate]").val();
           	  var suppliermessage = $("textarea[name=suppliermessage]").val();
           	  var supplier = document.getElementById('supplierChange').selectedIndex;
		          var destination = document.getElementById('destination').selectedIndex;
                  
              var itemq = 0;
              $("#createPurchase tr").not("thead tr").each(function(){
              	 itemq = $(this).closest("tr").find(".itqty").val();
              });
		     

		      //var rowCount = $("#createPurchase tr").not("thead tr").length;
		      
                                         
                
                if(ordernumber != ''  && stockdue != '' && expDelivery != '' && supplier != '' && destination != '' && itemq != ''){
		      
		       	     $.ajax({
  		      	type: "post",
  		      	url: "purchase/postpurchase",
  		      	data:{
		      		  "stockdue":stockdue,
		      		  "expectedDelivery":expDelivery,
     	    	    "suppliermessage":suppliermessage,
     	    	    "ordernumber":ordernumber,
     	    	    "supplierId":supplier,
     	    	    "storedestination":destination,
     	    	    _token: $("input[name=_purchasodertoken]").val()
		      	},
		      	success:function(data){
		      		       
                // count the rows and post each
		      	 var arr = new Array();
		         var counter = 0;

		        $(".xyz").each(function(i,el){
		         //console.log(arr.push(el));
		         arr.push(el);
		        });
		       

		        purchasedoRequest(counter);
		        	
                  function purchasedoRequest(counter)
                   {
                     
                          var itemid = $(arr[counter]).find(".itemid").val();
                          var itemq =  $(arr[counter]).find(".itqty").val();
                          var purchaseitemvat = $(arr[counter]).find(".purchaseitemvat").val();
                          var itemcst = $(arr[counter]).find(".itemscost").val();
                          var totalitemcost= itemcst * itemq * purchaseitemvat/100 + itemcst * itemq;


                           $.ajax({

                          	   	    url : 'purchase/postpurchaseItem',
					         	    type: 'post',
					         	    data: {
					         	    		"itemid":itemid,
					         	    	    "orderqty":itemq,
					         	    	    "itemcost":itemcst,
					         	    	    "itemtotalcost":totalitemcost,
					         	    	    "purchaseitemvat":purchaseitemvat,
					         	    	    //'lastpurchaseId':lastid,
					         	    	     _token: $("input[name=_purchasodertoken]").val()
					         	          },
					         	     dataType: "json",
					         	     success:function(data){
					         	     	     counter++;
                                           $.each(data,function(i,response){
                                           	  if(response  == "success" &&  counter == arr.length)
                                           	  {
                                                  toastr.success('Purchase Order created successfully!', '');
                                                  toastr.options.closeButton = true;
                                                   window.location.reload();
                                           	  }
                                           	   else if(response == "success" && counter< arr.length)
				                                {
				                                   purchasedoRequest(counter);
				                                }
                                           });
					         	     },error:function(data){
                                          console.log("an error occured");
					         	     }
                          });
                   }




		      	},error:function(data){
                 toastr.error('Please fill are fields!', '');
                 toastr.options.closeButton = true;
		      	}
		      });       
             }else{
             	toastr.error('Please fill are fields!', '');
                toastr.options.closeButton = true;
             }

           });







/*
* CALCULATION OF AMOUNTS ON PURCHASE ORDER
*/

var sumOfpurchasePricePeritem  = 0;
$("#purchasereceiptTable tr").not("thead tr").each(function(){

	//get price per item
	var sumOfTotalPerItem = parseInt($(this).closest("tr").find(".purchaseitemsubtotalcost").text());
	sumOfpurchasePricePeritem+=sumOfTotalPerItem;
	//console.log(sumOfpurchasePricePeritem);

	 // append the returned result to subtotal in calculateTable
    $(".purchasereceiptsubtotal").text(formatMoney(sumOfpurchasePricePeritem,"R")); 


    // vat calculations
    var itempurchasevat = $("input[name=purchasetaxreceipt]").val();
    var newpurchasevat = itempurchasevat*sumOfpurchasePricePeritem;

    // append the answer to the vat div
     $(".purchasevatreceipt").text(formatMoney(newpurchasevat,"R")); 

     // sum newpurchasevat and sumofpurchasepriceperitem
     var superPurchaseVat = newpurchasevat + sumOfpurchasePricePeritem;

     // THen append the result to super sub total
     $(".purchasesupertotal").text(formatMoney(superPurchaseVat,"R"));
});


// voucher
var sumOfpurchasePricePeritemvoucher  = 0;
$("#vouchertable tr").not("thead tr").each(function(){
	//get price per item
	var sumOfTotalPerItemvoucher = parseInt($(this).closest("tr").find(".subtotalvoucher").text());
	sumOfpurchasePricePeritemvoucher+=sumOfTotalPerItemvoucher;

	// append the returned result to subtotal in calculateTable
    $(".purchasereceiptsubtotalvoucher").text(formatMoney(sumOfpurchasePricePeritemvoucher,"R")); 


     // vat calculations
    var itempurchasevatvoucher = $("input[name=purchasetaxreceiptvoucher]").val();
    var newpurchasevatvoucher = itempurchasevatvoucher*sumOfpurchasePricePeritemvoucher;

    // append the answer to the vat div
     $(".purchasevatreceiptvoucher").text(formatMoney(newpurchasevatvoucher,"R")); 


     // sum newpurchasevat and sumofpurchasepriceperitem
     var superPurchaseVatvoucher = newpurchasevatvoucher + sumOfpurchasePricePeritemvoucher;

     // THen append the result to super sub total
     $(".purchasesupertotalvoucher").text(formatMoney(superPurchaseVatvoucher,"R"));
});


// var sumOfpurchasePricePeritemvoucher  = 0;
// $("#purchasereceiptTableVoucher tr").not("thead tr").each(function(){

// 	//get price per item
// 	var sumOfTotalPerItemvoucher = parseInt($(this).closest("tr").find(".purchaseitemsubtotalcostvoucher").text());
// 	sumOfpurchasePricePeritemvoucher+=sumOfTotalPerItemvoucher;
// 	//console.log(sumOfpurchasePricePeritem);

// 	 // append the returned result to subtotal in calculateTable
//     $(".purchasereceiptsubtotalvoucher").text(formatMoney(sumOfpurchasePricePeritemvoucher,"R")); 


//     // vat calculations
//     var itempurchasevatvoucher = $("input[name=purchasetaxreceiptvoucher]").val();
//     var newpurchasevatvoucher = itempurchasevatvoucher*sumOfpurchasePricePeritemvoucher;

//     // append the answer to the vat div
//      $(".purchasevatreceiptvoucher").text(formatMoney(newpurchasevatvoucher,"R")); 

//      // sum newpurchasevat and sumofpurchasepriceperitem
//      var superPurchaseVatvoucher = newpurchasevatvoucher + sumOfpurchasePricePeritemvoucher;

//      // THen append the result to super sub total
//      $(".purchasesupertotalvoucher").text(formatMoney(superPurchaseVatvoucher,"R"));
// });


/*
* CONFIRM AND RECEIVE THE ORDER
*/
$("#receiveAndConfirmPurchase").click(function(e){
	e.preventDefault();
	var purchasenumber  = $("input[name=deliverypoNumber]").val();
	var deliverynotenumber = $("input[name=deliveryNumber]").val();
	var deliveredQuantity  = $("input[name=deliveredQty]").val();
	var deliveredby         = $("input[name=deliveredby]").val();
	var deliverercontact         = $("input[name=deliverercontact]").val();
	var delivereddate         = $("input[name=delivereddate]").val();
	var delivereddamaged         = $("input[name=delivereddamaged]").val();
	var deliverycheckedby         = $("input[name=deliverycheckedby]").val();
	var deliverydescriptions         = $("textarea[name=deliverydescriptions]").val();



	if(purchasenumber != '' && deliverynotenumber != '' && deliveredQuantity != '' && deliveredby != '')
	{

		//do the ajax here
		$.ajax({
			type:'post',
			url:'receivepurchaseorder',
			data:{
				  "poNumber":purchasenumber,
				  "deliverynotenumber":deliverynotenumber,
				  "deliveredquantity":deliveredQuantity,
				  "deliveredby":deliveredby,
				  "deliverercontact":deliverercontact,
				  "delivereddate":delivereddate,
				  "delivereddamaged":delivereddamaged,
				  "deliverycheckedby":deliverycheckedby,
				  "deliverydescriptions":deliverydescriptions,
				  _token:$("input[name=_deliverdetailstoken]").val()},
			 success:function(data){
				//$("#supplierReceipt").modal('hide');
                 $.each(data,function(i,response){
                 	if(response == "qtyerror")
                 	{
                 		toastr.error('Quantity Does not match');
		                toastr.options.closeButton = true;
                 	}
                 	else if(response == "deliverydatenotmatch")
                 	{
                 		toastr.error('The expected derivery date is different');
		                toastr.options.closeButton = true;
                 	}

                 	else if(response == "deliverycomplete")
                 	{
                 	// 	toastr.success('Delivery Completed, Process payment');
		                // toastr.options.closeButton = true;
		                //  setInterval(function(){
					             //        window.location.reload();
					             //      },2000);

                       // Now update journal entry
                       var purchaAmount = $(".purchasesupertotal").text();
                       var ogpurchaAmount = parseFloat(purchaAmount.replace(/[^0-9-.]/g, ''));
                       var pruchaReference = $("input[name=deliverypoNumber]").val();
                       var purchasepaymentdate =     $("input[name=purchasepaymentdate]").val(); 
                       var purchasepaymentmethod = $("#purchasemethodofpayments option:selected").val();

                       
                       // call ajax for updatind purchase account in journal
                       $.ajax({
                            type:"post",
                            url: "newpurchasejournal",
                            data:{
                                  "amount":ogpurchaAmount,
                                  "reference":pruchaReference,
                                  "paymentdate":purchasepaymentdate,
                                  "methodtopay":purchasepaymentmethod,
                                  _token:$("input[name=_deliverdetailstoken]").val()
                                },
                            success:function(msg){
                                  if(msg.status === 'success'){
                                      toastr.success(msg.msg);
                                      toastr.options.closeButton = true;
                                      //t.draw(true);
                                      setInterval(function(){
                                        window.location.reload();
                                      },500);
                                    }
                            },error:function(data){
                                if(data.status === 422){
                                  toastr.error("Cannot delete the account(s)!", '');
                                  toastr.options.closeButton = true;
                                 }
                            }
                       });

                 	}
                 	// else if(response == "deliverynotcomplete")
                 	// {
                 	// 	toastr.error('Delivery was not successfully');
		                // toastr.options.closeButton = true;
                 	// }
                 	
                 });

			},error:function(){
              console.log(data);
			}
		});
	}else{
		// close the modal and alert user
		//$("#supplierReceipt").modal('hide');
		toastr.error('All fields are required');
		toastr.options.closeButton = true;
	}
});













 /*
   |____________________________
   | START EDIT PURCHASE JS
   |_________________________________
   */
      //DATE PICKER
        
        // Purchase Date picker
	  $('#editpurchaseOrderdata').datepicker({
	  	format: 'yyyy-mm-dd'
	  });
	  $('#editpurchaseOrderdelivery').datepicker({
	  	//format: 'dd-mm-yyyy'
	  	format: 'yyyy-mm-dd'
	  });

      // var id =  $(this).closest("tr").find(".bulkdel").val();
      $("#ListPurchases tbody").on("click","button[class=purchaseEditbtn]",function(){
      	var id =  $(this).closest("tr").find(".bulkdel").val();
        if(id != '')
        {
        	$.ajax({
        		type:"post",
        		url:"purchase/getPurchaseToEdit",
        		data:{"purchaseid":id,_token: $('input[name=_token]').val()},
        		success:function(data)
        		{
        			 console.log(data);
        			 $("#editcreatePurchasetable").empty();
        			$.each(data,function(i,purchase){
        				$(this).each(function(purchase, items){
        					//console.log(items.productName);
        					 $("#editpurchasemodal").find("input[name=editporderdata]").val(items.purchaseDueDate);
        					 $("#editpurchasemodal").find("input[name=editporderdeliverydate]").val(items.deliveryDate);
        					 $("#editpurchasemodal").find("input[name=editorderNumber]").val(items.poNumber);
        					 $("#editpurchasemodal").find("input[name=editpurchaseidentification]").val(items.purchId);
        					 
        					var edittotal = items.itemPrice * items.itemQuantity;

        					var editpurchtr = '';
        					editpurchtr = '<tr class="editxyz"><td><button class="editremoveTR" style="color: #ff7474;"><i class="icon ion-android-cancel"></i></button></td><td id="edititemrow">'+ items.productName +'</td><td><input type="number" class="form-control edititqty"  value="'+ items.itemQuantity +'" name="editItemQuantity"/></td><td><input type="number" class="form-control edititemscost" name="edititemscost" value="'+ items.itemPrice +'"></td><td><input type="number" value="'+ items.purchasevat +'" name="editpurchaseitemvat" class="editpurchaseitemvat" disabled="true"></td><td><input type="number" name="edittotalprice" class="edittotalprice" value="'+ edittotal +'"></td><td style="display:none"><input type="number" name="edititemid" class="edititemid" value="'+ items.itemId +'" hidded></td></tr>';
        			        $('#editcreatePurchasetable').append(editpurchtr);



                            /*******************************************
                            *  ON edit Remove row on edit
                            ********************************
                            */ 
                            $("#editpurchasemodal .editremoveTR").click(function(){
                            	$(this).closest("tr").remove();

                        

                           // calculate totalitems
                             var totalitems= 0;
                             $(".edititqty").each(function(){
                               var value = $(this).val();
                               if(!isNaN(value) && value.length != 0){
                               	totalitems += parseFloat(value);

                               }

                             });
                             $(".edittotalunits").html(totalitems);


                         

                         
                           // calculate subtotal
                             var subtotal = 0;
                             var vatonremoverow = 0;
                             $(".edittotalprice").each(function(){
                             	var value = $(this).val();
                             	if(!isNaN(value) && value.length !=0){
                             		subtotal += parseFloat(value);
                             		vatonremoverow = subtotal*items.purchasevat;
                             	}
                             });
                             $(".editsubtotal").html(formatMoney(subtotal,"R"));

                            // calculate VAT
                            $(".editvat").html(formatMoney(vatonremoverow,"R"));

                             // calculate supertotal
                             var supertotal = 0;
                             var vat = $(".editvat").text();
                             var vatOG = parseFloat(vat.replace(/[^0-9-.]/g, ''));
                             supertotal = vatOG + subtotal;
                             $(".editsupertotal").html(formatMoney(supertotal,"R"));
                            });
                           // End remove row

                            



                           
                            // calculate totalitems
	                         var totalitems= 0;
	                         $(".edititqty").each(function(){
	                           var value = $(this).val();
	                           if(!isNaN(value) && value.length != 0){
	                           	totalitems += parseFloat(value);

	                           }

	                         });
	                        
	                         $(".edittotalunits").html(totalitems);
	                        
	                     
	                       // calculate subtotal
	                         var subtotal = 0;
	                         $(".edittotalprice").each(function(){
	                         	var value = $(this).val();
	                         	if(!isNaN(value) && value.length !=0){
	                         		subtotal += parseFloat(value);
	                         	}
	                         });
	                         $(".editsubtotal").html(formatMoney(subtotal,"R"));


	                        // calculate VAT
	                        var ttp = $(".editsubtotal").text();
	                       
	                       var itemvat = $("#editcreatePurchasetable tr").find("input[name=editpurchaseitemvat]").val();
	                      
	                        var ttpOG = parseFloat(ttp.replace(/[^0-9-.]/g, ''));
	                        vat = (itemvat/100 * ttpOG);
	                        $(".editvat").html(formatMoney(vat,"R"));

	                         // calculate supertotal
	                         var supertotal = 0;
	                         var vat = $(".editvat").text();
	                         var vatOG = parseFloat(vat.replace(/[^0-9-.]/g, ''));
	                         supertotal = vatOG + ttpOG;
	                         $(".editsupertotal").html(formatMoney(supertotal,"R"));

                           





                            // Listen if order quantity is changed on edit
                            $(".editxyz").on("input",function(){
                         
                            	var itemq = parseInt($(this).closest("tr").find(".edititqty").val());
                            	var itemcst = $(this).closest("tr").find(".edititemscost").val();
                            	
                            	
                            	
                           
                            	 
                            	 //get total if quantity changed
                            	 var newtotalcst = itemq * itemcst;
                                 var newtotalcstappedn = $(this).closest("tr").find(".edittotalprice").val(newtotalcst);
                            	

                            	

                            	
                                
                                // sum tota items
                                 var totalitems= 0;
                                 $(".edititqty").each(function(){
                                   var value = $(this).val();
                                   if(!isNaN(value) && value.length != 0){
                                   	totalitems += parseFloat(value);

                                   }

                                 });
                                 $(".edittotalunits").html(totalitems);
                                  
                                  // calculate subtotal
                                 var subtotal = 0;
                                 $(".edittotalprice").each(function(){
                                 	var value = $(this).val();
                                 	if(!isNaN(value) && value.length !=0){
                                 		subtotal += parseFloat(value);
                                 	}
                                 });
                                 $(".editsubtotal").html(formatMoney(subtotal,"R"));

                                // calculate VAT
                                var vat = 0;
                                var ttp = $(".editsubtotal").text();
                                var ttpOG = parseFloat(ttp.replace(/[^0-9-.]/g, ''));
                                vat = (items.purchasevat/100 * ttpOG);
                                $(".editvat").html(formatMoney(vat,"R"));

                                 // calculate supertotal
                                 var supertotal = 0;
                                 var vat = $(".editvat").text();
                                 var vatOG = parseFloat(vat.replace(/[^0-9-.]/g, ''));
                                 supertotal = vatOG + ttpOG;
                                 $(".editsupertotal").html(formatMoney(supertotal,"R"));


                                 
                                 
                                 

                            }); //end when order quantity changed




        				});
        			});
                    var modalOptions = {
                    	'show':true,
                    	'keyboard':true,
                    	'transition':true,
                    	'backdrop' : "static"
                    }
        			$('#editpurchasemodal').modal(modalOptions);
        			
        		},
        		error:function(data)
        		{
        			console.log(data);
        		}
        	});

        }else{
        	toastr.error("No order selected!", '');
            toastr.options.closeButton = true;
        }
        
      });

        // LISTEN TO ON change on modal
        $("#editpurchasemodal #editsupplierChange").on("change", function(e){
       	e.preventDefault();
       	var optionSelected = $("option:selected", this);
       	var valueSelected = this.value;
       	 if(valueSelected == ''){
            toastr.error('Supplier is required', '');
		    toastr.options.closeButton = true;
       	 }else{
           $.ajax({
           	 type: "POST",
           	 url : "purchase/getSupplierDetails",
           	 data:{"supplierId":valueSelected,'_token':$('input[name=_getsuppliertoken]').val()},
           	 success:function(data){
                $.each(data, function(i, suppliers){
                    $.each(this, function(suppliers, supplier){
                    	  $("#editpurchasemodal #editpsupplieraddress").val(supplier.supplierAddress);
                    	  $("#editpurchasemodal #editpsupplieremail").val(supplier.supplierEmail);
                    });
                });
           	 },error:function(data){
           	 	console.log(data);
           	 }
           });
       	 }
       });
      





            // On Edit listen to form input if changed then go grab the item details
          $("#editpurchasemodal #editpruchaseValue").change("input",function(e){
          	e.preventDefault();
          	var searchedItem = $(this).val();

		          	if(searchedItem == ''){
                          toastr.error('Item name is required', 'Error');
                          toastr.options.closeButton = true;
		          	}else{
                         
                         $.ajax({
                         	  type:'post',
                         	  url: '/purchase/autocompleteItems',
                         	  data:{searchedItem,_token: $("input[name=_editsearchpurchasetoken]").val()},
                         	  dataType: 'json',
                         	  success:function(data){
                         	  	//$('#purchasemessagerow').remove();
	                                   $.each(data, function(i, items){
                                            if(items == "no match"){
                                               // console.log("item not found");
                                               toastr.error('Item not found', 'Error');
                                               toastr.options.closeButton = true;
                                            }else{
		                                            	//console.log("item found");
		                                            	toastr.success('You have successfully added item', 'Success');
		                                                toastr.options.closeButton = true;
		                                            	$.each(this, function(items, item){

		                                                    //console.log(item.productName);
		                                                    var total = '';
		                                                    total = 1*item.productBuyingPrice;

		                                                    var editnewpruchasetr = '';
		                                                    var itemclass = item.id;
		                                                    var qafterorder = parseInt(item.availableQuantity)+1;
		                                                    //alert(qafterorder);
                                                            
                                                           
		                                                    var itemvat  = item.vat*100;
		                                                    editnewpruchasetr+='<tr class="editxyz"><td><button class="editremoveTR" style="color: #ff7474;"><i class="icon ion-android-cancel"></i></button></td><td id="edititemrow">'+ item.productName +'</td><td><input type="number" class="form-control edititqty"  value="1" name="editorderquantity"/></td><td><input type="number" class="form-control edititemscost" name="edititemscost" value="'+ item.productBuyingPrice +'"></td><td><input type="number" value="'+ itemvat +'" name="editpurchaseitemvat" class="editpurchaseitemvat" disabled="true"></td><td><input type="number" name="edittotalprice" class="edittotalprice" value="'+ total +'"></td><td style="display:none"><input type="number" name="edititemid" class="edititemid" value="'+ item.id +'" hidded></td></tr>';
		                                                    $('#editcreatePurchasetable').append(editnewpruchasetr);





		                                                        // Remove row
		                                                        $("#editpurchasemodal #editcreatePurchasetable .editremoveTR").click(function(){
		                                                        	
		                                                        	$(this).closest("tr").remove();
                                                                 
		                                                    

		                                                       // calculate totalitems
		                                                         var totalitems= 0;
		                                                         //var qafterorder = 0;
								                                 $(".edititqty").each(function(){
								                                   var value = $(this).val();
								                                   if(!isNaN(value) && value.length != 0){
								                                   	totalitems += parseFloat(value);

								                                   }

								                                 });
								                                 $(".edittotalunits").html(totalitems);


								                             

		                                                     
		                                                       // calculate subtotal
								                                 var subtotal = 0;
								                                 var vatonremoverow = 0;
								                                 $(".edittotalprice").each(function(){
								                                 	var value = $(this).val();
								                                 	if(!isNaN(value) && value.length !=0){
								                                 		subtotal += parseFloat(value);
								                                 		vatonremoverow = subtotal*item.vat;
								                                 	}
								                                 });
								                                 $(".editsubtotal").html(formatMoney(subtotal,"R"));

								                                // calculate VAT
								                               
								                                //var ttp = $(".subtotal").text();
								                                //var itemvat = $("#createPurchase tr").find("input[name=purchaseitemvat]").val();
								                                //console.log(itemvat);

								                               // var ttpOG = parseFloat(ttp.replace(/[^0-9-.]/g, ''));
								                                //vat = (itemvat/100 * ttpOG);
								                                $(".editvat").html(formatMoney(vatonremoverow,"R"));

								                                 // calculate supertotal
								                                 var supertotal = 0;
								                                 var vat = $(".vat").text();
								                                 var vatOG = parseFloat(vat.replace(/[^0-9-.]/g, ''));
								                                 supertotal = vatOG + subtotal;
								                                 $(".editsupertotal").html(formatMoney(supertotal,"R"));
		                                                        });
		                                                       // End remove row







		                                                       




		                                                     

		                                                       // calculate totalitems
		                                                         var totalitems= 0;
								                                 $(".edititqty").each(function(){
								                                   var value = $(this).val();
								                                   if(!isNaN(value) && value.length != 0){
								                                   	totalitems += parseFloat(value);

								                                   }

								                                 });
								                                
								                                 $(".edittotalunits").html(totalitems);
								                                
		                                                     
		                                                       // calculate subtotal
								                                 var subtotal = 0;
								                                 $(".edittotalprice").each(function(){
								                                 	var value = $(this).val();
								                                 	if(!isNaN(value) && value.length !=0){
								                                 		subtotal += parseFloat(value);
								                                 	}
								                                 });
								                                 $(".editsubtotal").html(formatMoney(subtotal,"R"));


								                                // calculate VAT
								                                var ttp = $(".editsubtotal").text();
								                               
								                               var itemvat = $("#editcreatePurchasetable tr").find("input[name=editpurchaseitemvat]").val();
								                              
								                                var ttpOG = parseFloat(ttp.replace(/[^0-9-.]/g, ''));
								                                vat = (itemvat/100 * ttpOG);
								                                $(".editvat").html(formatMoney(vat,"R"));

								                                 // calculate supertotal
								                                 var supertotal = 0;
								                                 var vat = $(".editvat").text();
								                                 var vatOG = parseFloat(vat.replace(/[^0-9-.]/g, ''));
								                                 supertotal = vatOG + ttpOG;
								                                 $(".editsupertotal").html(formatMoney(supertotal,"R"));













		                                                    // Listen if order quantity is changed
		                                                    $(".editxyz").on("input",function(){
		                                                    	
		                                                    	var itemq = parseInt($(this).closest("tr").find(".edititqty").val());
		                                                    	var itemcst = $(this).closest("tr").find(".edititemscost").val();
		                                                    	
		                                                    	
		                                                  
		                                                    	 
		                                                    	 //get total if quantity changed
		                                                    	 var newtotalcst = itemq * itemcst;
		                                                         var newtotalcstappedn = $(this).closest("tr").find(".edittotalprice").val(newtotalcst);
		                                                    	

		                                                    	

		                                                    	
		                                                        
		                                                        // sum tota items
								                                 var totalitems= 0;
								                                 $(".edititqty").each(function(){
								                                   var value = $(this).val();
								                                   if(!isNaN(value) && value.length != 0){
								                                   	totalitems += parseFloat(value);

								                                   }

								                                 });
								                                 $(".edittotalunits").html(totalitems);
		                                                          
		                                                          // calculate subtotal
								                                 var subtotal = 0;
								                                 $(".edittotalprice").each(function(){
								                                 	var value = $(this).val();
								                                 	if(!isNaN(value) && value.length !=0){
								                                 		subtotal += parseFloat(value);
								                                 	}
								                                 });
								                                 $(".editsubtotal").html(formatMoney(subtotal,"R"));

								                                // calculate VAT
								                                var vat = 0;
								                                var ttp = $(".editsubtotal").text();
								                                var ttpOG = parseFloat(ttp.replace(/[^0-9-.]/g, ''));
								                                vat = (item.vat/100 * ttpOG);
								                                $(".editvat").html(formatMoney(vat,"R"));

								                                 // calculate supertotal
								                                 var supertotal = 0;
								                                 var vat = $(".editvat").text();
								                                 var vatOG = parseFloat(vat.replace(/[^0-9-.]/g, ''));
								                                 supertotal = vatOG + ttpOG;
								                                 $(".editsupertotal").html(formatMoney(supertotal,"R"));


								                                 
								                                 
								                                 

		                                                    }); //end when order quantity changed
                                                          

		                                                 
		                                            	});//end each item
                                                      

                                                     
                                              }//end else statement
	                                   });
                         	  },error:function(data){

                         	  }
                         });
                        $(this).val(""); // clearing the input
		          	}
          });// end scan items




     

     // UPDATE PURCHASE AFTER EDITING

      $("#editcreatepurchaseorder").click(function(e){
           	e.preventDefault();
           
              var editordernumber = $("input[name=editorderNumber]").val();
           	  var editstockdue = $("input[name=editporderdata]").val();
           	  var editexpDelivery  = $("input[name=editporderdeliverydate]").val();
           	  var editsuppliermessage = $("textarea[name=editsuppliermessage]").val();
           	  var editsupplier = document.getElementById('editsupplierChange').selectedIndex;
		          var editdestination = document.getElementById('editdestination').selectedIndex;
		          var editpurchIdentific = $("#editpurchasemodal").find("input[name=editpurchaseidentification]").val();
                  
	          var edititemq = 0;
	          $("#editcreatePurchasetable tr").not("thead tr").each(function(){
	          	 edititemq = $(this).closest("tr").find(".edititqty").val();
	          });

	           if(editordernumber != ''  && editstockdue != '' && editexpDelivery != '' && editsupplier != '' && editdestination != '' && edititemq != '')
	           {
                  
                   $.ajax({
                           type: "post",
					      	url: "purchase/editpostpurchase",
					      	data:{
					      		"editstockdue":editstockdue,
					      		"editexpectedDelivery":editexpDelivery,
			     	    	    "editsuppliermessage":editsuppliermessage,
			     	    	    "editordernumber":editordernumber,
			     	    	    "editsupplierId":editsupplier,
			     	    	    "editstoredestination":editdestination,
			     	    	    _token: $("input[name=_editsearchpurchasetoken]").val()
		      	         },
		      	         success:function(data)
		      	         {
                              
                    // update items details also 
                     var arr = new Array();
						         var counter = 0;

						        $(".editxyz").each(function(i,el){
						         //console.log(editarr.push(el));
						         arr.push(el);
						        });

						         editpurchasedoRequest(counter);
						         function editpurchasedoRequest(counter)
						         {
                                        var edititemid = $(arr[counter]).find(".edititemid").val();
					                    var edititemq =  $(arr[counter]).find(".edititqty").val();
					                    var editpurchaseitemvat = $(arr[counter]).find(".editpurchaseitemvat").val();
					                    var edititemcst = $(arr[counter]).find(".edititemscost").val();
					                    var edittotalitemcost= edititemcst * edititemq * editpurchaseitemvat/100 + edititemcst * edititemq;
					                    var editpurchIdentific = $("#editpurchasemodal").find("input[name=editpurchaseidentification]").val();


					                    $.ajax({
	                          	   	    url : 'purchase/editpostpurchaseItem',
						         	    type: 'post',
						         	    data: {
						         	    		"edititemid":edititemid,
						         	    	    "editorderqty":edititemq,
						         	    	    "edititemcost":edititemcst,
						         	    	    "edititemtotalcost":edittotalitemcost,
						         	    	    "editpurchaseitemvat":editpurchaseitemvat,
						         	    	    'currentupdatedpurchaseId':editpurchIdentific,
						         	    	     _token: $("input[name=_editsearchpurchasetoken]").val(),
						         	          },
						         	     dataType: "json",
						         	     success:function(data){
						         	     //	console.log(data);
						         	     	     counter++;
	                                           $.each(data,function(i,response){
	                                           	  //console.log(response);
	                                           	  if(response  == "success" &&  counter == arr.length)
	                                           	  {
	                                                  toastr.success('Purchase Order updated successfully!', '');
	                                                  toastr.options.closeButton = true;
	                                           	  }
	                                           	   else if(response == "success" && counter < arr.length)
					                                {
					                                   editpurchasedoRequest(counter);
					                                }
					                                else if(response == "errorinsertdetails" && counter < arr.length)
					                                {
					                                	editpurchasedoRequest(counter);
					                                }
					                                else if(response == "errorinsertdetails" && counter == arr.length)
					                                {
					                                	console.log(response);
					                                }
	                                           });
						         	     },error:function(data){
	                                          console.log("An error occured when inserting details");
						         	     }
	                          });

						         } //end of editpurchasedoRequest function

		      	         },error:function(data)
		      	         {
                           console.log(data);
		      	         }
                   });


	           }else{
                    
                    toastr.error('Please fill are fields!', '');
                    toastr.options.closeButton = true;
	           }

           });






/*
   |
   | END EDIT PURCHASE JS
   |
*/












});