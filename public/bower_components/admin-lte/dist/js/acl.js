$(document).ready(function(){

          // Table data list of roles
          $("#rolezTable").DataTable();
          // Delete role
          $("#rolezTable tr td:nth-child(5)").on("click", function(e){
             e.preventDefault();
             var roleid =  $(this).closest("tr").find(".roleid").text();
             
             // delete the role
             if(roleid != ''){
                  if(confirm("Are you sure?")){
                     $.ajax({
                        type: "post",
                        url:  "users/deleteRole",
                        data: {"roleid":roleid,_token: $('input[name=_token]').val()},
                        success:function(data){
                           if(data="success"){
                                window.setTimeout(function(){
                                window.location.reload();
                                },200);
                                toastr.success("Role deleted successfully", 'Success');
                                toastr.options.closeButton = true;
                           }else if(data="error"){
                               toastr.error("Erro occured, can not delete role", 'Error');
                               toastr.options.closeButton = true;
                           }
                        },error:function(data){
                           console.log(data);
                        }
                     });
                  }else{
                    return false;
                  }
             }else{
                 toastr.error("Opps!, Nothing to delete", '');
                 toastr.options.closeButton = true;
             }
          });


             // Fetch data to Edit role
              $("#rolezTable tr td:nth-child(4)").on("click", function(e){
                  e.preventDefault();
                  var roleid =  $(this).closest("tr").find(".roleid").text();
                   
                  if(roleid != ''){
                    $.ajax({
                      type:"post",
                      url: "users/editRole",
                      data:{"roleid":roleid,_token: $('input[name=_token]').val()},
                      success:function(data){
                        $.each(data, function(role,roledata){
                          $(this).each(function(roledata,roles){
                            //console.log(roles.rolename);
                            $('#Editrolemodal').find("input[name=editrolename]").val(roles.rolename);
                            $('#Editrolemodal').find("textarea[name=editroledescription]").val(roles.description);
                            $('#Editrolemodal').find("input[name=edited_id]").val(roles.id);
                          });
                        });

                      // show the modal with appended data
                      var modalOptions = {
                          'show':true,
                          'keyboard':true,
                          'transition':true,
                          'backdrop' : "static"
                        }
                     $('#Editrolemodal').modal(modalOptions);


                      },error:function(data){
                        console.log(data);
                      }
                    });
                  }else{
                      toastr.error("Error occured, Nothing to edit", '');
                      toastr.options.closeButton = true;
                  }
                });

          // Update Edited role.
          $("#EditroleSubmit").click(function(e){
               var editedrolename    = $('#Editrolemodal').find("input[name=editrolename]").val();
               var editeddescription = $('#Editrolemodal').find("textarea[name=editroledescription]").val();
               var editedId          = $('#Editrolemodal').find("input[name=edited_id]").val();

               if(editedrolename != '' && editeddescription != '' && editedId != '')
               {
                  $.ajax({
                      type:"post",
                      url: "users/updateRole",
                      data:{"roleid":editedId,"rolename":editedrolename,"descripts":editeddescription,_token: $('input[name=_token]').val()},
                      success:function(data){
                           window.setTimeout(function(){ window.location.reload();},200);
                           toastr.success("Role updated successfully", 'Success');
                           toastr.options.closeButton = true;

                      },error:function(data){
                         toastr.error("Erro occured, can not delete role", 'Error');
                         toastr.options.closeButton = true;
                      }
                  });
               }else{
                   toastr.error("All fields are required!", 'Error');
                   toastr.options.closeButton = true;
               }
          });


 
        
         // ROLE BUTTON
          $("#roleSubmit").click(function(e){
            e.preventDefault();
            var description = $('textarea#roledescription').val();
            var name = $('input[name=rolename]').val();
            if(name == ''){
            	 toastr.error('Role Name field is required', 'Error');
                 toastr.options.closeButton = true;
            }else{
		            $.ajax({
		            	url:'users/role',
		            	type:'post',
		            	data:{'rolename':$('input[name=rolename]').val(),'description':description,'_token':$('input[name=_roletoken]').val()},
		            	success:function(data){
		            		  window.setTimeout(function(){ window.location.reload();},200);
		            		  toastr.success('Role added successfully', 'Success');
		                  toastr.options.closeButton = true;

		            	},
		            	  error: function(data){
		                           toastr.error('Role was not created', 'Error');
		                           toastr.options.closeButton = true;
		                        }
		            });
         }
          }); //endRole


          // User/emmployees
          $("#empSubmit").click(function(e){
          	 
          	e.preventDefault();
            var empname = $('input[name=empname]').val();
            var empphonenumber = $('input[name=empphonenumber]').val();
            var empaddress = $('input[name=empaddress').val();
            var empcountry = $('input[name=empcountry]').val();
            var empemail = $('input[name=empemail]').val();
            var emppassword = $('input[name=emppassword]').val();
            var emppasswordconfirm = $('input[name=emppasswordconfirm]').val();

            // post with ajax
            $.ajax({
            	url:'users/registeruser',
            	type:'POST',
            	data:{
            		 'name':empname,
            		 'phone':empphonenumber,
            		 'address':empaddress,
            		 'country':empcountry,
            		 'email':empemail,
            		 'password':emppassword,
            		 'password confirmation':emppasswordconfirm,
            		 '_token':$('input[name=_emptoken]').val()
            		},
            	success:function(data){
            		  
                      toastr.success("User created successfully", 'Success');
		                  toastr.options.closeButton = true;

                       window.setTimeout(function(){
                       window.location.reload();
                     },500)

            	},
            	
            		 error: function(data){
            		 	       var errors = data.responseJSON;
            		 	       console.log(errors);
            		 	       $.each(errors, function(index, value){
            		 	       	   toastr.error(value, '');
		                           toastr.options.closeButton = true;
            		 	       });
		                        
		                        }
            	
            });
          });

   // Edit assigned permissions on manage tab
   $("#editedrolesmanager").on("change",function(e){
     e.preventDefault;
     $('#managepermissionsTable').empty();
     // Get selected role id
     var selectedRole = $("#editedrolesmanager option:selected").val()
      
     // Post selected role id and return assigned permission if available otherwise return all permissions
     $.ajax({
          type: "post",
          url:  "users/getPermissionsBasedOnSelectedRole",
          data: {"selectedroleid":selectedRole, _token: $('input[name=_token]').val()},
          success:function(data){
            if(data==""){
              $('#managepermissionsTable').empty();
                toastr.error("No permission(s)  assigned to selected role.", '');
                toastr.options.closeButton = true;
            }else{
               $.each(data,function(perms,permz){
                $.each(this,function(permz,permis){
                   var permissionTable = '';

                   permissionTable += '<tr><td>'+ permis.id +'</td> <td><strong>'+ permis.name +'</strong></td> <td><input type="checkbox" id="rolesperms" name="editedrolespermission[]"  value='+ permis.id +' class="flat-red" checked /> <input type="hidden" name="_permroletoken" value="<?php echo csrf_token(); ?>"></td></tr>';
                   $("#managepermissionsTable").append(permissionTable);
                });
              });
              //console.log("somedata");
            }
            

          },error:function(data){
            console.log(data);
          }
     });

   });


   // Update edited permissions on manage tab
   $("#updatepermissionRoleSubmit").click(function(e){
    e.preventDefault();

     var editedrolename = $("#editedrolesmanager option:selected").val()
     var editedcheckedRoles = $('#managepermissionsTable input:checkbox:checked').map(function() {
                          return this.value;
                         }).get();

     //console.log(editedrolename);

           //check if fields are empty
      // if(editedrolename == '' || editedcheckedRoles == ''){
      //     toastr.error("All fields are required", '');
      //     toastr.options.closeButton = true;
      // }else{
              $.ajax({
        type: "POST",
        url : "users/updatepermissions",
        data:{
            "editedrole_name":editedrolename,
            "editedpermission_name":editedcheckedRoles,
            "_token":$('input[name=_permroletoken]').val()},
       //dataType:"json",
       success:function(data){
           if(data == "perms_updated")
           {
             console.log("permissions updated");
            
             // toastr.success("Roles updated successfully", 'Success');
             // toastr.options.closeButton = true;
             // window.setTimeout(function(){
             // window.location.reload();
             //  },200);
           }else if(data == "nopermission"){
              toastr.error("One role must have at list one permission", 'Erorr');
              toastr.options.closeButton = true;
           }
          
           
           
          // toastr.success("Roles updated successfully", 'Success');
          // toastr.options.closeButton = true;
          //  window.setTimeout(function(){
          //    window.location.reload();
          //  },200)

       },error:function(data){
                 //var errors = data.responseJSON;
                 console.log(data);
              
                  //toastr.error("Permission(s) already assigned", 'Erorr');
                  //toastr.options.closeButton = true;
       }//end else
   });
     // }

   });

          
   // PERMISSION ROLE
   $("#permissionRoleSubmit").click(function(e){
   	e.preventDefault();

   	 // var rolename = document.getElementById('roles').selectedIndex;
     var rolename = $("#roles option:selected").val()
   	 var checkedRoles = $('input:checkbox:checked').map(function() {
                          return this.value;
                         }).get();
     

     //console.log(rolename);
    // console.log(checkedRoles);

       //check if fields are empty
      if(rolename == '' || checkedRoles == ''){
          toastr.error("All fields are required", '');
		      toastr.options.closeButton = true;
      }else{
      	 	    $.ajax({
   	    type: "POST",
   	    url : "users/createPermRole",
   	    data:{
   	    	  "role":rolename,
   	    	  "permission":checkedRoles,
   	    	  "_token":$('input[name=_permroletoken]').val()},
   	   //dataType:"json",
   	   success:function(data){
           //console.log(data);
           
          toastr.success("Roles assigned successfully", 'Success');
		      toastr.options.closeButton = true;
           window.setTimeout(function(){
             window.location.reload();
           },200)

   	   },error:function(data){
                 var errors = data.responseJSON;
                 console.log(errors);
              
                  toastr.error("Permission(s) already assigned", 'Erorr');
		              toastr.options.closeButton = true;
   	   }//end else
   });
      }
         
   });


   
  // role user defination
  $("#roleuserSubmit").click(function(e){
    e.preventDefault();
   var roleid = $("#rolesxx option:selected").val();
   //var userid = document.getElementById('myuserid').selectedIndex;
  var userid = $("#myuserid option:selected").val();



    if(roleid == 0 || userid == 0){
       toastr.error("All fields are required", '');
	   toastr.options.closeButton = true;
    }else{
       $.ajax({
       	   type: "POST",
       	   url: "users/userRoles",
       	   data:{'user':userid,'role':roleid, '_token':$('input[name=_roleusertoken]').val()},
       	   success:function(data){

       	   	//console.log(data);
       	   	toastr.success("User assigned successfully", 'Success');
		        toastr.options.closeButton = true;
             window.setTimeout(function(){
             window.location.reload();
           },500)

       	   },error:function(data){
            //var errors = data.responseJSON;
            		 	      // console.log(errors);
            		 	       //$.each(errors, function(index, value){
            		 	       	   toastr.error('User can be assigned one role only', '');
		                           toastr.options.closeButton = true;
            		 	     //  });
       	   }
       });
   }


  });



// COMPANY SETTINGS
$("#compSubmit").click(function(e){
  e.preventDefault();
  var name = $('input[name=compName]').val();
  var address1 = $('input[name=compAddress1]').val();
  var address2 = $('input[name=compAddress2]').val();
  var contact1 = $('input[name=compContact1]').val();
  var contact2 = $('input[name=compContact2]').val();
  var website = $('input[name=compWebsite]').val();

  $.ajax({
      type : "POST",
      url  : "settings/create",
      data : {
              "companyName":name,
              "companyAddress1":address1,
              "companyAddress2":address2,
              "companyContact1":contact1,
              "companyContact2":contact2,
              "companyWebsite":website,
              '_token':$('input[name=_settingstoken]').val()
             },
     success:function(data){
             toastr.success('Informations successfully saved', '');
             toastr.options.closeButton = true;
              window.setTimeout(function(){
             window.location.reload();
              },500)
     },error:function(data){
       var errors = data.responseJSON;
                  $.each(errors, function(index, value){
                               toastr.error(value, '');
                               toastr.options.closeButton = true;
                         });
       }
  });
});

//Reset pass
$("#resetpass").click(function(e){
  e.preventDefault();
  
  //var email = $('input[name=inputnewEmail]').val();
  var newpass = $('input[name=inputnewPassword]').val();
  var confirmedpass = $('input[name=newconfirmpassword]').val();

  $.ajax({
       url : '/auth/passchange',
       type : 'post',
       data:{
           // 'email':email,
            'password':newpass,
            'password confirmation':confirmedpass,
            '_token':$('input[name=_resetpasstoken]').val()
          },
       success: function(data){
           
            //window.setTimeout(function(){
             toastr.success('password successfully changed', '');
            toastr.options.closeButton = true;
            window.setTimeout(function(){
             window.location.reload();
           },500)
            

       },
       error:function(data){
        //console.log(data);

         var errors = data.responseJSON;
         if(errors == 'error')
         {
            toastr.error('email does not exist!', '');
            toastr.options.closeButton = true;
         }else{
          $.each(errors, function(index, value){
                               toastr.error(value, '');
                               toastr.options.closeButton = true;
                         });
         }
                  

       }
  });
});




/*
 ************************************************
 DESTROY MULTIPLE CATEGORIES
 ************************************************
*/
var mycatable = $("#categorydataTable").DataTable();

$("#deletemultiplecat").click(function(e){
  e.preventDefault();
 // mycatable.find("input[type=checkbox]").val();

    var checkedcategeories = $("#categorydataTable").find('input:checkbox:checked').map(function() {
                                        return this.value;
                                        }).get();
  
  // send ajax
  $.ajax({
         type:"POST",
          url:'/product/deletemulitiplecategory',
          data:{checkedcategeories, _token: $('input[name=_token]').val()},
          dataType: 'json',
          cache : false,
          success:function(msg){
            if(msg.status === 'success'){
                  toastr.success(msg.msg);
                  toastr.options.closeButton = true;
                  setInterval(function(){
                    window.location.reload();
                  },4000);
                }
          },error:function(data){
              if(data.status === 422){
                  toastr.error("Cannot delete the product(s)!", '');
                  toastr.options.closeButton = true;
                 }
          }
     });
});
/***
  END MULTIPLE CATEGORIES DESTROY
**/




/*
 ************************************************
 DESTROY MULTIPLE products
 ************************************************
*/


// $("#delMultProduct").click(function(e){
//   e.preventDefault();
//  // mycatable.find("input[type=checkbox]").val();

//     var checkedproduct = $("#getposdataTable").find('input:checkbox:checked').map(function() {
//                                         return this.value;
//                                         }).get();
//     console.log(checkedproduct);
  
//   // send ajax
//   $.ajax({
//          type:"POST",
//           url:'/product/delmultproduct',
//           data:{checkedproduct, _token: $('input[name=_token]').val()},
//           dataType: 'json',
//           cache : false,
//           success:function(msg){
//                 if(msg.status === 'success'){
//                   toastr.success(msg.msg);
//                   toastr.options.closeButton = true;
//                   setInterval(function(){
//                     window.location.reload();
//                   },3000);
//                 }
//           },error:function(data){
//                  if(data.status === 422){
//                   toastr.error("Cannot delete the product(s)!", '');
//                   toastr.options.closeButton = true;
//                  }
              
//           }
//      });
// });
/***
  END MULTIPLE CATEGORIES DESTROY
**/





 }); //End document ready


            // var $tabs = $('#createNotTab li');

            // $tabs.filter('.active').next('li').removeClass("disabled");
            // $tabs.filter('.active').next('li').find('a[data-toggle]').each(function () {
            // $(this).attr("data-toggle", "tab");
            // });

            // $tabs.filter('.active').next('li').find('a[data-toggle="tab"]').tab('show');

            // $tabs.filter('.active').prev('li').find('a[data-toggle="tab"]').each(function () {
            // $(this).attr("data-toggle", "").parent('li').addClass("disabled");;        
            // })



//             var $tabs = $('#createNotTab li');


// function showPrev() {
//     $tabs.filter('.active').prev('li').removeClass("disabled");
//     $tabs.filter('.active').prev('li').find('a[data-toggle]').each(function () {
//        $(this).attr("data-toggle", "tab");
//     });

//     $tabs.filter('.active').prev('li').find('a[data-toggle="tab"]').tab('show');

//     $tabs.filter('.active').next('li').find('a[data-toggle="tab"]').each(function () {
//         $(this).attr("data-toggle", "").parent('li').addClass("disabled");        
//     })
// }

// function showNext() {
//     $tabs.filter('.active').next('li').removeClass("disabled");
//     $tabs.filter('.active').next('li').find('a[data-toggle]').each(function () {
//         $(this).attr("data-toggle", "tab");
//     });

//     $tabs.filter('.active').next('li').find('a[data-toggle="tab"]').tab('show');

//     $tabs.filter('.active').prev('li').find('a[data-toggle="tab"]').each(function () {
//         $(this).attr("data-toggle", "").parent('li').addClass("disabled");;        
//     })
// }