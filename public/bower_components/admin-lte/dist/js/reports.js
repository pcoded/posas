$(document).ready(function(){
         
         // Date picker
         $('#reportdatefrom').datepicker({
	  	     format: 'yyyy-mm-dd'
	      });

         $('#reportdateto').datepicker({
	  	     format: 'yyyy-mm-dd'
	      });

         $('#editproductdate').datepicker({
           format: 'yyyy-mm-dd'
        });



        $('#profitlossfrom').datepicker({
           format: 'yyyy-mm-dd'
        });

         $('#profitlossto').datepicker({
           format: 'yyyy-mm-dd'
        });

          $('#balanceopeningdate').datepicker({
           format: 'yyyy-mm-dd'
        });

         $('#balanceclosingdate').datepicker({
           format: 'yyyy-mm-dd'
        });



            $('#salesummaryfrom').datepicker({
           format: 'yyyy-mm-dd'
        });

         $('#salesummaryto').datepicker({
           format: 'yyyy-mm-dd'
        });
  
      

         
          
           // format money
          function formatMoney(n, currency)
            {
               return currency + " " + n.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
            }

	        //Empty report table
          var purchaseTr = '';
           purchaseTr += '<tr><td colspan="8"><div class="text-center text-warning"><h3>' + "Nothing to show" + '</h3></div></td></tr>'
          $('#createGPR').append(purchaseTr);

          //$("#btnsubmitreport").attr("disabled",true);

         $("#btnsubmitreport").click(function(e){
          e.preventDefault();
          // get the inputs
          var reportdatefrom = $("input[name=reportdatefrom]").val();
          var reportdateto   = $("input[name=reportdateto]").val();
          var reportfilter   = document.getElementById('reportorderby').selectedIndex;

          var labels  = [], chartdata = [];
        
            

          if( reportdatefrom != '' && reportdateto != '' && reportfilter != '')
          {

          	// send to ajax
          	$.ajax({
          		  type:'post',
          		  url : 'reports/generalProfitReport',
                dataType:'json',
          		  data:{"from":reportdatefrom,"to":reportdateto,"filter":reportfilter,"_token":$("input[name=_gprtoken]").val()},
          		  success:function(data){
                      
                       $.each(data, function(i, sales){
                       	  if(sales == "no match")
                       	  {
                            $('#createGPR tr').not("thead tr").empty();
                            $('#createGPR').append(purchaseTr);

                       	  	  toastr.error('No report(s) found with you entries', '');
                              toastr.options.closeButton = true;

                       	  }else{
                       	  	 $('#empyreportrow').remove();
                              $('#createGPR tr').not("thead tr").empty();

                        

                       	  	 $.each(this, function(sales, sale){

                       	  	 	 var saletr='';
                       	  	 	 var calctotrw = '';
                       	  	 	 var calvat = sale.vat * sale.productSellingPrice * sale.totalquantity;
                                 var totalbuyings = sale.totalquantity * sale.productBuyingPrice;
                                 var totalsales    = sale.totalquantity * sale.productSellingPrice;

                                var profit      = totalsales - totalbuyings;

                       	  	 	 saletr += '<tr class="itemsalelist"> <td class="itemName">'+ sale.productName +'</td>  <td >'+ sale.totalquantity+'</td>  <td>'+ totalbuyings +'</td>    <td>'+ totalsales +'</td> <td> '+ calvat +'</td>  <td class="totalcost">'+ sale.totalcost +'</td> <td class="profit">'+ profit +'</td></tr> ';
                                

                                 $('#createGPR').append(saletr);

                                
                       	  	 });//end second each

                      	  	   // sum quantity sold colmns
                       	  	    var grandtotqty = 0;
                       	  	    $("#createGPR tr td:nth-child(2)").each(function(){
                       	  	         grandtotqty += parseFloat($(this).text());
                       	  	     });

                       	  	     // sum total purchase colmns
                       	  	    var grandtotpurchase = 0;
                       	  	    $("#createGPR tr td:nth-child(3)").each(function(){ grandtotpurchase += parseFloat($(this).text());});

                       	  	    // sum total selling price colmns
                       	  	    var grandtotsellingprice= 0;
                       	  	    $("#createGPR tr td:nth-child(4)").each(function(){ grandtotsellingprice += parseFloat($(this).text());});

                       	  	    // sum total vat colmns
                       	  	    var grandtotvat= 0;
                       	  	    $("#createGPR tr td:nth-child(5)").each(function(){ grandtotvat += parseFloat($(this).text());});

                       	  	     // sum total cost colmns
                       	  	    var grandtotcost= 0;
                       	  	    $("#createGPR tr td:nth-child(6)").each(function(){ grandtotcost += parseFloat($(this).text());});

                       	  	    // sum total profit colmns
                       	  	    var grandtotprofit= 0;
                       	  	    $("#createGPR tr td:nth-child(7)").each(function(){
                       	  	         grandtotprofit += parseFloat($(this).text());
                       	  	     });
                       	  	    //console.log(formatMoney(grandtotprofit, "R"));
                       	  	    
                       	  	    
                       	  	  calctotrw = '<tr style="font-size:20px;font-weight:bold"> <td class="text-right">Grand Total</td> <td class="toqty"> '+ grandtotqty +' </td>  <td class="grandcpurchase"> '+ formatMoney(grandtotpurchase,"R") +' </td> <td class="grandtoselling"> '+ formatMoney(grandtotsellingprice,"R")  +' </td> <td class="grandvat"> '+ formatMoney(grandtotvat,"R")  +' </td> <td class="grandtotcost"> '+ formatMoney(grandtotcost,"R") +' </td> <td id="grandtotprofit"> '+ formatMoney(grandtotprofit, "R") +' </td> </tr>';
                       	  	  $('#createGPR').append(calctotrw);


                                    
                                           
                                     $.each(data, function(i, sales){
                                           $.each(this, function(sales, item){

                                            var totalbuyings = item.totalquantity * item.productBuyingPrice;
                                            var totalsales    = item.totalquantity * item.productSellingPrice;

                                             var profit      = totalsales - totalbuyings;
                                             // console.log(item.totalcost);
                                            
                                             labels.push(item.productName);
                                             chartdata.push(profit);
                                           });
                                     });
                                    //console.log(labels);

                                      var areaChartData = {
                                      labels: labels,
                                        datasets: [
                                            {
                                                label: labels,
                                                fillColor: '#382765',
                                                color: "#f56954",
                                                highlight: "#f56954",
                                                data: chartdata
                                            },
                                            
                                        ]
                                    };
                                    



                                        //-------------
                                    //- BAR CHART -
                                    //-------------
                                    var myChartCanvas = $("#barChart").get(0).getContext("2d");
                                    
                                    // options
                                    var barChartOptions = {
                                    //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
                                    scaleBeginAtZero: true,
                                    //Boolean - Whether grid lines are shown across the chart
                                    scaleShowGridLines: true,
                                    //String - Colour of the grid lines
                                    scaleGridLineColor: "rgba(0,0,0,.05)",
                                    //Number - Width of the grid lines
                                    scaleGridLineWidth: 1,
                                    //Boolean - Whether to show horizontal lines (except X axis)
                                    scaleShowHorizontalLines: true,
                                    //Boolean - Whether to show vertical lines (except Y axis)
                                    scaleShowVerticalLines: true,
                                    //Boolean - If there is a stroke on each bar
                                    barShowStroke: true,
                                    //Number - Pixel width of the bar stroke
                                    barStrokeWidth: 2,
                                    //Number - Spacing between each of the X value sets
                                    barValueSpacing: 5,
                                    //Number - Spacing between data sets within X values
                                    barDatasetSpacing: 1,
                                    //String - A legend template
                                    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
                                    //Boolean - whether to make the chart responsive
                                    responsive: true,
                                    maintainAspectRatio: true
                                  };


                                  // Instantiate a new chart
                                    var myLineChart = new Chart(myChartCanvas).Bar(areaChartData,barChartOptions);



                       	  }//end else sale range found
                       });//end top each
          		  },error:function(data){
          		  	console.log(data);
          		  }
          	});
                  
          }else{

          	 toastr.error('All fields are required', '');
             toastr.options.closeButton = true;

          }

         
         });








 // DATA TABLE SERVER SIDE
  // var t = $("#reportsproductsdataTable").DataTable({
      
  //         paging: false,
  //         ordering:true,
  //         searching:true,
  //         processing: true,
  //         serverSide: false,
  //         select:true,
  //        order: [[ 1, "asc" ]],
  //         ajax: "products/ajaxGetProducts",
  //         dataSrc:"Data",
  //         columns:[
  //                   { data: 'productName', name: 'productName' },

  //                   { data: 'onhandQuantity',name:'onhandQuantity'},

  //                   { data: 'productBuyingPrice', name: 'productBuyingPrice' },
  //                   { data: 'productSellingPrice', name: 'productSellingPrice' },
               
  //                 ],

  //            // "columnDefs": [ {
  //            //  "targets": [0,6,7,8],
  //            //  "orderable": false,
  //            //  "searchable": false
                
  //            //  } ]
        
  // });

    function formatMoney(n, currency)
  {
      return currency + " " + n.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
   }

 //   var tqtyq= 0;

 // $("#reportsproductsdataTable tr td:nth-child(2)").each(function(){
 // // var itemId = item.productName
 //    tqtyq += parseFloat($(this).text());
 // });

 //  $("#totalitems").text(tqtyq);


  var tbyprice= 0;

 $("#reportsproductsdataTable tr td:nth-child(3)").each(function(){
 // var itemId = item.productName
    tbyprice += parseFloat($(this).text());
 });

  $("#totalprucprice").text(formatMoney(tbyprice,"R"));


var purctotal= 0;

 $("#reportsproductsdataTable tr td:nth-child(4)").each(function(){
 // var itemId = item.productName
    purctotal += parseFloat($(this).text());
 });

  $("#totalpruc").text(formatMoney(purctotal,"R"));


  var bytt= 0;

 $("#reportsproductsdataTable tr td:nth-child(5)").each(function(){
 // var itemId = item.productName
    bytt += parseFloat($(this).text());
 });

  $("#totbuy").text(formatMoney(bytt,"R"));


  var tots= 0;

 $("#reportsproductsdataTable tr td:nth-child(6)").each(function(){
 // var itemId = item.productName
    tots += parseFloat($(this).text());
 });

  $("#totbs").text(formatMoney(tots,"R"));





// Profit and Loss statement
$("#profitlossreportsbtn").click(function(e){
  e.preventDefault();
  var incomedatefrom = $("input[name=profitlossfrom]").val();
  var incomedateto = $("input[name=profitlossto]").val();

  // check against the inputs
  if(incomedatefrom !='' && incomedateto !=''){
       //call ajax
        var salesbalances=[];
        var cogsbalances=[];
        var expensesbalances=[];

       $.ajax({
               type: "post",
               url:  "/reports/finance/incomestatement",
               data: {"incomedatefrom":incomedatefrom,"incomedateto":incomedateto, "_token":$("input[name=_profitlosstoken]").val()},
               success:function(data){
                //console.log(data);
                   $.each(data,function(i,accounts){
                      $.each(this, function(accounts, accountdata){

                          // create sales array
                          var tsales = parseFloat(accountdata.totalsales);
                          if(isNaN(tsales)){
                             salesbalances.push(0);
                          }else{
                             salesbalances.push(tsales);
                          }
                         
                          
                           // create cogs array
                          var tcogs = parseFloat(accountdata.totalcogs);
                          if(isNaN(tcogs)){
                            cogsbalances.push(0);
                          }else{
                            cogsbalances.push(tcogs);
                          }
                          
                             
                          // create expenses array
                          var texpenses = parseFloat(accountdata.totalexpenses);
                          if(isNaN(texpenses)){
                            expensesbalances.push(0);
                          }else{
                            expensesbalances.push(texpenses);
                          }
                          
                      });
                   });

                    //attach details for income statement to view
                    
                    var grossprofit = salesbalances[0] - cogsbalances[1];
                    var netprofit = grossprofit - expensesbalances[2];

                    if(expensesbalances[2] > grossprofit){
                      $("#netprofitloss").text("Net Loss");
                    }else{
                      $("#netprofitloss").text("Net Profit");
                    }

                    $("#headerdate").text('From '+incomedatefrom+' to '+incomedateto+'');
                    $("#totalincomesales").text(formatMoney(salesbalances[0],"R"));
                    $("#totalincomecogs").text(formatMoney(cogsbalances[1],"R"));
                    $("#totalgrossprofit").text(formatMoney(grossprofit,"R"));
                    $("#totalexpensesincomestat").text(formatMoney(expensesbalances[2],"R"));
                    $("#totalnetprofit").text(formatMoney(netprofit,"R"));
                    
                    

               },error:function(data){
                console.log(data);
               }
       });
  }else{
    toastr.error('Date range not correct', '');
       toastr.options.closeButton = true;
  }
});



$("#balancesheetbtn").click(function(e){
  e.preventDefault();
   var balanceopeningdate = $("input[name=balanceopeningdate]").val();
   var balanceclosingdate = $("input[name=balanceclosingdate]").val();


   if(balanceopeningdate !='' && balanceclosingdate !=''){
       // call ajax
       $.ajax({
             type: "post",
             url:  "/reports/finance/balancesheet",
             data:{"balanceopeningdate":balanceopeningdate,"balanceclosingdate":balanceclosingdate,"_token":$("input[name=_balancesheettoken]").val()},
             success:function(data){
              console.log(data);
             },error:function(data){
              console.log(data);
             }
       });
   }else{
       toastr.error('Opening and Closing Date are required', '');
       toastr.options.closeButton = true;
   }
});



/*
 *SALES SUMMARY REPORT BY DATE RANGE
 */
$("#salessummarybutton").click(function(e){
    e.preventDefault();

    var summarydatefrom = $("input[name=salesummaryfrom]").val();
    var summarydateto = $("input[name=salesummaryto]").val();

    var userdatefrom =moment(summarydatefrom).format('MMMM D, YYYY');
    var userdateto =  moment(summarydateto).format('MMMM D, YYYY');

    var saleslabel = [];
    var salesdataset = [];

   // clear canvas for line chart so do not reappear on mouse hover
    $("#salessummaryline").remove();
    $("#canvasline").append('<canvas id="salessummaryline"></canvas>');

    if(summarydatefrom !='' && summarydateto !=''){
      
      //call ajax
      $.ajax({
             type:"post",
             url:"reports/salessummary",
             data:{"fromdate":summarydatefrom,"todate":summarydateto,"_token":$("input[name=_salessummarytoken]").val()},
             success:function(data){
                 
                 $.each(data,function(i,salessummaries){
                     $.each(this, function(salessummaries,salessummary){
                        saleslabel.push(salessummary.solddate);
                        salesdataset.push(salessummary.salestotal);
                     });
                 }); 

                 var data = {
                          labels: saleslabel,
                          datasets: [
                              {
                                  label: "My First dataset",
                                  //fillColor: "rgba(60,141,188,0.9)",
                                  fillColor : "rgba(172,194,132,0.4)",
                                  //strokeColor: "rgba(60,141,188,0.8)",
                                  strokeColor : "#ACC26D",
                                  pointColor: "#3b8bba",
                                  pointHighlightFill: "#fff",
                                  //pointHighlightStroke: "rgb(220,220,220)",
                                  pointStrokeColor : "#9DB86D",
                                  data: salesdataset,
                                  spanGaps: false,
                              }
                          ]
                      };
                       $("#headerdate").text("From " +userdatefrom+ " To " +userdateto);
                       var myChartCanvas = $("#salessummaryline").get(0).getContext("2d");
                        var lineOptions = {
                                     //multiTooltipTemplate: "<%= datasetLabel %> - <%= value %>",                 
                                    //Boolean - whether to make the chart responsive
                                    responsive: true,
                                    //maintainAspectRatio: true
                                  };
                       var myLineChart = new Chart(myChartCanvas).Line(data,lineOptions);
                 
             },error:function(data){
              console.log(data);
             }
      });

    }else{
       toastr.error('All fields are required', '');
       toastr.options.closeButton = true;
    }
});

});// End document