$(document).ready(function(){

          // Purchase Date picker
          $('#storetransferdate').datepicker({
            format: 'yyyy-mm-dd'
          });

         //Inventor transfer messager
          var purchaseTr = '';
           purchaseTr += '<td colspan="5"><div class="text-center text-warning"><h3>' + "There are no item(s) has been transfered" + '</h3></div></td>'
          $('#transfersemessagerow').append(purchaseTr);


          // listen to form input if changed then go grab the item details
          $("#stocktransferitem").change("input",function(e){
            e.preventDefault();
            var searchedItem = $(this).val();
            var storefrom = $("#storefrom").val();
            var storeto = $("#storeto").val();

                if(searchedItem == ''){
                          toastr.error('Item name is required', 'Error');
                          toastr.options.closeButton = true;
                }else{
                         
                         $.ajax({
                            type:'post',
                            url: '/stock/autocompletetrItems',
                            data:{searchedItem,storefrom,storeto,_token: $("input[name=_stocktransfertoken]").val()},
                            dataType: 'json',
                            success:function(data){
                              $('#transfersemessagerow').remove();
                                     $.each(data, function(i, items){
                                            if(items == "no match"){
                                               // console.log("item not found");
                                               toastr.error('Item(s) not found, Check name or item(s) does not exist in the source', 'Error');
                                               toastr.options.closeButton = true;
                                            }else{
                                                  //console.log("item found");
                                                  toastr.success('Item(s) added successfully', 'Success');
                                                    toastr.options.closeButton = true;
                                                  $.each(this, function(items, item){

                                                        //console.log(item.productName);
                                                        var total = '';
                                                        total = 1*item.productBuyingPrice;

                                                        var newpruchasetr = '';
                                                        var itemclass = item.id;
                                                        var qafterorder = parseInt(item.availableQuantity)+1;
                                                        //alert(qafterorder);
                                                            
                                                           
                                                        var itemvat  = item.vat*100;
                                                        newpruchasetr+='<tr class="stocktransferxyz"><td><button class="removeTR" style="color: #ff7474;"><i class="icon ion-android-cancel"></i></button></td><td id="itemrow">'+ item.productName +'</td><td><input type="number" class="form-control transferitqty"  value="1" name="transferquantity"/></td><td><input type="number" class="form-control itemscost" name="itemscost" value="'+ item.productBuyingPrice +'"></td><td><input type="number" value="'+ itemvat +'" name="purchaseitemvat" class="purchaseitemvat" disabled="true"></td><td style="display:none"><input type="number" name="transferitemid" class="transferitemid" value="'+ item.id +'"></td></tr>';
                                                        $('#stocktranserTable').append(newpruchasetr);





                                                            // Remove row
                                                            $(".removeTR").click(function(){
                                                              $(this).closest("tr").remove();

                                                            });
                                                          // End remove row


                                                          

                                                     
                                                  });//end each item
                                                      

                                                     
                                              }//end else statement
                                     });
                            },error:function(data){

                            }
                         });
                        $(this).val(""); // clearing the input
                }
          });// end scan items





    // Create stock transer
    $("#transferInventory").click(function(e){
      e.preventDefault();
      
       var arr = new Array();
       var counter = 0;

       $(".stocktransferxyz").each(function(i,el){
       //console.log(arr.push(el));
       arr.push(el);
       });

      transferstockRequest(counter);

      function transferstockRequest(counter)
      {

        // capture the inputs
       var storefrom = $("#storefrom").val();
       var storeto = $("#storeto").val();
       var transferquantity = $(arr[counter]).find("input[name=transferquantity]").val();
       var transferdate = $("input[name=storetransferdate]").val();
       var transferitemid  = $(arr[counter]).find("input[name=transferitemid]").val();


        if(storefrom  != '' && storeto  != '' && transferquantity != '' && transferdate  != '' && typeof transferitemid  != "undefined" && storefrom != storeto)
       {
          //console.log("we good");
          $.ajax({
            type:"post",
            url : "stock/createTransferStock",
            data: {
                "storesource":storefrom,
                "storedestination":storeto,
                "transferquantity":transferquantity,
                "transferdate":transferdate,
                "transferitemid":transferitemid,
               _token: $("input[name=_stocktransfertoken]").val()
            },success:function(data){
              counter++;
              $.each(data,function(i,response){
                    if(response == "success" &&  counter< arr.length){
                         transferstockRequest(counter)
                    } 
                    else if(response == "success" && counter == arr.length){
                       
                       toastr.success('Transfer completed successfully!', '');
                                   toastr.options.closeButton = true;
                    }
              });
            },error:function(data){

            }
          });
       }
       else if(storefrom  != '' && storeto  != '' && storefrom == storeto){
         toastr.warning('Oops!, You cant transfer within store', '');
         toastr.options.closeButton = true;
       }
       else{
        // fields are empty
        toastr.error('Please fill are fields!', '');
        toastr.options.closeButton = true;
       }

      }

    });






});