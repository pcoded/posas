$(document).ready(function(){

 // DATA TABLE SERVER SIDE
  var t = $("#csmsproductsdataTable").DataTable({
          dataSrc:"data",
          // scrollY: 400,
          // paging: false,
          // ordering:true,
          // searching:true,
          processing: true,
          serverSide: false,
         //  select:true,
         // order: [[ 1, "asc" ]],
          ajax: "products/ajaxGetProducts",
          columns:[
                    {data:'id',
                     render:function(data,type,row){
                      return '<label><input type="checkbox" class="bulkDelete" name="productsids[]" value='+ data +' class="flat-red"></label>'
                     }
                    },
                    { data: 'productName', name: 'productName' },

                     // { data: 'storeName', name: 'storeName' },

                    { data: 'availableQuantity',
                      render:function(data,type,row)
                      {
                        //var alertqty = '';
                     if(data > 3)
                     {
                       return '<span class="label label-info">'+ data +'</span>';
                     }
                     else
                     {
                       return '<span class="label label-danger" data-toggle="tooltip" data-placement="top" title="Inventory is almost over, you consider add more">'+ data +'</span>';
                     }
                      }
                    },

                    { data: 'productBuyingPrice', name: 'productBuyingPrice' },
                    { data: 'productSellingPrice', name: 'productSellingPrice' },
                    {data:'productBarcode',
                     render:function(data,type,row){
                      return '<button class="viewinventroybtn">Inventory</button>'
                     }
                    },
                  
                    {data:'id',
                     render:function(data,type,row){
                      return '<button class="editproductbtn">Edit</button>'
                     }
                    },

                    {data:'id',
                     render:function(data,type,row){
                      return '<button class="deleteproductbtn">Delete</button>'
                     }
                    },
                  ],

             "columnDefs": [ {
              "targets": [0,5,6,7],
              "orderable": false,
              "searchable": false
                
              } ],
             "fnRowCallback":
                function(nRow,data,type){
                  

                  var expiredate = data.expirationDate;

                  var todaysdate = new Date();
                  var newdate = moment(todaysdate).format('YYYY-MM-DD');
                  //var oneDay = 24*60*60*1000;

               if(expiredate != null){

                // First we split the values to arrays date1[0] is the year, [1] the month and [2] the day
                    expiredate  = expiredate.split('-');
                    newdate = newdate.split('-');
                 
                 // Now we convert the array to a Date object, which has several helpful methods
                 expiredate  = new Date(expiredate[0],expiredate[1],expiredate[2]);
                 newdate = new Date(newdate[0],newdate[1],newdate[2]);
                 
                 // We use the getTime() method and get the unixtime (in milliseconds, but we want seconds, therefore we divide it through 1000)
                 expiredate_unixtime = parseInt(expiredate.getTime() / 1000);
                 newdate_unixdate = parseInt(newdate.getTime() / 1000);
                   
                  // This is the calculated difference in seconds
                 var diff = expiredate_unixtime - newdate_unixdate;
                 
                 // in Hours
                 var diffhours = diff/60/60;

                 // in Days
                 var diffdays = diffhours / 24;

                  if(!isNaN(diffdays)){
                     var $nRow = $(nRow);
                     if(diffdays < 3){
                      $nRow.css({"background-color":"#FF851B"})
                     }
                     if(diffdays < 0){
                        $nRow.css({"background-color":"red"})
                     }
                  }
               }
                  return nRow;
                }
             
        
  });


       // Delete products
      $("#csmsproductsdataTable tbody").on("click","button[class=deleteproductbtn]",function(){
        var deleteid =  $(this).closest("tr").find(".bulkDelete").val();
       
       if(deleteid != '')
       {
          if(confirm("Are you sure you want to destroy this item?"))
          {

                   $.ajax({
                            type:"post",
                            url:"product/deleteSingleProduct",
                            data:{"delproduct":deleteid,_token: $('input[name=_token]').val()},
                            success:function(msg)
                            {

                                 if(msg.status === 'success'){
                                            toastr.success(msg.msg);
                                            toastr.options.closeButton = true;
                                            //t.draw(true);
                                            setInterval(function(){
                                              window.location.reload();
                                            },500);
                                          }

                            },error:function(data)
                            {
                                   if(data.status === 422){
                                        toastr.error("Cannot delete the product!", '');
                                        toastr.options.closeButton = true;
                                       }
                            }
                           });

          }else{
            return false;
          }
     
       }else{
            toastr.error("Opps!, Nothing to delete", '');
            toastr.options.closeButton = true;
       }
      });
      
      



      $(".bulkDelete").on('click', function(){
        // bulk
        var status = this.checked;
        $(".bulkDelete").each(function(){
          $(this).prop("checked",status);
        });
      });


       // BULCK DELETE
       $("#deleteTriger").on("click",function(event){
           if($('.bulkDelete:checked').length > 0){
             if(confirm("Are you sure you want to destroy this product(s)"))
              {

            var ids = [];
              $('.bulkDelete').each(function(i,prod){
                   if($(this).is(':checked')){
                    ids.push($(this).val());

                   }

              });

              // call ajax method to delete
              var checkedproduct = ids.toString();
              $.ajax({
                type:"post",
                url:"product/delmultproduct",
                data:{checkedproduct:ids,_token: $('input[name=_token]').val()},
                success:function(msg){
                     if(msg.status === 'success'){
                          toastr.success(msg.msg);
                          toastr.options.closeButton = true;
                          //t.draw(true);
                          setInterval(function(){
                            window.location.reload();
                          },1000);
                        }
                  //t.draw();
                },error:function(data){
                     if(data.status === 422){
                      toastr.error("Cannot delete the product(s)!", '');
                      toastr.options.closeButton = true;
                     }
                }
              });
              }else{
                return false;
              }

           }else{
                    toastr.error('No item(s) selected');
                    toastr.options.closeButton = true;
           }
         
       });







       // Get item to edit
       $("#csmsproductsdataTable tbody").on("click","button[class=editproductbtn]", function(e){
          e.preventDefault();

          var prodid =  $(this).closest("tr").find(".bulkDelete").val();

          if(prodid != '')
          {
              // call ajax to append products details on modal
              $.ajax({
                  type: "post",
                  url: "products/getproductEdit",
                  data:{"productid":prodid,_token: $('input[name=_token]').val()},
                  success:function(data)
                  {
                    //console.log(data);
                    $.each(data, function(i,productslists){
                       $(this).each(function(productslists, productsdetails){
                          // append product details on modal
                            $('#editproductmodal').find("input[name=editproductname]").val(productsdetails.productName);
                            $('#editproductmodal').find("input[name=editbarcode]").val(productsdetails.productBarcode);
                            $('#editproductmodal').find("input[name=edittotalPurchasingCost]").val(productsdetails.totalPurchaseprice);
                            $('#editproductmodal').find("input[name=editsupplierPricelist]").val(productsdetails.productBuyingPrice);
                            $('#editproductmodal').find("input[name=editsellingprice]").val(productsdetails.productSellingPrice);
                            $('#editproductmodal').find("input[name=editvat]").val(productsdetails.vat);
                            $('#editproductmodal').find("input[name=editonhandquantity]").val(productsdetails.onhandQuantity);
                            $('#editproductmodal').find("input[name=editallocatedquantity]").val(productsdetails.allocatedQuantity);
                            $('#editproductmodal').find("input[name=editavailablequantity]").val(productsdetails.availableQuantity);
                            $('#editproductmodal').find("input[name=editexpiredata]").val(productsdetails.expirationDate);
                            $('#editproductmodal').find("input[name=editsize]").val(productsdetails.productSize);
                            $('#editproductmodal').find("input[name=editcolor]").val(productsdetails.productcolor);
                            $('#editproductmodal').find("input[name=edittag]").val(productsdetails.productTag);
                            $('#editproductmodal').find("textarea[name=editdescriptions]").val(productsdetails.productDescriptions);
                            $('#editproductmodal').find("input[name=editasdhgasha]").val(productsdetails.id);
                       });
                    });

                    // show the modal with appended data
                      var modalOptions = {
                          'show':true,
                          'keyboard':true,
                          'transition':true,
                          'backdrop' : "static"
                        }
                     $('#editproductmodal').modal(modalOptions);


                  },error:function(data)
                  {
                     console.log(data);
                  }
              });

          }else{
               toastr.error('No product to edit');
               toastr.options.closeButton = true;
          }
       });


       // update products after editing
       // /product/productsupdate/
       $("#saveeditedproducts").click(function(e){
        e.preventDefault();
           
              var productname = $('#editproductmodal').find("input[name=editproductname]").val();
              var barcode = $('#editproductmodal').find("input[name=editbarcode]").val();
              var purchasprice = $('#editproductmodal').find("input[name=edittotalPurchasingCost]").val();
              var suppprice = $('#editproductmodal').find("input[name=editsupplierPricelist]").val();
              var saleprice  = $('#editproductmodal').find("input[name=editsellingprice]").val();
              var itevat = $('#editproductmodal').find("input[name=editvat]").val();
              var handqty = $('#editproductmodal').find("input[name=editonhandquantity]").val();
              var alloqty = $('#editproductmodal').find("input[name=editallocatedquantity]").val();
              var avalqty = $('#editproductmodal').find("input[name=editavailablequantity]").val();
              var expdate = $('#editproductmodal').find("input[name=editexpiredata]").val();
              var psize = $('#editproductmodal').find("input[name=editsize]").val();
              var pcolor = $('#editproductmodal').find("input[name=editcolor]").val();
              var ptag = $('#editproductmodal').find("input[name=edittag]").val();
              var pdescrpt = $("textarea[name=editdescriptions]").val();
              var pid = $('#editproductmodal').find("input[name=editasdhgasha]").val();
              var pstore  = $("#editproductmodal").find("#editstore option:selected").val();
              var pcat  = $("#editproductmodal").find("#editcategory option:selected").val();
              var psupplier  = $("#editproductmodal").find("#editsupplier option:selected").val();

              if(pid != '' && productname !='' && barcode != '' && suppprice !='' && saleprice !='' &&  itevat != '' && handqty != '' &&  avalqty !=''){
                  // call ajax
                  $.ajax({
                       type:"post",
                       url: "product/productsupdate",
                       data:{
                        "name":productname,
                        "onhandquantity":handqty,
                        "allocatedquantity":alloqty,
                        "availablequantity":avalqty,
                        "size":psize,
                        "color":pcolor,
                        "tag":ptag,
                        "asdhgasha":pid,
                        "barcode":barcode,
                        "category":pcat,
                        "supplier":psupplier,
                        "supplierPricelist":suppprice,
                        "vat":itevat,
                        "sellingprice":saleprice,
                        "store":pstore,
                        "category":pcat,
                        "expiredata":expdate,
                        "descriptions":pdescrpt,
                        _token: $("input[name=_editproductstoken]").val()
                       },success:function(msg){
                            
                            //$("#editaccountingModal").modal('hide');
                     if(msg.status === 'success'){
                          toastr.success(msg.msg);
                          toastr.options.closeButton = true;
                         
                          setInterval(function(){
                            window.location.reload();
                         },200);
                        }
                        else
                        {
                           toastr.warning("Noting to update", '');
                           toastr.options.closeButton = true;
                        }

                       },error:function(data){
                          if(data.status === 422){
                             toastr.warning("Noting to update", '');
                             toastr.options.closeButton = true;
                           }
                       }
                  });


              }else{

              }


       });





         // view inventory
                // Get item to edit
       $("#csmsproductsdataTable tbody").on("click","button[class=viewinventroybtn]", function(e){
          e.preventDefault();

          var inventoryId =  $(this).closest("tr").find(".bulkDelete").val();

          if(inventoryId != '')
          {
              // call ajax to append products details on modal
              $.ajax({
                  type: "post",
                  url: "products/getinventorydetails",
                  data:{"inveid":inventoryId,_token: $('input[name=_token]').val()},
                  success:function(data)
                  {
                    //console.log(data);
                    $.each(data, function(i,productslists){
                       $(this).each(function(productslists, productsdetails){

                        var expdate = moment(productsdetails.expirationDate).format('MMMM D, YYYY')
                          // append product details on modal
                            $('#viewinventorymodal .viewinvtable tbody').find(".inveproname").text(productsdetails.productName);
                            $('#viewinventorymodal .viewinvtable tbody').find(".invebarcode").text(productsdetails.productBarcode);
                            $('#viewinventorymodal .viewinvtable tbody').find(".invesell").text(productsdetails.productSellingPrice);
                            $('#viewinventorymodal .viewinvtable tbody').find(".invebuy").text(productsdetails.productBuyingPrice);
                            $('#viewinventorymodal .viewinvtable tbody').find(".invepurchprice").text(productsdetails.totalPurchaseprice);
                            $('#viewinventorymodal .viewinvtable tbody').find(".inveavailable").text(productsdetails.availableQuantity);
                            $('#viewinventorymodal .viewinvtable tbody').find(".invehandqty").text(productsdetails.onhandQuantity);
                            $('#viewinventorymodal .viewinvtable tbody').find(".invelastupdate").text(productsdetails.updated_at);
                            $('#viewinventorymodal').find(".invecreatedat").text(productsdetails.created_at);
                            $('#viewinventorymodal').find(".invsuppliername").text(productsdetails.supplier.supplierName);
                            $('#viewinventorymodal').find(".investorename").text(expdate);
                            // $('#viewinventorymodal').find("input[name=editcolor]").val(productsdetails.productcolor);
                            // $('#viewinventorymodal').find("input[name=edittag]").val(productsdetails.productTag);
                            // $('#viewinventorymodal').find("textarea[name=editdescriptions]").val(productsdetails.productDescriptions);
                            // $('#viewinventorymodal').find("input[name=editasdhgasha]").val(productsdetails.id);
                       });
                    });

                    // show the modal with appended data
                      var modalOptions = {
                          'show':true,
                          //'keyboard':true,
                          'transition':true,
                         // 'backdrop' : "static"
                        }
                     $('#viewinventorymodal').modal(modalOptions);


                  },error:function(data)
                  {
                     console.log(data);
                  }
              });

          }else{
               toastr.error('Item does not exist');
               toastr.options.closeButton = true;
          }
       });



});