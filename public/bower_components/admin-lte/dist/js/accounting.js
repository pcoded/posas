$(document).ready(function(){



	 // DATA TABLE SERVER SIDE
  var t = $("#chartsofaccoutingtabledata").DataTable({
          //scrollY: 500,
          paging: false,
          ordering:true,
          searching:true,
          processing: true,
          serverSide: false,
          select:true,
         //order: [[ 1, "asc" ]],
          ajax: "accouting/ajaxGetchartsaccounting",
          dataSrc:"Data",
          columns:[
                    {data:'chartaccId',
                     render:function(data,type,row){
                      return '<label><input type="checkbox" class="chartsids" name="chartsaccount[]" value='+ data +' class="flat-red"></label>'
                     }
                    },
                    { data: 'accIdenitifier', name: 'accIdenitifier' },
                    { data: 'accountName', name: 'accountName' },

                     { data: 'accName', name: 'accName' },


                    {data:'chartaccId',
                     render:function(data,type,row){
                      return '<button class="editaccount"><i class="fa fa-edit"></i> Edit</button>'
                     }
                    },

                    {data:'chartaccId',
                     render:function(data,type,row){
                      return '<button class="deletechartsofaccounttbtn"><i class="fa fa-trash-o" style="color:red"></i> Delete</button>'
                     }
                    },
                  ],

             // "columnDefs": [ {
             //  "targets": [0,6,7,8],
             //  "orderable": false,
             //  "searchable": false
                
             //  } ]
        
  });



  // new accounting 
  $(".submitnewaccount").click(function(e){
  	e.preventDefault();

    var accountname = $("#newaccountingModal").find("input[name=nameofaccount]").val();
   // var e = document.getElementById("accountcategory");
    var acccategory = $("#accountcategory option:selected").val();
    var accdescriptions = $("textarea[name=accountdescriptions]").val();
    var identifier = $("#newaccountingModal").find("input[name=accidentifier]").val();

    if(accountname != '' && acccategory != '')
    {
    	// post
    	$.ajax({
    		type: "post",
    		url : "accounting/newaccountingroute",
    		data:{"accountCategory":acccategory,"accidentifier":identifier,"accountName":accountname,"accdescriptions":accdescriptions,'_token':$('input[name=_newaccounttoken]').val()},
    		success:function(data)
    		{
   
    		      $("#newaccountingModal").modal('hide');
    		      toastr.success("Account created successfully", 'Success');
		          toastr.options.closeButton = true;
                  window.setTimeout(function(){
                      window.location.reload();
                      },500)

    		},error:function(data){
    			 var errors = data.responseJSON;
                 console.log(errors);
              
                  toastr.error("Error occured", 'Erorr');
		          toastr.options.closeButton = true;
    		}
    	});

    }else{
    	toastr.error('Account Name and Category are required!', '');
        toastr.options.closeButton = true;
    }

  });





  // delete charts of accounting
      $("#chartsofaccoutingtabledata tbody").on("click","button[class=deletechartsofaccounttbtn]",function(e){
      	e.preventDefault();
      	var deletechartid =  $(this).closest("tr").find(".chartsids").val();

       
       if(deletechartid != '')
       {

          if(confirm("Are you sure you want to destroy this account?"))
          {
               
	               $.ajax({
	         	type:"post",
	         	url:"accounting/deleteSingleaccount",
	         	data:{"accountname":deletechartid,_token: $('input[name=_token]').val()},
	         	success:function(msg)
	         	{

	         		   if(msg.status === 'success'){
	                          toastr.success(msg.msg);
	                          toastr.options.closeButton = true;
	                          //t.draw(true);
	                          setInterval(function(){
	                            window.location.reload();
	                          },500);
	                        }

	         	},error:function(data)
	         	{
	                 if(data.status === 422){
	                      toastr.error("Cannot delete the account(s)!", '');
	                      toastr.options.closeButton = true;
	                     }
	         	}
	         });

          }else{
          	return false;
          }
         
       }else{
            toastr.error("Opps!, Nothing to delete", '');
            toastr.options.closeButton = true;
       }
      });



   
   // MUlitple row delete accounts
   // $(".chartsids").on('click', function(){

   //      var status = this.checked;
   //      $(".chartsids").each(function(){
   //        $(this).prop("checked",status);
          
   //      });

   //    });


   // BULCK DELETE
       $("#deletechartsaccount").on("click",function(event){
           if($('.chartsids:checked').length > 0){
              if(confirm("Are you sure you want to destroy this account(s)"))
              {

              	              var accountsidz = [];
				              $('.chartsids').each(function(i,prod){
				                   if($(this).is(':checked')){
				                    accountsidz.push($(this).val());

				                   }

				              });



				              // call ajax method to delete
				             // var accountlists = accountsidz.toString();
				              $.ajax({
				                type:"post",
				                url:"accounting/deleteMultaccount",
				                data:{accountname:accountsidz,_token: $('input[name=_token]').val()},
				                success:function(msg){
				                     if(msg.status === 'success'){
				                          toastr.success(msg.msg);
				                          toastr.options.closeButton = true;
				                          //t.draw(true);
				                          setInterval(function(){
				                            window.location.reload();
				                         },500);
				                        }
				                  //t.draw();
				                },error:function(data){
				                     if(data.status === 422){
				                      toastr.error("Cannot delete the account(s)!", '');
				                      toastr.options.closeButton = true;
				                     }
				                }
				              });


              }else{
              	return false;
              }
           }else{
                    toastr.error('No account(s) selected');
                    toastr.options.closeButton = true;
           }
         
       });



   //edit account
   $("#chartsofaccoutingtabledata tbody").on("click","button[class=editaccount]",function(e){
      e.preventDefault();

      	var accountid =  $(this).closest("tr").find(".chartsids").val();

      	   if(accountid != '')
      	   {

      	   	// call ajax method
      	   	$.ajax({
      	   		type:"post",
        		url:"accounting/getAccountingToEdit",
        		data:{"accountid":accountid,_token: $('input[name=_token]').val()},
        		success:function(data)
        		{
                  $.each(data, function(i,accounts){
                     $(this).each(function(accounts, account)
                     	{
                     		// append to modal
                     		$("#editaccountingModal").find("input[name=editnameofaccount]").val(account.accountName);
                     		$("textarea[name=editaccountdescriptions]").val(account.descriptions);
                     		$("#editaccountingModal").find("input[name=editaccountId]").val(account.chartaccId);
                     		$("#editaccountingModal").find("input[name=editaccidentifier]").val(account.accIdenitifier);
                     	});
                  });

                  // show the modal with appended data
                  var modalOptions = {
                    	'show':true,
                    	'keyboard':true,
                    	'transition':true,
                    	'backdrop' : "static"
                    }
        			$('#editaccountingModal').modal(modalOptions);


        		},
        		error:function(data)
        		{
        			console.log(data);
        		}
      	   	});

      	   }else{
      	   	   toastr.error('No account to edit');
               toastr.options.closeButton = true;
      	   }
   });


   // update after edit
   $("#editaccount").click(function(e){
   	e.preventDefault();
   	  var newaccountname = $("#editaccountingModal").find("input[name=editnameofaccount]").val();
   	  var newaccountcat  = $("#editaccountingModal").find("#editaccountcategory option:selected").val();
   	  var editaccdescrpt = $("textarea[name=editaccountdescriptions]").val();
   	  var accId          = $("#editaccountingModal").find("input[name=editaccountId]").val();
   	  var editIdentifier = $("#editaccountingModal").find("input[name=editaccidentifier]").val();

   	  if(newaccountname !='' &&  newaccountcat != '')
   	  {
          // call ajax
          $.ajax({
          	   type:"post",
          	   url :"accounting/updateaccounting",
          	   data:{"newaccountname":newaccountname,"identifier":editIdentifier,"accid":accId,"accountcat":newaccountcat,"descripts":editaccdescrpt, _token: $("input[name=_editaccounttoken]").val()},
          	   success:function(msg)
          	   {     
          	   	     //$("#editaccountingModal").modal('hide');
          	   	     if(msg.status === 'success'){
                          toastr.success(msg.msg);
                          toastr.options.closeButton = true;
                         
                          setInterval(function(){
                            window.location.reload();
                         },500);
                        }
                        else if(msg.status === 'failed')
                        {
                           toastr.warning(msg.msg);
                           toastr.options.closeButton = true;
                        }
          	   },
          	   error:function(data)
          	   {
                  if(data.status === 422){
                       toastr.warning("Noting to update", '');
                       toastr.options.closeButton = true;
                     }
          	   }
          });
   	  }else{
            toastr.error('Account Name and Category are required!', '');
            toastr.options.closeButton = true;
   	   }

   });







    // journal Date picker
	  $('#journaldate').datepicker({
	  	format: 'yyyy-mm-dd'
	  });


 $('#expensedatepaid').datepicker({
      format: 'yyyy-mm-dd'
    });

    	 function formatMoney(n, currency)
		  {
		      return currency + " " + n.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
		  }
      
       var mm = 0;
       $("input[name=debitaccountdebitamount]").on("input", function(){
           var sds = parseFloat($(this).val());
           

           if(!isNaN(sds) && sds.length !=0){
	           $("#totaldebit").text(formatMoney(sds,"R"));
	           $(".jcredit").val(sds);
	           $("#totalcredit").text(formatMoney(sds,"R"));
            }else{
            	$("#totaldebit").text("R 0.00");
            	$(".jcredit").val("0");
	            $("#totalcredit").text("R 0.00");
            }
       });
    
      

		

    // save journal transations
    $("#savejournal").click(function(){

           function randomReferenceNumber(min, max)
            {
                return Math.floor(Math.random() * (max - min + 1) + min);
            }

            //var mynu = Math.floor(Math.random()*(max - min + 1) + min);
            var mynu ="";
            var len = 2;
            for(var i=0;i<len;i++)
            {
                mynu += randomReferenceNumber(1,100);
            }
            
              
            var referencenumber = mynu;



        var jdescription = $("textarea[name=journadescrpt]").val();
        var jdate = $("input[name=journaldate]").val();
       // var jdebitaccount = $("#journalbooktable").find("#debitaccount option:selected").val();
       // var jcreditaccount = $("#journalbooktable").find("#creditaccount option:selected").val();
       // var debitdescript = $("#journalbooktable").find("input[name=debitdescriptions]").val();
       // var creditdescript = $("#journalbooktable").find("input[name=creditdescriptions]").val();
       // var debitamount = $("#journalbooktable").find("input[name=jdebit]").val();
       // var creditamount = $("#journalbooktable").find("input[name=jcredit]").val();  

             var jdebitaccount = $("#journalbooktable").find("#debitaccount option:selected").val(); //account id
             var jdebitaccountdebitamount = $("#journalbooktable").find("input[name=debitaccountdebitamount]").val(); // debit amount
             var jdebitaccountcreditmount = $("#journalbooktable").find("input[name=debitaccountcreditamount]").val(); // credit amount
             var jdebitdescripts = $("#journalbooktable").find("input[name=debitdescriptions]").val();

             //credit account details
             var jcreditaccount = $("#journalbooktable").find("#creditaccount option:selected").val();
             var jcreditaccountdebitamount = $("#journalbooktable").find("input[name=creditaccountdebitamount]").val();
             var jcreditaccountcreditamount = $("#journalbooktable").find("input[name=creditaccountcreditamount]").val();
             var jcreditdescripts = $("#journalbooktable").find("input[name=creditdescriptions]").val();

       
        if(jdate != '' && jdebitaccount != '' && jcreditaccount !='' && jdebitaccountdebitamount !='' && jcreditaccountcreditamount !=''){
           
           if(jdebitaccount == jcreditaccount)
           {
             toastr.warning('You cant debit and credit on the same account', '');
             toastr.options.closeButton = true;
           }
           else if(jdebitaccountdebitamount != jcreditaccountcreditamount)
           {
              toastr.warning('Debit and Credit must be equal', '');
              toastr.options.closeButton = true;
           }else{
                    

                // post debit data
                $.ajax({
                     type:"post",
                     url:"accounting/newjournalroute",
                     data:{
                          "reference":referencenumber,
                          "debitdescript":jdebitdescripts,
                          "datecreated":jdate,
                          "debitedaccount":jdebitaccount,
                          "debitedamount":jdebitaccountdebitamount,
                          "creditedamount":jdebitaccountcreditmount,
                          "creditdescript":jcreditdescripts,
                          "crediteaccount":jcreditaccount,
                          "creditedamount":jcreditaccountcreditamount,
                          _token: $("input[name=_creditdebittoken]").val()},
                     success:function(data){
                        toastr.success('Transactions recorded successfully', '');
                        toastr.options.closeButton = true;
                         setInterval(function(){
                            window.location.reload();
                         },500);
                     },error:function(data)
                     {
                      console.log(data);
                     }
                });     


           }
        }else{
             toastr.error('All fields are required', '');
             toastr.options.closeButton = true;
        }


    });
		


    // expense claims
  var t = $("#claimsexpensetable").DataTable({
          //scrollY: 1,
          paging: false,
          ordering:true,
          searching:true,
          processing: true,
          serverSide: false,
          select:true,
         order: [[ 1, "asc" ]],
          ajax: "accouting/getclaims",
          dataSrc:"Data",
          columns:[
                    {data:'expenseid',
                     render:function(data,type,row){
                      return '<label><input type="checkbox" class="expenseIds" name="expensesids[]" value='+ data +' class="flat-red"></label>'
                     }
                    },
                    { data: 'expenseReceiptNumber', name: 'expenseReceiptNumber'},
                    { data: 'expensedatecreated', name: 'expensedatecreated'},
                    { data: 'expensevat', name: 'expensevat'},

                    { data: 'expenseStatus',
                      render:function(data,type,row)
                      {
                        
                     if(data == 0)
                     {
                       return '<span class="label label-warning">waiting Review</span>';
                     }
                     else if(data==1)
                     {
                        return '<span class="label label-success">Reviewed</span>';
                     }
                      }
                    },
                    {data:'expenseid',
                       render:function(data,type,row){
                        return '<button class="btnviewexpenses">Review</button>'
                       }
                      },

                        {data:'expenseid',
                     render:function(data,type,row){
                      return '<button class="editexpense"><i class="fa fa-edit"></i> Edit</button>'
                     }
                    },

                    {data:'expenseid',
                     render:function(data,type,row){
                      return '<button class="deleteexpense"><i class="fa fa-trash-o" style="color:red"></i> Delete</button>'
                     }
                    },
                  ],

             // "columnDefs": [ {
             //  "targets": [0,4,5,6],
             //  "orderable": false,
             //  "searchable": false
                
             //  } ]
        
  });

       function formatMoney(n, currency)
         {
          return currency + " " + n.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
         }


  // view expense details
  $("#claimsexpensetable tbody").on("click","button[class=btnviewexpenses]", function(e){
    e.preventDefault();
     var expensid =  $(this).closest("tr").find(".expenseIds").val();
     $("#expensebuttoncontrol").show();

      // call ajax
      $.ajax({
           type:"post",
           url:"accounting/expenseviewmore",
           data:{"expenseid":expensid,_token: $('input[name=_token]').val()},
           success:function(data){
               // console.log(data);
               //$("#viewexpensemodal .viewpreparedby").text("")
               $.each(data, function(i, expensedata){
                $.each(this, function(expensedata,expensedas){
                  //console.log(expensedas.expensevat);
                  
                  // if(expensedas.expenseStatus == 0){
                  //   var estatus = "waiting review";
                  // }elseif(expensedas.expenseStatus == 1){
                  //   var estatus = "approved";
                  // }

                  $("#viewexpensemodal .viewpreparedby").text(expensedas.name);
                  $("#viewexpensemodal .viewpaidto").text(expensedas.expenseReceiptNumber);
                  $("#viewexpensemodal .viewamountpaid").text(formatMoney(parseFloat(expensedas.expensevat),"R"));
                  $("#viewexpensemodal .viewreason").text(expensedas.expensedescriptions);

                  if(expensedas.expenseStatus == 0){
                    $("#viewexpensemodal .viewstatus").text("waiting for review");
                  }else if(expensedas.expenseStatus == 1){
                    $("#viewexpensemodal .viewstatus").text("Reviewed");
                    $("#expensebuttoncontrol").hide();
                  }
                  
                  $("#viewexpensemodal").find("input[name=acceptedexpenseid]").val(expensedas.expenseid);
                  $("#viewexpensemodal").find("input[name=expensedatepaid]").val(expensedas.expensedatecreated);
                  $("#viewexpensemodal select[name=paidtoccount]").append("<option selected='true' value="+expensedas.chartaccId+">"+expensedas.accountName+"</option>");
                });

               });
           },error:function(data){
            console.log(data);
           }
      });
          var modalOptions = {
              'show':true,
              'keyboard':true,
              'transition':true,
              'backdrop' : "static"
            }
         $('#viewexpensemodal').modal(modalOptions);
  });


// Accept expenses
$(".approveexpensebtn").click(function(e){
  e.preventDefault();
   var expenseduedate = $("#viewexpensemodal").find("input[name=expensedatepaid]").val();
   var expensecreditaccount = $("#viewexpensemodal").find("#paidfromaccount option:selected").val();
   var expensedebitaccount = $("#viewexpensemodal").find("#paidtoaccount option:selected").val();
   var expenseAmount = $("#viewexpensemodal .viewamountpaid").text();
   var originalamount  = parseFloat(expenseAmount.replace(/[^0-9-.]/g, ''));
   var expensedescriptions = $("#viewexpensemodal .viewreason").text();
   var acceptedexpenseid = $("#viewexpensemodal").find("input[name=acceptedexpenseid]").val();


       function randomexpensesReference(min, max)
            {
                return Math.floor(Math.random() * (max - min + 1) + min);
            }

            //var mynu = Math.floor(Math.random()*(max - min + 1) + min);
            var expensemynu ="";
            var len = 2;
            for(var i=0;i<len;i++)
            {
                expensemynu += randomexpensesReference(1,100);
            }
            
              
            var expensereferencenumber = expensemynu;


   //console.log(expensedescriptions);

   if(expenseduedate != '' && expensecreditaccount != '' && expensedebitaccount !='' && expenseAmount !='' && expensedescriptions !='')
   {

     // call ajax
     $.ajax({
           type: "post",
           url : "accounting/acceptExpenses",
           data:{
                  "expenseduedate":expenseduedate,
                  "expensecreditaccount":expensecreditaccount,
                  "expensedebitaccount":expensedebitaccount,
                  "originalamount":originalamount,
                  "expensedescriptions":expensedescriptions,
                  "expensereference":expensereferencenumber,
                  "acceptedexpenseid":acceptedexpenseid,
                  _token: $("input[name=_creditdebittoken]").val()
                },
                success:function(msg){
                      if(msg.status === 'success'){
                          toastr.success(msg.msg);
                          toastr.options.closeButton = true;
                         
                          //setInterval(function(){
                            window.location.reload();
                        // },400);
                        }
                        else if(msg.status === 'failed')
                        {
                           toastr.warning(msg.msg);
                           toastr.options.closeButton = true;
                        }
                },error:function(data){
                  console.log(data);
                }
     });

   }else{
            toastr.error('All fields are required!', '');
            toastr.options.closeButton = true;
   }
});




//delete expense claims
$("#claimsexpensetable tbody").on("click","button[class=deleteexpense]", function(e){
    e.preventDefault();
     var expensid =  $(this).closest("tr").find(".expenseIds").val();

     if(expensid !=''){
          if(confirm("Are you sure you want to destroy this expense!")){
              //call ajax to delete
              $.ajax({
                    type:"post",
                    url:"accounting/expenseclaimdelete",
                    data:{"expenseid":expensid,_token:$('input[name=_token]').val()},
                    success:function(msg){
                              if(msg.status === 'success'){
                                         toastr.success(msg.msg);
                                         toastr.options.closeButton = true;
                         
                                      setInterval(function(){
                                        window.location.reload();
                                     },500);
                                    }
                                      else if(msg.status === 'failed')
                                      {
                                         toastr.warning(msg.msg);
                                         toastr.options.closeButton = true;
                                      }

                    },error:function(data){
                             if(data.status === 422){
                                  toastr.error("Cannot delete the expense(s)!", '');
                                  toastr.options.closeButton = true;
                                 }
                    }
              });
          }else{
            return false;
          }
     }else{
             toastr.error('The expense claim is required!', '');
            toastr.options.closeButton = true;
     }

   });


});