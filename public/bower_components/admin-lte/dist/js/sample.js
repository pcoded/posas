<script type="text/javascript" language="javascript">
	
	var submitting = false;
	
	$(document).ready(function(){
	
		$( "#keyboard_toggle" ).click(function(e) {
			e.preventDefault();
			$( "#keyboardhelp" ).toggle();
		});
		
    $.fn.editable.defaults.mode = 'popup';     

	$('.fullscreen').on('click',function (e) {
		e.preventDefault();
		salesRecvFullScreen();
		$.get('https://demo.phppointofsale.com/index.php/home/set_fullscreen/1');
	});

	$('.dismissfullscreen').on('click',function (e) {
		e.preventDefault();
		salesRecvDismissFullscren();
		$.get('https://demo.phppointofsale.com/index.php/home/set_fullscreen/0');
	});
	    
    $('.xeditable').editable({
    	validate: function(value) {
            if ($.isNumeric(value) == '' && $(this).data('validate-number')) {
					return "Only numbers are allowed";
            }
        },
    	success: function(response, newValue) {
			 last_focused_id = $(this).attr('id');
 			 $("#register_container").html(response);
		}
    });

    $('.xeditable').on('shown', function(e, editable) {
    	editable.input.postrender = function() {
			//Set timeout needed when calling price_to_change.editable('show') (Not sure why)
			setTimeout(function() {
         editable.input.$input.select();
		}, 50);
    };
	});
	
	 $('.xeditable').on('hidden', function(e, editable) {
		 last_focused_id = $(this).attr('id');
		 $('#'+last_focused_id).focus();
		 $('#'+last_focused_id).select();
	 });
	
				
		// Look up receipt form handling
		
		$('#look-up-receipt').on('shown.bs.modal', function() {
	          $('#sale_id').focus();
	    });
			
		$('.look-up-receipt-form').on('submit',function(e){
			e.preventDefault();

			$('.look-up-receipt-form').ajaxSubmit({
				success:function(response)
				{
					if(response.success)
					{
						window.location.href = 'https://demo.phppointofsale.com/index.php/sales/receipt/'+$("#sale_id").val();
					}
					else
					{
						$('.look-up-receipt-error').html(response.message);
					}
				},
				dataType:'json'
			});
		});

		//Set Item tier after selection
		$('.item-tiers a').on('click',function(e){
			e.preventDefault();

			$('.selected-tier').html($(this).text());
			$.post('https://demo.phppointofsale.com/index.php/sales/set_tier_id', {tier_id: $(this).data('value')}, function(response)
			{
				$('.item-tiers').slideToggle("fast", function()
				{
					$("#register_container").html(response);					
				});
			});
		});

		//Slide Toggle item tier options
		$('.item-tier').on('click',function(e){
			e.preventDefault();
			$('.item-tiers').slideToggle("fast");
		});


		//Set Item tier after selection
		$('.select-sales-persons a').on('click',function(e){
			e.preventDefault();

			$('.selected-sales-person').html($(this).text());
			$.post('https://demo.phppointofsale.com/index.php/sales/set_sold_by_employee_id', {sold_by_employee_id: $(this).data('value')}, function()
			{
				$('.select-sales-persons').slideToggle("fast");
				$("#register_container").load('https://demo.phppointofsale.com/index.php/sales/reload');
			});
		});

		//Slide Toggle item tier options
		$('.select-sales-person').on('click',function(e){
			e.preventDefault();
			$('.select-sales-persons').slideToggle("fast");
		});
		
		checkPaymentTypeGiftcard();
		checkPaymentTypePoints();
		


		$('#toggle_email_receipt').on('click',function(e) {
			e.preventDefault();
	        var checkBoxes = $("#email_receipt");
	        checkBoxes.prop("checked", !checkBoxes.prop("checked")).trigger("change");
	        $(this).toggleClass('email-checked');

		})

		$('#email_receipt').change(function(e) 
		{	
			e.preventDefault();
			$.post('https://demo.phppointofsale.com/index.php/sales/set_email_receipt', {email_receipt: $('#email_receipt').is(':checked') ? '1' : '0'});
		});

		// Customer form script
		$('#item,#customer').click(function()
		{
			$(this).attr('value','');
		});


		// if #mode is changed
		$('.change-mode').click(function(e){
			e.preventDefault();
			if ($(this).data('mode') == "store_account_payment") { // Hiding the category grid
				$('#show_hide_grid_wrapper, #category_item_selection_wrapper').fadeOut();
			}else { // otherwise, show the categories grid
				$('#show_hide_grid_wrapper, #show_grid').fadeIn();
				$('#hide_grid').fadeOut();
			}
			$.post('https://demo.phppointofsale.com/index.php/sales/change_mode', {mode: $(this).data('mode')}, function(response)
			{
				$("#register_container").html(response);
			});
		});
		

									if (last_focused_id && last_focused_id != 'item')
				{
					$('#'+last_focused_id).focus();
					$('#'+last_focused_id).select();
				}
							$(document).focusin(function(event) 
			{
				last_focused_id = $(event.target).attr('id');
			});
			
			
			$('#select_customer_form').ajaxForm({target: "#register_container", beforeSubmit: salesBeforeSubmit});
			$('#add_item_form').ajaxForm({target: "#register_container", beforeSubmit: salesBeforeSubmit, success: itemScannedSuccess});
			


			// ITEM AUTOCOMPLETE
			$( "#item" ).autocomplete({
		 		source: 'https://demo.phppointofsale.com/index.php/sales/item_search',
				delay: 150,
		 		autoFocus: false,
		 		minLength: 0,
		 		select: function( event, ui ) 
		 		{
					$( "#item" ).val(ui.item.value);
		 			$('#add_item_form').ajaxSubmit({target: "#register_container", beforeSubmit: salesBeforeSubmit, success: itemScannedSuccess});

		 		},
			}).data("ui-autocomplete")._renderItem = function (ul, item) {
		         return $("<li class='item-suggestions'></li>")
		             .data("item.autocomplete", item)
			           .append('<a class="suggest-item"><div class="item-image">' +
									'<img src="' + item.image + '" alt="">' +
								'</div>' +
								'<div class="details">' +
									'<div class="name">' + 
										item.label +
									'</div>' +
									'<span class="attributes">' +
										'Category' + ' : <span class="value">' + (item.category ? item.category : "None") + '</span>' +
									'</span>' +
								'</div>')
		             .appendTo(ul);
		     };


			
		
             // CUSTOMER AUTOCOMPLETE
			$( "#customer" ).autocomplete({
		 		source: 'https://demo.phppointofsale.com/index.php/sales/customer_search',
				delay: 150,
		 		autoFocus: false,
		 		minLength: 0,
		 		select: function( event, ui ) 
		 		{
		 			$.post('https://demo.phppointofsale.com/index.php/sales/select_customer', {customer: ui.item.value }, function(response)
					{
						$("#register_container").html(response);
					});
		 		},
			}).data("ui-autocomplete")._renderItem = function (ul, item) {
		         return $("<li class='customer-badge suggestions'></li>")
		             .data("item.autocomplete", item)
			           .append('<a class="suggest-item"><div class="avatar">' +
									'<img src="' + item.avatar + '" alt="">' +
								'</div>' +
								'<div class="details">' +
									'<div class="name">' + 
										item.label +
									'</div>' + 
									'<span class="email">' +
										item.subtitle + 
									'</span>' +
								'</div></a>')
		             .appendTo(ul);
		     };
	     
	     

		$('#customer').blur(function()
		{
			$(this).attr('value',"Type customer name...");
		});
		
		$('#change_sale_date_enable').is(':checked') ? $("#change_sale_date_picker").show() : $("#change_sale_date_picker").hide(); 

		$('#change_sale_date_enable').click(function() {
			if( $(this).is(':checked')) {
				$("#change_sale_date_picker").show();
			} else {
				$("#change_sale_date_picker").hide();
			}
		});
		
		$('#comment').change(function() 
		{
		});
						
		$('#show_comment_on_receipt').change(function() 
		{
			$.post('https://demo.phppointofsale.com/index.php/sales/set_comment_on_receipt', {show_comment_on_receipt:$('#show_comment_on_receipt').is(':checked') ? '1' : '0'});
		});
		
				
		date_time_picker_field($("#change_sale_date"), JS_DATE_FORMAT + " "+JS_TIME_FORMAT);
		
      $("#change_sale_date").on("dp.change", function(e) {
			$.post('https://demo.phppointofsale.com/index.php/sales/set_change_sale_date', {change_sale_date: $('#change_sale_date').val()});			
      });
		
		//Input change
		$("#change_sale_date").change(function(){
			$.post('https://demo.phppointofsale.com/index.php/sales/set_change_sale_date', {change_sale_date: $('#change_sale_date').val()});			
		});

		$('#change_sale_date_enable').change(function() 
		{
			$.post('https://demo.phppointofsale.com/index.php/sales/set_change_sale_date_enable', {change_sale_date_enable: $('#change_sale_date_enable').is(':checked') ? '1' : '0'});
		});
		

		$('.delete-item, .delete-payment, #delete_customer, .delete-tax').click(function(event)
		{
			event.preventDefault();
			$("#register_container").load($(this).attr('href'));	
		});
		
		
		$('#redeem_discount,#unredeem_discount').click(function(event)
		{
			event.preventDefault();
			$("#register_container").load($(this).attr('href'));	
		});

		//Layaway Sale
		$("#layaway_sale_button").click(function(e)
		{
			e.preventDefault();
			bootbox.confirm("Are you sure you want to suspend this sale?", function(result)
			{
				if(result)
				{
					$.post('https://demo.phppointofsale.com/index.php/sales/set_comment', {comment: $('#comment').val()}, function() {
														$("#register_container").load('https://demo.phppointofsale.com/index.php/sales/suspend');
											});
				}
			});
		});

		//Estimate Sale
		$("#estimate_sale_button").click(function(e)
		{
			e.preventDefault();
			bootbox.confirm("Are you sure you want to suspend this sale?", function(result)
			{
				if(result)
				{
					$.post('https://demo.phppointofsale.com/index.php/sales/set_comment', {comment: $('#comment').val()}, function() {
													$("#register_container").load('https://demo.phppointofsale.com/index.php/sales/suspend/2');
											});
				}
			});
		});

		//Cancel Sale
		$("#cancel_sale_button").click(function(e)
		{
			e.preventDefault();
			bootbox.confirm("Are you sure you want to clear this sale? All items will cleared.", function(result)
			{
				if (result)
				{
					$('#cancel_sale_form').ajaxSubmit({target: "#register_container", beforeSubmit: salesBeforeSubmit});
				}
			});
		});
		//Select Payment
		$('.select-payment').on('click',selectPayment);
		
								
		function selectPayment(e)
		{
			e.preventDefault();
			$.post('https://demo.phppointofsale.com/index.php/sales/set_selected_payment', {payment: $(this).data('payment')});
			$('#payment_types').val($(this).data('payment'));
						// start_cc_processing
			$('.select-payment').removeClass('active');
			$(this).addClass('active');
			$("#amount_tendered").focus();
			$("#amount_tendered").select();
			$("#amount_tendered").attr('placeholder','');
			
			checkPaymentTypeGiftcard();
			checkPaymentTypePoints();
		}

		//Add payment to the sale 
		$("#add_payment_button").click(function(e)
		{
			e.preventDefault();

			$('#add_payment_form').ajaxSubmit({target: "#register_container", beforeSubmit: salesBeforeSubmit});
		});

		//Add payment to the sale when hit enter on amount tendered input
		$('#amount_tendered').bind('keypress', function(e) {
			if(e.keyCode==13)
			{
				e.preventDefault();
				
				//Quick complete possible
				if ($("#finish_sale_alternate_button").is(":visible"))
				{
					$('#add_payment_form').ajaxSubmit({target: "#register_container", beforeSubmit: salesBeforeSubmit, complete: function()
					{
						$('#finish_sale_button').trigger('click');
					}});
				}
				else
				{
					$('#add_payment_form').ajaxSubmit({target: "#register_container", beforeSubmit: salesBeforeSubmit});
				}
			}
		});

		//Select all text in the input when input is clicked
		$("input[type=text]").not(".description").click(function() {
			$(this).select();
		});
		

		// Finish Sale button
		$("#finish_sale_button").click(function(e)
		{
			e.preventDefault();
			
			var confirm_messages = [];
			
			//Prevent double submission of form
			$("#finish_sale_button").hide();
			$('#grid-loader').show();
			
			
									
							confirm_messages.push("Are you sure you want to submit this sale? This cannot be undone.");
							
				if (confirm_messages.length)						
				{
					bootbox.confirm(confirm_messages.join("<br />"), function(result)
					{
						if (result)
						{
							finishSale();
						}
						else
						{
							//Bring back submit and unmask if fail to confirm
							$("#finish_sale_button").show();
							$('#grid-loader').hide();
						}
					});
				}
				else
				{
					finishSale();
				}
		});
				
				
			if ($("#payment_types").val() == "Gift Card")
			{
				$('#finish_sale_alternate_button').removeClass('hidden');
				$('#add_payment_button').addClass('hidden');
			}
			else if($("#payment_types").val() == "Points")
			{
				$('#finish_sale_alternate_button').addClass('hidden');
				$('#add_payment_button').removeClass('hidden');
			}
			else
			{
				if ($('#amount_tendered').val()>=0.00)
				{
					$('#finish_sale_alternate_button').removeClass('hidden');
					$('#add_payment_button').addClass('hidden');
				}
				else
				{
					$('#finish_sale_alternate_button').addClass('hidden');
					$('#add_payment_button').removeClass('hidden');
				}
			}
		
		
		$('#amount_tendered').on('input',function(){
			if ($("#payment_types").val() == "Gift Card")
			{
				$('#finish_sale_alternate_button').removeClass('hidden');
				$('#add_payment_button').addClass('hidden');
			}
			else if($("#payment_types").val() == "Points")
			{
				$('#finish_sale_alternate_button').addClass('hidden');
				$('#add_payment_button').removeClass('hidden');
			}
			else
			{
				if ($('#amount_tendered').val()>=0.00)
				{
					$('#finish_sale_alternate_button').removeClass('hidden');
					$('#add_payment_button').addClass('hidden');
				}
				else
				{
					$('#finish_sale_alternate_button').addClass('hidden');
					$('#add_payment_button').removeClass('hidden');
				}
			}
			
		});

		$('#finish_sale_alternate_button').on('click',function(e){
			e.preventDefault();
			$('#add_payment_form').ajaxSubmit({target: "#register_container", beforeSubmit: salesBeforeSubmit, complete: function()
			{
				$('#finish_sale_button').trigger('click');
			}});
		});
		
				
		// Show or hide item grid
		$("#show_grid, .show-grid").on('click',function(e)
		{
			e.preventDefault();
			$("#category_item_selection_wrapper").slideDown();

			$('.show-grid').addClass('hidden');
			$('.hide-grid').removeClass('hidden');
		});

		$("#hide_grid,#hide_grid_top, .hide-grid").on('click',function(e)
		{
			e.preventDefault();
			$("#category_item_selection_wrapper").slideUp();

			$('.hide-grid').addClass('hidden');
			$('.show-grid').removeClass('hidden');
		});
	
		// Save credit card info
		$('#save_credit_card_info').change(function() 
		{
			$.post('https://demo.phppointofsale.com/index.php/sales/set_save_credit_card_info', {save_credit_card_info:$('#save_credit_card_info').is(':checked') ? '1' : '0'});
		});

		// Use saved cc info
		$('#use_saved_cc_info').change(function() 
		{
			$.post('https://demo.phppointofsale.com/index.php/sales/set_use_saved_cc_info', {use_saved_cc_info:$('#use_saved_cc_info').is(':checked') ? '1' : '0'});
		});

		// Prompt for cc info (EMV integration only)
		$('#prompt_for_card').change(function() 
		{
			$.post('https://demo.phppointofsale.com/index.php/sales/set_prompt_for_card', {prompt_for_card:$('#prompt_for_card').is(':checked') ? '1' : '0'});
		});

			      	$('.cart-number').html(0);
				  
		
	});
	// end of document ready


// Re-usable Functions 

	function checkPaymentTypeGiftcard()
	{
		if ($("#payment_types").val() == "Gift Card")
		{
			$("#amount_tendered").val('');
			$("#amount_tendered").attr('placeholder',"Swipe\/Type gift card #");
		
							$("#amount_tendered").focus();
				 
						}
	}

	function checkPaymentTypePoints()
	{
		if ($("#payment_types").val() == "Points")
		{
			$("#amount_tendered").val('');
			$("#amount_tendered").attr('placeholder',"Enter amount of points to use");
		
							$("#amount_tendered").focus();
			 
		}
	}

	function salesBeforeSubmit(formData, jqForm, options)
	{
		if (submitting)
		{ 	
			return false; 
		}
		submitting = true;
					$('.cart-number').html(0);
				 $("#ajax-loader").show();
		 $("#add_payment_button").hide();
		$("#finish_sale_button").hide();
	}

	function itemScannedSuccess(responseText, statusText, xhr, $form)
	{
		setTimeout(function(){$('#item').focus();}, 10);
	}
	
	function finishSale()
	{
		if ($("#comment").val())
		{
			$.post('https://demo.phppointofsale.com/index.php/sales/set_comment', {comment: $('#comment').val()}, function()
			{
				$('#finish_sale_form').submit();						
			});						
		}
		else
		{
			$('#finish_sale_form').submit();						
		}
		
	}
</script>