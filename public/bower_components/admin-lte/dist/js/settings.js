$(document).ready(function(){
 
 // DATA TABLE SERVER SIDE
  var t = $("#listofsettings").DataTable({
          //scrollY: 1,
          paging: false,
          ordering:true,
          searching:true,
          processing: true,
          serverSide: false,
          select:true,
         order: [[ 1, "asc" ]],
          ajax: "settings/ajaxGetSettings",
          dataSrc:"Data",
          columns:[
                    {data:'id',
                     render:function(data,type,row){
                      return '<label><input type="checkbox" class="companyids" name="companyids[]" value='+ data +' class="flat-red"></label>'
                     }
                    },
                    { data: 'companyName', name: 'companyName'},
                    { data: 'companyAddress1', name: 'companyAddress1'},
                    { data: 'companyContact1', name: 'companyContact1'},
                    { data: 'companyEmail', name: 'companyEmail'},

                    {data:'id',
                     render:function(data,type,row){
                      return '<button class="editcompany"><i class="fa fa-edit"></i> Edit</button>'
                     }
                    },

                    {data:'id',
                     render:function(data,type,row){
                      return '<button class="deletecompany"><i class="fa fa-trash-o" style="color:red"></i> Delete</button>'
                     }
                    },
                  ],

        
  });
  

     // delete settings
      $("#listofsettings tbody").on("click","button[class=deletecompany]",function(){
        var deleteid =  $(this).closest("tr").find(".companyids").val();
       
       if(deleteid != '')
       {
          if(confirm("Are you sure you want to destroy company details?"))
          {

                   $.ajax({
                            type:"post",
                            url:"settings/deleteSinglecompany",
                            data:{"deletecompan":deleteid,_token: $('input[name=_token]').val()},
                            success:function(msg)
                            {

                                 if(msg.status === 'success'){
                                            toastr.success(msg.msg);
                                            toastr.options.closeButton = true;
                                
                                            setInterval(function(){
                                              window.location.reload();
                                            },300);
                                          }

                            },error:function(data)
                            {
                                   if(data.status === 422){
                                        toastr.error("Cannot delete settings!", '');
                                        toastr.options.closeButton = true;
                                       }
                            }
                           });

          }else{
            return false;
          }
     
       }else{
            toastr.error("Opps!, Nothing to delete", '');
            toastr.options.closeButton = true;
       }
      });




// edit settings

      $("#listofsettings tbody").on("click","button[class=editcompany]",function(){
        var editid =  $(this).closest("tr").find(".companyids").val();
       
       if(editid != '')
       {
        

                   $.ajax({
                            type:"post",
                            url:"settings/editsettings",
                            data:{"editid":editid,_token: $('input[name=_token]').val()},
                            success:function(data)
                            {

                                       $.each(data, function(i,settings){
				                       $(this).each(function(settings, setting){
				                          // append product details on modal
				                            $('#editcompanydetails').find("input[name=companyname]").val(setting.companyName);
				                            $('#editcompanydetails').find("input[name=companyaddress1]").val(setting.companyAddress1);
				                            $('#editcompanydetails').find("input[name=companyaddress2]").val(setting.companyAddress2);
				                            $('#editcompanydetails').find("input[name=companyemail]").val(setting.companyEmail);
				                            $('#editcompanydetails').find("input[name=companycontact1]").val(setting.companyContact1);
				                            $('#editcompanydetails').find("input[name=contact2]").val(setting.companyContact2);
				                            $('#editcompanydetails').find("input[name=website]").val(setting.companyWebsite);
				                            $('#editcompanydetails').find("input[name=logo]").val(setting.companyLogo);
				                            $('#editcompanydetails').find("input[name=companyid]").val(setting.id);
				                           
				                       });
				                    });

                                   // show the modal with appended data
				                      var modalOptions = {
				                          'show':true,
				                          'keyboard':true,
				                          'transition':true,
				                          'backdrop' : "static"
				                        }
				                     $('#editcompanydetails').modal(modalOptions);

                            },error:function(data)
                            {
                               console.log(data);
                            }
                           });

     
     
       }else{
            toastr.error("Opps!, Nothing to edit", '');
            toastr.options.closeButton = true;
       }
      });



      // update the above settings
      $("#updatecompanysettingsbutton").click(function(e){
      	e.preventDefault();
      	var companyid  =  $('#editcompanydetails').find("input[name=companyid]").val();
      	var name =  $('#editcompanydetails').find("input[name=companyname]").val();
      	var address1 = $('#editcompanydetails').find("input[name=companyaddress1]").val();
      	var address2 = $('#editcompanydetails').find("input[name=companyaddress2]").val();
      	var email = $('#editcompanydetails').find("input[name=companyemail]").val();
      	var contact1 = $('#editcompanydetails').find("input[name=companycontact1]").val();
      	var contact2 =  $('#editcompanydetails').find("input[name=contact2]").val();
      	var website =  $('#editcompanydetails').find("input[name=website]").val();
      	var logo = $('#editcompanydetails').find("input[name=logo]").val();

      	if(companyid != '' && name != '' && address1 != '' && contact1 != ''){
            // call ajax to update the settings
            $.ajax({
            	type:"post",
            	url :"settings/updatesettings",
            	data:{"id":companyid,"name":name,"address1":address1,"address2":address2,"email":email,"contact1":contact1,"contact2":contact2,"website":website,_token: $('input[name=_token]').val()},
            	success:function(msg){
                       if(msg.status === 'success'){
                        toastr.success(msg.msg);
                        toastr.options.closeButton = true;
            
                        setInterval(function(){
                          window.location.reload();
                        },300);
                      }
            	},error:function(data){
                     if(data.status === 422){
	                    toastr.error("Cannot update settings!", '');
	                    toastr.options.closeButton = true;
	                   }
            	}
            });
      	}else{
            toastr.error("Fields with (*) are required", '');
            toastr.options.closeButton = true;
      	}

      });

});