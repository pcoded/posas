$(document).ready(function(){


  // DATA TABLE SERVER SIDE
  var t = $("#listofexpensestabledata").DataTable({
          //scrollY: 1,
          paging: false,
          ordering:true,
          searching:true,
          processing: true,
          serverSide: false,
          select:true,
         order: [[ 1, "asc" ]],
          ajax: "accouting/ajaxGetexpenses",
          dataSrc:"Data",
          columns:[
                    {data:'expenseid',
                     render:function(data,type,row){
                      return '<label><input type="checkbox" class="expenseIds" name="expensesids[]" value='+ data +' class="flat-red"></label>'
                     }
                    },
                    { data: 'expensedatecreated', name: 'expensedatecreated'},
                    { data: 'expensedescriptions', name: 'expensedescriptions'},
                    { data: 'expensevat', name: 'expensevat'},
                    { data: 'expenseStatus',
                      render:function(data,type,row)
                      {
                        
                     if(data == 0)
                     {
                       return '<span class="label label-warning">waiting approval</span>';
                     }
                     else if(data==1)
                     {
                        return '<span class="label label-success">approved</span>';
                     }
                      }
                    },

                    // {data:'expenseid',
                    //  render:function(data,type,row){
                    //   return '<button class="editexpense"><i class="fa fa-edit"></i> Edit</button>'
                    //  }
                    // },

                    // {data:'expenseid',
                    //  render:function(data,type,row){
                    //   return '<button class="deleteexpense"><i class="fa fa-trash-o" style="color:red"></i> Delete</button>'
                    //  }
                    // },
                  ],

             "columnDefs": [ {
              "targets": [0],
              "orderable": false,
              "searchable": false
                
              } ]
        
  });
       
      function formatMoney(n, currency)
		  {
		      return currency + " " + n.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
		  }







    




     // new expense Date picker
	  $('#newexpensedatepicker').datepicker({
	  	format: 'yyyy-mm-dd'
	  });

          // generate reference random number for tracking transaction
	     function randomexpenseReferenceNumber(min, max)
            {
                return Math.floor(Math.random() * (max - min + 1) + min);
            }

            //var mynu = Math.floor(Math.random()*(max - min + 1) + min);
            var expmynu ="";
            var len = 2;
            for(var i=0;i<len;i++)
            {
                expmynu += randomexpenseReferenceNumber(1,100);
            }
            
              
            var referenceexpensenumber = expmynu;


	  // post new expenses
	  $("#newexpensebutton").click(function(e){
	  	e.preventDefault();

	  	var expensedate = $("input[name=newexpensedatepicker]").val();
	  	var expensepaymentmethod =  $("#paymentmethod option:selected").val();
	  	var expensecategory = $("#expensecategory option:selected").val();
	  	var expenseamount = $("input[name=expenseamount]").val();
	  	var expensedescripts = $("textarea[name=expensereason]").val();
	  	var expreceipt = $("input[name=expensereceipt]").val();
        var exvat      = $("#expensevat option:selected").text();

	  	if(expensedate !='' && expensepaymentmethod !='' && expensecategory != '' && expenseamount !='' && expensedescripts !=''){
           // call ajax
           $.ajax({
           	    type: "post",
           	    url: "expenses/postnewexpense",
           	    data:{
           	    	 "datecreated":expensedate,
           	    	 "methodname":expensepaymentmethod,
           	    	 "expenseaccount":expensecategory,
           	    	 "expenseamount":expenseamount,
           	    	 "expedescripts":expensedescripts,
           	    	 "transactionreference":referenceexpensenumber,
           	    	 "receiptnumber":expreceipt,
           	    	 "expensevat":exvat,
           	    	 _token: $('input[name=_newexpensetoken]').val()
           	       },
           	    success:function(msg){
                  if(msg.status === 'success'){
                                  toastr.success(msg.msg);
                                  toastr.options.closeButton = true;
                        
                                  setInterval(function(){
                                    window.location.reload();
                                 },500);
                                }
           	    },error:function(data){
           	    	if(data.status === 422){
                              toastr.error("Failed to record expense!", '');
                              toastr.options.closeButton = true;
                             }
           	    }
           });
	  	}else{
	  		toastr.error('All fields are required!', '');
            toastr.options.closeButton = true;
	  	}
	  });
	
});