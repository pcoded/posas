$(document).ready(function(){
  // customer autocomplete
  $(function(){
    $(".customersauto").select2();
  });

// Auto-complete on sale input using product name
var xhr
new autoComplete({
    selector: 'input[name="itemName"]',
    minChars:2,
    source: function(term, suggest){
       
        term = term.toLowerCase();
        try { xhr.abort(); } catch(e){}
         xhr = $.getJSON('products/ajax-auto-complete', { itemName: term }, function(data){ 
                
             $.each(data, function(e, items){
              var matches = [];
              for (i=0; i<items.length; i++)
                 if (~items[i].toLowerCase().indexOf(term)) matches.push(items[i]);
              suggest(matches);

             });

         });
            
    }
});
 
   

  function formatMoney(n, currency)
  {
      return currency + " " + n.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
  }



     //CART TABLE 
    var trHTML1 = '';
     trHTML1 += '<td colspan="6"><div class="text-center text-warning"><h3>' + "There are no items in the cart" + '</h3></div></td>'
    $('#messagerow').append(trHTML1);

    $('#salesValue').change('input', function(e){
      e.preventDefault();
       
       // assign scanned item to mysale
       var mysale = $(this).val();
       var storename   = document.getElementById('storeNameonSale').selectedIndex;

        if(storename != '')
               {
                var storename   = storename;
               }else{
                 var storename     = 1; // choose the default store
               }
               //console.log(storename);
       
       // post the mysale from db
       $.ajax({
           type: "post",
           url:  "/sales/ajaxsale",
           data:{mysale,storename,_token: $("input[name=_token]").val()},
           success:function(data){
                   $.each(data, function(i, items){

                        if(items == "no match"){
                              toastr.error('Could not find item', '');
                              toastr.options.closeButton = true;
                        }else{
                                $('#messagerow').remove();
                                $.each(this, function(items, item){

                                         var trHTML='';
                                         var totalpriceqty = parseInt(item.productSellingPrice) * 1;
                                         var ogvat = item.vat * 100;

                                         // create  new row in the table
                                         trHTML += '<tr class="saleitemlist"> <td><button style="color: #ff6464;" class="remvrow"><i class="icon ion-android-cancel"></i></button></td>  <td class="itemName">'+ item.productName +'</td>  <td ><input type="number" value="1" class="itemquantity form-control"></td>  <td><input type="number" value="'+ item.productSellingPrice +'"  class="itemprice form-control" ></td>    <td><div class="input-group"><input type="number" value="'+ ogvat +'" class="form-control itemvat" disabled><span class="input-group-addon">%</span></div></td>  <td class="totalpriceqty">'+ totalpriceqty +'</td> <td style="display:none"><input type="number" name="itemid" class="saleitemid" value="'+ item.id +'" hidded></td><td style="display:none"><input type="number" name="costofgoodsold" class="costofgoodsold" value="'+ item.productBuyingPrice +'" hidded></td></tr>';
                                         $('#register').append(trHTML);
                                          
                                          /*
                                          *************************************************************
                                          *  LISTEN ON DELETE ROW
                                          *************************************************************
                                          */
                                           
                                         $(".remvrow").click(function(){
                                           $(this).closest("tr").remove();

                                            var rowCount = $("#register tr").length;
                                            if(rowCount < 2)
                                            {
                                              console.log("no rows");
                                            }


                                                    var currentprice = parseInt($(this).closest("tr").find(".itemprice").val());
                                                    var currentqty   = parseInt($(this).closest("tr").find(".itemquantity").val());

                                                    var newitempriceqty = currentqty*currentprice;
                                                     
                                                     if(!isNaN(newitempriceqty) && newitempriceqty.length !=0){
                                                      $(this).closest("tr").find(".totalpriceqty").text(newitempriceqty);
                                                    }else{
                                                      $(this).closest("tr").find(".totalpriceqty").text(currentprice);
                                                    }

                                                      

                                                    var total= 0;
                                                    var totalIncludeVat=0;
                                                   $("#register tr td:nth-child(6)").each(function(){
                                                   // var itemId = item.productName
                                                      total += parseFloat($(this).text());

                                                      // vat calculation
                                                      totalIncludeVat = total*item.vat + total;
                                                   });
                                                     // Append the totalPRICE
                                                  $("#totalAmount").text(formatMoney(total, "R"));
                                                  $("#subtotal").text(formatMoney(total, "R"));
                                                  $("#paidAmount").val(totalIncludeVat);
                                                  $("#amountDue").text(formatMoney(totalIncludeVat, "R"));



                                                      var totalAmount  = $("#amountDue").text();
                                                      var totalAmountNormal = parseFloat(totalAmount.replace(/[^0-9-.]/g, ''));

                                                     // cash paid by customer
                                                      var paidAmount = $("#paidAmount").val();

                                                     // change to return
                                                     var changeToreturn = paidAmount - totalAmountNormal; 
                                                      $("#change").text(formatMoney(changeToreturn,"R"));

                                                      


                                         }); /*END ON REMOVE ROW*/

                                                

                                                /*
                                                ********************************************************************
                                                * VALUE AS SOON AS ITEM SCANNED
                                                ********************************************************************
                                                */
                                                  var total= 0;
                                                  var totalIncludeVat=0;
                      
                                                   $("#register tr td:nth-child(6)").each(function(){
                                                   // var itemId = item.productName
                                                      total += parseFloat($(this).text());

                                                      // vat calculation
                                                      totalIncludeVat = total*item.vat + total;
                                                   });

                                               


                                                  // Append the totalPRICE
                                                  $("#totalAmount").text(formatMoney(total, "R"));
                                                  $("#subtotal").text(formatMoney(total, "R"));
                                                  $("#paidAmount").val(totalIncludeVat);
                                                  $("#amountDue").text(formatMoney(totalIncludeVat, "R"));

                                                  // Enable the complete sale button
                                                  $("#completesalebutton").attr('disabled',false);
                                                   

                                                   // change to return
                                                  var totalAmount  = $("#amountDue").text();
                                                  var totalAmountNormal = parseFloat(totalAmount.replace(/[^0-9-.]/g, ''));

                                                 // cash paid by customer
                                                  var paidAmount = $("#paidAmount").val();

                                                 // change to return
                                                 var changeToreturn = paidAmount - totalAmountNormal; 
                                                  $("#change").text(formatMoney(changeToreturn,"R"));
                                                /*
                                                ***********************************************************
                                                * END VALUES AS SOON AS ITEM SCANNED
                                                ***********************************************************
                                                */

                                                  
                                                  /*
                                                  *********************************************************************
                                                  * Listen if itemquantity is changed then update the totalpriceqty
                                                  ***********************************************************************
                                                  */ 
                                                  $(".saleitemlist").on("input", function(){
                                                    var currentprice = parseInt($(this).closest("tr").find(".itemprice").val());
                                                    var currentqty   = parseInt($(this).closest("tr").find(".itemquantity").val());
                                                    var currentvat   = parseInt($(this).closest("tr").find(".itemvat").val());

                                                    var newcurrentvat = currentvat/100;


                                                    var newitempriceqty = currentqty*currentprice;
                                                     
                                                     if(!isNaN(newitempriceqty) && newitempriceqty.length !=0){
                                                      $(this).closest("tr").find(".totalpriceqty").text(newitempriceqty);
                                                    }else{
                                                      $(this).closest("tr").find(".totalpriceqty").text(currentprice);
                                                    }

                                                   

                                                      

                                                    var total= 0;
                                                    var totalIncludeVat = 0;
                                                   $("#register tr td:nth-child(6)").each(function(){
                                                   // var itemId = item.productName
                                                      total += parseFloat($(this).text());
                                                      // vat calculation
                                                      totalIncludeVat = total*newcurrentvat + total;
                                                   });
                                                     // Append the totalPRICE
                                                  $("#totalAmount").text(formatMoney(total, "R"));
                                                  $("#subtotal").text(formatMoney(total, "R"));

                                                  if(!isNaN(totalIncludeVat)){
                                                    $("#paidAmount").val(totalIncludeVat);
                                                   $("#amountDue").text(formatMoney(totalIncludeVat, "R"));
                                                  }else{
                                                    $("#paidAmount").val(total);
                                                    $("#amountDue").text(formatMoney(total, "R"));
                                                  }
                                                  


                                                      var totalAmount  = $("#amountDue").text();
                                                      var totalAmountNormal = parseFloat(totalAmount.replace(/[^0-9-.]/g, ''));

                                                     // cash paid by customer
                                                      var paidAmount = $("#paidAmount").val();

                                                     // change to return
                                                     var changeToreturn = paidAmount - totalAmountNormal; 
                                                      $("#change").text(formatMoney(changeToreturn,"R"));

                                                  

                                                  });
                                                  /*
                                                  *************************************************8
                                                  * listen if paidAmount(cash wich customer pay) is changed then update the change
                                                  ***************************************************
                                                  */

                          
                                                  $("#paidAmount").on("input",function(){
                                                      var totalAmount  = $("#amountDue").text();
                                                      var totalAmountNormal = parseFloat(totalAmount.replace(/[^0-9-.]/g, ''));

                                                     // cash paid by customer
                                                      var paidAmount = $("#paidAmount").val();

                                                     // change to return
                                                     var changeToreturn = paidAmount - totalAmountNormal; 
                                                      $("#change").text(formatMoney(changeToreturn,"R"));
                                                  });
                                                  




                                });//end each this loop
                             }//end else success item
                        
                   });//end each item loop
           },
           error:function(data){
           console.log("could not find the item");
           }
       }); // End ajax request

      $(this).val(""); // clear the input

    }); // End scanned item

  $('input[type=checkbox]').change(function(){
    if(this.checked){
      console.log("checked");
    }else{
      console.log("unchecked");
    }
  });

   
  

 

 // posting complete sale
 $("#completesalebutton").click(function(e){
    e.preventDefault();


  function randomNumberRange(min, max)
    {
        return Math.floor(Math.random() * (max - min + 1) + min);
    }

    //var mynu = Math.floor(Math.random()*(max - min + 1) + min);
    var mynu ="";
    var len = 2;
    var so  = "SO";
    for(var i=0;i<len;i++)
    {
        mynu += randomNumberRange(1,100);
    }
    
      
    var sordernumber = so+mynu;



       
    var arr = new Array();
    var counter = 0;

     $(".saleitemlist").each(function(i,el){
      //console.log(arr.push(el));
      arr.push(el);
     });

     
     accountingrequest(counter);
     function accountingrequest(counter)
     {
          var accquantitysold = $(arr[counter]).find(".itemquantity").val();
          var accitemprice    =  $(arr[counter]).find(".itemprice").val();
          var costofsoldzs = $(arr[counter]).find(".costofgoodsold").val();

          var ogttalamount = accquantitysold * accitemprice;
          //var quantitysell = quantitysold;
          var costofsolditems = accquantitysold * costofsoldzs;


             $.ajax({
                  type:"post",
                  url:"accounting/salesjournalroute",
                  data:{"amount":ogttalamount,"costofsolditems":costofsolditems,"salereference":sordernumber,_token: $('input[name=_token]').val()},
                  success:function(data){
                     counter++;
                      $.each(data,function(i,response){
                           if(response == "successaccounting" && counter == arr.length){
                             console.log("transaction completed");
                           }else if(response == "successaccounting" && counter< arr.length){
                              accountingrequest(counter)
                           }
                      });
                  },error:function(data){
                    console.log(data);
                  }
            });
     }//end accounting transactions

     doRequest(counter);
     function doRequest(counter)
     {
               var saleitemid   = $(arr[counter]).find(".saleitemid").val();
               var customerid   = document.getElementById('customerdetls').selectedIndex;
               
               var quantitysold = $(arr[counter]).find(".itemquantity").val();
               var itemprice    =  $(arr[counter]).find(".itemprice").val();
               var itemvat      =  $(arr[counter]).find(".itemvat").val();
               var itemvatTopercent = itemvat/100;
               var totalCost = (quantitysold * itemprice) + (itemvatTopercent * (quantitysold * itemprice));
               
               if(customerid != '')
               {
                var customerid   = customerid;
               }else{
                 var customerid     = 1; // CURRENTLY WE DONT WANT TO KNOW OUR CUSTOMER
               }
              //console.log(customerid);
              
               
               if(saleitemid != '' && customerid != '' && quantitysold != '' && itemprice != '',customerid != '')
               {
                     $.ajax({
                            type:"post",
                            url: "sale/completeSale",
                            data:{
                           "product":saleitemid,
                           "customer":customerid,
                           "quantitysold":quantitysold,
                           "itemprice":itemprice,
                           "vat":itemvatTopercent,
                           "total":totalCost,
                           "saleordernumber":sordernumber,
                            _token: $("input[name=_itemsaletoken]").val()
                             },
                             dataType:"json",
                             success:function(data){
                              counter++;
                              //if(counter<arr.length)
                               // doRequest(counter);

                                $.each(data,function(i,response){
                         
                                //console.log(response);
                                if(response == "enteredqtyisgrt" &&  counter == arr.length){
                                   toastr.warning('Desired Quantity is Insufficient, you can still process the sale, but check your inventory', 'Warning');
                                   toastr.options.closeButton = true;

                                   toastr.success('Sale completed successfully!', '');
                                   toastr.options.closeButton = true;
                                  // toastr.options.showDuration = 4000;
                                   //toastr.options.hideDuration = 1000;
                                 
                                   
                                    // setInterval(function(){
                                       window.location.href = 'sales/completeSaleReceipt/'+sordernumber;
                                   // },4000);
                                }

                                else if(response == "enteredqtyisgrt" &&  counter< arr.length){
                                   toastr.warning('Desired Quantity is Insufficient, you can still process the sale, but check your inventory', 'Warning');
                                   toastr.options.closeButton = true;

                                    doRequest(counter);
                                }

                                else if(response == "itemnotfound")
                                {
                                   toastr.error('Item does not exists', '');
                                   toastr.options.closeButton = true;
                                }
                                 else if(response == "errorsavesale")
                                {
                                   toastr.error('Could not reduce the quantity', '');
                                   toastr.options.closeButton = true;
                                }
                                else if(response == "salecomplete" && counter == arr.length)
                                {

                                  toastr.success('Sale completed successfully!', '');
                                  toastr.options.closeButton = true;
                                 
                                    //setInterval(function(){
                                       window.location.href = 'sales/completeSaleReceipt/'+sordernumber;
                                    //},3000);
                              
                                }

                                 else if(response == "salecomplete" && counter< arr.length)
                                {
                                   doRequest(counter);
                                }
                               
                         });






                             },error:function(data){
                               console.log(data)
                              }
                     });




               }else{
                 toastr.error('All fields are required', '');
                  toastr.options.closeButton = true;
               }
     } // end function for posting sales details




 });





// new customer on sale
$("#onSalenewCustomer").click(function(e){
  e.preventDefault();

  // get entered customer details
  var customerName = $("#customerNameonsale").val();
  var customerCompanyName = $("#customerCompanyNameonsale").val();
  var customerEmail = $("#customerEmailonsale").val();
  var customerPhoneNumber  = $("#customerPhoneNumberonsale").val();
  var customerPhoto   = $("#customerPhotoonsale").val();
  var customerAddress = $("#customerAddressonsale").val();
  var customerCountry = $("#customerCountryonsale").val();


    //console.log(customerName);

           $.ajax({
                type: 'post',
                url:'customer/onsalescustomer',
                data:{
                      "customerName":customerName,
                      "customerCompanyName":customerCompanyName,
                      "customerEmail":customerEmail,
                      "customerPhoneNumber":customerPhoneNumber,
                      "customerPhoto":customerPhoto,
                      "customerAddress":customerAddress,
                      "customerCountry":customerCountry,
                      "_token":$("input[name=_onSaleNewCustomertoken]").val()
                    },
              success:function(data){
               //console.log(data);
               $(".newcustomer").modal( 'hide' ).data( 'bs.modal', null );
               $(".newcustomer").on("hidden",function(){
                   $(this).data('modal', null);

               });
                window.location.reload(true);
              },
              error:function(data){
                console.log(data);
              }
           });
  
});


// update receipt table
 var  sumOfTotalPricePerItemIncrement = 0;
$("#receiptTable tr").not("thead tr").each(function(){
  //get each price per item
   var sumOfTotalPricePerItem = parseInt($(this).closest("tr").find(".itemUnitePriceTotal").text());
   //sum each total
   sumOfTotalPricePerItemIncrement+=sumOfTotalPricePerItem;
   // append the returned result to subtotal in calculateTable
   $("#calculateTable tr").find(".receiptSubtotal").text(formatMoney(sumOfTotalPricePerItemIncrement,"R")); 



   // Vat calculations
   var itemvat = $("input[name=vatreceipt]").val();
  
   var newVat = itemvat * sumOfTotalPricePerItemIncrement;
    $("#calculateTable tr").find(".calculateTableVAT").text(formatMoney(newVat,"R"));
   
   // sume vat and subtotal
   var superItemstotalPrice = newVat + sumOfTotalPricePerItemIncrement;
   $("#calculateTable tr").find(".calculatedTableSuperTotal").text(formatMoney(superItemstotalPrice,"R"));
});




 // DATA TABLE SERVER SIDE
  /*var myt = $("#mylistofitemsold").DataTable({
         
          paging: false,
          ordering:true,
          searching:true,
          processing: true,
          serverSide: false,
          select:true,
          ajax: "sales/ajaxgetitemsreceipts",
          dataSrc:"Data",
          columns:[
                    {data:'id',name:'id'},
                    { data: 'soNumber',
                        render:function(data,type,row){
                      return '<a href="sales/completeSaleReceipt/'+ data +'"> '+ data +'<a/>'
                     }
                     },

                     { data: 'created_at', name: 'created_at' },

                  ],

           
        
  });
*/





});